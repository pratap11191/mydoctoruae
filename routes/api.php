<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('subscription_list/{id}','API\UserAPIController@subscription_list');

Route::post('plan_map','API\UserAPIController@plan_map');

// **************** chat api ************** //

Route::post('sendmessage', 'API\UserAPIController@sendmessage');

Route::post('getmessage', 'API\UserAPIController@getmsg');

Route::post('chatthread','API\UserAPIController@chatthreadpage');

Route::post('paymeoncheckout','API\UserAPIController@paymeoncheckout');


// ********************* end ****************************//



Route::post('users', 'API\UserAPIController@signup');

Route::post('login', 'API\UserAPIController@login');

Route::post('dumyimageuploade', 'API\UserAPIController@dumyimageuploade');

Route::post('checkchatkit', 'API\UserAPIController@checkchatkit');

Route::post('getroomid', 'API\UserAPIController@getroomid');

Route::post('sendMessage2', 'API\UserAPIController@sendMessage2');

Route::post('sendMessage3', 'API\UserAPIController@sendMessage3');

Route::get('checkchatkit1', 'API\UserAPIController@checkchatkit1');

Route::get('reviewandratting/{id}', 'API\UserAPIController@reviewandratting');

Route::post('bookinglist','API\UserAPIController@bookinglsit');

Route::post('notificationtrigger','API\UserAPIController@notificationtrigger');

Route::post('submitReview','API\UserAPIController@submitReview');

Route::post('notifyfromuser','API\UserAPIController@notifyfromuser');

Route::post('notifyfromdoctor','API\UserAPIController@notifyfromdoctor');

Route::post('notifylist','API\UserAPIController@notifylist');

Route::post('city_update','API\UserAPIController@city_update');

Route::get('all_city','API\UserAPIController@all_city');

Route::get('reasonlist','API\UserAPIController@reasonlist');

Route::post('reasonSubmit','API\UserAPIController@reasonSubmit');

Route::get('fulldoctordetails/{id}','API\UserAPIController@fulldoctordetails');

Route::post('dorcname','API\UserAPIController@dorcname');

Route::post('editdoctorprofile/{id}','API\UserAPIController@editdoctorprofile');

Route::post('doctordetaillist','API\UserAPIController@doctordetaillist');

Route::post('specialtiesbycity','API\UserAPIController@specialtiesbycity');

Route::get('specialty_all','API\HomeAPIController@specialty_all');

Route::get('specialty','API\HomeAPIController@specialty');

Route::get('issuelist','API\HomeAPIController@issuelist');

Route::post('forgotpass','API\HomeAPIController@forgotpass');

Route::post('userotp','API\UserAPIController@userotp');

Route::post('doctor','API\UserAPIController@doctor');

Route::post('city_update_doctor','API\UserAPIController@city_update_doctor');

Route::post('postnews','API\UserAPIController@postnews');

Route::post('newsfeedlist','API\UserAPIController@newsfeedlist');

Route::post('changepass','API\UserAPIController@changePassword');

Route::post('postlistbyusertypeid/{id}','API\UserAPIController@postlistbyuserid');

Route::post('editUserProfile/{id}','API\UserAPIController@editUserProfile');

Route::get('viewUserProfile/{id}','API\UserAPIController@eviewUserProfile');

Route::post('likepost','API\UserAPIController@likepost');

Route::post('commentOnPost','API\UserAPIController@commentOnPost');

Route::get('Commentlist/{id}','API\UserAPIController@Commentlist');

Route::post('savepost','API\UserAPIController@savepost');

Route::post('deletepost','API\UserAPIController@deletepost');

Route::post('savepostlist/{id}','API\UserAPIController@savepostlist');

Route::post('editdoctorprofile/{id}','API\UserAPIController@editdoctorprofile');

Route::get('viewdoctorprofile/{id}','API\UserAPIController@viewdoctorprofile');



Route::get('info',function(){
	return view('info');
});

Route::get('aboutUs',function(){
	return view('about_us');
});


Route::get('contactUs',function(){
	return view('contact_us');
});


Route::get('privacy',function(){
	return view('ptc');
});

Route::get('termscondition',function(){
	return view('ctextra');
});






