<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/autocomplete/fetch', 'API\HomeAPIController@fetch')->name('autocomplete.fetch');

Route::post('newpay','API\HomeAPIController@newpay');

Route::group(array('before' => 'csrf'), function()
{

Route::post('testai','API\HomeAPIController@testai');

});

Route::post('testai',array('before'=>'csrf','uses'=>'API\HomeAPIController@testai'));

Route::get('rem/{id}','API\HomeAPIController@removeins');

Route::get('termsandcondition','API\HomeAPIController@termsandcondition');

Route::get('thanks','API\HomeAPIController@thanks');

Route::get('privacyandpolicy','API\HomeAPIController@privacyandpolicy');

Route::post('submitReview','API\HomeAPIController@submitReview');

Route::get('paymentpayout','API\HomeAPIController@paymentpayout');

Route::post('commentOn','API\HomeAPIController@commentOn');

Route::get('gettheapp','API\HomeAPIController@gettheapp');

Route::get('/','API\HomeAPIController@index');

Route::any('forgot','API\HomeAPIController@forgotpassweb');

Route::get('plan_map','API\HomeAPIController@plan_map');



route::get('postpage/{id}','API\HomeAPIController@postpage');

route::any('usersignup','API\HomeAPIController@usersignup');

route::any('logout','API\HomeAPIController@logout');

route::any("chatOn","API\HomeAPIController@chatOn");

route::any("addPatient","API\HomeAPIController@addPatient");

route::any('editDoctorProfile','API\HomeAPIController@editDoctorProfile');

route::any('doctordetails/{id}','API\HomeAPIController@doctordetails');

Route::get('newsfeedlist','API\HomeAPIController@newsfeedlist_ui');

Route::get('newsfeedlist12','API\HomeAPIController@newsfeedlist12');


Route::get('topissuelist','API\HomeAPIController@issuelistpage');

Route::post('deletepost','API\HomeAPIController@deletepost');

Route::get('bookingListdoctor','API\HomeAPIController@bookingListdoctor');

Route::get('bookingListuser','API\HomeAPIController@bookingListuser');

Route::get('likepost/{post_id}','API\HomeAPIController@likepost');
Route::get('savepost/{post_id}','API\HomeAPIController@savepost');
route::any('doctorprofile','API\HomeAPIController@doctorprofile');

route::any('userprofile','API\HomeAPIController@userprofile');

route::any('userlogin','API\HomeAPIController@userlogin');

route::any('doctorlogin','API\HomeAPIController@doctorlogin');

route::any('doctorsignup','API\HomeAPIController@doctorsignup');

route::any('usermore','API\HomeAPIController@usermore');

route::any('doctormore','API\HomeAPIController@doctormore');

route::get('savepostlist','API\HomeAPIController@savepostlist');

route::any('subscription','API\HomeAPIController@subscription');

route::post('changePassword','API\HomeAPIController@changePassword');

route::get('aboutus',function(){
	return view('aboutus');
});

route::any('editUserProfile','API\HomeAPIController@editUserProfile');

route::any('commentOnPost/{message}/{id}','API\HomeAPIController@commentOnPost');

route::post('postnews','API\HomeAPIController@postnews');


route::any('listing','API\HomeAPIController@filterlsting');

route::any('gloabalsearch','API\HomeAPIController@gloabalsearch');

route::any('clearallsesion','API\HomeAPIController@clearallsesion');





