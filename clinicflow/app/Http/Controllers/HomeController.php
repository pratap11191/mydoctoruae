<?php

namespace App\Http\Controllers;


////////////////////////////////
use Illuminate\Support\Facades\Input;
use App\Mail\SendMailable;
use Mail;

////////////////////////////////
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;
use App\Clinic;
use App\City;
use App\Specialty;
use App\Doctor;
use App\Dtoken;
use App\Notification;
use App\Booking;
use App\Insurance;
use Carbon\Carbon;
use App\Token;
use App\Subscription;

class HomeController extends Controller
{
    //
    
    public function index()

    {


    }
    
    public function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('insurances')
        ->where('name', 'LIKE', "%{$query}%")
        ->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '
       <li><a href="#">'.$row->name.'</a></li>
       ';
      }
      $output .= '</ul>';
      echo $output;
     }
    }
    
   // }
   
   
   ////**************clinic subscription code start*************//////////////
      public function subscription()
    {

       /* if(SESSION::get('userdetails', 'default') == 'default'){
              return redirect('userlogin');*/
              
              if(SESSION::get('user_session', 'default') == 'default'){

              return redirect('userlogin');
              
              
              
        } else {

       // $id = SESSION::get('userdetails', 'default')->id; 
       
        $id = session()->get('user_session')->id;

       $listdetails = Subscription::get();

        $checkdoctorinplan = DB::table('clinicplans')->where('clinic_id',$id)->first();

        $return_array = array();

        if(!empty($checkdoctorinplan) && Carbon::now()->diffInSeconds($checkdoctorinplan->exp_date) > 0)
        {
            foreach ($listdetails as  $value) {
                if($value->id == $checkdoctorinplan->sub_id){
                    $value->flag_plan = "1";
                } else {
                    $value->flag_plan = "0";
                }

                $value->id = (string)$value->id;

                $value->exp_date = $checkdoctorinplan->exp_date;

                $return_array[] = $value;
            }
        } else {
            foreach ($listdetails as  $value) {
                $value->flag_plan = "-1";

                $value->id = (string)$value->id;

                $return_array[] = $value;
            }
        }

        return view('subscription',compact('listdetails'));

        }
    }
   
   ////************clinic subscription code ends**************///////////////
   
   
   
   
   
   
   
    
    public function addPatient(Request $request)
    {

    	 $input = $request->all();

         $val_arr = [
            'firstname' =>'required',
            'lastname'  =>'required',
            'email'     =>'required',
            'phone'     =>'required',
            'age'       =>'required',
            'gender'    =>'required',
            'apdate'    =>'required',
            'aptime'    =>'required',
            'doctor_id' =>'required',
        ];

        $msg = [
            'firstname.required' =>'Please enter Firstname',
            'lastname.required'  =>'Please enter lastname',
            'email.required'     =>'Please enter Email ID',
            'phone.required'     =>'Please enter Phone Number',
            'apdate.required'    =>'Please enter Appointment date',
            'aptime.required'    =>'Please enter Appointment time',
            
        ];


        

        $validator = Validator::make($input, $val_arr,$msg);


        if($validator->fails()){
           echo json_encode(array("0",$validator->errors()->first()));

            die();      
        }

        $post = Doctor::find($input['doctor_id']);

        $check_email = DB::select("SELECT * FROM `users` WHERE `email` = '".$input['email']."'");

        //dd();

        if(empty($check_email)) {
        	echo json_encode(array("0",'Email not found.'));

            die();
            
        }

        $msg = $post->fullname." confirm booking on  ".date('d/m/Y');

         if(empty($post->image))
        {
            $post->image = url('/')."/public/img/user_signup.png";
        } else {
            $post->image = url('/public/').'/'.$post->image;
        }

        $data1 = array("msg"=>$msg,"image"=>$post->image);

        $return_responce = array('data'=>json_encode($data1),'sender_id'=>$input['doctor_id'],'receiver_id'=>$check_email[0]->id,'usertype'=>'doctor');

        Notification::create($return_responce);

        $data1['time'] = Carbon::now()->diffForHumans();

        Booking::create($input);


        $dtokenbyuser = Token::where(['user_id'=>$check_email[0]->id])->first();

        $count_notification = Notification::where(['receiver_id'=>$check_email[0]->id,'status'=>'0'])->count();

        //dd($dtokenbyuser);

        if($dtokenbyuser->deviceType == 'android')
        {
            //dd($return_responce);

            if($check_email[0]->notification_flag == 1){
              $this->android_push($dtokenbyuser->deviceToken,$msg,"booking_confirmed",$count_notification,$data1);
            }
            
        }

        //  if($dtokenbyuser->deviceType == 'ios')
        // {
        //     //dd($return_responce);

        //     if($check_email[0]->notification_flag == 1){
        //       $this->iphone_push($dtokenbyuser->deviceToken,$msg,"booking_confirmed",$count_notification,$data1);
        //     }
            
        // }
       echo json_encode(array("1","Booking confirmed successfully"));
                die();
      // return $this->sendResponse(array("status"=>"success"), '',$request->path());
    }
    
    public function login(Request $request)
    {
    
        if ($request->isMethod('post')){
        
    
        $input = $request->all();
        
         $val_arr = [
            'email'=>'required',
            'password' => 'required',
        ];


        

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
          
            $messages =$validator->errors()->toArray();

          

            return view('login',compact('messages'));    
        }
        
        //dd($input);
          $is_exist = Clinic::join('cities','clinics.city_id','=','cities.id')->where(['clinics.email'=>$input['email'],'clinics.password'=>$input['password'],'clinics.admin_status'=>1])->select('clinics.id','clinics.email','clinics.city_id','clinics.fullname','clinics.speciality_id','clinics.image','clinics.licence_number','clinics.expertise_area','clinics.phone','clinics.insurance_accept','cities.name')->first();

          /*$is_exist        =DB::table('clinics')
                                            ->join('cities', 'cities.id', '=', 'clinics.city_id')
                                            ->select('clinics.id','clinics.email','clinics.city_id','clinics.fullname','clinics.speciality_id','clinics.image','clinics.licence_number','clinics.expertise_area','clinics.phone','cities.name')
                                            ->where(['clinics.email'=>$input['email'],'clinics.password'=>$input['password']])
                                             ->get();*/
         

          //dd($is_exist);
         if($is_exist){
           
            $request->session()->put('user_session',$is_exist);
            
            return redirect ('dashboard');

                     }else
                     {
                       //Session::flash('error', 'Invalid Username or Password ');
                       $request->session()->flash('error', 'username or password Incorrect'); 
                        return redirect('login');
                     }

        
        } else {
    
        return view('login');
        
        }
    }



 public function update(Request $request)
    {

      

       
        $data['citydata'] = City::all();
        $input = $request->all();
       // dd($input);
        $insudata          = DB::table('clinics')
                                            ->select('*')
                                            ->where('id',session()->get('user_session')->id)
                                            ->get();

        $insdata = $insudata[0]->insurance_accept;



         if(!empty($input['insurance_accept1'])){


        //echo  $insid = $input['insurance_accept1'];
         

                $return_insurance_string = "";
                $insurance_accept_aarray = explode(",", $input['insurance_accept1']);

                foreach ($insurance_accept_aarray as $value1new) {
                        $check_insurance_accept = Insurance::where('name',$value1new)->first();

                        if(empty($check_insurance_accept)){
                             $insu_array_o = array("name"=>$value1new);
                             $insu_array_details = Insurance::create($insu_array_o);
                             $return_insurance_string = $return_insurance_string.$insu_array_details->id.",";

                        } else {
                            $return_insurance_string = $return_insurance_string.$check_insurance_accept->id.",";
                        } 
                }

                  
                 
                    $datas   = chop($return_insurance_string,",");

                   /* echo $datas ;
                    exit;*/
                  
                  if(empty($insdata)){
                  $input['insurance_accept'] =  $datas;
                    }else{
                    $input['insurance_accept'] =  $datas.','.$insdata;

                    }
 
        }else{

         
          
          $input['insurance_accept'] = $insdata;
        } 

    //dd($input);    
/*echo"gupta ";
exit;*/


        $id = session()->get('user_session')->id;
        $val_arr = [
                    'fullname'          => 'required',  
                    'city_id'           => 'required'
                    ];

        
        $validator = Validator::make($input, $val_arr);
        
        if($validator->fails()){
          
            $messages =$validator->errors()->toArray();
            return view('dashboard',compact('messages'),$data);    
        }

        if ($request->hasFile('image')) {
        $image = $request->file('image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/assets/images');
        $image->move($destinationPath, $name);
        //$this->save();
        $data = array('fullname'=>$input['fullname'],
                      'city_id'=>$input['city_id'],
                      'image'=>$name);
                      //$this->save();
        //return view('dashboard',compact('messages'),$data);
        }else{
                    $data = array('fullname'=>$input['fullname'],
                                  'city_id' =>$input['city_id']);
                                 
        }

        /*if ($request->hasFile('image') &&  !empty($input['insurance_accept1'])) {
           $data = array('fullname'=>$input['fullname'],
                                  'city_id' =>$input['city_id'],
                                  'insurance_accept' =>$input['insurance_accept']
                                  'image'=>$name
                                  );

        }*/

         if(!empty($input['insurance_accept1'])){

          $data = array('fullname'=>$input['fullname'],
                        'city_id' =>$input['city_id'],
                        'insurance_accept' =>$input['insurance_accept']

                        );
         }

      // dd($data);

        $updated = Clinic::where('id', $id)
                              ->update($data);
   
          if($updated){
          //dd($post);
       $request->session()->flash('success', 'Update Profile Successfully'); 
       //$request->session()->put('user_session',$post);
       return redirect('dashboard');
            }

       
   

 }

 public function removeins(Request $req,$id){

 //echo $id;
 
 $getinsname      = DB::table('clinics')
                    ->select('insurance_accept')
                    ->where('id',session()->get('user_session')->id)
                    ->get();
//print_r($insname);
 $insname =  $getinsname[0]->insurance_accept;
 $insarr = explode(',',$insname);
 if(in_array($id,$insarr)){
  foreach (array_keys($insarr, $id) as $key) {
    unset($insarr[$key]);
}

 }

 $data =  implode(',',$insarr);


    $updated =       DB::table('clinics')
                    ->where('id', session()->get('user_session')->id)
                    ->update(['insurance_accept' => $data]);

if($updated){
  return redirect('dashboard');
}


  //exit;
 }

 public function signup(Request $request){

           $data['citydata'] = City::all();
           $data['specialtydata'] = Specialty::all();
          //dd($data['specialtydata']);

        if ($request->isMethod('post')){

         

        $input = $request->all();
//dd($input);
        $val_arr = [
                    'fullname'          => 'required',
                    'email'             => 'required|email|unique:clinics',
                    //'image'             => 'required',
                    'phone'             => 'required',
                    'city_id'           => 'required',
                    'speciality_id'     => 'required',
                    'expertise_area'    => 'required',
                    'licence_number'    => 'required',
                    'password'          => 'required|min:6',

                    
                   
                    ];

       $msg = [
                'fullname.required'         =>'Please enter Clinic Name',
                'email.required'            =>'Please enter Valid Email ID',
                'phone.required'            =>'Please enter Phone Number',
                'city_id.required'          =>'Please enter City',
                'speciality_id.required'    =>'Please enter Speciality',
                'expertise_area.required'   =>'Please enter Expertise_area',
                'licence_number.required'   =>'Please enter licence_number',
                'password.required'         =>'Please enter Password',
           ] ;            

        $validator = Validator::make($input, $val_arr,$msg);
        
       if($input['password']!=$input['password_confirmation']){
        $messages['passworderror']= 'Password and Confirm Password must be same';
        return view('signup',compact('messages'),$data);
          }

              if(empty($input['insurance_accept'])){
                $input['insurance_accept'] = "";
        } else {


                $return_insurance_string = "";
                $insurance_accept_aarray = explode(",", $input['insurance_accept']);

                foreach ($insurance_accept_aarray as $value1new) {
                        $check_insurance_accept = Insurance::where('name',$value1new)->first();

                        if(empty($check_insurance_accept)){
                             $insu_array_o = array("name"=>$value1new);
                             $insu_array_details = Insurance::create($insu_array_o);
                             $return_insurance_string = $return_insurance_string.$insu_array_details->id.",";

                        } else {
                            $return_insurance_string = $return_insurance_string.$check_insurance_accept->id.",";
                        } 
                }

                $input['insurance_accept'] = chop($return_insurance_string,",");
        }
          /*if($validator->fails() &&(empty($request->hasFile('image')))){
            $messages =$validator->errors()->toArray();
            

            
            $messages['image']    ='Please Choose a Image File'; 
         
           return view('signup',compact('messages'),$data);
  
        }

        $image = $request->file('image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/assets/images');
        $image->move($destinationPath, $name);
        $input['image'] = $name;*/

        if($validator->fails()){
          $messages =$validator->errors()->toArray();
          return view('signup',compact('messages'),$data);
  
        }
 
 //dd($input);
        $inserted = Clinic::create($input);
          if($inserted){
          //dd($post);
       // $request->session()->flash('success', 'Registered successful!');
       //$request->session()->put('user_session',$inserted);
       //return redirect('dashboard');
            return view('thanks');
            }
           // return redirect('dashboard');

        } else 
        {

          

            return view('signup',$data);

        }

    }
 

  public function dashboard(){

  
//dd(session()->get('user_session'));
    if(empty(session()->get('user_session'))){

      return redirect('login');
    }

           $data['userdata']          = DB::table('clinics')
                                            ->select('*')
                                            ->where('id',session()->get('user_session')->id)
                                            ->get();

//dd($data['userdata']);
                                           //echo  $data['userdata'][0]->fullname;
                                         //  exit;



           $data['citydata']          = City::all();
           $data['specialtydata']     = DB::table('specialties')
                                            ->select('id','name')
                                            ->get();

                                           // dd($data['specialtydata']);

           $data['doctordata']        =DB::table('doctors')
                                            ->join('specialties', 'specialties.id', '=', 'doctors.speciality_id')
                                            ->select('doctors.id', 'doctors.fullname','doctors.phone','doctors.email', 'specialties.name')
                                            ->where('clinic',session()->get('user_session')->fullname)
                                             ->get();
          
          $data['appointment']        =DB::table('bookings')
                                            ->join('doctors', 'doctors.id', '=', 'bookings.doctor_id')
                                            ->join('users', 'users.email', '=', 'bookings.email')
                                            ->select('bookings.firstname','bookings.gender','bookings.age','bookings.lastname','bookings.email','bookings.phone','bookings.apdate','bookings.aptime','doctors.id', 'doctors.fullname','users.image as userimage')
                                            ->where('clinic',session()->get('user_session')->fullname)
                                            ->where('bookings.apdate','>=',Carbon::now()->format('Y-m-d'))
                                            ->get();        

          $data['notification']        =DB::table('bookings')
                                            ->join('doctors', 'doctors.id', '=', 'bookings.doctor_id')
                                            ->join('users', 'users.email', '=', 'bookings.email')
                                            ->select('bookings.firstname','bookings.lastname','bookings.apdate','bookings.aptime', 'doctors.fullname','users.image as userimage')
                                            ->where('clinic',session()->get('user_session')->fullname)
                                            ->where('bookings.apdate','>=',Carbon::now()->format('Y-m-d'))
                                            ->get();  

                                             

        $data['totalnotification']     =DB::table('bookings')
                                            ->join('doctors', 'doctors.id', '=', 'bookings.doctor_id')
                                            ->join('users', 'users.email', '=', 'bookings.email')
                                            ->select(DB::raw("count(bookings.id) as count"))
                                            ->where('clinic',session()->get('user_session')->fullname)
                                            ->where('bookings.apdate','>=',Carbon::now()->format('Y-m-d'))
                                            ->get();    

//echo $data['totalnotification']['0']->count;
 // dd($data['totalnotification']);


            if(Session::has('user_session')) 
             {
          return view('dashboard',$data);
             } else{
               return redirect('login');
              }
   
     }

      public function forgetPassword(Request $request){

               $input = $request->all();

               $val_arr = [
                        'email'               => 'required',
                        ];

              $validator = Validator::make($input, $val_arr);
            
             if($validator->fails()){

                echo json_encode(array("0",$validator->errors()->first()));

                die();
                  
            } 
            
            $email = $input['email'];
          
            $data = Clinic::where(['email'=>$email])->first();
            if(!empty($data->email)){
           
            $newpassword = uniqid();
            

                        DB::table('clinics')
                                   ->where('id', $data->id)
                                   ->update(['password' => $newpassword]);

               $password = 'Your Password is:'.$newpassword ;
                 
                $postData ="";
                 try{
                    $send =  Mail::send('email.forget',['otp' =>$password], function($message) use ($postData,$email)
                                {
                                  $message->from('support@mobulous.co.in', 'Healthapp');
                                  $message->to($email, 'Healthapp forgot password')->subject('Forgot Password');
                                });

                     
                      echo json_encode(array("1","Password Send successfully on your registered email!"));
                      die();
                      }
                    catch(Exception $e){
                      echo json_encode(array("1","Password Send successfully on your registered email!"));
                    die();
                    } 

            
              }else{

                 echo json_encode(array("0","Invalid email Please enter Registerd Email!"));
                 die();

              }

              }


    public function changePassword(Request $request){
            $id = session()->get('user_session')->id;
     
            $input = $request->all();
 
            $val_arr = [
                        'oldpassword'               => 'required',
                        'password'                  => 'required|min:6',
                        'password_confirmation'     => 'required|min:6',
                        ];


            $msg = [
                        'oldpassword.required'               => 'Please enter Oldpassword',
                        'password.required'                  => 'Please enter Newpassword',
                        'password.min'                       => 'New Password must be at least 6 and at most 15 characters',
                        'password_confirmation.required'     => 'Please enter Confirm Newpassword',
                        'password_confirmation.min'          => 'Confirm Password must be at least 6 and at most 15 characters',
                        ];

            $validator = Validator::make($input, $val_arr,$msg);
            
             if($validator->fails()){

                echo json_encode(array("0",$validator->errors()->first()));

                die();
                  
            }  

            
            $data = Clinic::where(['id'=>$id])->first();
            $password = $data->password ;
            

           
           
            if($input['password'] != $input['password_confirmation'])
            {
    
               echo json_encode(array("0","Old Password and confirm password is not same pls fill same."));
                die();
            } 

            if($password != $input['oldpassword'])
            {
    
             
               echo json_encode(array("0","Old Password is wrong."));
                die();
            } else {
                $data = array('password'=>$input['password']);
                $updated = Clinic::where('id', $id)
                              ->update($data);

                   echo json_encode(array("1","Password Changed successfully!!"));
                die();
            } 


        }

public function searchdoctor(Request $request){
$input   = $request->all();
//dd($input);
$search =$input['search'];
$search = '%'.$search.'%';
//echo $search ;
//exit;
$doctor       =DB::table('doctors')
                                    ->join('specialties', 'specialties.id', '=', 'doctors.speciality_id')
                                    ->select('doctors.id', 'doctors.fullname','doctors.phone','doctors.email', 'specialties.name')
                                    ->where('clinic',session()->get('user_session')->fullname)
                                    ->where('fullname',  'LIKE',  $search)
                                    ->get();

 
//if(!empty($doctor[0]))
 return view('searchdoctor',compact('doctor'));



}

public function addDoctor(Request $request){
 
                        $input   = $request->all();
//dd($input);
                        $val_arr = [
                                      'fullname'           => 'required',
                                      'email'              => 'required|email|unique:doctors|unique:users',
                                      'password'           => 'required|min:6',
                                      'phone'              => 'required',
                                      'city_id'            => 'required',
                                      'speciality_id'      => 'required',
                        
                                  ];


                                  $msg = [
                                      'fullname.required'           => 'Please enter Fullname',
                                      'email.required'              => 'Please enter Email ID',
                                      'password.required'           => 'Please enter Password',
                                      'password.min'                => 'Password must be at least 6 and at most 15 characters',
                                      'phone.required'              => 'Please enter Phone Number',
                                      'city_id.required'            => 'Please enter Cityname',
                                      'speciality_id.required'      => 'Please enter Speciality',
                        
                                  ];
                        $validator = Validator::make($input, $val_arr,$msg);
            
                      if($validator->fails()){
                     // dd($validator->errors()->keys());
                      //exit;
                    //dd($validator->errors()->first());
                       // dd($validator->errors()->first('fullname'));

                        
                        echo json_encode(array("0",$validator->errors()->first()));
                        die();
                       } 
                          $input['licence_number'] = "";
                          $input['expertise_area'] = "";
                          $input['clinic'] = session()->get('user_session')->fullname;
                          $inserted = Doctor::create($input);
                          
                            /*$data = 'Your Password is:'.$input['password'].'And Password is:'.$input['email'];
                            $email = $input['email']; 
                            
                 
                            $postData ="";
                
                            $send =  Mail::send('email.forget',['datass' =>$data], function($message) use ($postData,$email)
                                        {
                                          $message->from('support@mobulous.co.in', 'MyDoctoruae');
                                          $message->to($email, 'Welcome to  MyDoctoruae')->subject('Forgot Password');
                                        });*/

  
                      $insertintodtoken = Dtoken::create(array("user_id"=>$inserted->id,"token"=>uniqid(),"deviceType"=>"website","deviceToken"=>uniqid()));
                      if($inserted){
                          
                          //$email2="info@mydoctoruae.com";
                          
                          
                             $from = "info@mydoctoruae.com";
                             $to = $input['email'];
                             $subject = "Welcome to Mydoctoruae";
                             $message = "Dear " .$input['fullname'].", \r\n";
                             $message .= "MyDoctor UAE Login URL: " ."https://mydoctoruae.com/doctorlogin"."\r\n";
                             $message .= "Your Username: " .$input['email']."\r\n Password: ".$input['password'];
                             $headers = "From:" . $from;
                             mail($to,$subject,$message, $headers);
	            
	          
                        echo json_encode(array("1","Add Doctor successfully!!"));
                      die();

                        }

              }


            public function logout(){
//dd(session()->get('user_session')); to show session all data

              session()->forget('user_session');
              return redirect('login');
                        
             }


}
