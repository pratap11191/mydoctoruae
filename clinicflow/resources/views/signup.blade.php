<?php 
//dd($messages);
/*echo $messages['image'];
exit;*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" href="{{asset('public/assets/images/fav.png')}}" type="image/png" sizes="16x16">
    
    <!--main style sheet-->
    <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	
 
</head>
<body>


    
     <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
   }
  </style>
   
    
	<!-- More Page Tabs Ups * -->
	
	<div class="loginclinetsbg">
	<div class="container">
        <div class="signindivs">
			<div class="morepagessection">
				
                  <div class="formslogins">
                       
                        
                        <div class="regisindivs">
                        <div class="col-md-12 main-hd">
                            <h1>Sign Up</h1>
                        </div>
                        <form  action="{{url('/')}}/signup" method="post" enctype="multipart/form-data">

			

   
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Clinic Name</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Clinic Name" name="fullname" value="<?php if(isset($_POST['fullname'])){echo $_POST['fullname'];}?>">

                                
								<span class="formerror"><?php
                                	if(isset($messages['fullname']['0']) && !empty($messages['fullname']['0'])){
                                		echo $messages['fullname']['0'];
                                	}
                                ?></span>
								</div>
								
								 <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Email ID</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>">


                                <span class="formerror"><?php
                                	if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                		echo $messages['email']['0'];
                                	}
                                ?></span>
                                
                                </div>


<!-- 
                                 <div class="col-md-12 col-sm-12">
                                <label for="exampleFormControlInput1">Image</label>
                    <input type="file" name="image" accept="image/*" class="form-control" id="exampleFormControlInput1" placeholder="" value="<?php if(isset($_POST['image'])){echo $_POST['image'];}?>">


                             <span class="formerror"><?php
                                    if((isset($messages['image']['0']) && !empty($messages['image']['0']))||  !empty($messages['image'])) {
                                       // exit;
                                        echo $messages['image'];
                                    }
                                ?></span> 
                                
                                </div> -->
								
								 <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Phone Number</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Phone Number" name="phone" value="<?php if(isset($_POST['phone'])){echo $_POST['phone'];}?>">

                                <span class="formerror"><?php
                                	if(isset($messages['phone']['0']) && !empty($messages['phone']['0'])){
                                		echo $messages['phone']['0'];
                                	}
                                ?></span>
                                  
								</div>


                                
								
								 <div class="col-md-6 col-sm-6">
                               <label for="exampleFormControlInput1">City</label>
                                <select placeholder="name@example.com" name="city_id" id="city_id" required="">


                                                                   
                                                              @foreach($citydata as $city)                                

                                                             <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                                   @endforeach
                                 
                                                                    
                                 
                                                                  </select>

                            
                                </div>

                                <span class="formerror"><?php
                                	if(isset($messages['city_id']['0']) && !empty($messages['city_id']['0'])){
                                		echo $messages['city_id']['0'];
                                	}
                                ?></span> 



								
								 <div class="col-md-12 col-sm-12">
                                <label for="exampleFormControlInput1">Speciality</label>
                                <select placeholder="name@example.com" name="speciality_id" id="speciality_id">
                                                                   
                                 
                                                                     @foreach($specialtydata as $specialty)                                

                                                             <option value="{{ $specialty->id }}">{{ $specialty->name }}</option>
                                                                   @endforeach
                                                                  </select>
								
                                 <input type="hidden" class="form-control" id="the_other_speciality" placeholder="enter your speciality" name="the_other_speciality">


                                 
                                </div>
                                 <span class="formerror"><?php
                                	if(isset($messages['speciality_id']['0']) && !empty($messages['speciality_id']['0'])){
                                		echo $messages['speciality_id']['0'];
                                	}
                                ?></span>
								
								
								
								 <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">License Number</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="License Number" name="licence_number" value="<?php if(isset($_POST['licence_number'])){echo $_POST['licence_number'];}?>">

                                <span class="formerror"><?php
                                	if(isset($messages['licence_number']['0']) && !empty($messages['licence_number']['0'])){
                                		echo $messages['licence_number']['0'];
                                	}
                                ?></span>
                                  
                                </div>
                                
								
								 <div class="col-md-6 col-sm-12">
                                <label for="exampleFormControlInput1">Expertise Areas</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Expertise Areas" name="expertise_area" value="<?php if(isset($_POST['expertise_area'])){echo $_POST['expertise_area'];}?>">

                                <span class="formerror"><?php
                                	if(isset($messages['expertise_area']['0']) && !empty($messages['expertise_area']['0'])){
                                		echo $messages['expertise_area']['0'];
                                	}
                                ?></span>
                                 
                                </div>
                                 
								
								 <div class="col-md-12 col-sm-12">
                                <div class="radionforms">
                                <label>Insurance Accepted</label>
                                <p><input type="radio" name="accepted" class="accepted" value="1">Yes
                                <input type="radio" name="accepted" class="accepted" value="0"> No</p>
                                </div>

                                <div id="divia" style="display: none;" >
                                	<input type="text" class="form-control" id="insurance_accept1" placeholder="enter your insurance" name="insurance_accept1">
                                	<div id="countryList">
    </div>
                                	<input type="hidden" class="form-control" id="insurance_accept" placeholder="enter your insurance" name="insurance_accept">
	                                <ol id="demo"></ol>

									<div class="addinsuranc">

									<input type='button' class="addinsubtn"  value='Submit' />
                                    
									<i  onclick='changeText2()' class="fa fa-plus addinsubtn"></i>
                                    
									</div>

                                </div>
								
								</div>
								
								 <div class="col-md-6 col-sm-6">
                                <label for="inputPassword">Password</label>
								<div class="showornotpas">
<input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" value="<?php if(isset($_POST['password'])){echo $_POST['password'];}?>">
<div class="paswdshowsck">
<input type="checkbox" class="oppwas" onclick="showpasswordFunction()"><i class="fa fa-eye"></i>
								</div>
                                </div>

								<span class="formerror"><?php
                                	if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                                		echo $messages['password']['0'];
                                	}
                                ?></span>

								</div>
								
								 <div class="col-md-6 col-sm-6">
                                <label for="inputPassword">Confirm Password</label>
								<div class="showornotpas">
<input type="password" class="form-control oppwasconf" id="inputconPassword" placeholder="Confirm Password" name="password_confirmation" value="<?php if(isset($_POST['password_confirmation'])){echo $_POST['password_confirmation'];}?>">
<div class="paswdshowsck">
<input type="checkbox" class="oppwas1" id="pwchecknext" onclick="showconfpasswordFunction()"><i class="fa fa-eye"></i>
								</div>

<span class="formerror"><?php
                                	if(isset($messages['passworderror'])){
                                		echo $messages['passworderror'];
                                	}
                                ?></span>
								</div>
								
                               <!-- <input type="hidden" name="_token" id="csrf-token" value="IztPPnwyX2ygMe1TTYnFEyAZ8OlHJpBVFyWEZGta">-->
<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
								</div>
								
								 <div class="col-md-12 col-sm-12">
                                <div class="actionloginbs">
                                <button type="submit"  id="signbtn"  class="btn btn-primary">Sign Up</button>
                                </div>
                                    <div class="center section"><small class="fs13">Already have an account?<a href="{{url('/')}}/login"> Login Here</a></small></div>                       
                            </div>
							</div>
                        </form>
                        </div>
						
						
						<div class="signupsrg col-md-6 col-sm-12">
    
					<div class="flexmnd">
							
							<div class="ctndb col-md-3 col-xs-3">
								<i class="fa fa-stethoscope"></i>
								
							</div>
							
							<div class="ctndb">
								<div class="main-hd">
									<h1>Why you love doctorapp</h1>
								</div>
								
								<ul>
								<li><p><i class="fa fa-handshake-o"></i> Simple, beautiful design - you will pick it up in minute</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Online Private Consultation for Personalized car</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Answer Open Questions by Patients and build your online reputation</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Reduce no shows and increase patient connect with comprehensive set of Reminders</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Record treatments and schedule visits in 1/4th the time compared to your diary or any other software</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Magical apps that help you do everything else easily on the go</p></li>
								
								</ul>
								<hr>			
							</div>
					</div>
					<div class="flexmnd">
						
						<div class="ctndb col-md-4 col-xs-3 col-sm-4">
							<i class="fa fa-credit-card"></i>
						</div>
						
						<div class="ctndb">
							<div class="main-hd">
								<h1>No String Attached</h1>
							</div>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae pellentesque erat. Proin congue massa quis lorem faucibus, at sollicitudin libero maximus. </p>
						</div>
					</div>
				</div>
						
						
                        
                    </div>
				
			</div>
		</div>
		</div>

        
	</div>
  <?php echo View::make('include/footer'); ?>
    	<!-- More Page Tabs Ups ENDS Here* -->
		
		
   

    <script src="{{asset('public/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap/bootstrap.min.js')}}"></script>
    
    <!--main custome js-->
    <script src="{{asset('public/assets/js/custome.js')}}"></script>
    
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  function checkpass(){

  var password = $('#inputPassword').val();
  var conpassword = $('#inputconPassword').val();
 if(password!=conpassword){
alert('password and confirm password should be same');

}

}
</script>


<script>
 function showconfpasswordFunction(){
 var x = document.getElementById("inputconPassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>



<script type="text/javascript">
    $(".accepted").click(function(){
        
        if($(this).val() == '1'){
            $('#divia').css('display','block');
        } else {
            $('#divia').css('display','none');
        }
    });
</script>


<script>
var list = document.getElementById('demo');
var lastid = 0;


function changeText2() {
    var firstname = document.getElementById('insurance_accept1').value;
    var insurance_accept_hidden = $('#insurance_accept').val();
    if(firstname){
    $('#insurance_accept').val(insurance_accept_hidden+","+firstname);
    var entry = document.createElement('li');
    entry.appendChild(document.createTextNode(firstname));
    entry.setAttribute('id','item'+lastid);
    var removeButton = document.createElement('button');
    removeButton.appendChild(document.createTextNode("x"));
    removeButton.setAttribute('onClick','removeName("'+'item'+lastid+'")');
    entry.appendChild(removeButton);
    lastid+=1;
    list.appendChild(entry);
}
}


function removeName(itemid){
    var item = document.getElementById(itemid);
   
    var insurance_accept_hidden = $('#insurance_accept').val();
   
    var newvar1 = insurance_accept_hidden.split(",");

    var i;
    for (i = 0; i < newvar1.length; i++) { 
        if(newvar1[i]+'remove' == $('#'+itemid).text()){
            
            newvar1.splice(i, 1);
        }
    }

    $('#insurance_accept').val(newvar1);
     list.removeChild(item);
}
</script>
<script>
$(document).ready(function(){

 $('#insurance_accept1').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('autocomplete.fetch') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'li', function(){  
        $('#insurance_accept1').val($(this).text());  
        $('#countryList').fadeOut();  
    });  

});
</script>

<script type="text/javascript">
  $("#insurance_accept1").keypress(function (evt) {
  
  var keycode = evt.charCode || evt.keyCode;
   // alert(keycode);
  if (keycode  == 44) { //Enter key's keycode
    return false;
  }
});
</script>
