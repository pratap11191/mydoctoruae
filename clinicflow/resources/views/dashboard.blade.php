<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
  <title> Clinic |  My Doctor</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <link rel="icon" href="{{asset('public/assets/images/fav.png')}}" type="image/png" sizes="16x16">
    
    <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{asset('public/assets/bootstrap/bootstrap.min.js')}}"></script>
</head>
<body>

<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-header statichdr doctorsflow">
        <div class="container-fluid">
    <div class="holdconts">
           
			
			<div class="toggledivmen"><i class="menu-tog fa fa-bars" aria-hidden="true"></i> <i class="fa fa-times"></i></div>
            <div class="menu-section">
                <div class="menues">
                    <ul class="menu-ul">
                        <li><a href="clinicprofile.html"><p>Welcome: </p> <span class="username">{{ session()->get('user_session')->fullname }}</span></a></li>
						<li class="userprofile"><span class="userico">
						<img src="{{asset('public/assets/images/user.png')}}"></span></li>
                    </ul>
					
					<div class="userlistingopn">
					<ul>
					<li><a href="clinicprofile.html">Home</a></li>
						<li><a href="#">About us</a></li>
						<li><a href="#">Contact	Us</a></li>
						<li><a href="#">Privacy	policy</a></li>
						<li><a href="#">Terms	and	conditions</a></li>
						<li><a href="#">Logout <i class="fa fa-sign-out"></i></a> </li>
					</ul>
					</div>
					
                </div>
            </div>
    </div>
        </div>
    </div> -->
    
    
   
    
	<!-- More Page Tabs Ups * -->
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-header statichdr doctorsflow">
							<div class="container">
								<div class="holdconts">
									   
										
										<div class="toggledivmen"><i class="menu-tog fa fa-bars" aria-hidden="true"></i> <i class="fa fa-times"></i></div>
										
										<div class="docthedrsrc col-md-6">
											<!-- <input type="text" placeholder="search appointments"><i class="fa fa-search"></i> -->
											<i class="sidebarclinic fa fa-bars" aria-hidden="true"></i>
										</div>

										<div class="menu-section">
					

										<div class="menues">
                                       	<ul class="menu-ul">
										<li class="userprofile"><span class="userico">
										<?php   if(empty($userdata[0]->image)){
									   // print_r(session()->get('user_session')->image );
									    ?>
										<img src="{{asset('public/assets/images/user.png')}}"></span>
										<?php }else{ ?>
	<img src="{{url('/')}}/public/assets/images/{{ $userdata[0]->image }}">
										
										<?php  } ?>		
													
													</li>
												</ul>
												
												<div class="userlistingopn">
												<ul>
												<li><a href="{{url('/')}}/dashboard">Home</a></li>
													<!-- <li><a href="#">About us</a></li>
													<li><a href="#">Contact	Us</a></li>
													<li><a href="#">Privacy	policy</a></li>
													<li><a href="#">Terms	and	conditions</a></li> -->
													<li><a href="{{url('/')}}/logout">Logout <i class="fa fa-sign-out"></i></a> </li>
												</ul>
												</div>
												
											</div>
										</div>
								</div>
							</div>
					</div>
	<div class="userpropge">
        <div class="container-fluid">
			<div class="morepagessection">
				<div class="">
				
				         <div class="client col-md-3 col-sm-4 col-lg-2">
						 
						 <div class="brandnames">
						 <a href="https://mydoctoruae.com"><img src="{{asset('public/assets/images/white-logo.png')}}" class="img-responsive" alt=""></a>
						 </div>
						 
						 <div class="doctorsflow">
							 <div class="menues">
								<ul class="menu-ul">
									<li class=""><span class="userico">
        <!--<img src="{{asset('public/assets/images/user.png')}}"></span>-->
<?php   if(empty($userdata[0]->image)){
									   // print_r(session()->get('user_session')->image );
									    ?>
										<img src="{{asset('public/assets/images/user.png')}}"></span>
										<?php }else{ ?>
	<img src="{{url('/')}}/public/assets/images/{{ $userdata[0]->image }}">
										
										<?php  } ?>

  </li>
									<li><a href="#"><p>Welcome: </p> <span class="username"><?php echo $userdata[0]->fullname?></span></a></li>
									
								</ul>
							 </div>
						 </div>
						<div class="tabdocts">
						
						  <button class="tablinks active" onclick="openCity(event, 'Userpro')"><i class="fa fa-user"></i>Edit Profile</button>
						<!-- <button class="tablinks" onclick="openCity(event, 'postscli')"><i class="fa fa-star"></i>Ratings and Reviews</button> -->
						  <button class="tablinks" onclick="openCity(event, 'notific')"><i class="fa fa-bullhorn"></i> Notification <span class="badge">{{$totalnotification['0']->count}}</span></button> 
						  <button class="tablinks" data-toggle="modal" data-target="#myModal" id="myModel"><i class="fa fa-key"></i> Change Password </button>
 <button class="tablinks" onclick="openCity(event, 'appointments')"><i class="fa fa-calendar"></i>Appointments</button>
						   <button class="tablinks" onclick="openCity(event, 'addoct')"><i class="fa fa-plus"></i>Doctors</button>
						   <!--<a href="{{url('/subscription')}}" target="_blank"><button class="tablinks" >Subscription </button></a>-->
						</div>
                        </div>


						<div class="contentblocks col-md-9 col-sm-8 col-lg-10">
						
					
						
					
						<div class="mainctn">
						<div id="Userpro" class="tabcontent" style="display:block;">
							<div class="userprofilename">
							
							
							<?php if(Session::get('success')){?>
<span class="alert alert-success">
{{ Session::get('success')}}
  </span> 
  <?php } ?>
  
<form action="{{ url('/')}}/update" method="post" enctype="multipart/form-data">
  
								<div class="userdescpro">
																
									<div class="imageuserdocs">
									    <div class="profileinf">
									  <?php   if(empty($userdata[0]->image)){
									   // print_r(session()->get('user_session')->image );
									    ?>
										<img  id="profile-img-tag" src="{{asset('public/assets/images/user.png')}}">
										<?php }else{ ?>
	<img  id="profile-img-tag" src="{{url('/')}}/public/assets/images/{{ $userdata[0]->image }}">
										
										<?php  } ?>
										<i class="fa fa-camera"></i>
										  <input  id="profile-img"  class="cameraupload" type="file" name="image" accept="image/*" >
										</div>
										 
		<h1 class="userdesigname">{{ $userdata[0]->fullname }}</h1>
		                             <?php $special   = DB::table('specialties')
                                            			->select('name')
                                            			->where('id',session()->get('user_session')->speciality_id)
                                            			->get();
                                            ?>
										 <p class="postypedt"><?php echo $special[0]->name;
?></p>
										 <p class="licen">Licence Number - {{ $userdata[0]->licence_number }}</p>
										
									</div>
									
									
									<div class="editinfofieldsdta">
									
									<div class="col-md-12 col-sm-12  col-lg-6 equalpart bdrsep">
										<div class="col-md-12">
											<label class="userfieldsnk" for="exampleFormControlInput1">Clinic Name</label>
											<span class="useridprint">{{ $userdata[0]->fullname  }}</span>
											<div class="editdownf accordion"><i class="fa fa-edit"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
												<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Clinic Name" name="fullname" value="{{ $userdata[0]->fullname }}" >
											</div>
											
										</div>
										<span class="formerror"><?php
                                	if(isset($messages['fullname']['0']) && !empty($messages['fullname']['0'])){
                                		echo $messages['fullname']['0'];
                                	}
                                   ?></span>
										
										<!-- <div class="col-md-12">
											<label class="userfieldsnk" for="exampleFormControlInput1">Working Hours</label>
											
											<span class="useridprint">8-10 Hours</span>
											<div class="editdownf accordion"><i class="fa fa-edit"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
												<select placeholder="name@example.com">
												  <option value="volvo">8-10</option>
												  <option value="volvo">5-14</option>
												</select>
											</div>
										</div> -->	
									</div>	
									
									<div class="col-md-12 col-sm-12  col-lg-6 equalpart">

										<!-- <div class="col-md-12">
											<label class="userfieldsnk" for="exampleFormControlInput1">Availablity(days)</label>
											
											<span class="useridprint">3-4</span>
											<div class="editdownf accordion"><i class="fa fa-edit"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
												<select placeholder="name@example.com">
												  <option value="volvo">3-4</option>
												  <option value="volvo">5-14</option>
												</select>
											</div>
											
											
											
										</div> -->
										
										<!-- <div class="col-md-12">
											<label class="userfieldsnk" for="exampleFormControlInput1">Location</label>
											<span class="useridprint">Sharjah, Dubai</span>
											<div class="editdownf accordion"><i class="fa fa-map-marker"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
												<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Location" name="city_id" >
											</div>
											
										</div> -->


										<div class="col-md-12">
										<div class="fullwdth">
										 <!--  <span class="useridprint"></span>  -->
											<label class="userfieldsnk" for="exampleFormControlInput1">Location</label>
											{{ session()->get('user_session')->name }}
											<div class="editdownf accordion"><i class="fa fa-map-marker"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
				<select class="limitedNumbChosen" id="city_id" name="city_id" >
						<option value="">select city</option>
						
       <?php foreach ($citydata as $value) { ?>	
        <option value="<?php echo $value->id; ?>" <?php if($userdata[0]->city_id == $value->id){ echo "selected"; } ?>><?php echo ucfirst($value->name); ?></option>
       <?php } ?>
		</select>
											</div>
											
										</div>
										</div>

										<span class="formerror"><?php
                                	if(isset($messages['city_id']['0']) && !empty($messages['city_id']['0'])){
                                		echo $messages['city_id']['0'];
                                	}
                                   ?></span>
										
										</div>


										<div class="col-md-12 col-sm-12 equalpart bdrsep insurancapts">
                                            <label class="" for="exampleFormControlInput1" style="color:red; margin: 0px 16px;float:left;">Insurance Accepted:-</label>
                                            <?php 
                                            $insurancearr = $userdata[0]->insurance_accept;
                                            if(!empty($insurancearr)){
                                            
                                            $insarray = explode(",",$insurancearr);

                                           ?>
                                            <label class="useridprint" >
                                                <?php
                                                    $ik2255 = 0;
                                                    foreach ($insarray as  $value) {
                                                    $insname =  $data['userdata']  = DB::table('insurances')
                                                                                     ->select('name')
                                                                                      ->where('id',$value)
                                                                                      ->get();

                                                     ?>
                                                     <p style="background: #31708f;border-radius: 5px; color: #fff; padding: 1px 4px; float: left; margin: 0 10px 0 0;">

                                                     

                                                      <a href="{{url('/')}}/rem/{{$value}}"><i class="fa fa-close" style="color: #f19191;"></i></a> 


                                                     {{ucfirst($insname[0]->name)}}</p>
                                                        <?php

                                                        $ik2255++;
                                                    }
                                                    
                                                    
                                            }
                                            else {
                                                echo "Not Any Insurance Accepted";
                                            }
                                                 ?>
                                               
                                                
                                            </label>
                                             <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                             <div class="profilepanel revelfieldedit formslogins" style="max-height: auto;">
                                                
                                                
                                  

                  
                                            
                                        </div>

<div class="editdownf accordion addinsuranc">
                  <input type='button' class="addinsubtn" onclick='changeText2()' value='Submit' />
                
                  </div>
                  
                  <div class="profilepanel revelfieldedit formslogins">
                    <input type="text" class="form-control" id="insurance_accept1" placeholder="enter your insurance" name="insurance_accept1">
                                  
                                  <input type="hidden" class="form-control" id="insurance_accept" placeholder="enter your insurance" name="insurance_accept">
                                  <ol id="demo"></ol>
                                  </div>
                  
                  
                  
                                            </div>

									<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
									    <div class="col-md-12 actionloginbs">
												<button type="submit" class="btn btn-primary">Save</button>
											</div>
									</div>
								</div>
								</form>

							</div>
						
						</div>

						<div id="postscli" class="tabcontent">
						  
                        
                        <div class="averageratings">
						
						
						
						
						<!-- Trigger the modal with a button -->
  
  
                        <h6>Average Ratings</h6>
                            <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                        
                            <h4>Reviews (57)</h4>
                          <div class="notiftabs">
                            <div class=" userpic">
                                <img src="https://mobulous.app/public/assets/images/user.png">
                            </div>
                            
                            <div class="usernotify">
                            <h6>Rohonda Adkins</h6>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p> 
                            <span class="notifytimes"><i class="fa fa-star"></i> 90%</span>
                            </div>
                            
                          </div>
                          
                          <div class="notiftabs">
                            <div class=" userpic">
                                <img src="https://mobulous.app/public/assets/images/user.png">
                            </div>
                            
                            <div class="usernotify">
                            <h6>Rohonda Adkins</h6>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p> 
                            <span class="notifytimes"><i class="fa fa-star"></i> 90%</span>
                            </div>
                            
                          </div>
                          
                          <div class="notiftabs">
                            <div class=" userpic">
                                <img src="https://mobulous.app/healthapp/public/assets/images/user.png">
                            </div>
                            
                            <div class="usernotify">
                            <h6>Rohonda Adkins</h6>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p> 
                            <span class="notifytimes"><i class="fa fa-star"></i> 90%</span>
                            </div>
                            
                          </div>
                        
						</div>
                       
						<div id="notific" class="tabcontent">
						<?php if(!empty($notification[0])){ ?>
						   @foreach($notification as $notifdata)
						  <div class="notiftabs">
							<div class=" userpic">

							<?php if($notifdata->userimage!=''){?>
								<img src="<?php if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
   
			echo $protocol."://".$_SERVER['SERVER_NAME'].'/public/'.$notifdata->userimage;
 }      ?>">
							 <?php	}else{ ?>
							 <img src="<?php if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
   
			echo $protocol."://".$_SERVER['SERVER_NAME'].'/public/userimage/'.'123.png';
 }      ?>">
							 <?php } ?> 
							</div>
							
							<div class="usernotify">
							<?php 
							 $dr = strtoupper($notifdata->fullname); 
                            if (strpos($dr, 'Dr') !== false) {
                                  $drname = ucfirst($notifdata->fullname);
								}else{
									 $drname =substr_replace($notifdata->fullname,'Dr.',0,0); //eggxs 
									//exit;
								}

							?>


							<p>{{ucfirst($notifdata->firstname).ucfirst($notifdata->lastname)}} have booked an appointment with {{ $drname }} on {{ date("d-m-Y", strtotime($notifdata->apdate)).' At '.$notifdata->aptime}}</p> 
							<!-- <span class="notifytimes">10h</span> -->

							</div>
							
						  </div>
						  @endforeach
						  
						  <?php }else{ ?>

 <div class="usernotify"> No Notification available for this Clinic</div>
						  <?php } ?>
                   
						</div>

						<!-- <div id="chngpswd" class="tabcontent">
							<div class="">
					<div class="formslogins">
					   
						
						<div class="regisindivs">
						<div class="main-hd">
							<h1>Change Password</h1>
						</div>
						<form action="{{url('/')}}/changepassword" method="post">
							<div class="form-group">
								
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Old Password.">
								
								
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="New Password">
								
								
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Password">
								
								<div class="actionloginbs">
								<button type="submit" class="btn btn-primary">Save</button>
								</div>
															
							</div>
						</form>
						</div>
						
					</div>
				</div>
						</div> -->
						
						<div id="appointments" class="tabcontent" style="display: none;">
						
						        <div id="addappoint" class="postadcontent" style="display: block">
								<div class="bookbtn">
				<button class="posttablinks btn btn-primary active" data-toggle="modal" data-target="#myModal2" id="myModel2"><i class="fa fa-calendar"></i> Add Appointments</button>
								</div>
								
								<div class="hading_by_title">
												<h4>My Appointments</h4>
								</div>
<?php  
if(!empty($appointment[0])){
foreach($appointment as $aptdata) { ?>

								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<!-- <a href="docter-newsfeed-user.html" class="newsfeed_a_all"> -->
												<div class="post-profile">
                                             <?php if($aptdata->userimage!=''){?>
													
													<img src="<?php if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
   
			echo $protocol."://".$_SERVER['SERVER_NAME'].'/public/'.$aptdata->userimage;
 }      ?>" class="img-responsive">
												
												<?php } else{ ?>

                                                   <img src="<?php if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
   
			echo $protocol."://".$_SERVER['SERVER_NAME'].'/public/userimage/'.'123.png';
 }      ?>" class="img-responsive">
													
													<?php } ?>
												</div>
												<div class="post-profile-paraes">
													<h4>{{ ucfirst($aptdata->firstname).' '.$aptdata->lastname}}</h4>
													<span class="bookinfo desg-post">{{ $aptdata->email}}</span>
													<span class="bookinfo desg-post">{{ $aptdata->phone}}</span>
													<span class="bookinfo desg-post">{{ ucfirst($aptdata->gender)}}</span>
													<span class="bookinfo desg-post">{{ ucfirst($aptdata->age)}} Years</span>
													<p class="appoints">Appointments:{{ $aptdata->apdate}} at {{ $aptdata->aptime}} To: Dr.{{ ucwords($aptdata->fullname)}}</p>
												</div>
												
												<div class="chatcomment">
												<!-- <i class="fa fa-comment" aria-hidden="true"></i> -->
												</div>
												
											</a>  
										</div>
										
									</div>
								</div>
								<?php } }else{ ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								No Appointments available for this Clinic
								</div>
								<?php } ?>
								<!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<a href="docter-newsfeed-user.html" class="newsfeed_a_all">
												<div class="post-profile">
													<img src="{{asset('public/assets/images/user.png')}}" class="img-responsive">
												</div>
												<div class="post-profile-paraes">
													<h4>Dr. Hannah Baker</h4>
													<span class="bookinfo desg-post">hannah@gmail.com</span>
													<span class="bookinfo desg-post">987654321</span>
													<p class="appoints">Appointments:22/05/2018 at 2:00pm</p>
												</div>
												
												<div class="chatcomment">
												
												</div>
												
											</a>  
										</div>
										
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<a href="docter-newsfeed-user.html" class="newsfeed_a_all">
												<div class="post-profile">
													<img src="{{asset('public/assets/images/user.png')}}" class="img-responsive">
												</div>
												<div class="post-profile-paraes">
													<h4>Dr. Hannah Baker</h4>
													<span class="bookinfo desg-post">hannah@gmail.com</span>
													<span class="bookinfo desg-post">987654321</span>
													<p class="appoints">Appointments:22/05/2018 at 2:00pm</p>
												</div>
												
												<div class="chatcomment">
												
												</div>
												
											</a>  
										</div>
										
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<a href="docter-newsfeed-user.html" class="newsfeed_a_all">
												<div class="post-profile">
													<img src="{{asset('public/assets/images/user.png')}}" class="img-responsive">
												</div>
												<div class="post-profile-paraes">
													<h4>Dr. Hannah Baker</h4>
													<span class="bookinfo desg-post">hannah@gmail.com</span>
													<span class="bookinfo desg-post">987654321</span>
													<p class="appoints">Appointments:22/05/2018 at 2:00pm</p>
												</div>
												
												<div class="chatcomment">
												
												</div>
												
											</a>  
										</div>
										
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<a href="docter-newsfeed-user.html" class="newsfeed_a_all">
												<div class="post-profile">
													<img src="{{asset('public/assets/images/user.png')}}" class="img-responsive">
												</div>
												<div class="post-profile-paraes">
													<h4>Dr. Hannah Baker</h4>
													<span class="bookinfo desg-post">hannah@gmail.com</span>
													<span class="bookinfo desg-post">987654321</span>
													<p class="appoints">Appointments:22/05/2018 at 2:00pm</p>
												</div>
												
												<div class="chatcomment">
												
												</div>
												
											</a>  
										</div>
										
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<a href="docter-newsfeed-user.html" class="newsfeed_a_all">
												<div class="post-profile">
													<img src="{{asset('public/assets/images/user.png')}}" class="img-responsive">
												</div>
												<div class="post-profile-paraes">
													<h4>Dr. Hannah Baker</h4>
													<span class="bookinfo desg-post">hannah@gmail.com</span>
													<span class="bookinfo desg-post">987654321</span>
													<p class="appoints">Appointments:22/05/2018 at 2:00pm</p>
												</div>
												
												<div class="chatcomment">
												
												</div>
												
											</a>  
										</div>
										
									</div>
								</div>
								
							 	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="your-posts-section bookingtabs">
										<div class="your-posts-pic-para">
											<a href="docter-newsfeed-user.html" class="newsfeed_a_all">
												<div class="post-profile">
													<img src="{{asset('public/assets/images/user.png')}}" class="img-responsive">
												</div>
												<div class="post-profile-paraes">
													<h4>Dr. Hannah Baker</h4>
													<span class="bookinfo desg-post">hannah@gmail.com</span>
													<span class="bookinfo desg-post">987654321</span>
													<p class="appoints">Appointments:22/05/2018 at 2:00pm</p>
												</div>
												
												<div class="chatcomment">
												
												</div>
												
											</a>  
										</div>
										
									</div>
								</div> -->
								
								</div>
								
								<div id="newappointment" class="postadcontent" style="display: none;">
								
								<div class="bookbtn">
								<button class="posttablinks btn btn-primary active" onclick="openPosts(event, 'addappoint')"><i class="fa fa-calendar"></i> back</button>
								</div>
								
								<div class="formslogins">
					   
						
						<div class="regisindivs">
						<div class="main-hd">
							<h1>Add Patient</h1>
						</div>
						<form>
							<div class="form-group">
							
								<div class="col-md-6">
							
							
								<label for="exampleFormControlInput1">First Name</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Patient Name..">
								
								<label for="exampleFormControlInput1">Last Name</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Last Name..">
								
								<label for="exampleFormControlInput1">Email ID</label>
								<input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
								
								<label for="exampleFormControlInput1">Phone Number</label>
								<input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Last Name..">
														
								</div>
								
								<div class="col-md-6">
								
								
								<label for="exampleFormControlInput1">Gender</label>
								<select>
								  <option value="volvo">Male</option>
								  <option value="volvo">Female</option>
								</select>
								
								<label for="exampleFormControlInput1">Age</label>
								<select>
								  <option value="age">18</option>
								  <option value="age">19</option>
								  <option value="age">20</option>
								  <option value="age">21</option>
								  <option value="age">22</option>
								  <option value="age">23</option>
								  <option value="age">24</option>
								  <option value="age">25</option>
								  <option value="age">26</option>
								  <option value="age">27</option>
								  <option value="age">28</option>
								  <option value="age">29</option>
								  <option value="age">30</option>
								  
								</select>
								
								<label for="exampleFormControlInput1">Appointment Date</label>
								<input class="form-control picker-input input-bboking" type="date" id="booking_date">
								
								<label for="exampleFormControlInput1">Appointment Time</label>
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder=" Your Name">
								
								</div>
							
							<div class="col-md-12">
								
								
								<div class="actionloginbs">
								<button type="submit" class="btn btn-primary">Add</button>
								</div>
								
						</div>
						<div class="col-md-12">
                                                      <option value="age">18</option>

						</div>
								
													
							</div>
						</form>
						</div>
						
					</div>
								
								</div>
				
				        </div>
						
						<div id="addoct" class="tabcontent" style="display:none;">
						<div class="bookbtn doctplus">
							<button class="posttablinks btn btn-primary active" data-toggle="modal" data-target="#myModal5" id="myModel5">
							<i class="fa fa-plus-square"></i> 
							</button>
						</div>



					<div>


                     <div class="availsearch">
					 <i class="fa fa-search opensrds"></i>
					  <input type="text" name="docname" id="doc_id" placeholder="Search Doctors" class="opennoe">
					  <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					  
					  </div>
	
						<div class="hading_by_title">
							<h4>Available Doctors</h4>
						</div>
<div id="docdiv">
	
</div>
 </div>	
						<div class="table-responsive">
						<!--<table class="table table-striped">
								<thead>
								  <tr>
									<th>S.No</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Phone Number</th>
									<th>Speciality</th>
								  </tr>
								</thead>
								<tbody>


     <?php $sn=1;?>
      @foreach($doctordata as $doctor)
								  <tr>
									<td>{{ $sn}}</td>
									<td>{{ $doctor->fullname }}</td>
									<td>{{ $doctor->email }}</td>
									<td>{{ $doctor->phone }}</td>
									<td>{{ $doctor->name }}</td>
								  </tr>
								  <?php $sn++; ?>
								@endforeach
								
								 
								</tbody>
						</table>-->
						</div>
						
						</div>
						
								
								
						</div>		
						</div>
						
						
				
				</div>
			</div>
		</div>
	</div>
	
	
	 <div class="addpatientform">
		  <div class="modal fade" id="myModal5"  style="display: none;">
			<div class="modal-dialog">
			
			  <!-- Modal content-->
			  <div class="modal-content">
				
				<div class="formslogins">	

						<div class="main-hd">
							<h1>Add Doctors</h1>
						</div>		
                    <button type="button" class="close" data-dismiss="modal">&times;</button>						
					<div class="regisindivs">

						<form>


   
                            <div class="form-group">
                            <div id="resultdoc"></div>
                                <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Fullname Name</label>
                                <input type="text" class="form-control" id="fullname" placeholder="Full Name" name="fullname" required="">

                                
								<span class="formerror" id="fnameerror" style="display: none;"></span>
								</div>
								
								 <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Email ID</label>
                                <input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" required="">


                                <span class="formerror"  id="emailerror" style="display: none;"></span>
                                
                                </div>
								
								 <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Phone Number</label>
                                <input type="number" class="form-control" id="phone" placeholder="Phone Number" name="phone" required="">

                                <span class="formerror" id="phoneerror" style="display: none;"></span>
                                  
								</div>


                                
								
								 <div class="col-md-6 col-sm-6">
                               <label for="exampleFormControlInput1">City</label>
                                <select placeholder="name@example.com" name="city_id" id="city_id" required="">


                                                                   
                                                                                              

                                                             <?php foreach ($citydata as $value) { ?>	
                                                             <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                                                                <?php } ?>           
                                                                                                    
                                                                    
                                 
                                                                  </select>

                            
                                </div>

                                <span class="formerror"></span> 


                                 <div class="col-md-6 col-sm-6">
                                <label for="inputPassword">Password</label>
								<div class="showornotpas">
                                <input type="password" class="form-control" id="password" placeholder="Password" name="password" required="">
	<div class="paswdshowsck">
        <input type="checkbox" class="oppwas" onclick="showpasswordFunction()"><i class="fa fa-eye"></i>
								</div>
                                </div>

								<span class="formerror" id="passerror" style="display: none;"></span>

								</div>
								
								 <div class="col-md-6 col-sm-6">
                                <label for="exampleFormControlInput1">Speciality</label>
                                <select placeholder="name@example.com" name="speciality_id" id="speciality_id" required="">
                                                                   
                                 
                                                                  @foreach($specialtydata as $specialty)                                   

                                                             <option value="{{ $specialty->id}}">{{ $specialty->name}}</option>
                                                                     @endforeach                       
                                                           
                                    
                                                            
                                                                                                                                     </select>
								
                                 <input type="hidden" class="form-control" id="the_other_speciality" placeholder="enter your speciality" name="the_other_speciality">
                                 <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />


                                 
                                </div>
                                 <span class="formerror"></span>

								
								 <div class="col-md-12 col-sm-12">
                                <div class="actionloginbs">
                                <button type="button" class="btn btn-primary" id="adddoc">Add Doctor</button>
                                </div>                    
                            </div>
							</div>
                        </form>
					</div>
				</div>
				</div>
			</div>
		  </div>
	 </div>	

  <!-- Modal -->
  <div class="changemodepaswd">
  <div class="modal fade" id="myModal"  style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="formslogins">
					   <div class="main-hd">
							<h1>Change Password</h1>
						</div>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<div class="regisindivs">
						
						<form>
						
						<div id="result"></div>
							<div class="form-group">
							 
								
				<input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Old Password." >	

					<span class="formerror" id="oldpassworderror" style="display: none;"></span>	
					
				<input type="password" class="form-control" id="passwordnew" name="password" placeholder="New Password" >
				<span class="formerror" id="passworderror" style="display: none;"></span>	

				<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm New Password" >
								
                 <span class="formerror" id="password_confirmationerror" style="display: none;"></span>

		<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
								
								<div class="actionloginbs">
								<button type="button"  id="myBtn" class="btn btn-primary">Save pass</button>
								</div>
															
							</div>
						</form>
						</div>
						
					</div>
					
      </div>
      
    </div>
  </div>
  </div>

  	<div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p>Doctor Added Successfully!!</p>
		   <button class="close allokbtns datadis12" >Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>

	<div class="deletesucchealthapp">
  <div class="modal fade" id="myModalchangepass" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p>Change Password Successfully!!</p>
		   <button class="close allokbtns datadis12" >Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>


  	<div class="deletesucchealthapp">
  <div class="modal fade" id="myModalpatient" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p>Appointment Added Successfully!!</p>
		   <button class="close allokbtns datadis12" >Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>

   <div class="addpatientform"> 
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="formslogins">
				 
						<div class="main-hd">
							<h1>Add Patient</h1>
						</div>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<div class="regisindivs">
						
						

							<div class="form-group">
							<div id="resultappointment">
							</div>
							
								<div class="col-md-6">
							
							
								<label for="exampleFormControlInput1">First Name</label>
								<input type="text" name="firstname" id="firstname" class="form-control"  placeholder="Patient Name..">

								<span class="formerror" id="firstnameerror" style="display: none;"></span>
								
								<label for="exampleFormControlInput1">Last Name</label>
								<input type="text" name="lastname" id="lastname" class="form-control"  placeholder="Last Name..">
								<span class="formerror" id="lastnameerror" style="display: none;"></span>
								
								<label for="exampleFormControlInput1">Email ID</label>
								<input type="email" name="email" id="email1" class="form-control"  placeholder="name@example.com">
								<span class="formerror" id="email1error" style="display: none;"></span>
								
								<label for="exampleFormControlInput1">Phone Number</label>
								<input type="number" name="phone" id="phone1" class="form-control" placeholder="phone Number..">
								<span class="formerror" id="phone1error" style="display: none;"></span>
														
								</div>
								
								<div class="col-md-6">
								
								
								<label for="exampleFormControlInput1">Gender</label>
								<select name="gender" id="gender">
								  <option value="male">Male</option>
								  <option value="female">Female</option>
								</select>
								
								<label for="exampleFormControlInput1">Age</label>
								<select id="age" name="age">
								 <?php

								 	for ($i=18; $i < 100 ; $i++) { 
								 		
								 ?>
								  <option value="{{$i}}">{{$i}}</option>
								  <?php
								  	}
								  ?>
								  
								</select>
								
								<label for="exampleFormControlInput1">Appointment Date</label>
	<input class="form-control picker-input input-bboking" type="date" name="apdate" id="apdate">

	<span class="formerror" id="apdateerror" style="display: none;"></span>
								
								<label for="exampleFormControlInput1">Appointment Time</label>
  <input type="text" name="aptime" class="form-control" id="aptime" placeholder=" Appointment time">

  <span class="formerror" id="aptimeerror" style="display: none;"></span>
								
								</div>
								 <div class="col-md-12 col-sm-12">
                               <label for="exampleFormControlInput1">Doctor</label>
                                <select  name="doctor_id" id="doctor_id" required="">


                                                                   
                                                                                              

                                                             <?php foreach ($doctordata as $doctor) { ?>	
                                                             <option value="<?php echo $doctor->id;?>"><?php echo $doctor->fullname;?></option>
                                                                <?php } ?>           
                                                                                                    
                                                                    
                                 
                                                                  </select>

                            
                                </div>
					<input type="hidden" name="_token" id="csrf-token1" value="{{ Session::token() }}" />			
							
							<div class="col-md-12">
								
								
								<div class="actionloginbs">
			<button type="button" id="addappointment" class="btn btn-primary">Add Patient</button>
								</div>
								
						</div>
						
								
													
							</div>
						
						</div>
						
					
					</div>
      </div>
      
    </div>

  </div>


   </div> 


   
  
    	<!-- More Page Tabs Ups ENDS Here* -->

      
  
  
 <script src="{{asset('public/assets/js/custome.js')}}"></script>
    <!--main custome js-->
   
       
</body>

</html>


<!-- <script>
$(document).ready(function(e){
    $('#sForm').on('submit',function(){
       // alert( $('#search').val());
     var  pass  =   $('#pass1').val();
     var  confpass = $('#pass2').val();
      // alert( pass);
     // if(pass!=confpass){
      //alert('new password  and conform password does not match');
      
     // location.replace('dashboard');
      }
    });
});
</script> -->


<script>

    $("#myBtn").click(function(){
      
        
   	var oldpassword 			= $("#oldpassword").val();
	var password 				= $("#passwordnew").val();
	var password_confirmation 	        = $("#password_confirmation").val();
	var _token 			        = $("#csrf-token").val();
   
//alert(password);
$.post("{{url('/')}}/changePassword",{oldpassword:oldpassword,password:password,password_confirmation:password_confirmation,_token:_token},function(result){
  
		    var data = JSON.parse(result);


		    if(data[0] == 1){
		    	
		    	$('#myModal').modal('hide'); 
		    	$('#myModalchangepass').modal('show'); 
		    	//$('#myModal').modal('show');
		    } else {

		    	if((data[1]=='Please enter Oldpassword')||(data[1]=='Old Password is wrong.')){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#oldpassworderror").show();
		    	  $("#oldpassworderror").text(data[1]);
		         }
		         if((data[1]=='Please enter Newpassword')||(data[1]=='New Password must be at least 6 and at most 15 characters')){

		         	
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#oldpassworderror").hide();
		    	  $("#passworderror").show();
		    	  $("#passworderror").text(data[1]);
		         }
                 if((data[1]=='Please enter Confirm Newpassword')||(data[1]=='Confirm Password must be at least 6 and at most 15 characters')){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#oldpassworderror").hide();
		    	  $("#passworderror").hide();
		    	  $("#password_confirmationerror").show();
		    	  $("#password_confirmationerror").text(data[1]);
		         }

		         if(data[1]=='Old Password and confirm password is not same pls fill same.'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#oldpassworderror").hide();
		    	  $("#passworderror").hide();
		    	  $("#password_confirmationerror").show();
		    	  $("#password_confirmationerror").text(data[1]);
		         }

		         if(data[1]=='Old Password is wrong.'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	   $("#password_confirmationerror").hide();
		    	  $("#passworderror").hide();
		    	  $("#oldpassworderror").show();
		    	  $("#oldpassworderror").text(data[1]);
		         }
  
		    	//$("#result").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    }
			//alert(data);
	});
   });
   
   $(".datadis12").click(function(){
		location.reload();
	});
</script>



<script>

    $("#adddoc").click(function(){
      
        
   	var fullname 			    = $("#fullname").val();
	var email 		            = $("#email").val();
	var password 			    = $("#password").val();
	var phone 		            = $("#phone").val();
	var city_id 			    = $("#city_id").val();
	var speciality_id 	            = $("#speciality_id").val();
	var _token 			    = $("#csrf-token1").val();
   

$.post("{{url('/')}}/addDoctor",{fullname:fullname,
	                                    email:email,
	                              password:password,
	                                    phone:phone,
	                                city_id:city_id,
	                    speciality_id:speciality_id,
	                                    _token:_token},function(result){
  
		    var data = JSON.parse(result);


		    if(data[0] == 1){
		    	
		    	$('#myModal5').modal('hide'); 
		    	$('#myModal4').modal('show'); 
		    	//$('#myModal').modal('show');
		    } else {

		    	if(data[1]=='Please enter Fullname'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#fnameerror").show();
		    	  $("#fnameerror").text(data[1]);
		         }
		         if((data[1]=='Please enter Email ID')||(data[1]=='The email has already been taken.')){
		          $("#fnameerror").hide();
		          $("#emailerror").show();
                  $("#emailerror").text(data[1]);
		         }
		         if(data[1]=='Please enter Phone Number'){
		          $("#fnameerror").hide();
		          $("#emailerror").hide();
		          $("#phoneerror").show();
                  $("#phoneerror").text(data[1]);
		         }
		        if((data[1]=='Please enter Password') || (data[1]=='Password must be at least 6 and at most 15 characters')){
		          $("#fnameerror").hide();
		          $("#emailerror").hide();
		          $("#phoneerror").hide();
		          $("#passerror").show();
                  $("#passerror").text(data[1]);
		         }

		    }
			//alert(data);
	});
   });
   
   $(".datadis12").click(function(){
		location.reload();
	});
</script>

<script>

    $("#addappointment").click(function(){

      //alert();
        
   	var firstname 			    = $("#firstname").val();
	var lastname 		            = $("#lastname").val();
	var email 			    = $("#email1").val();
	var phone 		            = $("#phone1").val();
        var gender 		            = $("#gender").val();
        var age 		            = $("#age").val();
        var apdate 		            = $("#apdate").val();
        var aptime 		            = $("#aptime").val();
	var doctor_id 			    = $("#doctor_id").val();
	var _token 			    = $("#csrf-token").val();


 $.post("{{url('/')}}/addPatient",{firstname:firstname,lastname:lastname,email:email,phone:phone,gender:gender,age:age,
                                                 apdate:apdate,aptime:aptime,
                                                 doctor_id:doctor_id,
	                                        _token:_token},function(result){
  
		    var data = JSON.parse(result);


		    if(data[0] == 1){
		    	
		    	$('#myModal2').modal('hide'); 
		    	$('#myModalpatient').modal('show'); 
		    	//$('#myModal').modal('show');
		    } else {

		    	if(data[1]=='Please enter Firstname'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").show();
		    	  $("#firstnameerror").text(data[1]);
		         }
		         if(data[1]=='Please enter lastname'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").hide();
		    	  $("#lastnameerror").show();
		    	  $("#lastnameerror").text(data[1]);
		         }
		          if(data[1]=='Please enter Email ID'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").hide();
		    	  $("#lastnameerror").hide();
		    	  $("#email1error").show();
		    	  $("#email1error").text(data[1]);
		         }

		         if(data[1]=='Please enter Phone Number'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").hide();
		    	  $("#lastnameerror").hide();
		    	  $("#email1error").hide();
		    	  $("#phone1error").show();
		    	  $("#phone1error").text(data[1]);
		         }
		         if(data[1]=='Please enter Appointment date'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").hide();
		    	  $("#lastnameerror").hide();
		    	  $("#email1error").hide();
		    	  $("#phone1error").hide();
		    	  $("#apdateerror").show();
		    	  $("#apdateerror").text(data[1]);
		         }
		         if(data[1]=='Please enter Appointment time'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").hide();
		    	  $("#lastnameerror").hide();
		    	  $("#email1error").hide();
		    	  $("#phone1error").hide();
		    	   $("#apdateerror").hide();
		    	  $("#aptimeerror").show();
		    	  $("#aptimeerror").text(data[1]);
		         }
		          if(data[1]=='Email not found.'){
		    	//$("#resultdoc").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    	  $("#firstnameerror").hide();
		    	  $("#lastnameerror").hide();
		    	  $("#email1error").hide();
		    	  $("#phone1error").hide();
		    	   $("#apdateerror").hide();
		    	  $("#aptimeerror").show();
		    	  $("#aptimeerror").text("Please book only your Registered Patient");
		         }




		    	//$("#resultappointment").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    }
			//alert(data);
	});
   });
   
   $(".datadis12").click(function(){
		location.reload();
	});
</script>

<script>
    function showpasswordFunction(){
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>

<script>
$(document).ready(function(){
//$("#doc_id").keyup(function(){
   var search = $('#doc_id').val();
   var _token = $("#csrf-token").val();

//alert(search);
$.post("{{url('/')}}/searchdoctor",{search:search,_token:_token},function(result){
  
		    //var data = JSON.parse(result);
                   // var result= html('#docdiv');
                      $('#docdiv').html(result);


	});

//});
});


$("#doc_id").keyup(function(){
   var search = $('#doc_id').val();
   var _token = $("#csrf-token").val();

//alert(search);
$.post("{{url('/')}}/searchdoctor",{search:search,_token:_token},function(result){
  
		    //var data = JSON.parse(result);
                   // var result= html('#docdiv');
                      $('#docdiv').html(result);


	});

});
</script>

<script>
			function readURL(input) {
					if (input.files && input.files[0]) {
					var reader = new FileReader();
										            
					reader.onload = function (e) {
							$('#profile-img-tag').attr('src', e.target.result);
								  }
							  reader.readAsDataURL(input.files[0]);
							 }
						 }
				$("#profile-img").change(function(){
						   readURL(this);
					 });
	 </script>

	 <script type="text/javascript">
  $("#insurance_accept1").keypress(function (evt) {
  
  var keycode = evt.charCode || evt.keyCode;
   // alert(keycode);
  if (keycode  == 44) { //Enter key's keycode
    return false;
  }
});
</script>
