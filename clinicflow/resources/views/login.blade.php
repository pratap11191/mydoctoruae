<!DOCTYPE html>
<html lang="en">
<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" href="{{asset('public/assets/images/fav.png')}}" type="image/png" sizes="16x16">
    
    <!--main style sheet-->
    <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	
 
</head>
<body>



    <!--############## model  ################ -->
     <div class="changemodepaswd">
  <div class="modal fade" id="myModal"  style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="formslogins">
					   
						
						<div class="regisindivs">
						<div class="main-hd">
							<h1>Forget Password</h1>
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<form>
						
						<div id="result"></div>
							<div class="form-group">
							 
								
				<input type="email" class="form-control" id="email" name="email" placeholder="Enter Registered Email" >			
					
				
								

		<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
								
								<div class="actionloginbs">
								<button type="button"  id="myBtn" class="btn btn-primary">Submit</button>
								</div>
															
							</div>
						</form>
						</div>
						
					</div>
      </div>
      
    </div>
  </div>
  </div>




  	<div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p> Password Sent successfully on your registered mail!!</p>
		   <button class="close allokbtns datadis12" >Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>
    <!--############## model  ################ -->
   
    
	<!-- More Page Tabs Ups * -->
	
	<div class="loginclinetsbg">
        <div class="logoindivs">
			<div class="morepagessection">
				
                  <div class="formslogins">
					    
							<div class="main-hd">
								<h1>Clinic Login</h1>
							</div>
							
							<div class="mainlgo">
							<a href="https://mydoctoruae.com/"><img src ="{{asset('public/assets/images/logo.png')}}"></a>
							</div>
<span class="formerror">
{{ Session::get('error')}}
  </span>
							
							<form action="{{url('/')}}/login" method="post">
								<div class="form-group">
									<label for="exampleFormControlInput1">Email ID</label>
									<input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Your Email ID">
									<span class="formerror"><?php
                                	if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                		echo $messages['email']['0'];
                                	}
                                ?></span>
									<label for="inputPassword">Password</label>
									<div class="showornotpas">
									<input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
										<div class="paswdshowsck">
											<input type="checkbox" class="oppwas" onclick="showpasswordFunction()"><i class="fa fa-eye"></i>
										</div>
									</div>
									<span class="formerror"><?php
                                	if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                                		echo $messages['password']['0'];
                                	}
                                ?></span>
									<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

	<div class="forgot-password-wrapper" onclick="openmodel();"><a href="#" ><i>Forgot Password?</i></a></div>


									

									<div class="actionloginbs">
									<button type="submit" class="btn btn-primary">Log In</button>
									</div>
									<div class="center section"><small class="fs13">New to My Doctor UAE?<a href="{{url('/')}}/signup"> Sign Up</a></small></div>						
								</div>
							</form>
						
					</div>
				
			</div>
		</div>
	</div>
	<?php echo View::make('include/footer'); ?>
  
    	<!-- More Page Tabs Ups ENDS Here* -->
		
		
   

    <!--<script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/bootstrap.min.js"></script>-->

    <script src="{{asset('public/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap/bootstrap.min.js')}}"></script>
    
    <!--main custome js-->
    <!--<script src="assets/js/custome.js"></script>-->
<script src="{{asset('public/assets/js/custome.js')}}"></script>
    
</body>
</html>
<script>

  function openmodel(){
	$("#myModal").modal()
	//$('#myModal4').modal('show'); 
}
</script>

<script>

    $("#myBtn").click(function(){
      
       	var email 			= $("#email").val();
	var _token 			= $("#csrf-token").val();
   
//alert(password);
$.post("{{url('/')}}/forgetPassword",{email:email,_token:_token},function(result){
  
		    var data = JSON.parse(result);


		    if(data[0] == 1){
		    	
		    	$('#myModal').modal('hide'); 
		    	$('#myModal4').modal('show'); 
		    	//$('#myModal').modal('show');
		    } else {
		    	$("#result").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    }
			//alert(data);
	});
   });
   
   $(".datadis12").click(function(){
		location.reload();
	}); 
</script>