<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <p>
                    <a href="https://mydoctoruae.com" title="Findoctor">
                        <img src="{{asset('public/assets/images/logo.png')}}"  alt="" class="img-responsive img-fluid">
                    </a>
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-2">
                <div class="footer-col">
                    <h5>About</h5>
                    <ul class="links">
                        <li><a href="https://mydoctoruae.com/aboutus">About us</a></li>
                        <!-- <li><a href="{{url('/newsfeedlist')}}">Newsfeed</a></li> -->
                        <li><a href="https://mydoctoruae.com/newsfeedlist">Newsfeed</a></li>
                        <li><a href="#0">FAQ</a></li>
                        <li><a href="{{url('/login')}}">Login</a></li>
                        <li><a href="{{url('/signup')}}">Register</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="footer-col">
                    <h5>Useful links</h5>
                    <ul class="links">
                        <li><a href="https://mydoctoruae.com/listing">Doctors</a></li>
                        <li><a href="https://mydoctoruae.com/clinicflow">Clinics</a></li>
                        <li><a href="https://mydoctoruae.com/topissuelist">Specialization</a></li>
                        <li><a href="https://mydoctoruae.com/doctorsignup">Join as a Doctor</a></li>
                        <li><a href="#0">Download App</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="footer-col">
                    <h5>Contact with Us</h5>
                    <ul class="contacts">
                        <li><a href="tel:61280932400"><i class="fa fa-mobile" aria-hidden="true"></i> + 61 23 8093 3400</a></li>
                        <li><a href="mailto:info@findoctor.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> help@findoctor.com</a></li>
                    </ul>
                    <div class="follow_us">
                        <h6>Follow us</h6>
                        <ul class="foolow-ull">
                            <li><a href="#0"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 last-footer">
                <div class="container">
					<div class="row">
						<div class="col-md-8 col-sm-8">
							<ul id="additional_links">
								<li><a href="#0">Terms and conditions</a></li>
								<li><a href="#0">Privacy</a></li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-4">
							<div id="copy">© 2018 My Doctor</div>
						</div>
					</div>
				</div>
		</div>
    </div>
	
	