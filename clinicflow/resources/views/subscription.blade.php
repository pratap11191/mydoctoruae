<html lang="en">
<head>
  <title> Clinic |  My Doctor</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <link rel="icon" href="{{asset('public/assets/images/fav.png')}}" type="image/png" sizes="16x16">
    
    <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{asset('public/assets/bootstrap/bootstrap.min.js')}}"></script>
</head>
<body>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-feedss">
        <div class="container">
		<div class="nfdc">
            <div class="hading_by_title">
                <h4>Subscription Plans</h4>
            </div>
            <?php

           // dd();
            if(!empty($listdetails->toArray())){

              if(isset($_GET['Subscription']) && !empty($_GET['Subscription'])){

                  $sublistunique_array = $listdetails->toArray();

                  $value = $sublistunique_array[$_GET['Subscription']-1];

               ?>

                           <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="your-posts-section">
                    <div class="your-subsc-pic-para">
                        <a href="" class="newsfeed_a_all">
                            <div class="subs-image">
                                <img src="https://mydoctoruae.com/{{$value['image']}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value['title'])}}</h4>
                <span class="subsprice">AED {{ucfirst($value['price'])}}</span>
                                <p>{{ucfirst($value['description'])}}</p>
                            </div>
                        </a>  
                    </div>
                    <?php

                    if($value['flag_plan'] == '-1')
                        { ?>
  <div class="subscriptionbtn">
                        <!-- <a href="{{url('/')}}/plan_map/{{$value['id']}}" class="btn btn-primary">Buy</a> -->
                         
                        
                           <link rel="stylesheet" href="https://www.paytabs.com/theme/express_checkout/css/express.css">
                            <script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
                            <script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                            <!-- Button Code for PayTabs Express Checkout -->
                            <div class="PT_express_checkout"></div>
                            <script type="text/javascript">
                             Paytabs("#express_checkout").expresscheckout({
                             settings:{
                             merchant_id: "10035457",
                             secret_key: "YCuZvQ4ujqNDftMgYwLDRimsR2mHFpYD1tDwjH1C7Pgnj2DhzoUqGhTykffZEeDGHgd0XeD54UsnX6EwvzWcRQKb50aKeNrW3EW1",
                             amount : "<?php echo $value['price']; ?>",
                             currency : "AED",
                             title : "<?php echo Request::session()->get('user_session', 'default')->email; ?>",
                             product_names: "{{ucfirst($value['id'])}}",
                             order_id: "{{ucfirst($value['id'])}}",
                             url_redirect: "https://mydoctoruae.com/testai1.php",
                             display_customer_info:1,
                             display_billing_fields:1,
                             display_shipping_fields:0,
                             language: "en",
                             redirect_on_reject: 0,
                             },
                              customer_info:{
                                first_name: "<?php echo Request::session()->get('user_session', 'default')->fullname; ?>",
                                phone_number: "<?php echo Request::session()->get('user_session', 'default')->phone; ?>",
                                country_code: "973",
                                email_address: "<?php echo Request::session()->get('user_session', 'default')->email; ?>"            
                            },
                             });
                            </script>
                    </div>
                    <?php

                    } else if($value['flag_plan'] == '0'){ ?>
  <div class="subscriptionbtn">
                       <!--  <button type="submit" class="btn btn-primary flag_planpp">Buy</button> -->
                    </div>

                    <?php

                    } else if($value['flag_plan'] == '1'){

                      ?>
                      <p style="color: red;">Your Subscription Expire Date is {{$value['exp_date']}}</p>
  <div class="subscriptionbtn">
                        <button type="submit" class="btn btn-primary">Purchased</button>
                    </div>

                    <?php

                    }
                    ?>
                  
                </div>
            </div>

              <?php

              } else {

                $ityu = 1;

                foreach ($listdetails->toArray() as  $value) {
                   
                   
            ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="your-posts-section">
                    <div class="your-subsc-pic-para">
                        <a href="?Subscription={{$ityu}}" class="newsfeed_a_all">
                            <div class="subs-image">
                                <img src="https://mydoctoruae.com/{{$value['image']}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value['title'])}}</h4>
								<span class="subsprice">AED {{ucfirst($value['price'])}}</span>
                                <p>{{ucfirst($value['description'])}}</p>
                            </div>
                        </a>  
                    </div>
                   
                  
                </div>
            </div>
           <?php 

           $ityu++;

            } }

       } else {
            echo "No Record Found!!";
       }

       ?>
			</div>
        </div>
    </div>

</body>

</html>
