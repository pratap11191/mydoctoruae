<html lang="en"><head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" href="https://mydoctoruae.com/clinicflow/public/assets/images/fav.png" type="image/png" sizes="16x16">
    
    <!--main style sheet-->
    <link rel="stylesheet" href="https://mydoctoruae.com/clinicflow/public/assets/css/style.css" type="text/css">
	
 
</head>
<body>

<style>
.thansknotesclinic {
    float: left;
    width: 100%;
    margin: 32px 0;
}

.loginclinetsbg .logoindivs{
height:60%;
}
</style>


    <!--############## model  ################ -->
     <div class="changemodepaswd">
  <div class="modal fade" id="myModal" style="display: none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="formslogins">
					   
						
						<div class="regisindivs">
						<div class="main-hd">
							<h1>Forget Password</h1>
							          <button type="button" class="close" data-dismiss="modal">×</button>
						</div>
						<form>
						
						<div id="result"></div>
							<div class="form-group">
							 
								
				<input type="email" class="form-control" id="email" name="email" placeholder="Enter Registered Email">			
					
				
								

		<input type="hidden" name="_token" id="csrf-token" value="keZwvzgIqzWoDvIJPvlqQt5vgd7GjdU3lB60bq03">
								
								<div class="actionloginbs">
								<button type="button" id="myBtn" class="btn btn-primary">Submit</button>
								</div>
															
							</div>
						</form>
						</div>
						
					</div>
      </div>
      
    </div>
  </div>
  </div>




  	<div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p> Password Sent successfully on your registered mail!!</p>
		   <button class="close allokbtns datadis12">Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>
    <!--############## model  ################ -->
   
    
	<!-- More Page Tabs Ups * -->
	
	<div class="loginclinetsbg">
        <div class="logoindivs">
			<div class="morepagessection">
				
                  <div class="formslogins">
					    
							<div class="main-hd">
								<h1>Waiting For Admin Approval</h1>
							</div>
							
							<div class="mainlgo">
							<a href="https://mydoctoruae.com/"><img src="https://mydoctoruae.com/clinicflow/public/assets/images/logo.png"></a>
							</div>
<span class="formerror">

  </span>
							
							<div class="thansknotesclinic">
							<h1>Thanks for signing up with Us.</h1>
							<p>You will be able to access application features once We approve your profile.</p>
							<p>You will receive an E-mail shortly.</p>
							</div>
							
							<form action="https://mydoctoruae.com/clinicflow/login" method="post">
								<div class="form-group">
									<div class="center section"><small class="fs13">Already Have an account?<a href="https://mydoctoruae.com/clinicflow/"> Login</a></small></div>						
								</div>
							</form>
						
					</div>
				
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <p>
                    <a href="https://mydoctoruae.com" title="Findoctor">
                        <img src="https://mydoctoruae.com/clinicflow/public/assets/images/logo.png" alt="" class="img-responsive img-fluid">
                    </a>
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-2">
                <div class="footer-col">
                    <h5>About</h5>
                    <ul class="links">
                        <li><a href="https://mydoctoruae.com/aboutus">About us</a></li>
                        <!-- <li><a href="https://mydoctoruae.com/clinicflow/newsfeedlist">Newsfeed</a></li> -->
                        <li><a href="https://mydoctoruae.com/newsfeedlist">Newsfeed</a></li>
                        <li><a href="#0">FAQ</a></li>
                        <li><a href="https://mydoctoruae.com/clinicflow/login">Login</a></li>
                        <li><a href="https://mydoctoruae.com/clinicflow/signup">Register</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="footer-col">
                    <h5>Useful links</h5>
                    <ul class="links">
                        <li><a href="https://mydoctoruae.com/listing">Doctors</a></li>
                        <li><a href="https://mydoctoruae.com/clinicflow">Clinics</a></li>
                        <li><a href="https://mydoctoruae.com/topissuelist">Specialization</a></li>
                        <li><a href="https://mydoctoruae.com/doctorsignup">Join as a Doctor</a></li>
                        <li><a href="#0">Download App</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="footer-col">
                    <h5>Contact with Us</h5>
                    <ul class="contacts">
                        <li><a href="tel:61280932400"><i class="fa fa-mobile" aria-hidden="true"></i> + 61 23 8093 3400</a></li>
                        <li><a href="mailto:info@findoctor.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> help@findoctor.com</a></li>
                    </ul>
                    <div class="follow_us">
                        <h6>Follow us</h6>
                        <ul class="foolow-ull">
                            <li><a href="#0"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 last-footer">
                <div class="container">
					<div class="row">
						<div class="col-md-8 col-sm-8">
							<ul id="additional_links">
								<li><a href="#0">Terms and conditions</a></li>
								<li><a href="#0">Privacy</a></li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-4">
							<div id="copy">© 2018 My Doctor</div>
						</div>
					</div>
				</div>
		</div>
    </div>
	
	  
    	<!-- More Page Tabs Ups ENDS Here* -->
		
		
   

    <!--<script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/bootstrap.min.js"></script>-->

    <script src="https://mydoctoruae.com/clinicflow/public/assets/js/jquery.min.js"></script>
    <script src="https://mydoctoruae.com/clinicflow/public/assets/bootstrap/bootstrap.min.js"></script>
    
    <!--main custome js-->
    <!--<script src="assets/js/custome.js"></script>-->
<script src="https://mydoctoruae.com/clinicflow/public/assets/js/custome.js"></script>
    


<script>

  function openmodel(){
	$("#myModal").modal()
	//$('#myModal4').modal('show'); 
}
</script>

<script>

    $("#myBtn").click(function(){
      
       	var email 			= $("#email").val();
	var _token 			= $("#csrf-token").val();
   
//alert(password);
$.post("https://mydoctoruae.com/clinicflow/forgetPassword",{email:email,_token:_token},function(result){
  
		    var data = JSON.parse(result);


		    if(data[0] == 1){
		    	
		    	$('#myModal').modal('hide'); 
		    	$('#myModal4').modal('show'); 
		    	//$('#myModal').modal('show');
		    } else {
		    	$("#result").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    }
			//alert(data);
	});
   });
   
   $(".datadis12").click(function(){
		location.reload();
	}); 
</script><div id="shadowMeasureIt"></div><div id="divCoordMeasureIt"></div><div id="divRectangleMeasureIt"><div id="divRectangleBGMeasureIt"></div></div></body></html>