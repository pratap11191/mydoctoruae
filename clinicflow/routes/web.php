<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
/*Route::any('thanks', function () {
    return view('thanks');
});*/

Route::post('/autocomplete/fetch', 'API\HomeAPIController@fetch')->name('autocomplete.fetch');




Route::any('/','HomeController@login');
Route::any('login','HomeController@login');

Route::any('subscription','HomeController@subscription');
Route::any('signup',        'HomeController@signUp');
Route::any('dashboard',     'HomeController@dashboard');
Route::any('logout',        'HomeController@logout');
Route::any('update',        'HomeController@update');
Route::any('changePassword','HomeController@changePassword');
Route::any('addDoctor','HomeController@addDoctor');
Route::any('searchdoctor','HomeController@searchdoctor');

Route::any('forgetPassword','HomeController@ForgetPassword');

Route::get('rem/{id}','HomeController@removeins');


route::any("addPatient","HomeController@addPatient");