<?php


$data = array("autoCapTime"=> "24",
    			"autoCapture"=> "Y",
    			"email"=> $_POST['email'],
    			"value"=>$_POST['value'],
    			"currency"=> $_POST['currency'],
    			"trackId"=> $_POST['trackId'],
    			"cardToken"=> $_POST['cardToken']);                                                                    
$data_string = json_encode($data);                                                                                   
                                                                                                                     
$ch = curl_init('https://sandbox.checkout.com/api2/v2/charges/token');                                                                      
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Authorization: sk_test_2e4e308d-a5ab-4b8d-bb54-6610bc7f5629',                                                                          
    'Content-Type: application/json;charset=UTF-8',                                                                                
    'Content-Length: ' . strlen($data_string))                                                                       
);                                                                                                                   
                                                                                                                     
$result = curl_exec($ch);


curl_close($ch);

echo $result;
?>

