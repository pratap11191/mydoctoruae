<?php

namespace App\Http\Controllers\Admin;

use App\AdvertisementCount;
use App\Http\Controllers\Controller;
use App\RestaurantList;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Mail;
use Redirect;
use Session;
use View;
use Crypt;
use App\Advertisement;
use App\Doctor;

class AdminController extends Controller
{

    public function index(Request $request)
    {

    }
    
    public function adupload()
    {
    
        return view('admins.ad_genre');
    
    }
    
     public function uploadTopic(Request $request)
    {
        $input = $request->all();
        
        Topic::create($input);
        
        return redirect('admin/genre_list');
    }

    public function login(Request $request)
    {
        if ((Auth::user())) {
            return redirect()->intended(route('admins.user_list'));
        } else {
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'email'    => 'required|email|max:190',
                    'password' => 'required|max:190',
                ]);

                if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => '1'], $request->remember)) {
                    return redirect('admin/user_list');
                } else {
                    $request->session()->flash('alert-danger', 'Wrong Credentials ');
                    return redirect('/admin/login');
                }
            }
            return view('admins.login');
        }

    }

    public function changeStatus($id)
    {

        $user = User::find($id);

        if($user->admin_status == "0"){
            $user->admin_status = "1";
        } else {
            $user->admin_status = "0";
        }

        $user->save();

        return back();
    }

    public function changeStatusad($id)
    {

        $ad = Advertisement::find($id);

        if($ad->status == "0"){
            $ad->status = "1";
        } else {
            $ad->status = "0";
        }

        $ad->save();

        return back();
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/admin/login');
    }

    public function user_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj   = new CommonAdminController;
            $users = User::where('is_admin', '0')->orderBy('id', 'DESC')->get();
            foreach ($users as $user) {
                if(empty($user->image)){
                $user->image = url('/public/img/user-default.png');
                }
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            return view('admins.user_list', compact('users'));
        }
    }
    

      
    public function common_delete($id,$table)
    {
       
            DB::table($table)->where('id', $id)->delete();

            return back();
           
    }

    
    public function profile(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $admin = User::where("is_admin", "1")->first();
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'email'    => 'required|email|max:190',
                    'password' => 'required|max:190',
                    'fullname'     => 'required|max:190',
                    'image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                ]);
                $obj = new CommonAdminController;
                if (!empty($request->file('image'))) {
                    if (file_exists(public_path('profile' . '/' . $admin->image))) {
                        @unlink(public_path('profile' . '/' . $admin->image));
                    }
                    if (file_exists(public_path('profile_resize' . '/' . $admin->image))) {
                        @unlink(public_path('profile_resize' . '/' . $admin->image));
                    }
                    $image = $obj->upload_image($request);
                } else {
                    $image = $admin->pic_file;
                }

                $update = array(
                    'email'           => $request->email,
                    'password'        => Hash::make($request->password),
                    'confirmpassword' => $request->password,
                    'fullname'        => $request->name,
                    'image'        => $image,
                );
                if (User::where('id', $admin->id)->update($update)) {
                    $request->session()->flash('alert-success', 'Updated successfully.');
                    return redirect('/admin/profile');
                } else {
                    $request->session()->flash('alert-danger', 'Internal error .');
                }
            } else {
                return view('admins.profile', compact('admin'));
            }
        }
    }

    
    public function forgot_password(Request $request)
    {
        try {
            if (Auth::user()) {
                return redirect()->intended(route('admins.user_list'));
            } elseif ($request->isMethod('post')) {
                $this->validate($request, [
                    'email' => 'required|email',
                ]);
                $create = User::where('is_admin', '1')->where('email', $request->email)->first();
                if (!empty($create)) {
                    $password                = $create->confirmpassword; //mt_rand(10000, 99999);
                    $create->password        = Hash::make($password);
                    $create->confirmpassword = $password;
                    if ($create->save()) {
                        $email   = $create->email;
                        $subject = "New Password";
                        if (Mail::send('emails.forgot_password', ['newPassword' => $password, 'email' => $create->email], function ($message) use ($email, $subject) {
                            $message->to($email)->subject($subject);
                        })) {
                            $request->session()->flash('alert-success', 'Password send to your mail');
                            return redirect('/admin/login');
                        } else {
                            $request->session()->flash('alert-danger', 'Fail to send.');
                            return redirect('/admin/forgot_password');
                        }

                    } else {
                        $request->session()->flash('alert-success', 'Internal Server Error');
                    }
                } else {
                    $request->session()->flash('alert-danger', 'Not a valid user.');
                }
                return Redirect::back();
            } else {
                return view('admins.forgot_password');
            }

        } catch (Exception $e) {

        }
    }

    public function change_user_status(Request $request){
        $id = Crypt::decrypt($request->id);
    }

}
