<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\User;
use App\Token;
use Validator;
use Image;
use Intervention\Image\ImageServiceProvider;
use DB;
use Chatkit;
use App\City;
use App\Doctor;
use App\Otp;
use App\Dtoken;
use App\Post;
use App\Specialty;
use App\Like;
use App\Savepost;
use Carbon\Carbon;
use App\Comment;
use App\Doctorrating;
use App\Insurance;
use App\Reason;
use App\Cancellation;
use App\Booking;
use App\Notification;
use App\Room;
use App\Chat;
use Mail;
use App\Plan;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Crypt;
use App\Subscription;

class UserAPIController extends APIBaseController
{
    
    public function index()
    {
        
    }

    public function plan_map(Request $request)
    {
        $input = $request->all();


        $val_arr = [
            'doctor_id' => 'required',
            'sub_id'=>'required',
            'token'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Dtoken::where(['user_id'=>$input['doctor_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }


        $listdetails = Subscription::where('id',$input['sub_id'])->first();

        if(!empty($listdetails)){
            $input['exp_date'] = Carbon::now()->addDays($listdetails->validity);

            $checkdoctorinplan = DB::table('plans')->where('doctor_id',$input['doctor_id'])->first();

            if(!empty($checkdoctorinplan)){
                $checkdoctorinplan->sub_id = $input['sub_id'];
                $checkdoctorinplan->exp_date = $input['exp_date'];
                $checkdoctorinplan->save();
            } else {
                Plan::create($input);
            }

            return $this->sendResponse1(['status'=>"success"], 'Subscription plan successfully updated',$request->path());
        } else {
            return $this->sendError($request->path(),'Subscription Id wrong!');
        }

    }

    public function subscription_list(Request $request,$id)
    {
        $listdetails = Subscription::get();

        $checkdoctorinplan = DB::table('plans')->where('doctor_id',$id)->first();

        $return_array = array();

        if(!empty($checkdoctorinplan) && Carbon::now()->diffInSeconds($checkdoctorinplan->exp_date) > 0)
        {
            foreach ($listdetails as  $value) {
                if($value->id == $checkdoctorinplan->sub_id){
                    $value->flag_plan = "1";
                } else {
                    $value->flag_plan = "0";
                }

                $value->id = (string)$value->id;
                $value->image = url('/').'/'.$value->image;


                $value->exp_date = $checkdoctorinplan->exp_date;

                $return_array[] = $value;
            }
        } else {
            foreach ($listdetails as  $value) {
                $value->flag_plan = "-1";

                $value->id = (string)$value->id;
                
                $value->image = url('/').'/'.$value->image;


                $return_array[] = $value;
            }
        }

        return $this->sendResponse1($listdetails, 'Subscription list retrieved successfully',$request->path());
    }

    public function chatthreadpage(Request $request)
    {
        $input = $request->all();


        $val_arr = [
            'user_id' => 'required',
            'usertype'=>'required',
            'token'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if($input['usertype'] == 'user')
        {
             $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

            $chatlist = DB::select("SELECT doctors.image,doctors.id as user_id,doctors.fullname,chats.msg,chats.id as chat_id,chats.created_at FROM chats INNER JOIN doctors on (doctors.id=chats.doctor_id) WHERE chats.id IN ( SELECT MAX(id) FROM chats GROUP BY doctor_id ) and chats.user_id='".$input['user_id']."' order by chats.id desc"); 

          // dd($chatlist); 

        } elseif($input['usertype'] == 'doctor')
        {
             $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

            $chatlist = DB::select("SELECT users.image,users.id as user_id,users.fullname,chats.msg,chats.id as chat_id,chats.created_at FROM chats INNER JOIN users on (users.id=chats.user_id) WHERE chats.id IN ( SELECT MAX(id) FROM chats GROUP BY doctor_id ) and chats.doctor_id='".$input['user_id']."' order by chats.id desc");

            // dd($chatlist);   
        }

         $return_array = array();

        foreach($chatlist as $value) {
              $value->user_id = (string)$value->user_id;
              $value->created_at =  Carbon::parse($value->created_at)->diffForHumans();
              
               if(empty($value->image)){
                    $value->image = url('/')."/public/img/user_signup.png";
                } else {
                   $value->image = url('/').'/public/'.$value->image;
                }

              $return_array[] = $value;
        }

        $price = array();
foreach ($return_array as $key => $row)
{
    $price[$key] = $row->chat_id;
}
array_multisort($price, SORT_DESC, $return_array);


        return $this->sendResponse($return_array, 'Message thread retrieve successfully',$request->path());




    }

    public function getmsg(Request $request)
    {
        $input = $request->all();


        $val_arr = [
            'user_id' => 'required',
            'doctor_id'=>'required',
            'usertype'=>'required',
            'token'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $userdetails = User::find($input['user_id']);

        if(empty($userdetails->image)){
            $userdetails->image = url('/')."/public/img/user_signup.png";
        } else {
           $userdetails->image = url('/').'/public/'.$userdetails->image;
        }

        $doctordetails = Doctor::find($input['doctor_id']);

        if(empty($doctordetails->image)){
            $doctordetails->image = url('/')."/public/img/user_signup.png";
        } else {
           $doctordetails->image = url('/').'/public/'.$doctordetails->image;
        }

        if($input['usertype'] == 'user')
        {
             $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                $room_name = $doctordetails->fullname;  

        } elseif($input['usertype'] == 'doctor')
        {
             $check_token = Dtoken::where(['user_id'=>$input['doctor_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                $room_name = $userdetails->fullname;    
        }


        $chatlist = Chat::where(["user_id"=>$input['user_id'],"doctor_id"=>$input['doctor_id']])->orderBy('id', 'DESC')->get()->toArray();

        $return_array = array();

        foreach ($chatlist as $value) {
              $value['created_at'] =  Carbon::parse($value['created_at'])->diffForHumans();
              $userarrycheck = explode("_", $value['msg_from']);

              if($userarrycheck[0] == $input['usertype']){
                   $userflag = "1"; 
              } else {
                    $userflag = "0";
              }

              if($userarrycheck[0] == 'user'){
                   $userusniqueimage = $userdetails->image; 
              } else {
                    $userusniqueimage = $doctordetails->image;
              }

              $return_array[] = array("unique_id"=>(string)$value['id'],"image"=>$userusniqueimage,"msg"=>$value['msg'],"time"=>$value['created_at'],"flag"=>$userflag);
        }

          $price = array();
foreach ($return_array as $key => $row)
{
    $price[$key] = $row['unique_id'];
}
array_multisort($price, SORT_ASC, $return_array);

        return $this->sendResponse(array("room_name"=>$room_name,"msg_thread"=>$return_array), 'Message retrieve successfully',$request->path());
    }

    public function sendmessage(Request $request)
    {
        $input = $request->all();


        $val_arr = [
            'user_id' => 'required',
            'doctor_id'=>'required',
            'usertype'=>'required',
            'token'=>'required',
            'msg'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if($input['usertype'] == 'user')
        {
             $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                $check_token1 = Dtoken::where(['user_id'=>$input['doctor_id']])->first();

                $input['msg_from'] = $input['usertype']."_".$input['user_id'];

                 $userdetails = User::find($input['user_id']);

                    if(empty($userdetails->image)){
                        $userdetails->image = url('/')."/public/img/user_signup.png";
                    } else {
                       $userdetails->image = url('/').'/public/'.$userdetails->image;
                    }
                    
                $other_id = (string)$input['user_id'];    
        } elseif($input['usertype'] == 'doctor')
        {
             $check_token = Dtoken::where(['user_id'=>$input['doctor_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                $check_token1 = Token::where(['user_id'=>$input['user_id']])->first();

                $input['msg_from'] = $input['usertype']."_".$input['doctor_id'];

                 $userdetails = Doctor::find($input['doctor_id']);

                    if(empty($userdetails->image)){
                        $userdetails->image = url('/')."/public/img/user_signup.png";
                    } else {
                       $userdetails->image = url('/').'/public/'.$userdetails->image;
                    }
                    
              $other_id = (string)$input['doctor_id'];         
        }

        $chatrem = Chat::create($input);



        $return_array = array("user_id"=>$other_id,"unique_id"=>(string)$chatrem->id,"image"=>$userdetails->image,"msg"=>$input['msg'],"time"=>Carbon::parse($chatrem->created_at)->diffForHumans(),"flag"=>"1");
        
        $return_arraynew = array("user_id"=>$other_id,"unique_id"=>(string)$chatrem->id,"image"=>$userdetails->image,"msg"=>$input['msg'],"time"=>Carbon::parse($chatrem->created_at)->diffForHumans(),"flag"=>"0");

        if($check_token1->deviceType == 'android')
        {
            
           
             $this->android_push($check_token1->deviceToken,$input['msg'],"chat",1,$return_arraynew);
           
        }


         if($check_token1->deviceType == 'ios')
        {
            //dd($return_responce);

          
              $this->iphone_push($check_token1->deviceToken,$input['msg'],"chat",1,$return_arraynew);
            
            
        }

        return $this->sendResponse1($return_array, 'Message sent successfully',$request->path());

    }

    public function getroomid(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'user_id' => 'required',
            'doctor_id'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }


        $check_room_record = Room::where(['user_id'=>$input['user_id'],'doctor_id'=>$input['doctor_id']])->first();

        if(empty($check_room_record)){
                 $chatkit = new Chatkit\Chatkit([
                  'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
                  'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
                ]);

                $uservalue =    $chatkit->createRoom([
                                  'creator_id' => "user_".$input['user_id'],
                                  'name' => 'my chat room'.$input['user_id'].$input['doctor_id'],
                                  'private' => false,
                                  'user_ids' => ["doctor_".$input['doctor_id']]
                                ]);

                $input['id'] = $uservalue['body']['id'];

                Room::create($input);

                return $this->sendResponse(array("room_id"=>(string)$uservalue['body']['id']), 'Room Id retrieve successfully',$request->path());
        } else {
                 return $this->sendResponse(array("room_id"=>(string)$check_room_record->id), 'Room Id retrieve successfully',$request->path());
        }
    }

    public function dumyimageuploade(Request $request)
    {
        $name = "";

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5(time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/userimage1');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            //$userdetails->image = 'userimage/'.$name;
        }

        return $this->sendResponse(array("path"=>url('/')."/userimage1/".$name), 'Image Uploaded successfully',$request->path());
    }
    
    public function checkchatkit(Request $request)
    {
         
        $input = $request->all();

        $val_arr = [
            'user_id' => 'required',
            'name'=>'required',
            'image'=>'required',
            'phone'=>'required',
            'status'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }
        
        $chatkit = new Chatkit\Chatkit([
      'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
      'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
    ]);

        $uservalue =    $chatkit->createUser([
                              'id' => $input['user_id'],
                              'name' => $input['name'],
                              'avatar_url' => $input['image'],
                              'custom_data' => [
                                'phone' => $input['phone'],
                                'status' => $input['status'],
                              ]
                            ]);
        
         return $this->sendResponse($uservalue, 'dumy chatkit record updated successfully',$request->path());   
    }

    public function checkchatkit1(Request $request)
    {
         
       
        $chatkit = new Chatkit\Chatkit([
      'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
      'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
    ]);

        $uservalue =    $chatkit->createRoom([
                          'creator_id' => 'user_70',
                          'name' => 'my room',
                          'private' => false,
                          'user_ids' => ['doctor_61']
                        ]);
        
         return $this->sendResponse($uservalue['body']['id'], 'dumy chatkit record updated successfully',$request->path());   
    }


    public function sendMessage2(Request $request)
    {
        
        $input = $request->all();

        $val_arr = [
            'sender_id' => 'required',
            'text'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        } 
       
        $chatkit = new Chatkit\Chatkit([
      'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
      'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
    ]);

        $uservalue =    $chatkit->sendMessage([
                          'sender_id' => $input['sender_id'],
                          'room_id' => 14588577,
                          'text' => $input['text'],
                          'attachment' => [
                                'resource_link'=>$input['resource_link'],
                                'type'=> $input['type']
                              ]
                        ]);
        
         return $this->sendResponse($uservalue, 'dumy chatkit record updated successfully',$request->path());   
    }


    public function sendMessage3(Request $request)
    {
        
        $input = $request->all();

        $val_arr = [
            'room_id' => 'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        } 
       
        $chatkit = new Chatkit\Chatkit([
      'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
      'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
    ]);

        $uservalue =    $chatkit->getRoomMessages([
                          'room_id' => $input['room_id']
                        ]);
        
         return $this->sendResponse($uservalue, 'dumy chatkit record updated successfully',$request->path());   
    }

    public function reviewandratting(Request $request,$id)
    {
        $global_rate = 0;
         $review_details = DB::select("SELECT users.image,doctorratings.rating*20 as rating,doctorratings.review,users.fullname FROM `doctorratings` INNER JOIN users on (users.id=doctorratings.user_id) where doctorratings.doctor_id='".$id."'");

            foreach ($review_details as $value) {
                  if(empty($value->image)){
                        $value->image = url('/')."/public/img/user_signup.png";
                    } else {
                       $value->image = url('/').'/public/'.$value->image;
                    }

                    $global_rate = $global_rate + $value->rating;
            }

            //$recorde_detail[0]->num_of_review = (string)sizeof($review_details);

            $detail_return = array("avg_rate"=>(string)($this->avgrating($id)/20),"review_count"=>(string)sizeof($review_details),"details"=>$review_details);

             return $this->sendResponse($detail_return, 'Review list retrieved successfully',$request->path());
    }

    public function avgrating($id)
    {
        $global_rate = 0;
         $review_details = DB::select("SELECT doctorratings.rating*20 as rating FROM `doctorratings` where doctorratings.doctor_id='".$id."'");

            foreach ($review_details as $value) {
                 

                    $global_rate = $global_rate + $value->rating;
            }

            if(sizeof($review_details) == '0'){
                return "0";
            } else {
                return (string)(round($global_rate/(sizeof($review_details))));
            }

    }

    public function notificationtrigger(Request $request)
    {
        $flag_notify = "ON";

        $input = $request->all();

        $val_arr = [
            'user_id' => 'required',
            'usertype'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if($input['usertype'] == 'user')
        {
            $userlist = User::find($input['user_id']);

            if($userlist->notification_flag == 1){
                $userlist->notification_flag = "0";
                $userlist->save();
                 $flag_notify = "OFF";
            } else {
                 $userlist->notification_flag = "1";
                 $flag_notify = "ON";
                 $userlist->save();
            }
        } else if($input['usertype'] == 'doctor')
        {
            $userlist = Doctor::find($input['user_id']);


            if($userlist->notification_flag == 1){
                $userlist->notification_flag = "0";
                $flag_notify = "OFF";

                $userlist->save();
            } else {
                 $userlist->notification_flag = "1";
                 $flag_notify = "ON";
                 $userlist->save();
            }
        }

        return $this->sendResponse(array("status"=>$flag_notify), 'Notification state change successfully',$request->path());
    }

    public function bookinglsit(Request $request)
    {
        $input = $request->all();

        $record_list_data = array();

        $record_list_data['day1'] = $record_list_data['day2'] =$record_list_data['day3'] = $record_list_data['day4'] = $record_list_data['day5'] = $record_list_data['day6'] = $record_list_data['day7'] = array();

        //dd($details_list_array);

        $val_arr = [
            'user_id' => 'required',
            'usertype'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $day_total = array("day1"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(0)))),"day"=> date('d',strtotime(Carbon::now()->addDays(0)))),"day2"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(1)))),"day"=> date('d',strtotime(Carbon::now()->addDays(1)))),"day3"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(2)))),"day"=> date('d',strtotime(Carbon::now()->addDays(2)))),"day4"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(3)))),"day"=> date('d',strtotime(Carbon::now()->addDays(3)))),"day5"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(4)))),"day"=> date('d',strtotime(Carbon::now()->addDays(4)))),"day6"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(5)))),"day"=> date('d',strtotime(Carbon::now()->addDays(5)))),"day7"=>array("day_name"=>$this->short_to_week1(date('D',strtotime(Carbon::now()->addDays(6)))),"day"=> date('d',strtotime(Carbon::now()->addDays(6)))));

       

        $current_month = date("M Y");

        //dd($current_month);

        if($input['usertype'] == 'user')
        {
            $userdetailsu = User::find($input['user_id']);

            if (is_null($userdetailsu)) {
                return $this->sendError($request->path(),'user not found.');
            }

            $details_list = DB::select("SELECT doctors.clinic,doctors.image,doctors.fullname,specialties.name as speciality_name,cities.name as city_name,bookings.apdate,bookings.aptime,doctors.id as doctor_id,doctors.avilability,doctors.fee FROM doctors INNER join bookings on (bookings.doctor_id = doctors.id) INNER join cities on (cities.id = doctors.city_id) INNER JOIN specialties on (specialties.id = doctors.speciality_id) where bookings.email='".$userdetailsu->email."'");

            //dd($details_list);

           

            foreach ($details_list as $value) 
            {
               
                if(empty($value->image))
                {
                    $value->image = url('/')."/public/img/user_signup.png";
                } else {
                    $value->image = url('/public/').'/'.$value->image;
                }

                $value->msg = "Appointment: ".$value->apdate." at ".date("h:ia",strtotime($value->aptime)); 

                    $date = str_replace('/', '-', $value->apdate);


                   
                 $value->avilability = array_map('trim',explode(",", $value->avilability));

                  //dd(Carbon::parse(Carbon::now()->addDays(0)->format('Y-m-d'))->diffInDays($date));

                if(Carbon::parse(Carbon::now()->addDays(0)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day1'][] = $value; 
                }  


                if(Carbon::parse(Carbon::now()->addDays(1)->format('Y-m-d'))->diffInDays($date) == 0){
                       $record_list_data['day2'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(2)->format('Y-m-d'))->diffInDays($date) == 0){
                       $record_list_data['day3'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(3)->format('Y-m-d'))->diffInDays($date) == 0){
                       $record_list_data['day4'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(4)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day5'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(5)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day6'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(6)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day7'][] = $value; 
                }  
            }

         //   die();

             return $this->sendResponse(array("month"=>$current_month,"day1"=>$day_total['day1'],"day2"=>$day_total['day2'],"day3"=>$day_total['day3'],"day4"=>$day_total['day4'],"day5"=>$day_total['day5'],"day6"=>$day_total['day6'],"day7"=>$day_total['day7'],"booking_list"=>$record_list_data), 'User Booking list retrieve successfully',$request->path());

        } else if($input['usertype'] == 'doctor')
        {
            $userdetailsd = Doctor::find($input['user_id']);


            if (is_null($userdetailsd)) {
                return $this->sendError($request->path(),'user not found.');
            }


            $details_list = DB::select("SELECT bookings.age as user_age,bookings.gender as user_gender,users.image,bookings.firstname as fullname,cities.name as city_name,bookings.apdate,bookings.aptime,users.id as user_id,users.email,bookings.phone FROM users INNER join bookings on (bookings.email = users.email) INNER join cities on (cities.id = users.city_id)  where bookings.doctor_id='".$input['user_id']."'");

           

            foreach ($details_list as $value) 
            {
                $value->user_id = (string)$value->user_id;
                if(empty($value->image))
                {
                    $value->image = url('/')."/public/img/user_signup.png";
                } else {
                    $value->image = url('/public/').'/'.$value->image;
                }

                 $date = str_replace('/', '-', $value->apdate);

                $value->msg = "Appointment: ".$value->apdate." at ".date("h:ia",strtotime($value->aptime)); 

                if(Carbon::parse(Carbon::now()->addDays(0)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day1'][] = $value; 
                }  


                if(Carbon::parse(Carbon::now()->addDays(1)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day2'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(2)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day3'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(3)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day4'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(4)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day5'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(5)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day6'][] = $value; 
                }

                if(Carbon::parse(Carbon::now()->addDays(6)->format('Y-m-d'))->diffInDays($date) == 0){
                        $record_list_data['day7'][] = $value; 
                }  
            }

             return $this->sendResponse(array("month"=>$current_month,"day1"=>$day_total['day1'],"day2"=>$day_total['day2'],"day3"=>$day_total['day3'],"day4"=>$day_total['day4'],"day5"=>$day_total['day5'],"day6"=>$day_total['day6'],"day7"=>$day_total['day7'],"booking_list"=>$record_list_data), 'Doctor Booking list retrieve successfully',$request->path());

            
        }
    }

    public function notifylist(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'user_id' => 'required',
            'usertype'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

       
        $notifydatalist = Notification::where(['receiver_id'=>$input['user_id'],'usertype'=>$input['usertype']])->orderBy('id', 'DESC')->get();

        $data_array = array();

        foreach($notifydatalist->toArray() as $value){
            
            $value['data'] = json_decode($value['data']);

            $value['data']->user_id = $value['sender_id'];
            $value['data']->usertype = $value['usertype'];
             $value['data']->time = Carbon::parse($value['created_at'])->diffForHumans(Carbon::now());

             $data_array[] = $value['data'];
            //dd($value['data']);
            //$value->data
        }

        Notification::where(['receiver_id'=>$input['user_id'],'usertype'=>$input['usertype']])->update(['status' => "1"]);


         return $this->sendResponse($data_array, 'Notification list retrieved successfully',$request->path());



    }

    public function notifyfromuser(Request $request)
    {
        $input = $request->all();

         $val_arr = [
            'user_id' => 'required',
            'doctor_id' => 'required',
            'token'=>'required',
        ];


        

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $post = User::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'User not found.');
        }

         $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }

        $msg = $post->fullname." sent invitation on ".date('d/m/Y');

         if(empty($post->image))
        {
            $post->image = url('/')."/public/img/user_signup.png";
        } else {
            $post->image = url('/public/').'/'.$post->image;
        }

        $data1 = array("msg"=>$msg,"image"=>$post->image);

        $return_responce = array('data'=>json_encode($data1),'sender_id'=>$input['user_id'],'receiver_id'=>$input['doctor_id'],'usertype'=>'user');

        Notification::create($return_responce);

        $data1['time'] = Carbon::now()->diffForHumans();


        $dtokenbyuser = Dtoken::where(['user_id'=>$input['doctor_id']])->first();

        $count_notification = Notification::where(['receiver_id'=>$input['doctor_id'],'status'=>'0'])->count();

        if($dtokenbyuser->deviceType == 'android')
        {
            //dd($return_responce);
            $uniqueflag = Doctor::find($input['doctor_id']);

            if($uniqueflag->notification_flag == 1){
             $this->android_push($dtokenbyuser->deviceToken,$msg,"booking",$count_notification,$data1);
            }
        }

         if($dtokenbyuser->deviceType == 'ios')
        {
            //dd($return_responce);
            $uniqueflag = Doctor::find($input['doctor_id']);

            if($uniqueflag->notification_flag == 1){
             $this->iphone_push($dtokenbyuser->deviceToken,$msg,"booking",$count_notification,$data1);
            }
        }

       return $this->sendResponse(array("status"=>"success"), 'Request sent successfully',$request->path());

    }

    public function notifyfromdoctor(Request $request)
    {
        $input = $request->all();

         $val_arr = [
            'doctor_id' => 'required',
            'token'=>'required',
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'age'=>'required',
            'gender'=>'required',
            'apdate'=>'required',
            'aptime'=>'required',
        ];


        

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $post = Doctor::find($input['doctor_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'doctor not found.');
        }

         $check_token = Dtoken::where(['user_id'=>$input['doctor_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }

        $check_email = DB::select("SELECT * FROM `users` WHERE `email` = '".$input['email']."'");

        //dd();

        if(empty($check_email)) {
            return $this->sendError($request->path(),'email not found.');
        }

        $msg = $post->fullname." confirm booking on  ".date('d/m/Y');

         if(empty($post->image))
        {
            $post->image = url('/')."/public/img/user_signup.png";
        } else {
            $post->image = url('/public/').'/'.$post->image;
        }

        $data1 = array("msg"=>$msg,"image"=>$post->image);

        $return_responce = array('data'=>json_encode($data1),'sender_id'=>$input['doctor_id'],'receiver_id'=>$check_email[0]->id,'usertype'=>'doctor');

        Notification::create($return_responce);

        $data1['time'] = Carbon::now()->diffForHumans();

        Booking::create($input);


        $dtokenbyuser = Token::where(['user_id'=>$check_email[0]->id])->first();

        $count_notification = Notification::where(['receiver_id'=>$check_email[0]->id,'status'=>'0'])->count();

        if($dtokenbyuser->deviceType == 'android')
        {
            //dd($return_responce);

            if($check_email[0]->notification_flag == 1){
              $this->android_push($dtokenbyuser->deviceToken,$msg,"booking_confirmed",$count_notification,$data1);
            }
            
        }

         if($dtokenbyuser->deviceType == 'ios')
        {
            //dd($return_responce);

            if($check_email[0]->notification_flag == 1){
              $this->iphone_push($dtokenbyuser->deviceToken,$msg,"booking_confirmed",$count_notification,$data1);
            }
            
        }

       return $this->sendResponse(array("status"=>"success"), 'Booking confirmed successfully',$request->path());

    }

    public function signup(Request $request)
    {
        $input = $request->all();

        

         $val_arr = [
            'fullname' => 'required',
            'phone' => 'required',
            'email'=>'required',
            'password' => 'required|confirmed|min:6',
        ];


        

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

       
        
       

        $check_username = User::where('email',$input['email'])->first();

        if (!empty($check_username)) {
            return $this->sendError($request->path(),'email already exist as user'); 
        }
        
        $check_phone = User::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone)) {
            return $this->sendError($request->path(),'phone number already exist as user'); 
        }

        $check_username1 = Doctor::where('email',$input['email'])->first();

        if (!empty($check_username1)) {
            return $this->sendError($request->path(),'email already exist as doctor'); 
        }
        
        $check_phone1 = Doctor::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone1)) {
            return $this->sendError($request->path(),'phone number already exist as doctor'); 
        }



        $post = User::create($input);

        $token_s = str_random(25);


        $tokens_array = array('user_id'=>$post->id,'token'=>$token_s,'deviceType'=>$input['deviceType'],'deviceToken'=>$input['deviceToken']);

        $token_saver = Token::create($tokens_array);


        $return_array = $post->toArray();

        $return_array['token'] = $token_s;

        unset($return_array['id']);

        unset($return_array['confirmpassword']);

        $return_array['user_id'] =  (string)$post->toArray()['id'];

        $return_array['city_id'] = "";

        if(empty($return_array['image']))
        {
            $return_array['image'] = url('/')."/public/img/user_signup.png";
        }

        //  $chatkit = new Chatkit\Chatkit([
        //           'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
        //           'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
        //         ]);

        // $uservalue =    $chatkit->createUser([
        //                   'id' => "user_".$post->id,
        //                   'name' => $post->fullname,
        //                   'avatar_url' => $return_array['image'],
        //                 ]);


        return $this->sendResponse($return_array, 'User created successfully.',$request->path());
    }


    public function city_update(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'city_name'=>'required',
            'user_id' => 'required',
            'token' => 'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $post = User::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'User not found.');
        }

         $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }


        $search_city_name = City::where(['name'=>$input['city_name']])->first();

        if (empty($search_city_name)) {
            return $this->sendError($request->path(),'City not found in list');
        }

        $post->city_id = (string)$search_city_name->id;

        $post->save();

        $return_array = $post->toArray();

        unset($return_array['id']);

        $return_array['user_id'] =  (string)$post->toArray()['id'];


        return $this->sendResponse($return_array, 'City successfully updated for user',$request->path());


    }

    public function city_update_doctor(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'city_name'=>'required',
            'user_id' => 'required',
            'token' => 'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $post = Doctor::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'Doctor not found.');
        }

         $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }


        $search_city_name = City::where(['name'=>$input['city_name']])->first();

        if (empty($search_city_name)) {
            return $this->sendError($request->path(),'City not found in list');
        }

        $post->city_id = (string)$search_city_name->id;

        $post->save();

        $return_array = $post->toArray();

        unset($return_array['id']);

        $return_array['user_id'] =  (string)$post->toArray()['id'];


        return $this->sendResponse($return_array, 'City successfully updated for doctor',$request->path());


    }


    public function login(Request $request)
    {
       $input = $request->all();

       $validator = Validator::make($input, [
            'email'=>'required',
            'password' => 'required',
            'deviceToken'=>'required',
            'deviceType'=>'required|in:android,ios',
            'usertype'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }


        if ($input['usertype'] == "user") {
            $details = User::whereRaw(" password = '".$input['password']."' and admin_status='1' and (phone = '".$input['email']."' or email = '".$input['email']."')")->first();

        if (empty($details)) {

            return $this->sendError($request->path(),"password or email is incorrect");
        }

        $token_s = str_random(25);


        $token_saver = Token::where('user_id',$details['id'])->update(['token'=>$token_s,'deviceToken'=>$input['deviceToken'],'deviceType'=>$input['deviceType']]);


        $details['user_id'] =  (string)$details['id'];


        unset($details['id']);

        //unset($details['id']);

        //$details['image'] = url('/public/').'/'.$details['image'];

        $details['token'] = $token_s;

         if(empty($details['image']))
        {
            $details['image'] = url('/')."/public/img/user_signup.png";
        } else {
            $details['image'] = url('/public/').'/'.$details['image'];
        }


       return $this->sendResponse($details, 'User login successfully.',$request->path());
        } else if($input['usertype'] == "doctor")
        {
             $details = Doctor::whereRaw(" password = '".$input['password']."' and admin_status='1' and (phone = '".$input['email']."' or email = '".$input['email']."')")->first();

        if (empty($details)) {

            return $this->sendError($request->path(),"password or email is incorrect");
        }

        $token_s = str_random(25);


        $token_saver = Dtoken::where('user_id',$details['id'])->update(['token'=>$token_s,'deviceToken'=>$input['deviceToken'],'deviceType'=>$input['deviceType']]);


        $details['user_id'] =  (string)$details['id'];


        unset($details['id']);

        $details['token'] = $token_s;

        if(empty($details['image']))
        {
            $details['image'] = url('/')."/public/img/user_signup.png";
        } else {
            $details['image'] = url('/public/').'/'.$details['image'];
        }


       return $this->sendResponse($details, 'Doctor login successfully.',$request->path());
        }

     

        
    }





    public function doctor(Request $request)
    {
        $input = $request->all();
        $val_arr = [
            'email' => 'required',
            'speciality_id' => 'required',
            'clinic'=>'required',
            'licence_number'=>'required',
            'expertise_area'=>'required',
            'password' => 'required|confirmed|min:6',
            'fullname'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }


        $check_username = User::where('email',$input['email'])->first();

        if (!empty($check_username)) {
            return $this->sendError($request->path(),'email already exist as user'); 
        }
        
        

        $check_username1 = Doctor::where('email',$input['email'])->first();

        if (!empty($check_username1)) {
            return $this->sendError($request->path(),'email already exist as doctor'); 
        }


        if(!empty($input['the_other_speciality']) && isset($input['the_other_speciality'])){
                    $spl_new_entry = array('name'=>$input['the_other_speciality']);
                    $Specialty_record = Specialty::create($spl_new_entry);
                    $input['speciality_id'] = (string)$Specialty_record->id;
        }

        if(empty($input['insurance_accept'])){
                $input['insurance_accept'] = "";
        } else {
                $return_insurance_string = "";
                $insurance_accept_aarray = explode(",", $input['insurance_accept']);

                foreach ($insurance_accept_aarray as $value1new) {
                        $check_insurance_accept = Insurance::where('name',$value1new)->first();

                        if(empty($check_insurance_accept)){
                             $insu_array_o = array("name"=>$value1new);
                             $insu_array_details = Insurance::create($insu_array_o);
                             $return_insurance_string = $return_insurance_string.$insu_array_details->id.",";

                        } else {
                            $return_insurance_string = $return_insurance_string.$check_insurance_accept->id.",";
                        } 
                }

                $input['insurance_accept'] = chop($return_insurance_string,",");
        }
        
       



        $post = Doctor::create($input);

        

        $token_s = str_random(25);


        $tokens_array = array('user_id'=>$post->id,'token'=>$token_s,'deviceType'=>$input['deviceType'],'deviceToken'=>$input['deviceToken']);

        $token_saver = Dtoken::create($tokens_array);


        $return_array = $post->toArray();

        $return_array['token'] = $token_s;

        unset($return_array['id']);

        unset($return_array['confirmpassword']);

        $return_array['user_id'] =  (string)$post->toArray()['id'];

        $return_array['city_id'] = "";

        if(empty($return_array['image']))
        {
            $return_array['image'] = url('/')."/public/img/user_signup.png";
        }

        //  $chatkit = new Chatkit\Chatkit([
        //           'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
        //           'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
        //         ]);

        // $uservalue =    $chatkit->createUser([
        //                   'id' => "doctor_".$post->id,
        //                   'name' => $post->fullname,
        //                   'avatar_url' => $return_array['image'],
        //                 ]);


        return $this->sendResponse($return_array, 'Doctor created successfully.',$request->path());




           
    }

    public function userotp(Request $request)
    {
        $input = $request->all();
        $check_username = User::where('email',$input['email'])->first();

        if (!empty($check_username)) {
            return $this->sendError($request->path(),'email already exist as user'); 
        }
        
        $check_phone = User::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone)) {
            return $this->sendError($request->path(),'phone number already exist as user'); 
        }

        $check_username1 = Doctor::where('email',$input['email'])->first();

        if (!empty($check_username1)) {
            return $this->sendError($request->path(),'email already exist as doctor'); 
        }
        
        $check_phone1 = Doctor::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone1)) {
            return $this->sendError($request->path(),'phone number already exist as doctor'); 
        }


        $check_phone_in_otp = DB::table('otps')->wherePhone($input['phone'])->first();

        if(!empty($check_phone_in_otp))
        {
            $check_phone_in_otp111 = Otp::find($check_phone_in_otp->id);
            $check_phone_in_otp111->otp = (string)rand(1000,9999);
            $check_phone_in_otp111->save();

             $return_array = $check_phone_in_otp111->toArray();
            unset($return_array['id']);

            $email = $input['email'];

             $postData = "";

            try{
                     Mail::send('emails.otps', ['otp' =>$check_phone_in_otp111->otp], function($message) use ($postData,$email)
                                {
                                  $message->from('info@mydoctoruae.com', 'Healthapp');
                                  $message->to($email, 'Healthapp')->subject('Otp Verification');
                                });

                     return $this->sendResponse($return_array, 'Otp send successfully.',$request->path());

                 }
                 catch(Exception $e){
                                    return $this->sendResponse($return_array, 'Otp send successfully.',$request->path());

                                } 

             return $this->sendResponse($return_array, 'Otp send successfully.',$request->path());
        } else {
            $insert_array = array('otp'=>(string)rand(1000,9999),'phone'=>$input['phone']);
            $check_phone_in_otp1 = Otp::create($insert_array);

            $return_array = $check_phone_in_otp1->toArray();
            unset($return_array['id']);

             $email = $input['email'];

             $postData = "";

            try{
                     Mail::send('emails.otps', ['otp' =>$insert_array['otp']], function($message) use ($postData,$email)
                                {
                                  $message->from('support@mobulous.co.in', 'Healthapp');
                                  $message->to($email, 'Healthapp')->subject('Otp Verification');
                                });

                     return $this->sendResponse($return_array, 'Otp send successfully.',$request->path());

                 }
                 catch(Exception $e){
                                    return $this->sendResponse($return_array, 'Otp send successfully.',$request->path());

                                } 
             
        }



    }

    public function newsfeedlist(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'user_id' => 'required',   
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.`image` as post_image,'doctor' as usertype FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' 
union all
SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image` as user_image,`posts`.`id` as post_id, '' as speciality_name,`posts`.`image` as post_image,'user' as usertype FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user') as x order by x.post_id desc");

        

        foreach ($get_news_feedlist as $value) {
            
            $value->post_id = (string)$value->post_id;

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

            $like_recorde = DB::table('likes')->where(['user_id'=>$input['user_id'],'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($like_recorde))
            {
                $value->likeflag = "0";
            } else {
                $value->likeflag = "1";
            }

            $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>$input['user_id'],'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($saveposts_recorde))
            {
                $value->saveflag = "0";
            } else {
                $value->saveflag = "1";
            }
        }

        $return_array = $get_news_feedlist;
       // unset($return_array['id']);
        //unset($return_array['image']);
       // $return_array['post_id'] = (string)$get_news_feedlist['id'];
        


       return $this->sendResponse($return_array, 'Post list retrieve successfully Submited',$request->path());
    }

    public function postnews(Request $request)
    {
        $input = $request->all();

         $val_arr = [
            'title' => 'required|max:25',
            'description' => 'required|max:500',
            'user_id'=>'required',
            'usertype'=>'required',
            'token'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if(empty($input['url'])){
             $input['url'] = "";
        }

       

        if($input['usertype'] == 'user')
        {
             $post = User::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'User not found.');
        }

         $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }


        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5($input['user_id'].time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/postimage');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $input['image'] = 'postimage/'.$name;
        }

        $newpost = Post::create($input);
        $return_array = $newpost->toArray();

        unset($return_array['id']);
        $return_array['post_id'] = (string)$newpost->toArray()['id'];

        if(empty($return_array['image'])){
           $return_array['image'] = ""; 
        } else {
           
           $return_array['image'] = url('/public/').'/'.$newpost->toArray()['image'];
        }
        
        
        return $this->sendResponse($return_array, 'Post successfully Submited.',$request->path());


        } else if($input['usertype'] == 'doctor')
        {
             $post = Doctor::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'Doctor not found.');
        }

         $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5($input['user_id'].time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/postimage');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $input['image'] = 'postimage/'.$name;
        }

        

        $newpost = Post::create($input);

        $return_array = $newpost->toArray();

        unset($return_array['id']);
        //unset($return_array['image']);
        $return_array['post_id'] = (string)$newpost->toArray()['id'];
        $return_array['image'] = url('/public/').'/'.$newpost->toArray()['image'];
        return $this->sendResponse($return_array, 'Post successfully Submited',$request->path());
        }






    }

    public function changePassword(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'oldpassword'=>'required',
            'password' => 'required|confirmed|min:6',
            'usertype'=>'required',
            'token'=>'required',
            'user_id'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }


        if($input['usertype'] == 'user')
        {
            $post = User::find($input['user_id']);


            if (is_null($post)) {
                return $this->sendError($request->path(),'User not found.');
            }

             $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

            if (empty($check_token)) {
                return $this->sendError($request->path(),'Token Expire');
            }

            if($post->password != $input['oldpassword'])
            {
                return $this->sendError($request->path(),'Old Password is wrong');
            } else {
                $post->password = $input['password'];
                $post->save();

                return $this->sendResponse(array('status'=>'success'), 'Password Changed successfully',$request->path());
            }
        } else if($input['usertype'] == 'doctor')
        {
            $post = Doctor::find($input['user_id']);


            if (is_null($post)) {
                return $this->sendError($request->path(),'Doctor not found.');
            }

             $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

            if (empty($check_token)) {
                return $this->sendError($request->path(),'Token Expire');
            }

            if($post->password != $input['oldpassword'])
            {
                return $this->sendError($request->path(),'Old Password is wrong');
            } else {
                $post->password = $input['password'];
                $post->save();

                return $this->sendResponse(array('status'=>'success'), 'Password Changed successfully',$request->path());
            }
        }





    }

    public function postlistbyuserid(Request $request,$id)
    {
        $input = $request->all();

        if($input['usertype'] == 'user')
        {
            $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image`,`posts`.`id` as post_id, '' as speciality_name,`posts`.image as post_image FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user' and `posts`.`user_id`='".$id."') as x order by x.post_id desc");
        } else if($input['usertype'] == 'doctor')
        {
            $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.image as post_image FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' and `posts`.`user_id`='".$id."') as x order by x.post_id desc");
        }

        foreach ($get_news_feedlist as $value) {
            
            $value->post_id = (string)$value->post_id;

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

            if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

            $like_recorde = DB::table('likes')->where(['user_id'=>$id,'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($like_recorde))
            {
                $value->likeflag = "0";
            } else {
                $value->likeflag = "1";
            }

            $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>$id,'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($saveposts_recorde))
            {
                $value->saveflag = "0";
            } else {
                $value->saveflag = "1";
            }

           
           
        }

        



        return $this->sendResponse($get_news_feedlist, 'Post list retrieve successfully',$request->path());


    }

    public function likepost(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'token'=>'required',
            'usertype'=>'required',
            'user_id'=>'required',
            'post_id'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $like_recorde = DB::table('likes')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->first();

        if(empty($like_recorde))
        {
                if($input['usertype'] == 'user')
        {
            $post = User::find($input['user_id']);


            if (is_null($post)) {
                return $this->sendError($request->path(),'User not found.');
            }

             $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

            if (empty($check_token)) {
                return $this->sendError($request->path(),'Token Expire');
            }

            $like_colmn = Like::create($input);

            return $this->sendResponse(array('status'=>'success','likeflag'=>'1'), 'Post liked successfully',$request->path());


        } else if($input['usertype'] == 'doctor')
        {
           $post = Doctor::find($input['user_id']);


            if (is_null($post)) {
                return $this->sendError($request->path(),'Doctor not found.');
            }

             $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

            if (empty($check_token)) {
                return $this->sendError($request->path(),'Token Expire');
            } 

            $like_colmn = Like::create($input);  

            return $this->sendResponse(array('status'=>'success','likeflag'=>'1'), 'Post liked successfully',$request->path());
        }
        } else {
            
            DB::table('likes')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();

            return $this->sendResponse(array('status'=>'success','likeflag'=>'0'), 'Post unliked successfully',$request->path());
        }

        


    }

    public function savepost(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'token'=>'required',
            'usertype'=>'required',
            'user_id'=>'required',
            'post_id'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->first();

        if(empty($saveposts_recorde))
        {
            if($input['usertype'] == 'user')
            {
                $post = User::find($input['user_id']);


                if (is_null($post)) {
                    return $this->sendError($request->path(),'User not found.');
                }

                 $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                $Savepost_colmn = Savepost::create($input);

                return $this->sendResponse(array('status'=>'success','saveflag'=>'1'), 'Post save successfully',$request->path());


            } else if($input['usertype'] == 'doctor')
            {
               $post = Doctor::find($input['user_id']);


                if (is_null($post)) {
                    return $this->sendError($request->path(),'Doctor not found.');
                }

                 $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                } 

                $Savepost_colmn = Savepost::create($input);  

                return $this->sendResponse(array('status'=>'success','saveflag'=>'1'), 'Post save successfully',$request->path());
            }
        } else {

            DB::table('saveposts')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();

            return $this->sendResponse(array('status'=>'success','saveflag'=>'0'), 'Post unsave successfully',$request->path());
        }

        


    }

     public function commentOnPost(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
              'post_id' => 'required',
              'message' => 'required',
              'user_id' =>'required',
              'usertype'=>'required',
              'token'=>'required',
          ]);


          if($validator->fails()){
              return $this->sendError($request->path(),$validator->errors()->first());       
          }


          if($input['usertype'] == 'user')
          {
                $post = User::find($input['user_id']);


                if (is_null($post)) {
                    return $this->sendError($request->path(),'User not found.');
                }

                 $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                Comment::create($input);

          } else if($input['usertype'] == 'doctor') {

               $post = Doctor::find($input['user_id']);


                if (is_null($post)) {
                    return $this->sendError($request->path(),'Doctor not found.');
                }

                 $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                } 

                Comment::create($input);
          }


        

        

        $return_array = DB::select("select * from(SELECT comments.id as comment_id,users.image,users.fullname,users.email,comments.message,comments.created_at FROM `comments` inner join `users` on (users.id = comments.user_id) where comments.`usertype` = 'user' and comments.post_id ='".$input['post_id']."'
union ALL
SELECT comments.id as comment_id,doctors.image,doctors.fullname,doctors.email,comments.message,comments.created_at FROM `comments` inner join `doctors` on (doctors.id = comments.user_id) where comments.`usertype` = 'doctor' and comments.post_id ='".$input['post_id']."') as x order by x.comment_id DESC");

        foreach ($return_array as $value) {
             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

             $value->created_at = Carbon::parse($value->created_at)->diffForHumans();
        }


          return $this->sendResponse($return_array, 'Post Comment list retrieved successfully.',$request->path());




    }

     public function Commentlist(Request $request,$id)
    {
         $return_array = DB::select("select * from(SELECT comments.id as comment_id,users.image,users.fullname,users.email,comments.message,comments.created_at FROM `comments` inner join `users` on (users.id = comments.user_id) where comments.`usertype` = 'user' and comments.post_id ='".$id."'
union ALL
SELECT comments.id as comment_id,doctors.image,doctors.fullname,doctors.email,comments.message,comments.created_at FROM `comments` inner join `doctors` on (doctors.id = comments.user_id) where comments.`usertype` = 'doctor' and comments.post_id ='".$id."') as x order by x.comment_id DESC");


         foreach ($return_array as $value) {
             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

             $value->created_at = Carbon::parse($value->created_at)->diffForHumans();
        }




          return $this->sendResponse($return_array, 'Post Comment list retrieved successfully.',$request->path());
    }

    public function editUserProfile(Request $request,$id)
    {
        $input = $request->all();

        $val_arr = [
            'token'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }


        $userdetails = User::find($id);

        if (is_null($userdetails)) {
                return $this->sendError($request->path(),'User not found.');
            }

        $check_token = Token::where(['user_id'=>$id,'token'=>$input['token']])->first();

            if (empty($check_token)) {
                return $this->sendError($request->path(),'Token Expire');
            }    


        $name = "";

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5($id.time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/userimage');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $userdetails->image = 'userimage/'.$name;
        }

        
        if(!empty($input['email']) && isset($input['email'])){
            $userdetails->email = $input['email'];
        }

        if(!empty($input['phone']) && isset($input['phone'])){
            $userdetails->phone = $input['phone'];
        }
            
        $userdetails->save(); 

        $return_array = $userdetails->toArray();

        unset($return_array['id']);
        unset($return_array['confirmpassword']);

         $return_array['user_id'] = (string)$userdetails->toArray()['id']; 

         if(empty($return_array['image']))
         {
            $return_array['image'] = url('/')."/public/img/user_signup.png";
         } else {
            $return_array['image'] = url('/public/').'/'.$userdetails->toArray()['image'];
         }

        // $chatkit = new Chatkit\Chatkit([
        //           'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
        //           'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
        //         ]);

        // $uservalue =    $chatkit->updateUser([
        //                   'id' => "user_".$userdetails->id,
        //                   'name' => $userdetails->fullname,
        //                   'avatar_url' => $return_array['image'],
        //                 ]);

          

        return $this->sendResponse($return_array, 'User Profile successfully updated',$request->path());

    }


    public function eviewUserProfile(Request $request,$id)
    {
          $userdetails = User::find($id);

        if (is_null($userdetails)) {
                return $this->sendError($request->path(),'User not found.');
            }  

            $return_array = $userdetails->toArray();

        unset($return_array['id']);
        unset($return_array['confirmpassword']);

         $return_array['user_id'] = (string)$userdetails->toArray()['id']; 

         if(empty($return_array['image']))
         {
            $return_array['image'] = url('/')."/public/img/user_signup.png";
         } else {
            $return_array['image'] = url('/public/').'/'.$userdetails->toArray()['image'];
         }


            return $this->sendResponse($return_array, 'User Profile details',$request->path());
    }

     public function savepostlist(Request $request,$id)
    {
        $input = $request->all();

        $return_arrj = array();

        $get_news_feedlist = DB::select("select posts.usertype as ignuser,posts.user_id as ignuserid, posts.id as post_id,posts.title,posts.url,posts.description,posts.created_at,posts.image as post_image from saveposts inner join posts on (posts.id = saveposts.post_id) where saveposts.user_id ='".$id."' and saveposts.usertype='".$input['usertype']."' order by post_id desc");


         foreach ($get_news_feedlist as $value) {

             $value->post_id = (string)$value->post_id;

           if($value->ignuser == 'user'){

             $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

            
            $userlist = DB::table($value->ignuser."s")->where('id',$value->ignuserid)->first();

           // print_r($value);

            if(!empty($userlist)){
           
            $value->fullname = $userlist->fullname;
            if(empty($userlist->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$userlist->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

          
               $value->speciality_name = ""; 
           


            $return_arrj[] = $value;

        }

           } else {

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

            
            $userlist = DB::table("doctors")->join("specialties","doctors.speciality_id","=","specialties.id")->where('doctors.id',$value->ignuserid)->select("doctors.fullname","doctors.image","specialties.name")->first();

           // print_r($);

            if(!empty($userlist)){
           
            $value->fullname = $userlist->fullname;
            if(empty($userlist->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$userlist->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

          
              
                
                $value->speciality_name = $userlist->name;

            


            $return_arrj[] = $value;

        }

           }

        }

        //die();

        //dd($return_array);

        



        return $this->sendResponse($return_arrj, 'Saved Post list retrieve successfully',$request->path());




    }


    public function deletepost(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'token'=>'required',
            'user_id'=>'required',
            'post_id'=>'required',
            'usertype'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if($input['usertype'] == 'user'){

                $post = User::find($input['user_id']);


                if (is_null($post)) {
                    return $this->sendError($request->path(),'User not found.');
                }

                 $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                }

                DB::table('posts')->where(['user_id'=>$input['user_id'],'id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();


        } else if($input['usertype'] == 'doctor')
        {


               $post = Doctor::find($input['user_id']);


                if (is_null($post)) {
                    return $this->sendError($request->path(),'Doctor not found.');
                }

                 $check_token = Dtoken::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

                if (empty($check_token)) {
                    return $this->sendError($request->path(),'Token Expire');
                } 

               DB::table('posts')->where(['user_id'=>$input['user_id'],'id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();
        }

        return $this->sendResponse(array('status'=>'success'), 'post deleted successfully',$request->path());


    }

    public function viewdoctorprofile(Request $request,$id)
    {
            $doctor_details = Doctor::find($id);


        if (is_null($doctor_details)) {
                return $this->sendError($request->path(),'User not found.');
            }  

            $return_array = $doctor_details->toArray();

             unset($return_array['id']);
             unset($return_array['confirmpassword']);

         $return_array['user_id'] = (string)$doctor_details->toArray()['id']; 

            if(empty($return_array['image']))
         {
            $return_array['image'] = url('/')."/public/img/user_signup.png";
         } else {
            $return_array['image'] = url('/public/').'/'.$doctor_details->toArray()['image'];
         }

         if(empty($return_array['insurance_accept']))
         {
            $return_array['insurance_accept'] = "";
         }

         $specialties_name = DB::table('specialties')->where('id',$return_array['speciality_id'])->first();

         $city_name = DB::table('cities')->where('id',$return_array['city_id'])->first();

         $return_array['city_name'] = $city_name->name;

          $return_array['specialties_name'] = $specialties_name->name;

          $return_array['avilability'] = $this->long_to_week($return_array['avilability']);

         return $this->sendResponse($return_array, 'Doctor Profile details',$request->path());


    }

     public function editdoctorprofile(Request $request,$id)
    {
                $input = $request->all();

                $val_arr = [
                    'token'=>'required',
                ];

                $validator = Validator::make($input, $val_arr);


                if($validator->fails()){
                    return $this->sendError($request->path(),$validator->errors()->first());       
                }


                $userdetails = Doctor::find($id);

                if (is_null($userdetails)) {
                        return $this->sendError($request->path(),'User not found.');
                    }

                $check_token = Dtoken::where(['user_id'=>$id,'token'=>$input['token']])->first();

                    if (empty($check_token)) {
                        return $this->sendError($request->path(),'Token Expire');
                    }    


                $name = "";

                if ($request->hasFile('image')) 
                {
                    $image = $request->file('image');
                    $name = md5($id.time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/userimage');
                    $imagePath = $destinationPath. "/".  $name;
                    $image->move($destinationPath, $name);
                    $userdetails->image = 'userimage/'.$name;
                }

                if(!empty($input['city_id'])){
                    $userdetails->city_id = $input['city_id'];
                }

                $userdetails->qualification = $input['qualification'];
                $userdetails->fee = $input['fees'];
                $userdetails->start_time = $input['start_time'];
                $userdetails->end_time = $input['end_time'];
                $userdetails->clinic = $input['clinic'];
               
                $userdetails->avilability = $this->short_to_week($input['avilability']);

                $userdetails->save();


                $return_array = $userdetails->toArray();

             unset($return_array['id']);
             unset($return_array['confirmpassword']);

             if(empty($return_array['insurance_accept']))
         {
            $return_array['insurance_accept'] = "";
         }

         $return_array['avilability'] = $this->long_to_week($return_array['avilability']);

         $return_array['user_id'] = (string)$userdetails->toArray()['id']; 

            if(empty($return_array['image']))
         {
            $return_array['image'] = url('/')."/public/img/user_signup.png";
         } else {
            $return_array['image'] = url('/public/').'/'.$userdetails->toArray()['image'];
         }

         if(empty($return_array['insurance_accept']))
         {
            $return_array['insurance_accept'] = "";
         }

         //  $chatkit = new Chatkit\Chatkit([
         //          'instance_locator' => 'v1:us1:40649b45-1597-4d9d-946d-e906813cb7f6',
         //          'key' => 'bc0f3994-d8bf-4e9a-a3d4-7a6851396c25:N/AJPda74klTU4N7mZ76/+8sfGmprAcrBvcqM2t8RMs='
         //        ]);

         // $uservalue =    $chatkit->updateUser([
         //                  'id' => "doctor_".$userdetails->id,
         //                  'name' => $userdetails->fullname,
         //                  'avatar_url' => $return_array['image'],
         //                ]);

                return $this->sendResponse($return_array, 'Doctor Profile updated successfully',$request->path());

    }

    public function short_to_week1($a)
    {
   

        $a = str_replace("Mon","M",$a);

  

        $a = str_replace("Tue","Tu",$a);

       
        $a = str_replace("Wed","W",$a);

       

        $a = str_replace("Thu","Th",$a);

     

        $a = str_replace("Fri","F",$a);

        $a = str_replace("Sat","Sa",$a);

       

        $a = str_replace("Sun","S",$a);

        return $a;
    }

    public function short_to_week($a)
    {
        $a = str_replace("Monday","M",$a);

     
        $a = str_replace("Tuesday","Tu",$a);


        $a = str_replace("Wednesday","W",$a);

      

        $a = str_replace("Thursday","Th",$a);

      

        $a = str_replace("Friday","F",$a);

      
        $a = str_replace("Saturday","Sa",$a);

        $a = str_replace("Sunday","Su",$a);


        return $a;
    }

    public function long_to_week($a)
    {
        $a = str_replace("M","Monday",$a);

        $a = str_replace("Tu","Tuesday",$a);

        $a = str_replace("W","Wednesday",$a);

        $a = str_replace("Th","Thursday",$a);

        $a = str_replace("F","Friday",$a);

        $a = str_replace("Sa","Saturday",$a);

        $a = str_replace("Su","Sunday",$a);


        return $a;
    }


    public function all_city(Request $request)
    {
        $all_name_cities = DB::table('cities')->select('id','name')->get();

        return $this->sendResponse($all_name_cities, 'City list retrieve successfully',$request->path());
    }


     public function specialtiesbycity(Request $request)
    {
        $input = $request->all();

        if(empty($input['city_id'])){
                $all_name = DB::table('specialties')->select('id','name')->whereNotIn('id',['7'])->get();
        
                foreach ($all_name as  $value) {
                    
                     $value->id = (string)$value->id;
                }
                
                return $this->sendResponse($all_name, 'Specialties list by city retrieve successfully.',$request->path());
        } else {
                $recorde_detail = DB::select("SELECT specialties.id,specialties.name FROM `doctors` inner join specialties on (specialties.id=doctors.speciality_id) where doctors.city_id='1' group by doctors.speciality_id");
                // $recorde_detail = DB::table('doctors')->join('specialties','doctors.speciality_id','=','specialties.id')->groupBy('doctors.speciality_id')->pluck('specialties.name','doctors.speciality_id');
                foreach ($recorde_detail as  $value) {
                    
                     $value->id = (string)$value->id;
                }
                return $this->sendResponse($recorde_detail, 'Specialties list by city retrieve successfully',$request->path());
        }
    }

    public function dorcname(Request $request)
    {
        $input = $request->all();

        $query_part = "";

        if(!empty($input['city_id'])){
            if(!empty($input['speciality_id'])){
                $query_part = "SELECT fullname as name,clinic as clinic_name FROM `doctors` WHERE `speciality_id`='".$input['speciality_id']."' and `city_id`='".$input['city_id']."'";
            } else {
                $query_part = "SELECT fullname as name,clinic as clinic_name FROM `doctors` WHERE `city_id`='".$input['city_id']."'";
            }
        } else {
            if(!empty($input['speciality_id'])){
                $query_part = "SELECT fullname as name,clinic as clinic_name FROM `doctors` WHERE `speciality_id`='".$input['speciality_id']."'";
            } else {
                $query_part = "SELECT fullname as name,clinic as clinic_name FROM `doctors`";
            }
        }

        $recorde_detail = DB::select($query_part);


        $return_array = array();



        foreach ($recorde_detail as $value) {
             //dd($value);
            $return_array[]['name'] = $value->name;
            $return_array[]['name'] = $value->clinic_name;
        }
                // $recorde_detail = DB::table('doctors')->join('specialties','doctors.speciality_id','=','specialties.id')->groupBy('doctors.speciality_id')->pluck('specialties.name','doctors.speciality_id');

                return $this->sendResponse($return_array, 'Doctor and clinic name list retrieve successfully',$request->path());


    }

    public function doctordetaillist(Request $request)
    {
        

        $input = $request->all();

        $query_part = "";

        if(!empty($input['city_id'])){
            if(!empty($input['speciality_id'])){
                $query_part = "doctors.`speciality_id`='".$input['speciality_id']."' and doctors.`city_id`='".$input['city_id']."'";
            } else {
                $query_part = "doctors.`city_id`='".$input['city_id']."'";
            }
        } else {
            if(!empty($input['speciality_id'])){
                $query_part = "doctors.`speciality_id`='".$input['speciality_id']."'";
            } else {
                $query_part = "";
            }
        }

        if(!empty($input['search_name'])){
            if(empty($query_part) || $query_part == ''){
                $query_part = " doctors.`fullname`='".$input['search_name']."' or doctors.`clinic`='".$input['search_name']."'";
            } else {
                $query_part = $query_part." and (doctors.`fullname`='".$input['search_name']."' or doctors.`clinic`='".$input['search_name']."')";
            }
        }

        if(!empty($input['minrange'])){
            if(empty($query_part) || $query_part == ''){
                $query_part = " doctors.`fee` between ".$input['minrange']." and ".$input['maxrange']."";
            } else {
                $query_part = $query_part." and (doctors.`fee` between ".$input['minrange']." and ".$input['maxrange'].")";
            }
        }

        $match_insurance1new = array();

        if(!empty($input['insurance'])){
                $insurance_list = Insurance::get();

                $match_insurance1 = explode(",", $input['insurance']);

                foreach ($insurance_list as $key => $value) {
                     if(in_array($value->name,$match_insurance1)){
                        $match_insurance1new[] = $value->id;
                     }
                }
        }

        //dd($match_insurance1new);

        if(!empty($query_part)){
            $query_part = " where ".$query_part;
        }

      

        $recorde_detail = DB::select("SELECT doctors.insurance_accept as insurance_accepting,doctors.fee as fees,doctors.fullname,doctors.id as user_id,doctors.clinic,doctors.avilability,doctors.image,specialties.name as speciality_name,cities.name as city_name FROM `doctors` inner join cities on (cities.id=doctors.city_id) inner JOIN specialties on (specialties.id=doctors.speciality_id)".$query_part);

        $recorde_detailstart = array();

        foreach ($recorde_detail as $value) {

            $new_match_array = sizeof(array_intersect($match_insurance1new, explode(",", $value->insurance_accepting)));

            if(empty($value->insurance_accepting)){
                $value->insurance_accepting = "";
            }
            
            $value->user_id = (string)$value->user_id;

            if(empty($value->image)){
                    $value->image = url('/')."/public/img/user_signup.png";
                } else {
                    $value->image = url('/').'/public/'.$value->image;
                }

                $value->rating = $this->avgrating($value->user_id);
                
                if(empty($value->avilability)){
                    $value->avilability =array();
                } else {
                    $value->avilability = array_map('trim',explode(",", $value->avilability));
                }

                 

            if(!empty($input['insurance'])){

                  if($new_match_array > 0){
                    if(!empty($input['rating'])){
                        if($input['rating']*20 == $value->rating){
                             $recorde_detailstart[] = $value;  
                        }
                    } else {
                         $recorde_detailstart[] = $value;
                    }
                  }
                    
            } else {
                   if(!empty($input['rating'])){
                        if($input['rating']*20 == $value->rating){
                             $recorde_detailstart[] = $value;  
                        }
                    } else {
                         $recorde_detailstart[] = $value;
                    }
                    
            }
        }
                // $recorde_detail = DB::table('doctors')->join('specialties','doctors.speciality_id','=','specialties.id')->groupBy('doctors.speciality_id')->pluck('specialties.name','doctors.speciality_id');

        $insurance_accept_name = Insurance::pluck('name');

        $maxfee = DB::select('SELECT fee FROM `doctors` group by fee');



        $response_final = array("doctorlist"=>$recorde_detailstart,"minrange"=>min($maxfee)->fee,"maxrange"=>max($maxfee)->fee,"insurance_accept_list"=>$insurance_accept_name);

                return $this->sendResponse($response_final, 'Doctors list retrieve successfully',$request->path());


    }


    public function fulldoctordetails(Request $request,$id)
    {
        $recorde_detail = DB::select("SELECT doctors.phone,doctors.fee as fees,doctors.fullname,doctors.id as user_id,doctors.clinic,doctors.avilability,doctors.image,specialties.name as speciality_name,cities.name as city_name,doctors.expertise_area,doctors.insurance_accept,doctors.qualification,doctors.start_time,doctors.end_time FROM `doctors` inner join cities on (cities.id=doctors.city_id) inner JOIN specialties on (specialties.id=doctors.speciality_id) where doctors.id='".$id."'");
        
            
            $recorde_detail[0]->user_id = (string)$recorde_detail[0]->user_id;

           
            if(empty($recorde_detail[0]->image)){
                $recorde_detail[0]->image = url('/')."/public/img/user_signup.png";
            } else {
               $recorde_detail[0]->image = url('/').'/public/'.$recorde_detail[0]->image;
            }

            if(empty($recorde_detail[0]->insurance_accept)){
                $recorde_detail[0]->insurance_accept = array();
            } else {
                $insurance_array_accept = array();

                $exp_array = explode(",", $recorde_detail[0]->insurance_accept);

                foreach ($exp_array as $value_name) {
                      $exp_array_reco = Insurance::whereId($value_name)->first();
                      
                      if(!empty($exp_array_reco)){
                        $insurance_array_accept[] = $exp_array_reco->name;
                      }

                      
                }

                 $recorde_detail[0]->insurance_accept = $insurance_array_accept;
            }

            if(empty($recorde_detail[0]->avilability))
            {
                 $recorde_detail[0]->avilability = [];
            } else {
                 $recorde_detail[0]->avilability = array_map('trim',explode(",", $recorde_detail[0]->avilability));
            }

         
            if($recorde_detail[0]->start_time != '-')
            {
                $recorde_detail[0]->start_time = $this->changetime($recorde_detail[0]->start_time);
            }

            if($recorde_detail[0]->end_time != '-')
            {
                $recorde_detail[0]->end_time = $this->changetime($recorde_detail[0]->end_time);
            }

            $global_rate = 0;

            $review_details = DB::select("SELECT users.image,doctorratings.rating*20 as rating,doctorratings.review,users.fullname,doctorratings.created_at FROM `doctorratings` INNER JOIN users on (users.id=doctorratings.user_id) where doctorratings.doctor_id='".$id."' order by doctorratings.id desc");

            foreach ($review_details as $value) {
                  if(empty($value->image)){
                        $value->image = url('/')."/public/img/user_signup.png";
                    } else {
                       $value->image = url('/').'/public/'.$value->image;
                    }

                    $value->created_at = Carbon::parse($value->created_at)->diffForHumans();

                     $global_rate = $global_rate + $value->rating;
            }

            $recorde_detail[0]->rating = $this->avgrating($id);

            $recorde_detail[0]->num_of_review = (string)sizeof($review_details);

            $recorde_detail[0]->review_list = $review_details;

            return $this->sendResponse($recorde_detail[0], 'Doctors details retrieve successfully',$request->path());


    }


    public function submitReview(Request $request)
    {
        $input = $request->all();

          $val_arr = [
            'token'=>'required',
            'user_id'=>'required',
            'doctor_id'=>'required',
            'rating'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if(empty($input['review'])){
            $input['review'] = "";
        }

        $post = User::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'User not found.');
        }

         $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }

         $check_already_rate = Doctorrating::where(["doctor_id"=>$input['doctor_id'],"user_id"=>$input['user_id']])->first();

        if(!empty($check_already_rate)){
                 return $this->sendError($request->path(),"You Already Rate This Doctor");    
        }

        Doctorrating::create($input);

        return $this->sendResponse(array('status'=>'success'), 'Doctors review submited successfully',$request->path());


    }


    public function reasonlist(Request $request)
    {
        $reasonlist = DB::table('reasons')->get();
        
        

        $return_array = array();

        foreach ($reasonlist as  $value) {
            
           // dd($value);
                
                $return_array[] = array("reason_id"=>(string)$value->id,"name"=>(string)$value->name);
        }

        return $this->sendResponse($return_array, 'Reason list retrieve successfully',$request->path());

    }

    public function reasonSubmit(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'token'=>'required',
            'user_id'=>'required',
            'doctor_id'=>'required',
            'reason_id'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $post = User::find($input['user_id']);


        if (is_null($post)) {
            return $this->sendError($request->path(),'User not found.');
        }

         $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Token Expire');
        }

        Cancellation::create($input);

        return $this->sendResponse(array('status'=>'success'), 'Cancellation submited successfully',$request->path());


    }


    public function changetime($newtext)
    {
        if($newtext == '00')
        {
            return "12 AM";
        } else {
            $newtext1 = fmod($newtext,12);

            $newtext12 = (int)($newtext/12);

           

           if($newtext12 == '0'){
                 return $newtext1." AM";
           } else {
                 return $newtext1." PM";
           }
        }


    }


     

    
   
}
