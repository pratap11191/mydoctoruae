<?php

namespace App\Http\Controllers\API;

use Illuminate\Pagination\LengthAwarePaginator;
use Propaganistas\LaravelPhone\LaravelPhoneServiceProvider;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\User;
use App\Token;
use Validator;
use Image;
use Intervention\Image\ImageServiceProvider;
use DB;
use App\Video;
use App\Hashtag;
use App\Category;
use App\Follower;
use Illuminate\Support\Facades\Input;
use App\Rating;
use App\Mail\SendMailable;
USE App\Fametalerating;
use Mail;
use Illuminate\Support\Facades\Crypt;
use App\Doctor;
use App\Dtoken;
use Session;
use App\Specialty;
use App\Post;
use App\Like;
use App\Savepost;
use Carbon\Carbon;
use App\Comment;
use App\Insurance;
use App\Doctorrating;
use App\Notification;
use App\Booking;
use App\Subscription;
use App\Plan;

class HomeAPIController extends APIBaseController
{
    //

    public function index()
    {
        $postlist = $this->newsfeedlist();

        $all_name_cities = DB::table('cities')->select('id','name')->get()->toArray();

        $all_name = DB::table('specialties')->select('id','name','img')->where('remark','1')->orderBy("name","ASC")->take(8)->get();
        
        foreach ($all_name as  $value) {
             $value->img = url('/').$value->img;
        }

        $all_name = $all_name->toArray();

        return view('index',compact('postlist','all_name_cities','all_name'));
    }
    
    
    
     public function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('insurances')
        ->where('name', 'LIKE', "%{$query}%")
        ->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '
       <li><a href="#">'.$row->name.'</a></li>
       ';
      }
      $output .= '</ul>';
      echo $output;
     }
    }




    public function privacyandpolicy(Request $request)
    {
        return view('privacyandpolicy');
    }

    // public function thanks(Request $request)
    // {
    //     return view('thanks');
    // }
    
     public function paymentpayout(Request $request)
    {
        return view('paymentpayout');
    }
    
    public function termsandcondition(Request $request)
    {
        return view('termsandcondition');
    }
    
    public function gettheapp(Request $request)
    {
        return view('gettheapp');
    }
    
    public function testai(Request $request)
    {
        return view('test');
    }

    public function chatOn(Request $request)
    {
        return view("chatOn");
    }

    public function bookingListdoctor()
    {
        return view('bookingdoctor_listing');
    }

    public function bookingListuser()
    {
         if(SESSION::get('userdetails', 'default') == 'default'){

                return redirect('userlogin');
        } else {

                return view('bookinglistuser');

        }
    }



    public function issuelistpage(Request $request)
    {
         $all_name = DB::table('specialties')->select('id','name','img')->where('remark','1')->orderBy("name","ASC")->get();
        
        foreach ($all_name as  $value) {
             $value->img = url('/').$value->img;
        }

        $all_name = $all_name->toArray();


         $currentPage = LengthAwarePaginator::resolveCurrentPage();

         $itemCollection = collect($all_name);
 
        // Define how many items we want to be visible in each page
        $perPage = 12;
 
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
 
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
 
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('issuelist',['items' => $paginatedItems]);
    }

    public function clearallsesion(Request $request)
    {
      $request->session()->forget('city_id');
      $request->session()->forget('search_name');
      $request->session()->forget('speciality_id');
      $request->session()->forget('insurance');
      $request->session()->forget('rating');
      $request->session()->forget('maxfees');

      return redirect('listing');
    }

    public function gloabalsearch(Request $request)
    {
        $this->clearallsesion($request);

        $input = $request->all();

        $KEY = $input["searchkey"];

        $city_list_id = DB::table('cities')->where('name','like',"%{$KEY}%")->first();

        $specialties_list = DB::table('specialties')->where('name','like',"%{$KEY}%")->first(); 



       // dd($specialties_list);

        if(!empty($city_list_id)){
            $request->session()->put('city_id',$city_list_id->id);
        }

        if(!empty($specialties_list)){
            $request->session()->put('speciality_id',$specialties_list->id);
        }  

        $request->session()->put('search_name',$input['searchkey']);
        

        return $this->filterlsting1($request);
        
    }

    public function filterlsting1(Request $request)
    {
      $city_list = DB::table('cities')->select('id','name')->get()->toArray();
      $specialties_list = DB::table('specialties')->select('id','name')->get()->toArray(); 
      $name_list = DB::table('doctors')->select('id','fullname')->get()->toArray();
      $clinic_list = DB::table('doctors')->select('id','clinic as fullname')->get()->toArray();

      $name_list1 = array_merge($name_list,$clinic_list);

       $input = $request->all();

       $query_part = "";

       // if(!empty($input)){
       //     //dd($input);
       // }

       if(!empty($input['city_id']))
       {
           //dd($input['city_id']);
                 $request->session()->put('city_id',$input['city_id']);
           
       } 

       if(!empty($input['search_name']))
       {
            
                 $request->session()->put('search_name',$input['search_name']);
           
            
       }

       if(!empty($input['speciality_id']))
       {
           
                 $request->session()->put('speciality_id',$input['speciality_id']);
            
            
       } 

       $match_insurance1new = array();

       if(!empty($input['insurance']))
       {
            $insurance_list = Insurance::get();

            $match_insurance1 = explode(",", $input['insurance']);

            foreach ($insurance_list as $key => $value) {
                 if(in_array($value->name,$match_insurance1)){
                    $match_insurance1new[] = $value->id;
                 }
            }
            $request->session()->put('insurance',$input['insurance']);
       }

       if(!empty($input['rating']))
       {
              $request->session()->put('rating',$input['rating']);
       }

       if(!empty($input['maxfees']))
       {
            $request->session()->put('maxfees',$input['maxfees']);
       }

       

        if(!empty($request->session()->get('city_id')) && $request->session()->get('city_id') != '100' ){
            if(!empty($request->session()->get('speciality_id')) && $request->session()->get('speciality_id') != '100'){
                $query_part = "doctors.`speciality_id`='".$request->session()->get('speciality_id')."' or doctors.`city_id`='".$request->session()->get('city_id')."'";

                
            } else {
                $query_part = "doctors.`city_id`='".$request->session()->get('city_id')."'";
            }

             
             
        } else {
            if(!empty($request->session()->get('speciality_id')) && $request->session()->get('speciality_id') != '100'){
                $query_part = "doctors.`speciality_id`='".$request->session()->get('speciality_id')."'";
                 
            } else {
                $query_part = "";
            }
        }

        //dd($query_part);

        if(!empty($request->session()->get('search_name')) && $request->session()->get('search_name') != '100'){
            if(empty($query_part) || $query_part == ''){
                $query_part = " doctors.`fullname` like '%".$request->session()->get('search_name')."%' or doctors.`clinic` like '%".$request->session()->get('search_name')."%'";
            } else {
                $query_part = $query_part." or (doctors.`fullname` like '%".$request->session()->get('search_name')."%' or doctors.`clinic` like '%".$request->session()->get('search_name')."%')";
            }

             
        }

          if(!empty($request->session()->get('maxfees'))){
            if(empty($query_part) || $query_part == ''){
                $query_part = " doctors.`fee` < ".$request->session()->get('maxfees');
            } else {
                $query_part = $query_part." and doctors.`fee` < ".$request->session()->get('maxfees');
            }

             
        }

        if(!empty($query_part)){
            $query_part = " where ".$query_part;
        }

        $recorde_detail = DB::select("SELECT doctors.insurance_accept as insurance_accepting,doctors.fee,doctors.fullname,doctors.id as user_id,doctors.clinic,doctors.avilability,doctors.image,specialties.name as speciality_name,cities.name as city_name FROM `doctors` inner join cities on (cities.id=doctors.city_id) inner JOIN specialties on (specialties.id=doctors.speciality_id)".$query_part);

         $recorde_detailstart = array();

        foreach ($recorde_detail as $value) {
            //dd($value->image);
            if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                $value->image = url('/').'/public/'.$value->image;
            }

 

            $new_match_array = sizeof(array_intersect($match_insurance1new, explode(",", $value->insurance_accepting)));

            $value->rating = $this->avg_rating($value->user_id);


            if(!empty($request->session()->get('insurance')) && $request->session()->get('insurance') != '100'){

                  if($new_match_array > 0){
                    if(!empty($request->session()->get('rating'))){
                        if($request->session()->get('rating')*20 == $value->rating){
                             $recorde_detailstart[] = $value;  
                        }
                    } else {
                         $recorde_detailstart[] = $value;
                    }
                  }
                    
            } else {
                   if(!empty($request->session()->get('rating'))){
                        if($request->session()->get('rating')*20 == $value->rating){
                             $recorde_detailstart[] = $value;  
                        }
                    } else {
                         $recorde_detailstart[] = $value;
                    }
                    
            }


        }



        $currentPage = LengthAwarePaginator::resolveCurrentPage();

             $itemCollection = collect($recorde_detailstart);
     
            // Define how many items we want to be visible in each page
            $perPage = 6;
     
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
     
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
     
            // set url path for generted links
            $paginatedItems->setPath($request->url());

            $insurance_accept_name = Insurance::pluck('name');

        $maxfee = DB::select('SELECT fee FROM `doctors` group by fee');



        if(!empty($maxfee)){
             $minrange = min($maxfee)->fee;

            $maxrange = max($maxfee)->fee;
        } else {
             $minrange = 0;

            $maxrange = 0;
        }

       

      return view('filter_listing',['items' => $paginatedItems,'city_list'=>$city_list,'specialties_list'=>$specialties_list,'name_list1'=>$name_list1,'minrange'=>$minrange,'maxrange'=>$maxrange,'insurance_accept_name'=>$insurance_accept_name]);
    }

    public function filterlsting(Request $request)
    {
      $city_list = DB::table('cities')->select('id','name')->get()->toArray();
      $specialties_list = DB::table('specialties')->select('id','name')->get()->toArray(); 
      $name_list = DB::table('doctors')->select('id','fullname')->get()->toArray();
      $clinic_list = DB::table('doctors')->select('id','clinic as fullname')->get()->toArray();

      $name_list1 = array_merge($name_list,$clinic_list);

       $input = $request->all();

       $query_part = "";

       // if(!empty($input)){
       //     //dd($input);
       // }

       if(!empty($input['city_id']))
       {
           //dd($input['city_id']);
                 $request->session()->put('city_id',$input['city_id']);
           
       } 

       if(!empty($input['search_name']))
       {
            
                 $request->session()->put('search_name',$input['search_name']);
           
            
       }

       if(!empty($input['speciality_id']))
       {
           
                 $request->session()->put('speciality_id',$input['speciality_id']);
            
            
       } 

       $match_insurance1new = array();

       if(!empty($input['insurance']))
       {
            $insurance_list = Insurance::get();

            $match_insurance1 = explode(",", $input['insurance']);

            foreach ($insurance_list as $key => $value) {
                 if(in_array($value->name,$match_insurance1)){
                    $match_insurance1new[] = $value->id;
                 }
            }
            $request->session()->put('insurance',$input['insurance']);
       }

       if(!empty($input['rating']))
       {
              $request->session()->put('rating',$input['rating']);
       }

       if(!empty($input['maxfees']))
       {
            $request->session()->put('maxfees',$input['maxfees']);
       }

       

        if(!empty($request->session()->get('city_id')) && $request->session()->get('city_id') != '100' ){
            if(!empty($request->session()->get('speciality_id')) && $request->session()->get('speciality_id') != '100'){
                $query_part = "doctors.`speciality_id`='".$request->session()->get('speciality_id')."' and doctors.`city_id`='".$request->session()->get('city_id')."'";

                
            } else {
                $query_part = "doctors.`city_id`='".$request->session()->get('city_id')."'";
            }

             
             
        } else {
            if(!empty($request->session()->get('speciality_id')) && $request->session()->get('speciality_id') != '100'){
                $query_part = "doctors.`speciality_id`='".$request->session()->get('speciality_id')."'";
                 
            } else {
                $query_part = "";
            }
        }

        //dd($query_part);

        if(!empty($request->session()->get('search_name')) && $request->session()->get('search_name') != '100'){
            if(empty($query_part) || $query_part == ''){
                $query_part = " doctors.`fullname` like '%".$request->session()->get('search_name')."%' or doctors.`clinic` like '%".$request->session()->get('search_name')."%'";
            } else {
                $query_part = $query_part." and (doctors.`fullname` like '%".$request->session()->get('search_name')."%' or doctors.`clinic` like '%".$request->session()->get('search_name')."%')";
            }

             
        }

          if(!empty($request->session()->get('maxfees'))){
            if(empty($query_part) || $query_part == ''){
                $query_part = " doctors.`fee` < ".$request->session()->get('maxfees');
            } else {
                $query_part = $query_part." and doctors.`fee` < ".$request->session()->get('maxfees');
            }

             
        }

        if(!empty($query_part)){
            $query_part = " where ".$query_part;
        }

        $recorde_detail = DB::select("SELECT doctors.insurance_accept as insurance_accepting,doctors.fee,doctors.fullname,doctors.id as user_id,doctors.clinic,doctors.avilability,doctors.image,specialties.name as speciality_name,cities.name as city_name FROM `doctors` inner join cities on (cities.id=doctors.city_id) inner JOIN specialties on (specialties.id=doctors.speciality_id)".$query_part);

         $recorde_detailstart = array();

        foreach ($recorde_detail as $value) {
            //dd($value->image);
            if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                $value->image = url('/').'/public/'.$value->image;
            }

 

            $new_match_array = sizeof(array_intersect($match_insurance1new, explode(",", $value->insurance_accepting)));

            $value->rating = $this->avg_rating($value->user_id);


            if(!empty($request->session()->get('insurance')) && $request->session()->get('insurance') != '100'){

                  if($new_match_array > 0){
                    if(!empty($request->session()->get('rating'))){
                        if($request->session()->get('rating')*20 == $value->rating){
                             $recorde_detailstart[] = $value;  
                        }
                    } else {
                         $recorde_detailstart[] = $value;
                    }
                  }
                    
            } else {
                   if(!empty($request->session()->get('rating'))){
                        if($request->session()->get('rating')*20 == $value->rating){
                             $recorde_detailstart[] = $value;  
                        }
                    } else {
                         $recorde_detailstart[] = $value;
                    }
                    
            }


        }



        $currentPage = LengthAwarePaginator::resolveCurrentPage();

             $itemCollection = collect($recorde_detailstart);
     
            // Define how many items we want to be visible in each page
            $perPage = 6;
     
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
     
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
     
            // set url path for generted links
            $paginatedItems->setPath($request->url());

            $insurance_accept_name = Insurance::pluck('name');

        $maxfee = DB::select('SELECT fee FROM `doctors` group by fee');



        if(!empty($maxfee)){
             $minrange = min($maxfee)->fee;

            $maxrange = max($maxfee)->fee;
        } else {
             $minrange = 0;

            $maxrange = 0;
        }

       

      return view('filter_listing',['items' => $paginatedItems,'city_list'=>$city_list,'specialties_list'=>$specialties_list,'name_list1'=>$name_list1,'minrange'=>$minrange,'maxrange'=>$maxrange,'insurance_accept_name'=>$insurance_accept_name]);
    }

    public function avg_rating($userid)
    {
        $all_details = Doctorrating::whereDoctorId($userid)->sum('rating');

        $all_details_count = Doctorrating::whereDoctorId($userid)->count();

        if($all_details_count == '0'){
            return 0;
        } else {
            return ($all_details/$all_details_count)*20;
        }

        
    }

    public function plan_map(Request $request,$id)
    {
       
        $input['sub_id'] =  $id;

        $input['doctor_id'] = SESSION::get('userdetails', 'default')->id;

        $listdetails = Subscription::where('id',$input['sub_id'])->first();

        if(!empty($listdetails)){
            $input['exp_date'] = Carbon::now()->addDays($listdetails->validity);

            $checkdoctorinplan = DB::table('plans')->where('doctor_id',$input['doctor_id'])->first();

            if(!empty($checkdoctorinplan)){
                $checkdoctorinplan->sub_id = $input['sub_id'];
                $checkdoctorinplan->exp_date = $input['exp_date'];
                $checkdoctorinplan->save();
            } else {
                Plan::create($input);
            }

           return back();
        } else {
            return back();
        }

    }

    public function subscription()
    {

         if(SESSION::get('userdetails', 'default') == 'default'){

              return redirect('userlogin');
        } else {

        $id = SESSION::get('userdetails', 'default')->id; 

       $listdetails = Subscription::get();

        $checkdoctorinplan = DB::table('plans')->where('doctor_id',$id)->first();

        $return_array = array();

        if(!empty($checkdoctorinplan) && Carbon::now()->diffInSeconds($checkdoctorinplan->exp_date) > 0)
        {
            foreach ($listdetails as  $value) {
                if($value->id == $checkdoctorinplan->sub_id){
                    $value->flag_plan = "1";
                } else {
                    $value->flag_plan = "0";
                }

                $value->id = (string)$value->id;

                $value->exp_date = $checkdoctorinplan->exp_date;

                $return_array[] = $value;
            }
        } else {
            foreach ($listdetails as  $value) {
                $value->flag_plan = "-1";

                $value->id = (string)$value->id;

                $return_array[] = $value;
            }
        }

        return view('subscription',compact('listdetails'));

        }
    }

    public function usermore(Request $request)
    {
         if(SESSION::get('userdetails', 'default') == 'default'){

              return redirect('userlogin');
        } else {

                $savepostlist = $this->savepostlist();

                $currentPage = LengthAwarePaginator::resolveCurrentPage();

                 $itemCollection = collect($savepostlist);
         
                // Define how many items we want to be visible in each page
                $perPage = 9;
         
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
         
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
         
                // set url path for generted links
                $paginatedItems->setPath($request->url());

                return view('usermore',['items' => $paginatedItems]);

        }
    }

    public function doctormore(Request $request)
    {
         if(SESSION::get('userdetails', 'default') == 'default'){

              return redirect('doctorlogin');
        } else {
                $savepostlist = $this->savepostlist();

                $currentPage = LengthAwarePaginator::resolveCurrentPage();

                 $itemCollection = collect($savepostlist);
         
                // Define how many items we want to be visible in each page
                $perPage = 9;
         
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
         
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
         
                // set url path for generted links
                $paginatedItems->setPath($request->url());

                return view('doctormore',['items' => $paginatedItems]);
        }
    }

     public function savepostlist()
    {
         $return_arrj = array();
        
         if(SESSION::get('userdetails', 'default') == 'default'){

            $id = "";
            $input['usertype'] = "user";
        } else {
              $id = SESSION::get('userdetails', 'default')->id;
            $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;
        }

       
            $get_news_feedlist = DB::select("select posts.usertype as ignuser,posts.user_id as ignuserid, posts.id as post_id,posts.title,posts.url,posts.description,posts.created_at from saveposts inner join posts on (posts.id = saveposts.post_id) where saveposts.user_id ='".$id."' and saveposts.usertype='".$input['usertype']."' order by post_id desc");


         foreach ($get_news_feedlist as $value) {


          if($value->ignuser == 'user'){

             $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

            
            $userlist = DB::table($value->ignuser."s")->where('id',$value->ignuserid)->first();

           // print_r($value);

            if(!empty($userlist)){
           
            $value->fullname = $userlist->fullname;
            if(empty($userlist->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$userlist->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

          
               $value->speciality_name = ""; 
           


            $return_arrj[] = $value;

        }

           } else {

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

            
            $userlist = DB::table("doctors")->join("specialties","doctors.speciality_id","=","specialties.id")->where('doctors.id',$value->ignuserid)->select("doctors.fullname","doctors.image","specialties.name")->first();

           // print_r($);

            if(!empty($userlist)){
           
            $value->fullname = $userlist->fullname;
            if(empty($userlist->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$userlist->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

          
              
                
                $value->speciality_name = $userlist->name;

            


            $return_arrj[] = $value;

        }

           }

        
           
        }

        //dd("");
        return $return_arrj;

    }

    public function likepost($post_id)
    {
       
        if(SESSION::get('userdetails', 'default') == 'default'){

                echo url('/userlogin').'_'."0";

                die();

        } else {
            $input['user_id'] = SESSION::get('userdetails', 'default')->id;
            $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;
            $input['post_id'] = Crypt::decryptString($post_id);

        $like_recorde = DB::table('likes')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->first();

        if(empty($like_recorde))
        {
                if($input['usertype'] == 'user')
        {
            $post = User::find($input['user_id']);


            if (is_null($post)) {
                

                echo "User not found."."_"."1";
                die();
            }


            $like_colmn = Like::create($input);
             
            echo "Post liked successfully!"."_"."1"."_"."1";
                die();


        } else if($input['usertype'] == 'doctor')
        {
           $post = Doctor::find($input['user_id']);


            if (is_null($post)) {
               
                
                 echo "Doctor not found!!"."_"."1";
                die();
            }

            $like_colmn = Like::create($input);  

             echo "Post liked successfully!"."_"."1"."_"."1";
                die();
        }
        } else {
            
            DB::table('likes')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();

           
                 echo "Post unliked successfully!"."_"."1"."_"."0";
                die();
            
        }

        }


    }

    public function savepost($post_id)
    {
        if(SESSION::get('userdetails', 'default') == 'default'){

                echo url('/userlogin').'_'."0";

                die();

        } else {
            $input['user_id'] = SESSION::get('userdetails', 'default')->id;
            $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;
            $input['post_id'] = Crypt::decryptString($post_id);

        $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->first();

        if(empty($saveposts_recorde))
        {
            if($input['usertype'] == 'user')
            {
                $post = User::find($input['user_id']);


                if (is_null($post)) {
                  echo "User not found."."_"."1";
                die();
                }

                $Savepost_colmn = Savepost::create($input);
                echo "Post save successfully!"."_"."1"."_"."1";
                die();


            } else if($input['usertype'] == 'doctor')
            {
               $post = Doctor::find($input['user_id']);


                if (is_null($post)) {
                   
                    echo "Doctor not found!!"."_"."1";
                    die();
                }

                $Savepost_colmn = Savepost::create($input);  

                    echo "Post save successfully!"."_"."1"."_"."1";
                    die();
            }
        } else {

            DB::table('saveposts')->where(['user_id'=>$input['user_id'],'post_id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();


            
                 echo "Post unsave successfully!"."_"."1"."_"."0";
                die();
           
        }


      }

        


    }

    public function newsfeedlist()
    {
        
        if(SESSION::get('userdetails', 'default') == 'default'){

            $input['user_id'] = "";
            $input['usertype'] = "user";
        } else {
             $input['user_id'] = SESSION::get('userdetails', 'default')->id;
            $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;
        }
  

        $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.`image` as post_image,'doctor' as usertype FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' 
union all
SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image` as user_image,`posts`.`id` as post_id, '' as speciality_name,`posts`.`image` as post_image,'user' as usertype FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user') as x order by x.post_id desc");

        

        foreach ($get_news_feedlist as $value) {

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

            $like_recorde = DB::table('likes')->where(['user_id'=>$input['user_id'],'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($like_recorde))
            {
                $value->likeflag = "0";
            } else {
                $value->likeflag = "1";
            }

            $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>$input['user_id'],'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($saveposts_recorde))
            {
                $value->saveflag = "0";
            } else {
                $value->saveflag = "1";
            }
        }

        $get_news_feedlist;
       // unset($return_array['id']);
        //unset($return_array['image']);
       // $return_array['post_id'] = (string)$get_news_feedlist['id'];
        


       return $get_news_feedlist;
    }


     public function newsfeedlist12()
    {
  

        $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.`image` as post_image,'doctor' as usertype FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' 
union all
SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image` as user_image,`posts`.`id` as post_id, '' as speciality_name,`posts`.`image` as post_image,'user' as usertype FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user') as x order by x.post_id desc");

        

        foreach ($get_news_feedlist as $value) {

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }
        }

        //$get_news_feedlist;
       // unset($return_array['id']);
        //unset($return_array['image']);
       // $return_array['post_id'] = (string)$get_news_feedlist['id'];
        


       return json_encode($get_news_feedlist);
    }


    public function newsfeedlist_ui(Request $request)
    {
        $postlist = $this->newsfeedlist();

        $currentPage = LengthAwarePaginator::resolveCurrentPage();

         $itemCollection = collect($postlist);
 
        // Define how many items we want to be visible in each page
        $perPage = 9;
 
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
 
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
 
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('newsfeedlist',['items' => $paginatedItems]);
    }


   public function specialty(Request $request)
   {
        $all_name = DB::table('specialties')->select('id','name')->where('remark','1')->orderBy("name","ASC")->get();
        
         foreach($all_name as $value){
            $value->id = (string)$value->id;
        }
        
        $retrun_array['specialties'] = $all_name;

        return $this->sendResponse($retrun_array, 'Specialties retrieve successfully.',$request->path());
   }

   public function issuelist(Request $request)
   {
        $all_name = DB::table('specialties')->select('id','name','img')->where('remark','1')->orderBy("name","ASC")->get();
        
        foreach ($all_name as  $value) {
             $value->img = url('/').$value->img;
             $value->id = (string)$value->id;
        }

        return $this->sendResponse($all_name, 'Issue list retrieve successfully.',$request->path());
   }

   public function specialty_all(Request $request)
   {
        $all_name = DB::table('specialties')->select('id','name')->whereNotIn('id',['7'])->get();
        
       
        
        $retrun_array['specialties'] = $all_name;

        return $this->sendResponse($retrun_array, 'Specialties list retrieve successfully.',$request->path());
   }

  
   
   public function forgotpass(Request $request)
   {
            $input = $request->all();

            $validator = Validator::make($input, [
                'email'=>'required',
                'usertype'=>'required',
            ]);


            if($validator->fails()){
                return $this->sendError($request->path(),$validator->errors()->first());       
            }


            if($input['usertype'] == 'user'){

              $details = User::whereEmail($input['email'])->first();

          //dd($details);

            if (empty($details)) {
                return $this->sendError($request->path(),"Email id does not exist with us!");
            }else{

             $otp = rand(100000,999999);

              $details->password = $otp;
         
              $details->save();  
        
                 $email   = $input['email'];
                 $subject = "New Password";
                 
                $postData ="";
                 try{
                     Mail::send('emails.forgotPassword', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('support@mobulous.co.in', 'Healthapp');
                                  $message->to($email, 'Healthapp')->subject('New Password Request');
                                });

                     return $this->sendResponse(['status'=>"success"],'User mail sent successfully',$request->path());

                 }
                 catch(Exception $e){
                                    return $this->sendResponse(['status'=>"success"],'User mail sent successfully',$request->path());

                                }   
                


    


            }     

            } else if($input['usertype'] == 'doctor') {

              $details = Doctor::whereEmail($input['email'])->first();

          //dd($details);

            if (empty($details)) {
                return $this->sendError($request->path(),"Email id does not exist with us!");
            }else{

             $otp = rand(100000,999999);

            
              $details->password = $otp;
         
              $details->save();  
       
        
                 $email   = $input['email'];
                 $subject = "New Password";
                 
                $postData ="";
                 try{
                     Mail::send('emails.forgotPassword', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('support@mobulous.co.in', 'Healthapp');
                                  $message->to($email, 'Healthapp')->subject('New Password Request');
                                });

                     return $this->sendResponse(['status'=>"success"],'User mail sent successfully',$request->path());

                 }
                 catch(Exception $e){
                                    return $this->sendResponse(['status'=>"success"],'User mail sent successfully',$request->path());

                                }   
                


    


            }     

            }

            
   
   }

   public function forgotpassweb(Request $request)
   {
       if ($request->isMethod('post')){

            $input = $request->all();

        $val_arr = [
            'email'=>'required',
        ];

        $message = [
            'email.required'=>'Please enter Email ID to continue',
        ];

        $validator = Validator::make($input, $val_arr,$message);


        if($validator->fails()){

          Session::flash('message_forgot', $validator->errors()->first()); 

                  unset($_POST);

               return back();
              
        }

        $check_mail = DB::select("select * from (SELECT `email`,`password` FROM `doctors`
UNION ALL
select `email`,`password` FROM `users`) as x where x.email = '".$input['email']."'");

        if(empty($check_mail)){
           Session::flash('message_forgot', "Please enter valid Email ID"); 

                  unset($_POST);

               return back();
        }

        

        $email   = $input['email'];



                 $subject = "Your Password";
                 
                $postData ="";
                 try{
                     Mail::send('emails.forgotPassword', ['otp' =>$check_mail[0]->password], function($message) use ($postData,$email)
                                {
                                  $message->from('support@mobulous.co.in', 'Healthapp');
                                  $message->to($email, 'Healthapp forgot password')->subject('Your Password');
                                });

                      Session::flash('message_forgot', "mail sent successfully"); 

                  unset($_POST);

               return back();

                 }
                 catch(Exception $e){
                                     Session::flash('message_forgot',"Mail sent successfully"); 

                  unset($_POST);

               return back();

                                } 


       } else {
           return view('forgot');
       }
      
       
   
   }

   public function postpage(Request $request,$id)
   {

      $post_id = Crypt::decryptString($id);

      $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.`image` as post_image,'doctor' as usertype FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' 
union all
SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image` as user_image,`posts`.`id` as post_id, '' as speciality_name,`posts`.`image` as post_image,'user' as usertype FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user') as x where x.post_id = '".$post_id."'");

       $return_array1 = DB::select("select * from(SELECT comments.id as comment_id,users.image,users.fullname,users.email,comments.message,comments.created_at FROM `comments` inner join `users` on (users.id = comments.user_id) where comments.`usertype` = 'user' and comments.post_id ='".$post_id."'
            union ALL
            SELECT comments.id as comment_id,doctors.image,doctors.fullname,doctors.email,comments.message,comments.created_at FROM `comments` inner join `doctors` on (doctors.id = comments.user_id) where comments.`usertype` = 'doctor' and comments.post_id ='".$post_id."') as x order by x.comment_id DESC");


         foreach ($return_array1 as $value) {
             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

             $value->created_at = Carbon::parse($value->created_at)->diffForHumans();
        }


      $retrun_array = $get_news_feedlist[0];

      $randomlist = Post::inRandomOrder()->take(5)->get()->toArray();

      //dd($randomlist);
      
      return view('postdescription',compact('retrun_array','return_array1','randomlist'));
   }


   public function postnews(Request $request)
    {
       
        $input = $request->all();

      $input['user_id'] = SESSION::get('userdetails', 'default')->id;
      $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;

        if(empty($input['url'])){
             $input['url'] = "";
        }

       

        if($input['usertype'] == 'user')
        {
             $post = User::find($input['user_id']);


        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5($input['user_id'].time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/postimage');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $input['image'] = 'postimage/'.$name;
        }

        $newpost = Post::create($input);
        $return_array = $newpost->toArray();

        unset($return_array['id']);
        $return_array['post_id'] = (string)$newpost->toArray()['id'];

        if(empty($return_array['image'])){
           $return_array['image'] = ""; 
        } else {
           
           $return_array['image'] = url('/public/').'/'.$newpost->toArray()['image'];
        }
        
        
       return back();


        } else if($input['usertype'] == 'doctor')
        {
             $post = Doctor::find($input['user_id']);

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5($input['user_id'].time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/postimage');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $input['image'] = 'postimage/'.$name;
        }

        

        $newpost = Post::create($input);

        $return_array = $newpost->toArray();

        unset($return_array['id']);
        //unset($return_array['image']);
        $return_array['post_id'] = (string)$newpost->toArray()['id'];
        $return_array['image'] = url('/public/').'/'.$newpost->toArray()['image'];
        return back();
        }






    }

 
    public function usersignup(Request $request)
   {
     

      if ($request->isMethod('post')){

        $input = $request->all();

        $val_arr = [
            'fullname' => 'required|max:25',
            'phone' => 'required|phone:AE',
            'email'=>'required',
            'password_confirmation'=>'required',
            'password' => 'required|confirmed|min:6',
        ];

      $messages = [
            'fullname.required'=>'Please enter your Full Name',
            'email.required'=>'Please enter Email ID',
            'phone.required'=>'Please enter Phone Number',
            'password.required'=>'Please enter Password',
            'password.min'=>'Password must be at least 6 and at most 15 characters',
            'password_confirmation.required'=>'Please re-enter your Password',
            'password.confirmed'=>'Password and Confirm Password must be same',
        ];

        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){
            //return $this->sendError($request->path(),$validator->errors()->first());  

            //dd(); 
            $messages =$validator->errors()->toArray();

            //dd();

            if($messages['phone'][0] = 'validation.phone'){
              $messages['phone'][0] = "Please Enter Vaild Phone number";
            }

            return view('usersignup',compact('messages'));    
        }

        $check_username = User::where('email',$input['email'])->first();

        if (!empty($check_username)) {
            $messages['email'][0] = "Email Already exist";
             return view('usersignup',compact('messages'));    
        }
        
        $check_phone = User::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone)) {
            $messages['phone'][0] = "Phone number Already exist";
             return view('usersignup',compact('messages'));    
           
        }

        $check_username1 = Doctor::where('email',$input['email'])->first();

        if (!empty($check_username1)) {
             $messages['email'][0] = "Email Already exist";
              return view('usersignup',compact('messages'));    
        }
        
        $check_phone1 = Doctor::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone1)) {
            $messages['phone'][0] = "Phone number Already exist";
             return view('usersignup',compact('messages'));    
           
        }



        $details = User::create($input);


        $token_s = str_random(25);


        $tokens_array = array('user_id'=>$details->id,'token'=>$token_s,'deviceType'=>'website','deviceToken'=>$_SERVER['SERVER_ADDR']);

        $token_saver = Token::create($tokens_array);

        $details->token = $token_s;

         $details->usertype = 'user';
        session(['userdetails' => $details]);

        return redirect('userprofile');
        
      }
       else {
         return view('usersignup');
      }
      

   }

   public function logout(Request $request)
   {
      $request->session()->flush();

     // dd($request->session()->get('userdetails', 'default'));

      return redirect('/');
   }

   public function userlogin(Request $request)
   {
        if ($request->isMethod('post')){

          $input = $request->all();

            $val_arr = [
            'email'=>'required',
            'password' => 'required',
        ];


         $messages = [
            'email.required'=>'Please enter Email ID',
            'password.required'=>'Please enter Password',
        ];

        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){
          
            $messages =$validator->errors()->toArray();

          

            return view('userlogin',compact('messages'));    
        }

        //$details = DB::select("SELECT * FROM `users` WHERE `password`='".$input['password']."' and `admin_status`='1' and (`email`='".$input['email']."' or `phone`='".$input['email']."')");

        $details1 = DB::select("SELECT * FROM `users` WHERE `admin_status`='1' and (`email`='".$input['email']."' or `phone`='".$input['email']."')");

        if (empty($details1)) {
            $messages1 = "Please enter valid Email ID/ Phone Number";

            $messages['email'][0] = $messages1;
            return view('userlogin',compact('messages'));
        }

        $details = DB::select("SELECT * FROM `users` WHERE `password`='".$input['password']."' and `admin_status`='1' and (`email`='".$input['email']."' or `phone`='".$input['email']."')");

        if (empty($details)) {
            $messages1 = "Please enter valid password";

            $messages['password'][0] = $messages1;
            return view('userlogin',compact('messages'));
        }

        $token_s = str_random(25);

        $details = $details[0];

        $token_saver = Token::where('user_id',$details->id)->update(['token'=>$token_s,'deviceToken'=>$_SERVER['SERVER_ADDR'],'deviceType'=>'website']);


         $details->token = $token_s;

         $details->usertype = 'user';

        session(['userdetails' => $details]);

        return redirect('userprofile');

        } else {
            return view('userlogin');
        }
   }


   public function doctorlogin(Request $request)
   {
        if ($request->isMethod('post')){

          $input = $request->all();

            $val_arr = [
            'email'=>'required',
            'password' => 'required',
        ];


         $messages = [
            'email.required'=>'Please enter Email ID',
            'password.required'=>'Please enter Password',
        ];

        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){
          
            $messages =$validator->errors()->toArray();

          

            return view('doctorlogin',compact('messages'));    
        }

         $details1 = DB::select("SELECT * FROM `doctors` WHERE `admin_status`='1' and (`email`='".$input['email']."' or `phone`='".$input['email']."')");

        if (empty($details1)) {
            $messages1 = "Please enter valid Email ID/ Phone Number";

            $messages['email'][0] = $messages1;
            return view('doctorlogin',compact('messages'));
        }

       $details = DB::select("SELECT * FROM `doctors` WHERE `password`='".$input['password']."' and `admin_status`='1' and (`email`='".$input['email']."' or `phone`='".$input['email']."')");

        if (empty($details)) {
            $messages1 = "Please enter valid password";

            $messages['password'][0] = $messages1;
            return view('doctorlogin',compact('messages'));
        }

          
        $token_s = str_random(25);

        $details = $details[0];

        $token_saver = Dtoken::where('user_id',$details->id)->update(['token'=>$token_s,'deviceToken'=>$_SERVER['SERVER_ADDR'],'deviceType'=>'website']);

        $details->token = $token_s;

         $specialties_name = DB::table('specialties')->where('id',$details->speciality_id)->first();

         $city_name = DB::table('cities')->where('id',$details->city_id)->first();

         $details->city_name = $city_name->name;

          $details->specialties_name = $specialties_name->name;

          $details->usertype = 'doctor';

          $details->start_time = $this->changetime($details->start_time);

          $details->end_time = $this->changetime($details->end_time);

        session(['userdetails' => $details]);

        if(empty(SESSION::get('userdetails', 'default')->insurance_accept)){
                        SESSION::get('userdetails', 'default')->insurance_accept = "";
                        SESSION::get('userdetails', 'default')->insurance_accept1 = "";
                } else {
                        $return_insurance_string = "";
                        $insurance_accept_aarray = explode(",", SESSION::get('userdetails', 'default')->insurance_accept);

                        foreach ($insurance_accept_aarray as $value1new) {
                                $check_insurance_accept = Insurance::where('id',$value1new)->first();

                                
                                    $return_insurance_string = $return_insurance_string.$check_insurance_accept->name.",";
                                 
                        }

                        SESSION::get('userdetails', 'default')->insurance_accept1 = SESSION::get('userdetails', 'default')->insurance_accept;

                        SESSION::get('userdetails', 'default')->insurance_accept = chop($return_insurance_string,",");
                }



        return redirect('doctorprofile');

        } else {
            return view('doctorlogin');
        }
   }


   public function doctorsignup(Request $request)
   {
     
     $all_name = DB::table('specialties')->select('id','name')->where('remark','1')->orderBy("name","ASC")->get()->toArray();

     $all_name_cities = DB::table('cities')->select('id','name')->get()->toArray();

      if ($request->isMethod('post')){

        $input = $request->all();

        $val_arr = [
            'email' => 'required',
            'speciality_id' => 'required',
            'city_id'=>'required',
            'clinic'=>'required',
            'licence_number'=>'required',
            'expertise_area'=>'required',
            'password_confirmation'=>'required',
            'password' => 'required|min:6|confirmed',
            'fullname'=>'required|max:25',
            'phone'=>'required|phone:AE'
        ];


        $messages = [
            'fullname.required'=>'Please enter your Full Name',
            'email.required'=>'Please enter Email ID',
            'phone.required'=>'Please enter Phone Number',
            'clinic.required'=>'Please enter Clinic Name',
            'licence_number.required'=>'Please enter License Number',
            'expertise_area.required'=>'Please enter Expertise Areas',
            'password.required'=>'Please enter Password',
            'password.min'=>'Password must be at least 6 and at most 15 characters',
            'password_confirmation.required'=>'Please re-enter your Password',
            'password.confirmed'=>'Password and Confirm Password must be same',
        ];

        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){
            //return $this->sendError($request->path(),$validator->errors()->first());  

            //dd(); 
            $messages =$validator->errors()->toArray();

            //dd($messages['fullname']['0']);

            return view('doctorsignup',compact('messages','all_name','all_name_cities'));    
        }

          $check_username = User::where('email',$input['email'])->first();

        if (!empty($check_username)) {
            $messages['email'][0] = "Email Already exist";
             return view('doctorsignup',compact('messages','all_name','all_name_cities'));    
        }
        
        $check_phone = User::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone)) {
            $messages['phone'][0] = "Phone number Already exist";
             return view('doctorsignup',compact('messages','all_name','all_name_cities'));    
           
        }

        $check_username1 = Doctor::where('email',$input['email'])->first();

        if (!empty($check_username1)) {
             $messages['email'][0] = "Email Already exist";
              return view('doctorsignup',compact('messages','all_name','all_name_cities'));      
        }
        
        $check_phone1 = Doctor::where('phone',$input['phone'])->first();
        
        //dd($check_phone);

        if (!empty($check_phone1)) {
            $messages['phone'][0] = "Phone number Already exist";
             return view('doctorsignup',compact('messages','all_name','all_name_cities'));     
           
        }

        if(!empty($input['the_other_speciality']) && isset($input['the_other_speciality'])){
                    $spl_new_entry = array('name'=>$input['the_other_speciality']);
                    $Specialty_record = Specialty::create($spl_new_entry);
                    $input['speciality_id'] = (string)$Specialty_record->id;
        }

         if(empty($input['insurance_accept'])){
                $input['insurance_accept'] = "";
        } else {
                $return_insurance_string = "";
                $insurance_accept_aarray = explode(",", $input['insurance_accept']);

                foreach ($insurance_accept_aarray as $value1new) {
                        $check_insurance_accept = Insurance::where('name',$value1new)->first();

                        if(empty($check_insurance_accept)){
                             $insu_array_o = array("name"=>$value1new);
                             $insu_array_details = Insurance::create($insu_array_o);
                             $return_insurance_string = $return_insurance_string.$insu_array_details->id.",";

                        } else {
                            $return_insurance_string = $return_insurance_string.$check_insurance_accept->id.",";
                        } 
                }

                $input['insurance_accept'] = chop($return_insurance_string,",");
        }



        $details = Doctor::create($input);


        $token_s = str_random(25);


        $tokens_array = array('user_id'=>$details->id,'token'=>$token_s,'deviceType'=>'website','deviceToken'=>$_SERVER['SERVER_ADDR']);

        $token_saver = Dtoken::create($tokens_array);

        $details->token = $token_s;

        $details->usertype = 'doctor';

        $specialties_name = DB::table('specialties')->where('id',$details->speciality_id)->first();

     

        $city_name = DB::table('cities')->where('id',$details->city_id)->first();

        $details->city_name = $city_name->name;

        $details->specialties_name = $specialties_name->name;

        $details->start_time = $this->changetime($details->start_time);

        $details->end_time = $this->changetime($details->end_time);


        // session(['userdetails' => $details]);

        // if(empty(SESSION::get('userdetails', 'default')->insurance_accept)){
        //                 SESSION::get('userdetails', 'default')->insurance_accept = "";
        //         } else {
        //                 $return_insurance_string = "";
        //                 $insurance_accept_aarray = explode(",", SESSION::get('userdetails', 'default')->insurance_accept);

        //                 foreach ($insurance_accept_aarray as $value1new) {
        //                         $check_insurance_accept = Insurance::where('id',$value1new)->first();

                                
        //                             $return_insurance_string = $return_insurance_string.$check_insurance_accept->name.",";
                                 
        //                 }

        //                 SESSION::get('userdetails', 'default')->insurance_accept1 = SESSION::get('userdetails', 'default')->insurance_accept;

        //                 SESSION::get('userdetails', 'default')->insurance_accept = chop($return_insurance_string,",");
        //         }

        return view('thanks');
        
      }
       else {
         return view('doctorsignup',compact('all_name','all_name_cities'));
      }
      

   }


    public function deletepost(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'post_id'=>'required',
        ];

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            //return $this->sendError($request->path(),$validator->errors()->first()); 
            echo "success";

                die();      
        }

        $input['user_id'] = SESSION::get('userdetails', 'default')->id;
        $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;

        if($input['usertype'] == 'user'){

                $post = User::find($input['user_id']);


                DB::table('posts')->where(['user_id'=>$input['user_id'],'id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();

                echo "success";

                die();


        } else if($input['usertype'] == 'doctor')
        {


               $post = Doctor::find($input['user_id']);


        

               DB::table('posts')->where(['user_id'=>$input['user_id'],'id'=>$input['post_id'],'usertype'=>$input['usertype']])->delete();

               echo "success";

                die();
        }

        //return $this->sendResponse(array('status'=>'success'), 'post deleted successfully',$request->path());


    }

    public function commentOn(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
              'post_id' => 'required',
              'message' => 'required',
          ]);

         $input['user_id'] = SESSION::get('userdetails', 'default')->id;
        $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;


          if($validator->fails()){
              return $this->sendError($request->path(),$validator->errors()->first());       
          }


           Comment::create($input);

        $return_array = DB::select("select * from(SELECT comments.id as comment_id,users.image,users.fullname,users.email,comments.message,comments.created_at FROM `comments` inner join `users` on (users.id = comments.user_id) where comments.`usertype` = 'user' and comments.post_id ='".$input['post_id']."'
union ALL
SELECT comments.id as comment_id,doctors.image,doctors.fullname,doctors.email,comments.message,comments.created_at FROM `comments` inner join `doctors` on (doctors.id = comments.user_id) where comments.`usertype` = 'doctor' and comments.post_id ='".$input['post_id']."') as x order by x.comment_id DESC");

        foreach ($return_array as $value) {
             if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

             $value->created_at = Carbon::parse($value->created_at)->diffForHumans();
        }


          return $this->sendResponse($return_array, 'Post Comment list retrieved successfully.',$request->path());




    }

   public function changePassword(Request $request)
    {

        $input = $request->all();

        $val_arr = [
            'oldpassword'=>'required',
            'password_confirmation'=>'required',
            'password' => 'required|min:6|confirmed',
            'usertype'=>'required',
        ];

         $messages = [

        'oldpassword.required' => 'The old password field is required.',
        'password.required'=>'Please enter New Password',
        'password_confirmation.required'=>'Please re-enter your New Password',
        'password.min'=>'New Password must be at least 6 and at most 15 characters',
        'password.confirmed'=>'New Password and Confirm Password must be same',

         ];

         $input['user_id'] = SESSION::get('userdetails', 'default')->id;

        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){

            echo json_encode(array("0",$validator->errors()->first()));

            die();
              
        }


        if($input['usertype'] == 'user')
        {
            $post = User::find($input['user_id']);


            if (is_null($post)) {

                echo json_encode(array("0","User not found."));
                die();
            }

            

            if($post->password != $input['oldpassword'])
            {

             
               echo json_encode(array("0","Old Password is wrong."));
                die();
            } else {
                $post->password = $input['password'];
                $post->save();

              

            
                   echo json_encode(array("1","Password Changed successfully!!"));
                die();
            }
        } else if($input['usertype'] == 'doctor')
        {
            $post = Doctor::find($input['user_id']);


            if (is_null($post)) {

                
                 echo json_encode(array("0","Doctor not found"));
                die();
            }

             

            if($post->password != $input['oldpassword'])
            {
                  echo "0_Old Password is wrong.";
                  echo json_encode(array("0","Old Password is wrong."));
                die();
                
            } else {
                $post->password = $input['password'];
                $post->save();

               
                echo json_encode(array("1","Password Changed successfully!!"));
                die();
                
            }
        }





    }

    public function addPatient(Request $request)
    {
         $input = $request->all();

         $val_arr = [
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'age'=>'required',
            'gender'=>'required',
            'apdate'=>'required',
            'aptime'=>'required',
        ];


        $messages = [

        'firstname.required' => 'Please enter First Name.',
        'lastname.required' => 'Please enter Last Name.',
        'email.required' => ' Please enter Email ID.',
        'phone.required' => 'Please enter Phone Number.',
        'apdate.required' => 'Please select Appointment Date.',
        'aptime.required' => 'Please enter Appointment time.',
         ];

        

        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){
           echo json_encode(array("0",$validator->errors()->first()));

            die();      
        }

        $input['doctor_id'] = SESSION::get('userdetails', 'default')->id;

        $post = Doctor::find($input['doctor_id']);

        $check_email = DB::select("SELECT * FROM `users` WHERE `email` = '".$input['email']."'");

        //dd();

        if(empty($check_email)) {
            echo json_encode(array("0",'Please enter valid Email ID.'));

            die();
            
        }

        $msg = $post->fullname." confirm booking on  ".date('d/m/Y');

         if(empty($post->image))
        {
            $post->image = url('/')."/public/img/user_signup.png";
        } else {
            $post->image = url('/public/').'/'.$post->image;
        }

        $data1 = array("msg"=>$msg,"image"=>$post->image);

        $return_responce = array('data'=>json_encode($data1),'sender_id'=>$input['doctor_id'],'receiver_id'=>$check_email[0]->id,'usertype'=>'doctor');

        Notification::create($return_responce);

        $data1['time'] = Carbon::now()->diffForHumans();

        Booking::create($input);


        $dtokenbyuser = Token::where(['user_id'=>$check_email[0]->id])->first();

        $count_notification = Notification::where(['receiver_id'=>$check_email[0]->id,'status'=>'0'])->count();

        if($dtokenbyuser->deviceType == 'android')
        {
            //dd($return_responce);

            if($check_email[0]->notification_flag == 1){
              $this->android_push($dtokenbyuser->deviceToken,$msg,"booking_confirmed",$count_notification,$data1);
            }
            
        }

        //  if($dtokenbyuser->deviceType == 'ios')
        // {
        //     //dd($return_responce);

        //     if($check_email[0]->notification_flag == 1){
        //       $this->iphone_push($dtokenbyuser->deviceToken,$msg,"booking_confirmed",$count_notification,$data1);
        //     }
            
        // }
       echo json_encode(array("1","Booking confirmed successfully"));
                die();
      // return $this->sendResponse(array("status"=>"success"), '',$request->path());
    }


     public function postlistbyuserid(Request $request,$id)
    {
        

        foreach ($get_news_feedlist as $value) {

            $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

            if(empty($value->image)){
                $value->image = url('/')."/public/img/user_signup.png";
            } else {
                 $value->image = url('/public/').'/'.$value->image;
            }

            if(empty($value->post_image)){
                $value->post_image = "";
            } else {
                $value->post_image = url('/public/').'/'.$value->post_image;
            }

            $like_recorde = DB::table('likes')->where(['user_id'=>$id,'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($like_recorde))
            {
                $value->likeflag = "0";
            } else {
                $value->likeflag = "1";
            }

            $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>$id,'post_id'=>$value->post_id,'usertype'=>$input['usertype']])->first();

            if(empty($saveposts_recorde))
            {
                $value->saveflag = "0";
            } else {
                $value->saveflag = "1";
            }

           
           
        }

        



        return $this->sendResponse($get_news_feedlist, 'Post list retrieve successfully',$request->path());


    }

    public function doctorprofile(Request $request)
    {

         

            if(SESSION::get('userdetails', 'default') == 'default'){

                return redirect('doctorlogin');


            } else {


                 $city_list = DB::table('cities')->select('id','name')->get()->toArray();

                if(SESSION::get('userdetails', 'default')->usertype == 'user')
                {
                            $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image`,`posts`.`id` as post_id, '' as speciality_name,`posts`.image as post_image FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user' and `posts`.`user_id`='".SESSION::get('userdetails', 'default')->id."') as x order by x.post_id desc");
                } else if(SESSION::get('userdetails', 'default')->usertype == 'doctor')
                {
                            $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.image as post_image FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' and `posts`.`user_id`='".SESSION::get('userdetails', 'default')->id."') as x order by x.post_id desc");
                }

                  foreach ($get_news_feedlist as $value) {

                         $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

                        if(empty($value->image)){
                            $value->image = url('/')."/public/img/user_signup.png";
                        } else {
                             $value->image = url('/public/').'/'.$value->image;
                        }

                        if(empty($value->post_image)){
                            $value->post_image = "";
                        } else {
                            $value->post_image = url('/public/').'/'.$value->post_image;
                        }

                        $like_recorde = DB::table('likes')->where(['user_id'=>SESSION::get('userdetails', 'default')->id,'post_id'=>$value->post_id,'usertype'=>SESSION::get('userdetails', 'default')->usertype])->first();

                        if(empty($like_recorde))
                        {
                            $value->likeflag = "0";
                        } else {
                            $value->likeflag = "1";
                        }

                        $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>SESSION::get('userdetails', 'default')->id,'post_id'=>$value->post_id,'usertype'=>SESSION::get('userdetails', 'default')->usertype])->first();

                        if(empty($saveposts_recorde))
                        {
                            $value->saveflag = "0";
                        } else {
                            $value->saveflag = "1";
                        }

                  }

            if(SESSION::get('userdetails', 'default')->usertype == 'user'){
                  return redirect('userlogin');
            }   
                return view('doctorprofile',compact('get_news_feedlist','city_list'));      
                
            }
    }


    public function userprofile(Request $request)
    {

         

            if(SESSION::get('userdetails', 'default') == 'default'){

                return redirect('userlogin');


            } else {
                if(SESSION::get('userdetails', 'default')->usertype == 'user')
                {
                            $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`users`.`fullname`,`users`.`image`,`posts`.`id` as post_id, '' as speciality_name,`posts`.image as post_image FROM `posts` inner join `users` on (`posts`.`user_id`=`users`.`id`) where `posts`.`usertype`='user' and `posts`.`user_id`='".SESSION::get('userdetails', 'default')->id."') as x order by x.post_id desc");
                } else if(SESSION::get('userdetails', 'default')->usertype == 'doctor')
                {
                            $get_news_feedlist = DB::select("select * from(SELECT `posts`.`title`,`posts`.`url`,`posts`.`description`,`posts`.`created_at`,`doctors`.`fullname`,`doctors`.`image`,`posts`.`id` as post_id, `specialties`.`name` as speciality_name,`posts`.image as post_image FROM `posts` inner join `doctors` on (`posts`.`user_id`=`doctors`.`id`) inner join `specialties` on (`specialties`.`id`=`doctors`.`speciality_id`)   where `posts`.`usertype`='doctor' and `posts`.`user_id`='".SESSION::get('userdetails', 'default')->id."') as x order by x.post_id desc");
                }

                  foreach ($get_news_feedlist as $value) {

                         $value->shareurl = url('/')."/postpage/".Crypt::encryptString($value->post_id);

                        if(empty($value->image)){
                            $value->image = url('/')."/public/img/user_signup.png";
                        } else {
                             $value->image = url('/public/').'/'.$value->image;
                        }

                        if(empty($value->post_image)){
                            $value->post_image = "";
                        } else {
                            $value->post_image = url('/public/').'/'.$value->post_image;
                        }

                        $like_recorde = DB::table('likes')->where(['user_id'=>SESSION::get('userdetails', 'default')->id,'post_id'=>$value->post_id,'usertype'=>SESSION::get('userdetails', 'default')->usertype])->first();

                        if(empty($like_recorde))
                        {
                            $value->likeflag = "0";
                        } else {
                            $value->likeflag = "1";
                        }

                        $saveposts_recorde = DB::table('saveposts')->where(['user_id'=>SESSION::get('userdetails', 'default')->id,'post_id'=>$value->post_id,'usertype'=>SESSION::get('userdetails', 'default')->usertype])->first();

                        if(empty($saveposts_recorde))
                        {
                            $value->saveflag = "0";
                        } else {
                            $value->saveflag = "1";
                        }

                  }

            if(SESSION::get('userdetails', 'default')->usertype == 'doctor'){
                  return redirect('doctorlogin');
            }  
                return view('userprofile',compact('get_news_feedlist'));    
                
            }
    }



     public function editUserProfile(Request $request)
    {
        $input = $request->all();

       
        $id = SESSION::get('userdetails', 'default')->id;



        $userdetails = User::find($id);

        
        $name = "";

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = md5($id.time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/userimage');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $userdetails->image = 'userimage/'.$name;
        }

        
        if(!empty($_POST['email']) && isset($_POST['email'])){
            $userdetails->email = $_POST['email'];

            SESSION::get('userdetails', 'default')->email = $_POST['email']; 
        }

        if(!empty($_POST['phone']) && isset($_POST['phone'])){
            $userdetails->phone = $_POST['phone'];

            SESSION::get('userdetails', 'default')->phone = $_POST['phone']; 
        }
            
        $userdetails->save(); 

        $return_array = $userdetails->toArray();

        

        SESSION::get('userdetails', 'default')->image = $userdetails->toArray()['image'];
         

          
       return back();
        

    }


    public function commentOnPost($message,$id)
    {

       if(SESSION::get('userdetails', 'default') == 'default'){

                echo url('/userlogin')."_"."0";
            }
        else {
                $input['post_id'] = $id;

                $input['message'] = $message; 

                $input['user_id'] = SESSION::get('userdetails', 'default')->id;

                $input['usertype'] = SESSION::get('userdetails', 'default')->usertype;

                 Comment::create($input);

                    $return_array = DB::select("select * from(SELECT comments.id as comment_id,users.image,users.fullname,users.email,comments.message,comments.created_at FROM `comments` inner join `users` on (users.id = comments.user_id) where comments.`usertype` = 'user' and comments.post_id ='".$input['post_id']."'
                union ALL
                SELECT comments.id as comment_id,doctors.image,doctors.fullname,doctors.email,comments.message,comments.created_at FROM `comments` inner join `doctors` on (doctors.id = comments.user_id) where comments.`usertype` = 'doctor' and comments.post_id ='".$input['post_id']."') as x order by x.comment_id DESC");



                    $return_string = '<label  class="cmthda" for="exampleFormControlInput1">'.sizeof($return_array).' Comments</label>';

                     foreach ($return_array as $value) {
                             if(empty($value->image)){
                                $value->image = url('/')."/public/img/user_signup.png";
                            } else {
                                 $value->image = url('/public/').'/'.$value->image;
                            }

                             $value->created_at = Carbon::parse($value->created_at)->diffForHumans();

                             $return_string = $return_string.'<div class="notiftabs">
                                            <div class=" userpic">
                                                <img src="'.$value->image.'">
                                            </div>
                                            
                                            <div class="usernotify">
                                            <label  class="cmthda usernamedoci">'.ucfirst($value->fullname).'</label>
                                            <p>'.ucfirst($value->message).'</p> 
                                            <span class="notifytimes commentdays">'.$value->created_at.'</span>
                                                
                                            </div>  
                                    </div>';
                        }

                        //echo "<pre>"; print_r($return_string); die();
                        echo $return_string."_"."1";

                        die();

            }


    }


    public function editDoctorProfile(Request $request)
    {
        if(SESSION::get('userdetails', 'default') == 'default'){

                return redirect('doctorlogin');
            }
        else {

            $input = $request->all();

             $id = SESSION::get('userdetails', 'default')->id;

             $doctors_d = Doctor::find($id);

             if(isset($input['avilability']) && !empty($input['avilability'])){
                $doctors_d->avilability = implode(",", $input['avilability']);

                SESSION::get('userdetails', 'default')->avilability = implode(",", $input['avilability']);
             }

             if ($request->hasFile('image')) 
                {
                    $image = $request->file('image');
                    $name = md5($id.time()).rand(1000,9999).'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/userimage');
                    $imagePath = $destinationPath. "/".  $name;
                    $image->move($destinationPath, $name);
                    $doctors_d->image = 'userimage/'.$name;
                }

             $doctors_d->city_id = explode(",",$input['city_id'])[0];

             $doctors_d->clinic = $input['clinic'];

             $doctors_d->qualification = $input['qualification'];

             $doctors_d->fee = $input['fee'];

             $doctors_d->start_time = $input['start_time'];

             $doctors_d->end_time = $input['end_time'];

             $doctors_d->insurance_accept = $this->addnewstrbymatch(SESSION::get('userdetails', 'default')->insurance_accept1,$input['insurance_accept1']);

             $doctors_d->save();


            session(['userdetails' => $doctors_d]);

             SESSION::get('userdetails', 'default')->city_name = explode(",",$input['city_id'])[1];

             SESSION::get('userdetails', 'default')->city_id = explode(",",$input['city_id'])[0];

             SESSION::get('userdetails', 'default')->clinic = $input['clinic'];

             SESSION::get('userdetails', 'default')->qualification = $input['qualification'];

             SESSION::get('userdetails', 'default')->fee = $input['fee'];

             SESSION::get('userdetails', 'default')->start_time = $this->changetime($input['start_time']);

             SESSION::get('userdetails', 'default')->end_time = $this->changetime($input['end_time']);

              SESSION::get('userdetails', 'default')->usertype = 'doctor';

             if(!empty($doctors_d->image))
             {
                
                SESSION::get('userdetails', 'default')->image = $doctors_d->image;
             }




             if(empty(SESSION::get('userdetails', 'default')->insurance_accept)){
                        SESSION::get('userdetails', 'default')->insurance_accept = "";
                } else {
                        $return_insurance_string = "";
                        $insurance_accept_aarray = explode(",", SESSION::get('userdetails', 'default')->insurance_accept);

                        foreach ($insurance_accept_aarray as $value1new) {
                                $check_insurance_accept = Insurance::where('id',$value1new)->first();

                                
                                    $return_insurance_string = $return_insurance_string.$check_insurance_accept->name.",";
                                 
                        }

                        SESSION::get('userdetails', 'default')->insurance_accept1 = SESSION::get('userdetails', 'default')->insurance_accept;

                        SESSION::get('userdetails', 'default')->insurance_accept = chop($return_insurance_string,",");
                }

             return back();


        }

    }


    public function doctordetails($id)
    {
        $recorde_detail = DB::select("SELECT doctors.start_time,doctors.end_time,doctors.fee,doctors.fullname,doctors.id as user_id,doctors.clinic,doctors.avilability,doctors.image,specialties.name as speciality_name,cities.name as city_name,doctors.expertise_area,doctors.insurance_accept,doctors.qualification FROM `doctors` inner join cities on (cities.id=doctors.city_id) inner JOIN specialties on (specialties.id=doctors.speciality_id) where doctors.id='".$id."'");

           
            if(empty($recorde_detail[0]->image)){
                $recorde_detail[0]->image = url('/')."/public/img/user_signup.png";
            } else {
               $recorde_detail[0]->image = url('/').'/public/'.$recorde_detail[0]->image;
            }


            $recorde_detail[0]->rating = $this->avgrating($id);

            if(empty($recorde_detail[0]->insurance_accept)){
                $recorde_detail[0]->insurance_accept = array();
            } else {
                $insurance_array_accept = array();

                $exp_array = explode(",", $recorde_detail[0]->insurance_accept);

                foreach ($exp_array as $value_name) {
                      $exp_array_reco = Insurance::whereId($value_name)->first();
                      
                      if(!empty($exp_array_reco)){
                        $insurance_array_accept[] = $exp_array_reco->name;
                      }

                      
                }

                 $recorde_detail[0]->insurance_accept = $insurance_array_accept;
            }

            if($recorde_detail[0]->start_time != '-')
            {
                $recorde_detail[0]->start_time = $this->changetime($recorde_detail[0]->start_time);
            }

            if($recorde_detail[0]->end_time != '-')
            {
                $recorde_detail[0]->end_time = $this->changetime($recorde_detail[0]->end_time);
            }

            $review_details = DB::select("SELECT users.image,doctorratings.rating*20 as rating,doctorratings.review,users.fullname,doctorratings.created_at FROM `doctorratings` INNER JOIN users on (users.id=doctorratings.user_id) where doctorratings.doctor_id='".$id."'");

            foreach ($review_details as $value) {
                  if(empty($value->image)){
                        $value->image = url('/')."/public/img/user_signup.png";
                    } else {
                       $value->image = url('/').'/public/'.$value->image;
                    }
            }

            $recorde_detail[0]->num_of_review = sizeof($review_details);

            $recorde_detail[0]->review_list = $review_details;

            $details_doctor = $recorde_detail[0];

            //return $this->sendResponse($recorde_detail[0], 'Doctors details retrieve successfully',$request->path());

            //dd($details_doctor);

            return view('doctordetails',compact('details_doctor'));

    }

    public function avgrating($id)
    {
        $global_rate = 0;
         $review_details = DB::select("SELECT doctorratings.rating*20 as rating FROM `doctorratings` where doctorratings.doctor_id='".$id."'");

            foreach ($review_details as $value) {
                 

                    $global_rate = $global_rate + $value->rating;
            }

            if(sizeof($review_details) == '0'){
                return "0";
            } else {
                return (string)(round($global_rate/(sizeof($review_details))));
            }

    }


    public function changetime($newtext)
    {
        if(!empty($newtext) && $newtext != '-'){
        if($newtext == '00')
        {
            return "12 AM";
        } else {
            $newtext1 = fmod($newtext,12);

            $newtext12 = (int)($newtext/12);

           

           if($newtext12 == '0'){
                 return $newtext1." AM";
           } else {
                 return $newtext1." PM";
           }
        }

        } else {
            return "-";
        }


    }

    public function submitReview(Request $request)
    {
        $input = $request->all();

          $val_arr = [
            'doctor_id'=>'required',
            'rating'=>'required',
        ];

        $input['user_id'] = SESSION::get('userdetails', 'default')->id;

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        if(empty($input['review'])){
            $input['review'] = "";
        }

        $post = User::find($input['user_id']);

        $check_already_rate = Doctorrating::where(["doctor_id"=>$input['doctor_id'],"user_id"=>$input['user_id']])->first();

        if(!empty($check_already_rate)){
                 return $this->sendError($request->path(),"You Already Rate This Doctor");    
        }

        Doctorrating::create($input);

         $review_details = DB::select("SELECT users.image,doctorratings.rating*20 as rating,doctorratings.review,users.fullname FROM `doctorratings` INNER JOIN users on (users.id=doctorratings.user_id) where doctorratings.doctor_id='".$input['doctor_id']."' order by doctorratings.id desc");

            foreach ($review_details as $value) {
                  if(empty($value->image)){
                        $value->image = url('/')."/public/img/user_signup.png";
                    } else {
                       $value->image = url('/').'/public/'.$value->image;
                    }

                     //$global_rate = $global_rate + $value->rating;
            }

            //$recorde_detail[0]->rating = $this->avgrating($id);

            //$recorde_detail[0]->num_of_review = (string)sizeof($review_details);

            //$recorde_detail[0]->review_list = $review_details;

        return $this->sendResponse(array("review_details"=>$review_details,"rating"=>$this->avgrating($input['doctor_id'])), 'Doctors review submited successfully',$request->path());


    }

    public function removeins(Request $req,$id)
    {
        if(SESSION::get('userdetails', 'default') == 'default'){

                return redirect('doctorlogin');
            }
        else {


             $user_id = SESSION::get('userdetails', 'default')->id;

             $userrec = Doctor::find($user_id);

             $newisn = $this->removenewstrbymatch(SESSION::get('userdetails', 'default')->insurance_accept1,$id);

             $userrec->insurance_accept = $newisn;

             $userrec->save();

             $return_insurance_string = "";

             if(!empty($newisn)){
                 $insurance_accept_aarray = explode(",", $newisn);

             foreach ($insurance_accept_aarray as $value1new) {
                    $check_insurance_accept = Insurance::where('id',$value1new)->first();

                   
                        $return_insurance_string = $return_insurance_string.$check_insurance_accept->name.",";
                     
             }

             SESSION::get('userdetails', 'default')->insurance_accept = chop($return_insurance_string,",");

             SESSION::get('userdetails', 'default')->insurance_accept1 = $newisn;
             } else {
               SESSION::get('userdetails', 'default')->insurance_accept = "";

              SESSION::get('userdetails', 'default')->insurance_accept1 = "";
             }
            
            

             return back();

           }
    }

    public function addnewstrbymatch($str1,$str2)
    {
        if(!empty($str2)){

            if(empty($str1)){
              $newarr = array();
            } else {
               $newarr = explode(",", $str1);
            }

            $netset = Insurance::where('name',$str2)->first();

            if(!empty($netset)){
                $str2 = (string)$netset->id;
                if(in_array($str2, $newarr)){
                return $str2;
            } else {
                $newarr[] = $str2;
                return implode(",",$newarr);   
            }

            } else{
                $newinsname = array("name"=>$str2);
                $difggset = Insurance::create($newinsname);
                $newarr[] = (string)$difggset->id;
                return implode(",",$newarr);
            }
            } else {
                 return $str1;
            } 
    }

    public function removenewstrbymatch($str1,$str2)
    {
        $newarr = explode(",", $str1);

        if(in_array($str2, $newarr)){
            $newarr = array_flip($newarr);
            unset($newarr[$str2]);
            return implode(",",array_flip($newarr));
        } else {
            return implode(",",$newarr);
        } 
    }

}
