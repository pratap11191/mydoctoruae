<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    //
    
    protected $fillable = [
        'doctor_id','sub_id','exp_date'
    ];
}
