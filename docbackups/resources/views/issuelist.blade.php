@extends('layout.site') 
@section('title', 'Issue List')
@section('content')
 <form action="{{url('/')}}/listing" method="post">
        
         <input type="hidden" name="speciality_id" id="speciality_id">
        <input id="index_old" type="submit" style="display: none;">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
    </form>
<div class="news-feedss">
        <div class="container">
		<div class="nfdc">
            <div class="hading_by_title">
                <h4>TOP ISSUES</h4>
            </div>
            <?php

            	if(!empty($items)) {

            		foreach ($items as  $value) {
            		
            ?>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="categrese">
                    <a id="issue_value{{$value->id}}" data="{{$value->id}}">
                    <img src="{{$value->img}}" class="img-responsive" alt="">
                    
                        <div class="categrese-name">
                            <h5>{{ucfirst($value->name)}}</h5>
                        </div>
                    </a>
                </div>
            </div>

            <script type="text/javascript">
    $('#issue_value{{$value->id}}').click(function(){
            $('#speciality_id').val($('#issue_value{{$value->id}}').attr('data'));
            //alert($('#city_value{{$value->id}}').attr('data'));
            $('#index_old').click();
    });
</script>
          
			<?php

		}

		?>

 <div class="col-md-12">{{ $items->links() }} </div>


		<?php

		}

		?>
		</div>
        </div>
    </div>
@endsection