@extends('layout.site') 
@section('title', 'User Profile')
@section('content')
@if (Session::has('message_passchange'))
	<div class="alert alert-info">{{ Session::get('message_passchange') }}</div>
@endif
<style type="text/css">
    .para1{
        max-height: 55px;
        overflow: hidden;
    }
</style>
<div class="userpropge">
        <div class="container">
			<div class="morepagessection">
				<div class="">
						<div class="tabdocts">
						  <button class="tablinks active" onclick="openCity(event, 'Userpro')">My Profile</button>
						  <button class="tablinks" onclick="openCity(event, 'London')">Your Posts</button>
						  <button class="tablinks" onclick="openCity(event, 'Paris')">Notification</button>
						  <button class="tablinks" data-toggle="modal" data-target="#myModal2">Change Password</button>
						</div>

						
						<div id="Userpro" class="tabcontent" style="display:block;">
							<div class="userprofilename">
							    <form action="{{url('/')}}/editUserProfile" method="post" enctype="multipart/form-data">
								<div class="userdescpro">
								     
									<!-- <h4>My Profile</h4> -->
									
									<p id="userprofileresponse"></p>
									<div class="imageuserdocs">
										<div class="profileinf">
										<img id="profile-img-tag" src="<?php
										if(empty(Request::session()->get('userdetails', 'default')->image)){
											echo url('/')."/public/img/user_signup.png";;
										} else {
											echo url('/').'/public/'.Request::session()->get('userdetails', 'default')->image;
										}
										?>">
										<i class="fa fa-camera"></i>
										
										  <input id="profile-img" class="cameraupload" type="file" name="image" accept="image/*">
										</div>
										<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
										<script type="text/javascript">
										    function readURL(input) {
										        if (input.files && input.files[0]) {
										            var reader = new FileReader();
										            
										            reader.onload = function (e) {
										                $('#profile-img-tag').attr('src', e.target.result);
										            }
										            reader.readAsDataURL(input.files[0]);
										        }
										    }
										    $("#profile-img").change(function(){
										        readURL(this);
										    });
										</script>
										 <h1><?php echo Request::session()->get('userdetails', 'default')->fullname; ?></h1>
									</div>
									
									
									<div class="editinfofieldsdta">
									
									<div class="col-md-6 equalpart bdrsep">
										<div class="fullwdth">
											<label class="userfieldsnk" for="exampleFormControlInput1">Email Id</label>
											<span class="useridprint"><?php echo Request::session()->get('userdetails', 'default')->email; ?></span>
											<div class="editdownf accordion"><i class="fa fa-edit"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
												<input type="email" class="form-control" id="email_saveprofile" placeholder="name@example.com" name="email" value="<?php echo Request::session()->get('userdetails', 'default')->email; ?>">
											</div>
											
										</div>
									</div>
									<div class="col-md-6 equalpart">
										<div class="fullwdth">
											<label class="userfieldsnk" for="exampleFormControlInput1">Contact Number</label>
											
											<span class="useridprint"><?php echo Request::session()->get('userdetails', 'default')->phone; ?></span>
											<div class="editdownf accordion"><i class="fa fa-edit"></i></div>
											
											<div class="profilepanel revelfieldedit formslogins">
												
												<input name="phone" type="text" class="form-control" id="phone_saveprofile" placeholder="phone number" value="<?php echo Request::session()->get('userdetails', 'default')->phone; ?>">
											</div>
											
											
										</div>
									  </div>
									  
									  <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
											<div class="actionloginbs">
												<button id="save_user_profile" type="submit" class="btn btn-primary save_user_profile">Save</button>
											</div>
									
									</div>
								</div>
							</div>
						</form>
						</div>
						<div id="London" class="tabcontent">
						  <div id="Addposts" class="postadcontent" style="display:block;">
						  
						  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-feedss">
						  
						  <div class="bookbtn">
						 
						  </div>
								<div class="">
									<div class="hading_by_title">
										<h4>Your Posts</h4>
									</div>
									<?php
									

									if(empty($get_news_feedlist)){ ?>
										<p class="nodtafdpos">No record found</p>
							<?php		} else {

									foreach ($get_news_feedlist as $value) {
										
									//print_r($value);

							 ?>

									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<div class="your-posts-section">
											<div class="your-posts-pic-para">
												<a href="{{$value->shareurl}}" class="newsfeed_a_all">
													<div class="post-profile">
														<img src="{{$value->image}}" class="img-responsive">
													</div>
													<div class="post-profile-paraes">
														<h4>{{ucfirst($value->title)}}</h4>
														
														<p class="para1">{{ucfirst($value->description)}}</p>
													</div>
												</a>  
											</div>
											<div class="chatcomment delepostuda" data="{{$value->post_id}}">
													<i class="fa fa-trash"></i>
											</div>
											<div class="your-post-anchore">
												<ul class="yourpost-ul">
													<li><a href="{{$value->shareurl}}"><span class="yourposticon"><i class="fa fa-comment" aria-hidden="true"></i></span></a></li>											
													<li><a href="#"><span class="yourposticon"><i class="fa fa-share-alt" aria-hidden="true"></i></span></a>
														<ul class="your-post-share-ul">
															<li><a href="https://api.whatsapp.com/send?&text={{$value->shareurl}}" target="_blank">Whatsapp</a></li>
                                     <li><a href="https://www.facebook.com/sharer/sharer.php?u={{$value->shareurl}}" target="_blank">Facebook</a></li>
                                     <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{$value->shareurl}}&title={{ucfirst($value->title)}}&summary={{ucfirst($value->description)}}&source=Healthapp" target="_blank">LinkedIn</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</div>

								<?php

}

									}
									?>
									
								</div>
							</div>
							</div>
							
							<div id="addpostform" class="postadcontent" style="display:none;">		
								<div class="bookbtn">
								<button class="posttablinks btn btn-primary" onclick="openPosts(event, 'Addposts')"><i class="fa fa-caret-left"></i> back</button>
								</div>
								
								<div class="formslogins">
									<div class="regisindivs">
									<div class="main-hd">
										<h1>Add Posts Here</h1>
									</div>
									<form>
										<div class="form-group">
											
											<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Post Title.">
											
											<div class="poslable">
											<label class="optionalst">Post Image (optional) </label>
											<input type="file" class="form-control" id="exampleFormControlInput1" placeholder="Description">
											</div>
											
											<textarea placeholder="Description"></textarea>

											<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="News Feed url (optional)">
											
											<div class="actionloginbs">
											<button type="submit" class="btn btn-primary">Save</button>
											</div>
																		
										</div>
									</form>
									</div>
								</div>
							</div>
							
						</div>

						<div id="Paris" class="tabcontent">
						
						   <!--<div class="enablenotify">
							<label class="switch">
								  <input type="checkbox" checked="">
								  <span class="slidergreen round"></span>
							</label>
							<p class="enabtxt">Enable/Disable Notification</p>
                           </div> -->
                           <?php
							  // Here is the data we will be sending to the service
							  $some_data = array(
							    'user_id' => Request::session()->get('userdetails', 'default')->id, 
							    'usertype' => "doctor"
							  );  

							  $curl = curl_init();
							  // You can also set the URL you want to communicate with by doing this:
							  // $curl = curl_init('http://localhost/echoservice');

							  // We POST the data
							  curl_setopt($curl, CURLOPT_POST, 1);
							  // Set the url path we want to call
							  curl_setopt($curl, CURLOPT_URL, url('/')."/api/notifylist");  
							  // Make it so the data coming back is put into a string
							  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							  // Insert the data
							  curl_setopt($curl, CURLOPT_POSTFIELDS, $some_data);

							  // You can also bunch the above commands into an array if you choose using: curl_setopt_array

							  // Send the request
							  $result = curl_exec($curl);

							 
							  curl_close($curl);

							// echo $result;

							  if(!empty(json_decode($result)->data)){

							 $result1 = json_decode($result)->data;

							 //print_r($result1);

							  foreach($result1 as $value) {
							  	
							 // print($value);
							  	
							?>
						  <div class="notiftabs">
							<div class=" userpic">
								<img src="{{$value->image}}">
							</div>
							
							<div class="usernotify">
							<p>{{ucfirst($value->msg)}}</p> 
							<span class="notifytimes">{{ucfirst($value->time)}}</span>
							</div>
							
						  </div>
						  
						 <?php

						}

					} else {
						echo "No Notification found!!";
					}

						?>
						  
						
						</div>

						<div id="Tokyo" class="tabcontent">
							<div class="">
					<div class="formslogins">
					   
						
						<!-- <div class="regisindivs">
						<div class="main-hd">
							<h1>Change Password</h1>
						</div>
						<form action="{{action('API\HomeAPIController@changePassword')}}" method="post" >
						{{ csrf_field() }}
							<div class="form-group">
								
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Old Password." name="oldpassword">
								
								
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="New Password" name="password">
								
									
								<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Password" name="password_confirmation">
								
								<div class="actionloginbs">
								<button type="submit" class="btn btn-primary">Save</button>
								</div>
															
							</div>
						</form>
						</div> -->
						
					</div>
				</div>
						</div>
						
						
						
				</div>
			</div>
		</div>
	</div>
	
	<div class="docconfirmbew">
	<div class="modal fade" id="myModal2" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  
			<!-- Modal Header -->
			
			
			<!-- Modal body -->
			<div class="modal-body">
			 <div class="formslogins">
					   <div class="main-hd">
							<h1>Change Password</h1>
						</div>
						 <button type="button" class="close" data-dismiss="modal">×</button>
						
						<div class="regisindivs">
						<img src="https://mydoctoruae.com/public/assets/images/logo.png" alt="" class="img-responsive img-fluid">
						 <div id="resultpass"></div>
							<div class="form-group">
								
								<input type="text" class="form-control" id="oldpassword" placeholder="Old Password." name="oldpassword">
								
								
								<input type="text" class="form-control" id="password" placeholder="New Password" name="password">
								
								
								<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
									
								<input type="text" class="form-control" id="password_confirmation" placeholder="Confirm Password" name="password_confirmation">
								
								<div class="actionloginbs">
								<button id="getpasschange" type="button" class="btn btn-primary">Save</button>
								</div>
															
							</div>
						
						</div>
						
					</div>
			</div>
			
			<!-- Modal footer -->
			
			
		  </div>
		</div>
	</div>
	</div>
  

  <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal3" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p> Your Post Deleted Successfully</p>
		   <button class="close allokbtns" id="datadis12">Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>

   <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p> Password Changed successfully!!</p>
		   <button class="close allokbtns datadis12" >Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>


<script type="text/javascript">

   $("#getpasschange").click(function(){
   		var oldpassword = $("#oldpassword").val();

	var password = $("#password").val();

	var password_confirmation = $("#password_confirmation").val();

	var usertype = "user";

	var _token = $("#csrf-token").val();

	$.post("{{url('/')}}/changePassword",{oldpassword:oldpassword,password:password,password_confirmation:password_confirmation,usertype:usertype,_token:_token},function(result){
		    var data = JSON.parse(result);

		    if(data[0] == 1){
		    	$('#myModal2').modal('hide'); 
		    	$('#myModal4').modal('show'); 
		    } else {
		    	$("#resultpass").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    }
			//alert(data);
	});
   });
	
	

</script>

<script type="text/javascript">
	$(".delepostuda").click(function(){
		var post_id = $(this).attr("data");
		var _token = "{{ csrf_token() }}";
	    $.post("{{url('/')}}/deletepost",{post_id:post_id,_token:_token},function(result){
	    	$('#myModal3').modal('show'); 
	    });	
	});

	$(".datadis12").click(function(){
		location.reload();
	});
</script>
@endsection