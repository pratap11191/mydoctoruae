@extends('layout.site') 
@section('title', 'Doctor Signup')
@section('content')
 <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
   }
  </style>
<div class="docconsult">
        <div class="container">
            <div class="doctorsgbga">
                
                <div class="">
                    <div class="formslogins">
                       
                        
                        <div class="regisindivs">
                        <div class="col-md-12 main-hd">
                            <h1>Sign Up</h1>
                        </div>
                        <form action="{{action('API\HomeAPIController@doctorsignup')}}" method="post">
                            <div class="form-group">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                 <label for="exampleFormControlInput1">Full Name</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Full Name" name="fullname" value="<?php if(isset($_POST["fullname"])){ echo $_POST["fullname"]; } ?>">
                                <span class="formerror"><?php
                                    if(isset($messages['fullname']['0']) && !empty($messages['fullname']['0'])){
                                        echo $messages['fullname']['0'];
                                    }
                                ?></span>
                                </div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="exampleFormControlInput1">Email ID</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" name="email"  value="<?php if(isset($_POST["email"])){ echo $_POST["email"]; } ?>">
                                <span class="formerror"><?php
                                    if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                        echo $messages['email']['0'];
                                    }
                                ?></span>
                                </div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="exampleFormControlInput1">Phone Number</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Phone Number" name="phone" value="<?php if(isset($_POST["phone"])){ echo $_POST["phone"]; } ?>">
                                  <span class="formerror"><?php
                                    if(isset($messages['phone']['0']) && !empty($messages['phone']['0'])){
                                        echo $messages['phone']['0'];
                                    }
                                ?></span>
								</div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                               <label for="exampleFormControlInput1">City</label>
                                <select placeholder="name@example.com" name="city_id" id="city_id" required>
                                 <?php

                                   foreach ($all_name_cities as $key => $value) {
                                     
                                  
                                ?>
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                 
                                  <?php

                              }

                              ?>
                                </select>
                                </div>
								
								 <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1">Speciality</label>
                                <select placeholder="name@example.com" name="speciality_id" id="speciality_id">
                                 <?php

                                   foreach ($all_name as $key => $value) {
                                     
                                  
                                ?>
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                 
                                  <?php

                              }

                              ?>
                                </select>
								
                                 <input type="hidden" class="form-control" id="the_other_speciality" placeholder="enter your speciality" name="the_other_speciality">
                                </div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="exampleFormControlInput1">Clinic</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Clinic" name="clinic" value="<?php if(isset($_POST["clinic"])){ echo $_POST["clinic"]; } ?>">
                                  <span class="formerror"><?php
                                    if(isset($messages['clinic']['0']) && !empty($messages['clinic']['0'])){
                                        echo $messages['clinic']['0'];
                                    }
                                ?></span>
                                
								</div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="exampleFormControlInput1">License Number</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="License Number" name="licence_number" value="<?php if(isset($_POST["licence_number"])){ echo $_POST["licence_number"]; } ?>">
                                  <span class="formerror"><?php
                                    if(isset($messages['licence_number']['0']) && !empty($messages['licence_number']['0'])){
                                        echo $messages['licence_number']['0'];
                                    }
                                ?></span>
                                </div>
								
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1">Expertise Areas</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Expertise Areas" name="expertise_area" value="<?php if(isset($_POST["expertise_area"])){ echo $_POST["expertise_area"]; } ?>">
                                  <span class="formerror"><?php
                                    if(isset($messages['expertise_area']['0']) && !empty($messages['expertise_area']['0'])){
                                        echo $messages['expertise_area']['0'];
                                    }
                                ?></span>
                                </div>
								
								 <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="radionforms">
                                <label>Insurance Accepted</label>
                                <p><input type="radio" name="accepted" class="accepted" value="1">Yes
                                <input type="radio" name="accepted" class="accepted" value="0"> No</p>
                                </div>
                                <div id="divia" style="display: none;" >
                                	<input type="text" class="form-control" id="insurance_accept1" placeholder="enter your insurance" name="insurance_accept1">
                                	<div id="countryList">
    </div>
                                	<input type="hidden" class="form-control" id="insurance_accept" placeholder="enter your insurance" name="insurance_accept">
	                                <ol id="demo"></ol>
									<div class="addinsuranc">
									<input type='button' class="addinsubtn" onclick='changeText2()' value='Submit' />
									<i class="fa fa-plus"></i>
									</div>
                                </div>
								
								</div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="inputPassword">Password</label>
								<div class="showornotpas">
                                <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
								<div class="paswdshowsck">
								<input type="checkbox" class="oppwas" onclick="showpasswordFunction()"><i class="fa fa-eye"></i>
								</div>
                                  <span class="formerror"><?php
                                    if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                                        echo $messages['password']['0'];
                                    }
                                ?></span>
                                </div>
								</div>
								
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="inputPassword">Confirm Password</label>
								<div class="showornotpas">
								
                                <input type="password" class="form-control oppwasconf" id="inputPassword" placeholder="Confirm Password" name="password_confirmation">
								<div class="paswdshowsck">
								<input type="checkbox" id="pwchecknext"><i class="fa fa-eye"></i>
								</div>
								</div>
								 <span class="formerror"><?php
                                    if(isset($messages['password_confirmation']['0']) && !empty($messages['password_confirmation']['0'])){
                                        echo $messages['password_confirmation']['0'];
                                    }
                                ?></span>
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
								</div>
								
								 <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="actionloginbs">
                                <button type="submit" class="btn btn-primary">Sign Up</button>
                                </div>
                                    <div class="center section"><small class="fs13">Already have an account?<a  href="{{action('API\HomeAPIController@doctorlogin')}}"> Login Here</a></small></div>                       
                            </div>
							</div>
                        </form>
                        </div>
                        
                    </div>
                </div>
				
				<div class="signupsrg formslogins col-md-6 col-sm-12">
    
					<div class="flexmnd">
							
							<div class="ctndb col-md-3 col-xs-3">
								<i class="fa fa-stethoscope"></i>
								
							</div>
							
							<div class="ctndb">
								<div class="main-hd">
									<h1>Why you love doctorapp</h1>
								</div>
								
								<ul>
								<li><p><i class="fa fa-handshake-o"></i> Simple, beautiful design - you will pick it up in minute</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Online Private Consultation for Personalized car</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Answer Open Questions by Patients and build your online reputation</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Reduce no shows and increase patient connect with comprehensive set of Reminders</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Record treatments and schedule visits in 1/4th the time compared to your diary or any other software</p></li>
								<li><p><i class="fa fa-handshake-o"></i> Magical apps that help you do everything else easily on the go</p></li>
								
								</ul>
								<hr>			
							</div>
					</div>
					<div class="flexmnd">
						
						<div class="ctndb col-md-4 col-xs-3 col-sm-4">
							<i class="fa fa-credit-card"></i>
						</div>
						
						<div class="ctndb">
							<div class="main-hd">
								<h1>No String Attached</h1>
							</div>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae pellentesque erat. Proin congue massa quis lorem faucibus, at sollicitudin libero maximus. </p>
						</div>
					</div>
				</div>
				
            </div>
        </div>
    </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#speciality_id").change(function(){
       if($('#speciality_id').val() == '7'){
       		$('#the_other_speciality').attr('type','text');
       } else {
       		$('#the_other_speciality').attr('type','hidden');
       }
    });
});
</script> 

<script type="text/javascript">
	$(".accepted").click(function(){
		
		if($(this).val() == '1'){
			$('#divia').css('display','block');
		} else {
			$('#divia').css('display','none');
		}
	});
</script>

<script>
var list = document.getElementById('demo');
var lastid = 0;


function changeText2() {
    var firstname = document.getElementById('insurance_accept1').value;
    var insurance_accept_hidden = $('#insurance_accept').val();
    $('#insurance_accept').val(insurance_accept_hidden+","+firstname);
    var entry = document.createElement('li');
    entry.appendChild(document.createTextNode(firstname));
    entry.setAttribute('id','item'+lastid);
    var removeButton = document.createElement('button');
    removeButton.appendChild(document.createTextNode("x"));
    removeButton.setAttribute('onClick','removeName("'+'item'+lastid+'")');
    entry.appendChild(removeButton);
    lastid+=1;
    list.appendChild(entry);
}


function removeName(itemid){
    var item = document.getElementById(itemid);
   
    var insurance_accept_hidden = $('#insurance_accept').val();
   
    var newvar1 = insurance_accept_hidden.split(",");

    var i;
	for (i = 0; i < newvar1.length; i++) { 
	    if(newvar1[i]+'remove' == $('#'+itemid).text()){
	    	
	    	newvar1.splice(i, 1);
	    }
	}

	$('#insurance_accept').val(newvar1);
	 list.removeChild(item);
}
</script>

<script>
$(document).ready(function(){

 $('#insurance_accept1').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('autocomplete.fetch') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'li', function(){  
        $('#insurance_accept1').val($(this).text());  
        $('#countryList').fadeOut();  
    });  

});
</script>

@endsection

