@extends('layout.site') 
@section('title', 'About Us')
@section('content')

<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
   
</head>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;">
        <div class="container">
		
		    <div class="col-md-12"><h1>About Us </h1></div>
		    <div class="col-md-12">
			<p>Mydoctor is the best way to seek medical help. To make it seamless and smooth, we have created a platform which lets you connect with any medical professional instantly. Find out immediately about your insurance acceptance, availability of doctor and reviews about the place you want to visit..</p>
			</div>
			
			<div class="col-md-12" style="text-align:center; margin: 20px 0;"><h4>Mydoctor For Everyone</h4></div>
			<div class="col-md-6 linewel" style="border-right: 1px solid #ddd; min-height: 244px;">
			<p><strong>Talk to Doctor:</strong> You can ask queries for free or get into one-on-one interaction with doctors. </p>
			<p><strong>Stay healthy and fit:</strong> The Health Feed from healthcare experts aims to keep you healthy and fit. If that is not all, you can find a doctor nearby, check doctor's profile and book an appointment too.</p>
			</div>
			
			<div class="col-md-6">
			<p><img src="/public/assets/images/slider-banner/1m.png" class="img-responsive" alt=""></p>
			</div>
			
			<div class="col-md-12 ceter" style="text-align:center; margin: 20px 0;"><h4>Mydoctor For Doctors</h4><p><strong>Mydoctor ensures that doctors save time and enhance practice effortlessly.</strong></p></div>
			
			<div class="col-md-6" style="border-right: 1px solid #ddd; min-height: 244px;">
			<img src="/public/assets/images/slider-banner/4m.png" class="img-responsive" alt="">
			</div>
			
			<div class="col-md-6">
			<p><strong>Grow Your Practice:</strong> Through this doctors grow their outreach to patients across the country, by interacting with them. They not only get thanked by those whom they help out but also get recommended by peers. This increases their popularity in the online community and in the medical fraternity.</p>
			
			<p><strong>Manage Your Practice: </strong> This is highly evolved and easy-to-use practice management software. It enables doctors to manage information for one or more clinics. Doctors can be in touch with their patients and manage their practice on the go with mydoctor app.</p>
			</div>
		</div>
</div>
@endsection