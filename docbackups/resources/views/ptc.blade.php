<!DOCTYPE html>
<html lang="en">
<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    
    <!--main style sheet-->
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	
  
    
</head>
<body>

<div class="col-md-12 aboutus">
	<div class="container">
	    <div class="desc">
		    <h1>MY PRIVACY POLICY</h1>
			
        </div>
		
		<div class="feature">
			

			<ul>
				<li>
				<h6>1. OUR PRIVACY PROMISE</h6>
					<ul>
						<li>
						
							<p>12 We promise to keep your data safe and private. We promise not to sell your data and to give you ways to manage and review your marketing choices at any time.</p>
						</li>
					</ul>
				</li>
				
				<h6>2. WHEN WE COLLECT INFORMATION ABOUT YOU</h6>
				
				<li><p>We may collect personal information from you during your use of the website or the app, such as when you register for an account, purchase products and/or services, or when you participate in competitions, questionnaires and surveys. We may also collect information when you opt-in to newsletters, when you contact us (via phone, email, live chat or social media), and when you provide certain content to My Doctor App, for example testimonials and customer reviews. Additionally, we may collect personal information via implicit data capture measures when you visit the website, such as studying which pages you read the most and through the use of cookies (for further information on cookies, please see the ‘How we use cookies’ section.</p></li>
				
				<h6>3. WHAT INFORMATION WE COLLECT ABOUT YOU AS A My Doctor APP CUSTOMER</h6>
				<li><p>The type of information we may collect includes:</p></li>
				<p>Name</p>
				<p>Contact information: email address and telephone number, and name and number of your designated emergency contact</p>
				<p>Location information: postcode and IP address and Latitude, Longitude when allowing location on the app</p>
				<p>Date of Birth</p>
				<p>Health information (provided upon completion of your health questionnaire during your first purchase).</p>
				<p>Details of the device used to purchase or contact us.</p>
				<p>Facebook User ID if you created a My Doctor App account using Facebook.</p>
				<p>Your marketing communication preferences.</p>
				<p>Details of your previous visits to the website or app including your purchase.</p>
				<p>Other information relevant to customer surveys and/or offers or competitions.</p>
				<p>Information connected with reviews you leave on our website or app.</p>
				<p>Body analysis through the Bluetooth scale after registering.</p>	
			</ul>
			

			<ul>
			<h6>4. WHAT INFORMATION WE COLLECT ABOUT YOU AS A PARTNER </h6>
				<li>
				<p>39 Where an individual has provided their personal details as contact information for their business we may collect the following personal data:</p>
					<ul>
						<li><p>Name</p></li>
						<li><p>Contact information: email address and telephone number(s).</p></li>
						<li><p>Details of the device used to contact us.</p></li>
						<li><p>Details of your previous visits to the website or app.</p></li>
						<li><p>Other information relevant to operator surveys.</p></li>
						
					</ul>
				</li>
				
			
			
			</ul>
			
		</div>
	</div>
</div>

   
</body>
</html>
