@extends('layout.site') 
@section('title', 'My Doctor Chat')
@section('content')

<div class="news-feedss chaholders">
        <div class="container">
           <div class="chat-section"> 
			
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 chatboards">
                <div class="row">
				<div class="chat-headers-imges backbtns">
				<i class="fa fa-long-arrow-left"></i>
				</div>
				<div id="one" class="tabcontent">
                    <div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="{{asset('public/assets/images/author-main1.jpg')}}" class="img-responsive">
                                    <span class="chat_useru_profile-name">James Bond</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button class="sen_btn_chat_mssg"><img src="{{asset('public/assets/images/chat_send.png')}}" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				</div>


				<div id="two" class="tabcontent">
                    <div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="{{asset('public/assets/images/author-main2.jpg')}}" class="img-responsive">
                                    <span class="chat_useru_profile-name">John Doe</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button class="sen_btn_chat_mssg"><img src="{{asset('public/assets/images/chat_send.png')}}" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				</div>


                <div id="three" class="tabcontent" style="display:block;">
                    <div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="{{asset('public/assets/images/author-main3.jpg')}}" class="img-responsive">
                                    <span class="chat_useru_profile-name">Peter Kevin</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button class="sen_btn_chat_mssg"><img src="{{asset('public/assets/images/chat_send.png')}}" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				</div>


            <div id="four" class="tabcontent">
                    <div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="{{asset('public/assets/images/author-main4.jpg')}}" class="img-responsive">
                                    <span class="chat_useru_profile-name">David Ward</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button class="sen_btn_chat_mssg"><img src="{{asset('public/assets/images/chat_send.png')}}" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				</div>


                <div id="five" class="tabcontent">
                    <div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="{{asset('public/assets/images/author-main5.jpg')}}" class="img-responsive">
                                    <span class="chat_useru_profile-name">Quincy Catherine</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button class="sen_btn_chat_mssg"><img src="{{asset('public/assets/images/chat_send.png')}}" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				</div>

                <div id="six" class="tabcontent">
                    <div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="{{asset('public/assets/images/author-main1.jpg')}}" class="img-responsive">
                                    <span class="chat_useru_profile-name">Samual Martin</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button class="sen_btn_chat_mssg"><img src="{{asset('public/assets/images/chat_send.png')}}" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				</div>				
                </div>
            </div>
			
			<div class="chat_list-side col-xs-12 col-sm-4 col-md-3 col-lg-3">
                <div class="row">
                        <ul>
                            <li class="tablinks" onclick="openCity(event, 'one')">
                                <a href="#" class="activechats">
                                    <div class="img-profile-section-chat-side">
                                        <img src="{{asset('public/assets/images/author-main1.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="img-profile-section-chat-aside">
                                        <h4>James Patrick</h4>
                                        <p>Active</p>
                                    </div>
                                </a>
                            </li>
                            <li class="tablinks" onclick="openCity(event, 'two')">
                                <a href="#" class="activechats">
                                    <div class="img-profile-section-chat-side">
                                        <img src="{{asset('public/assets/images/author-main2.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="img-profile-section-chat-aside">
                                        <h4>John Doe</h4>
                                        <p>Away</p>
                                    </div>
                                </a>
                            </li>
                            <li  class="tablinks active" onclick="openCity(event, 'three')">
                                <a href="#" class="activechats">
                                    <div class="img-profile-section-chat-side">
                                        <img src="{{asset('public/assets/images/author-main3.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="img-profile-section-chat-aside">
                                        <h4>Peter Kevin</h4>
                                        <p>Active</p>
                                    </div>
                                </a>
                            </li>
                            <li class="tablinks" onclick="openCity(event, 'four')">
                                <a href="#" class="activechats">
                                    <div class="img-profile-section-chat-side">
                                        <img src="{{asset('public/assets/images/author-main4.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="img-profile-section-chat-aside">
                                        <h4>David Ward</h4>
                                        <p>Away</p>
                                    </div>
                                </a>
                            </li>
                            <li class="tablinks" onclick="openCity(event, 'five')">
                                <a href="#" class="activechats">
                                    <div class="img-profile-section-chat-side">
                                        <img src="{{asset('public/assets/images/author-main5.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="img-profile-section-chat-aside">
                                        <h4>Quincy Catherine</h4>
                                        <p>Active</p>
                                    </div>
                                </a>
                            </li>
                            <li class="tablinks" onclick="openCity(event, 'six')">
                                <a href="#" class="activechats">
                                    <div class="img-profile-section-chat-side">
                                        <img src="{{asset('public/assets/images/author-main1.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="img-profile-section-chat-aside">
                                        <h4>Samual Martin</h4>
                                        <p>Active</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
            </div>
            
        </div>
    </div>
</div>
@endsection