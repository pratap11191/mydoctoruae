@extends('layout.admins') 
@section('title', 'User List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    User Management List
                </h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                               Phone No. 
                            </th>
                            <th>
                                Gender
                            </th>
                           
                            <th>
                                Profile Image
                            </th>
                            <th>
                             Action 
                             </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->username}}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->phone}}
                            </td>
                            <td>
                                {{$user->gender}}
                            </td>
                            
                            
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                             <td>
                             <!-- <a  href="{{url('admin/edit_user_details')}}/{{$user->id}}"><i class="fa fa-edit" style="font-size:24px;color:black;"></i>
                               </a> -->
                                <a class="action_an" href="{{url('admin/common_delete')}}/{{$user->id}}/users" >
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                             </td>
                            <td>
                               
                               
                                        @if($user->admin_status == 0)
                                         <a class="action_an btn btn-danger" href="{{url('admin/change_status')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Inactive"; ?>
                                         </span>
                                </a>
                                        @else
                                         <a class="action_an btn btn-success" href="{{url('admin/change_status')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Active" ?>
                                         </span>
                                </a>
                                        @endif
                                   
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

@endsection