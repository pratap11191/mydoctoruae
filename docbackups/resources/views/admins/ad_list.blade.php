@extends('layouts.admins') 
@section('title', 'Ads List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Advertisement List
                </h3>
                <a style="float:right;margin-bottom:10px;" class="action_an btn btn-primary" href="{{url('admin/upload-image')}}" >
                                 Create New                                         
                                </a>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Banner
                            </th>
                            
                            <th>
                                Date
                            </th>
                            <th>
                             Action 
                             </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($ads))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($ads as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                            <?php
                            
                            
                            if($user->type == 'video'){
                            
                            ?>
                            
                            <a href="{{url('/')}}/public/ads/{{$user->ad_image}}" target="_blank">Play Video</a>
                                
                                <?php
                                
                                
                                
                                } else {
                                
                                ?>
                               
                                 <img alt="Avatar" class="img-circle"  src="{{url('/')}}/public/ads/{{$user->ad_image}}" style="height:50px">
                                </img>
                                <?php
                                
                                
                                }
                                
                                ?>
                                
                            </td>
                            <td>
                                {{$user->created_at}}
                            </td>
                           
                           
                            
                            
                           
                             <td>
                           
                                <a class="action_an" href="{{url('admin/common_delete')}}/{{$user->id}}/advertisements" >
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
                                    </span>
                                </a>
                             </td>
                            <td>
                               
                               
                                        @if($user->status == 0)
                                         <a class="action_an btn btn-danger" href="{{url('admin/change_status_ad')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Inactive"; ?>
                                         </span>
                                </a>
                                        @else
                                         <a class="action_an btn btn-success" href="{{url('admin/change_status_ad')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Active" ?>
                                         </span>
                                </a>
                                        @endif
                                   
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

@endsection