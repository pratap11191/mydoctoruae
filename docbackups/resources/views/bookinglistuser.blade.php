@extends('layout.site') 
@section('title', 'Booking list of user')
@section('content')


<div id="Addposts" class="postadcontent newsfeeddivs" style="display: block;">
	<div class="userbookingaddp">
		<div class="container">
				<div class="hading_by_title">
						<h4><?php echo ucfirst(Request::session()->get('userdetails', 'default')->usertype); ?> Appointments</h4>
				</div>
				
				   <?php
							  // Here is the data we will be sending to the service
							  $some_data = array(
							    'user_id' => Request::session()->get('userdetails', 'default')->id, 
							    'usertype' => Request::session()->get('userdetails', 'default')->usertype
							  );  

							  $curl = curl_init();
							  // You can also set the URL you want to communicate with by doing this:
							  // $curl = curl_init('http://localhost/echoservice');

							  // We POST the data
							  curl_setopt($curl, CURLOPT_POST, 1);
							  // Set the url path we want to call
							  curl_setopt($curl, CURLOPT_URL, url('/')."/api/bookinglist");  
							  // Make it so the data coming back is put into a string
							  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							  // Insert the data
							  curl_setopt($curl, CURLOPT_POSTFIELDS, $some_data);

							  // You can also bunch the above commands into an array if you choose using: curl_setopt_array

							  // Send the request
							  $result = curl_exec($curl);

							 
							  curl_close($curl);

							// echo $result;

							 $result1 = json_decode($result)->data;

							 //print_r($result1);

							 
							  	
							?>
				
				
				<div class="col-md-12 flitby">
				
				
			
					<!-- <div class="filterbyapp">
						<h3>Filter By</h3>
					</div> -->
					
					<div class="formslogins">
						<div class="form-group">
							<div class="appbk">
								<p><strong>By Dates: </strong></p><p><?php echo $result1->month; ?></p></i>
							</div>
							
							<div class="daysshed">
							
							
							<div class="text-left">
							
							
							<ul id="datesbooks">
							<?php

								if(Request::session()->get('userdetails', 'default')->usertype == 'doctor'){

							?>
							<div class="bookbtn">
								<button class="posttablinks btn btn-primary" data-toggle="modal" data-target="#myModal2">Add Patient <i class="fa fa-plus"></i></button>
							</div>

							<?php

						}

						?>

							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day1')">
								<div class="ddy"><?php echo $result1->day1->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day1->day; ?></div>
							  </li>
							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day2')">
								<div class="ddy"><?php echo $result1->day2->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day2->day; ?></div>
							  </li>
							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day3')">
								<div class="ddy"><?php echo $result1->day3->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day3->day; ?></div>
							  </li>
							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day4')">
								<div class="ddy"><?php echo $result1->day4->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day4->day; ?></div>
							  </li>
							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day5')">
								<div class="ddy"><?php echo $result1->day5->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day5->day; ?></div>
							  </li>
							  
							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day6')">
								<div class="ddy"><?php echo $result1->day6->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day6->day; ?></div>
							  </li>
							  
							  <li class="daysp" class="tablinks" onclick="openCity(event, 'day7')">
								<div class="ddy"><?php echo $result1->day7->day_name; ?></div>
								<div class="datesddy"><?php echo $result1->day7->day; ?></div>
							  </li>
							</ul>
						</div>
							</div>
							
						</div>
					</div>
			
				</div>

				<?php

				if(Request::session()->get('userdetails', 'default')->usertype == 'doctor'){

					?>

					<div id="day1" class="col-md-12 appolist tabcontent" style="display: block;">

				<?php

				//print_r();

					foreach ($result1->booking_list->day1 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				</div>
				
				<div id="day2" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day2 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>


				<?php

			}

			?>
				
				
				</div>
				
				
				
				<div id="day3" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day3 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>


				<?php

			}

			?>
				
				
				</div>
				
				
				<div id="day4" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day4 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				
				</div>
				
				
				<div id="day5" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day5 as $value12) {
						
						
				?>
				
			<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				</div>
				
				
				<div id="day6" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day6 as $value12) {
						
						
				?>
				
			<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				
				</div>
				
				
				<div id="day7" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day7 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->email)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->phone)}}</span>
							
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						
						</div>    
                    </div>
					
                </div>
					</div>
				</a>


				<?php

			}

			?>
				
				
				
				</div>

					<?php

					} else { ?>


					<div id="day1" class="col-md-12 appolist tabcontent" style="display: block;">

				<?php

				//print_r();

					foreach ($result1->booking_list->day1 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>USD ${{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				</div>
				
				<div id="day2" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day2 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>USD ${{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				</div>
				
				
				
				<div id="day3" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day3 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>USD ${{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				</div>
				
				
				<div id="day4" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day4 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>USD ${{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				
				</div>
				
				
				<div id="day5" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day5 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>USD ${{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				</div>
				
				
				<div id="day6" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day6 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>USD ${{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				
				</div>
				
				
				<div id="day7" class="col-md-12 appolist tabcontent" style="display: none;">
				
				<?php

				//print_r();

					foreach ($result1->booking_list->day7 as $value12) {
						
						
				?>
				
				<a href="#">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

						<div class="your-posts-section bookingtabs doctlsttbsa bookapps">
                    <div class="your-posts-pic-para">
                            <div class="post-profile">
                                <img src="{{ucfirst($value12->image)}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value12->fullname)}}</h4>
                                <span class="bookinfo desg-post">{{ucfirst($value12->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value12->city_name)}}</span>
								<span class="appoi">Appointment: <p class="dateapp">{{ucfirst($value12->apdate)}}</p> at <p class="timeapp">{{ucfirst($value12->aptime)}}</p></span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						<i class="fa fa-comment" aria-hidden="true"></i>
						<p>AED  {{ucfirst($value12->fee)}}</p>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availablity:</span> <ul class="yourpost-ul">
									<?php
										
										if(!empty($value12->avilability)){

											foreach ($value12->avilability as  $valuenew) {
												
										 ?>

										 <li>{{$valuenew}}</li>


										<?php
									}

										} 
									?>	
									</ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
				</a>

				<?php

			}

			?>
				
				
				
				</div>

					<?php 


				}

				?>
				
				
				
				
				
		</div>
	</div>
	
	</div>
	
	<div id="addpostform" class="col-md-12  postadcontent newsfeeddivs" style="display: none;">
	
	
	<div class="bookbtn">
					<button class="posttablinks btn btn-primary"><i class="fa fa-caret-left"></i> back</button>
					</div>
					
					<div class="formslogins">
					   
						
						<div class="regisindivs">
						<div class="main-hd">
							<h1>Add Patient</h1>
						</div>
						
						</div>
						
					</div>
	</div>
	
	<div class="docconfirmbew">
	<div class="modal fade" id="myModal2" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  
			<!-- Modal Header -->
			
			
			<!-- Modal body -->
			<div class="modal-body">
			 <div class="formslogins">
					   <div class="main-hd">
							<h1>Add Patient</h1>
						</div>
						 <button type="button" class="close" data-dismiss="modal">×</button>
						
						<div class="regisindivs">
						<img src="https://mydoctoruae.com/public/assets/images/logo.png" alt="" class="img-responsive img-fluid">
						<div id="resultpass"></div>
							<div class="form-group">
							
								<label for="exampleFormControlInput1">First Name</label>
								<input type="text" class="form-control" placeholder="Patient Name.." name="firstname" id="firstname">
								
								<label for="exampleFormControlInput1">Last Name</label>
								<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name..">
								
								<label for="exampleFormControlInput1">Email ID</label>
								<input type="email" class="form-control" id="email" placeholder="name@example.com" name="email">
								
								<label for="exampleFormControlInput1">Phone Number</label>
								<input type="text" class="form-control" id="phone" placeholder="phone number" name="phone">
								
								<label for="exampleFormControlInput1">Gender</label>
								<select id="gender" name="gender">
								  <option  value="Male">Male</option>
								  <option value="Female">Female</option>
								</select>
								
								<label for="exampleFormControlInput1">Age</label>
								<select id="age" name="age">
								 <?php

								 	for ($i=18; $i < 100 ; $i++) { 
								 		
								 ?>
								  <option value="{{$i}}">{{$i}}</option>
								  <?php
								  	}
								  ?>
								  
								</select>
								
								
								<label for="exampleFormControlInput1">Appointment Date</label>
								<input class="form-control picker-input input-bboking" type="date" id="apdate" name="apdate">
								
								<label for="exampleFormControlInput1">Appointment Time</label>
								<input type="text" class="form-control" id="aptime" placeholder="time" name="aptime">
								
								<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
								
								<div class="actionloginbs">
								<button id="getpasschange" type="button" class="btn btn-primary">Add</button>
								</div>
													
							</div>
						
						</div>
						
					</div>
			</div>
			
			<!-- Modal footer -->
			
			
		  </div>
		</div>
	</div>
	</div>

<div class="deletesucchealthapp">
  <div class="modal fade" id="myModal3" style="display: none;">
		<div class="modal-dialog">
		  <div class="modal-content">
		  <div class="modal-body">
		  <div class="succhealth">
		   <i class="fa fa-check"></i>
		   </div>
		  
		 
		 
		  <div class="setbtnoks">
		   <h1>Success!</h1>
		  <p> Appointment Fixed Successfully</p>
		   <button class="close allokbtns" id="datadis12">Ok</button>
		  </div>
		  </div>

		  </div>
		  </div>
		  </div>
   </div>

	<script type="text/javascript">

   $("#getpasschange").click(function(){
   		var firstname = $("#firstname").val();
   		var lastname = $("#lastname").val();
   		var email = $("#email").val();
   		var phone = $("#phone").val();
   		var gender = $("#gender").val();
   		var age = $("#age").val();
   		var apdate = $("#apdate").val();
   		var aptime = $("#aptime").val();
	

	var _token = $("#csrf-token").val();

	$.post("{{url('/')}}/addPatient",{firstname:firstname,lastname:lastname,email:email,phone:phone,gender:gender,age:age,apdate:apdate,aptime:aptime,_token:_token},function(result){
		    var data = JSON.parse(result);
		   // alert(data[0]);
		    if(data[0] == 1){
		    	$('#myModal2').modal('hide'); 
		    	$('#myModal3').modal('show'); 
		    } else {
		    	$("#resultpass").html('<div class="alert alert-danger">'+data[1]+'</div>');
		    }
			//alert(data);
	});
   });
	
	
	$("#datadis12").click(function(){
		location.reload();
	});

</script>

@endsection