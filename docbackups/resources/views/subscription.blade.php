@extends('layout.site') 
@section('title', 'Subscription list')
@section('content')

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-feedss">
        <div class="container">
		<div class="nfdc">
            <div class="hading_by_title">
                <h4>Subscription Plans</h4>
            </div>
            <?php

           // dd();
            if(!empty($listdetails->toArray())){

                foreach ($listdetails->toArray() as  $value) {
                   
                   //print_r($value);
            ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="your-posts-section">
                    <div class="your-subsc-pic-para">
                        <a href="subscriptiondescription.html" class="newsfeed_a_all">
                            <div class="subs-image">
                                <img src="{{url('/')}}/{{$value['image']}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>{{ucfirst($value['title'])}}</h4>
								<span class="subsprice">AED {{ucfirst($value['price'])}}</span>
                                <p>{{ucfirst($value['description'])}}</p>
                            </div>
                        </a>  
                    </div>
                    <?php

                    if($value['flag_plan'] == '-1')
                        { ?>
  <div class="subscriptionbtn">
                        <a href="{{url('/')}}/plan_map/{{$value['id']}}" class="btn btn-primary">Buy</a>
                    </div>
                    <?php

                    } else if($value['flag_plan'] == '0'){ ?>
  <div class="subscriptionbtn">
                        <button type="submit" class="btn btn-primary flag_planpp">Buy</button>
                    </div>

                    <?php

                    } else if($value['flag_plan'] == '1'){

                      ?>
                      <p style="color: red;">Your Subscription Expire Date is {{$value['exp_date']}}</p>
  <div class="subscriptionbtn">
                        <button type="submit" class="btn btn-primary">Purchased</button>
                    </div>

                    <?php

                    }
                    ?>
                  
                </div>
            </div>
           <?php 

            }

       } else {
            echo "No Record Found!!";
       }

       ?>
			</div>
        </div>
    </div>
<script type="text/javascript">
    $('.flag_planpp').click(function(){
        alert("You’ve already purchased a subscription");
    });
</script>
    @endsection