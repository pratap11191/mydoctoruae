@extends('layout.site') 
@section('title', 'Doctor Profile')
@section('content')
@if (Session::has('message_passchange'))
    <div class="alert alert-info">{{ Session::get('message_passchange') }}</div>
@endif

 <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
   }
  </style>
<div class="userpropge">
        <div class="container">
            <div class="morepagessection">
                <div class="">
                        <div class="tabdocts">
                          <button class="tablinks active" onclick="openCity(event, 'Userpro')">My Profile</button>
                          <button class="tablinks" onclick="openCity(event, 'London')">Your Posts</button>
                          <button class="tablinks" onclick="openCity(event, 'Paris')">Notification</button>
                          <button class="tablinks"  id="myModel2">Change Password</button>
                        </div>


                        <div id="Userpro" class="tabcontent" style="display:block;">
                            <div class="userprofilename">
                                <form action="{{url('/')}}/editDoctorProfile" method="post" enctype="multipart/form-data">
                                <div class="userdescpro">
                                
                                <!--    <h4>My Profile</h4> -->
                                    
                                    <div class="imageuserdocs">
                                        <div class="profileinf">
                                        <img id="profile-img-tag" src="<?php
                                        if(empty(Request::session()->get('userdetails', 'default')->image)){
                                            echo 'https://mydoctoruae.com/public/img/user_signup.png';
                                        } else {
                                            echo url('/').'/public/'.Request::session()->get('userdetails', 'default')->image;
                                        }
                                        ?>">
                                        <i class="fa fa-camera"></i>
                                          <input id="profile-img" class="cameraupload" type="file" name="image" accept="image/*">
                                        </div>
                                        
                                        <script type="text/javascript">
                                            function readURL(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();
                                                    
                                                    reader.onload = function (e) {
                                                        $('#profile-img-tag').attr('src', e.target.result);
                                                    }
                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }
                                            $("#profile-img").change(function(){
                                                readURL(this);
                                            });
                                        </script>
                                         <h1 class="userdesigname"><?php echo ucfirst(Request::session()->get('userdetails', 'default')->fullname); ?></h1>
                                         <p class="postypedt"><?php echo ucfirst(Request::session()->get('userdetails', 'default')->specialties_name); ?></p>
                                         <p class="licen">License Number - <?php echo Request::session()->get('userdetails', 'default')->licence_number; ?></p>
                                        
                                    </div>
                                    
                                    
                                    <div class="editinfofieldsdta">
                                    
                                        <div class="col-md-6 col-sm-12 equalpart bdrsep">
                                        <div class="fullwdth">
                                            <label class="userfieldsnk" for="exampleFormControlInput1">Clinic Name</label>
                                            <span class="useridprint"><?php echo Ucfirst(Request::session()->get('userdetails', 'default')->clinic); ?></span>
                                            <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                            
                                            <div class="profilepanel revelfieldedit formslogins">
                                                
                                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Clinic Name" value="<?php echo Ucfirst(Request::session()->get('userdetails', 'default')->clinic); ?>" name="clinic">
                                            </div>
                                            
                                        </div>
                                        
                                        
                                        <div class="fullwdth">
                                            <label class="userfieldsnk" for="exampleFormControlInput1"> Qualification</label>
                                            <span class="useridprint"><?php echo Ucfirst(Request::session()->get('userdetails', 'default')->qualification); ?></span>
                                            <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                            
                                            <div class="profilepanel revelfieldedit formslogins">
                                                
                                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Qualification" name="qualification" value="<?php echo Ucfirst(Request::session()->get('userdetails', 'default')->qualification); ?>">
                                            </div>
                                            
                                        </div>
                                        
                                        
                                        <div class="fullwdth">
                                            <label class="userfieldsnk" for="exampleFormControlInput1">Fees (AED)</label>
                                            <span class="useridprint"><?php echo Ucfirst(Request::session()->get('userdetails', 'default')->fee); ?></span>
                                            <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                            
                                            <div class="profilepanel revelfieldedit formslogins">
                                                
                                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Fees (USD)" name="fee" value="<?php echo Ucfirst(Request::session()->get('userdetails', 'default')->fee); ?>">
                                            </div>
                                            
                                        </div>
                                    
                                    </div>
                                        
                                        
                                        <div class="col-md-6 col-sm-12 equalpart">
                                        <div class="fullwdth">
                                            <label class="userfieldsnk" for="exampleFormControlInput1">Working Hours</label>
                                            
                                            <span class="useridprint"><?php echo Request::session()->get('userdetails', 'default')->start_time." to ".Request::session()->get('userdetails', 'default')->end_time; ?></span>
                                            <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                            
                                            <div class="profilepanel revelfieldedit formslogins">
                                                
                                                <div class="fromrage col-md-6">
                                                <p><strong>From:</strong> Hours</p>
                                                <select placeholder="name@example.com" name="start_time">
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                                  <option value="10">10</option>
                                                  <option value="11">11</option>
                                                  <option value="12">12</option>
                                                  <option value="13">13</option>
                                                  <option value="14">14</option>
                                                  <option value="15">15</option>
                                                  <option value="16">16</option>
                                                  <option value="17">17</option>
                                                  <option value="18">18</option>
                                                  <option value="19">19</option>
                                                  <option value="20">20</option>
                                                  <option value="21">21</option>
                                                  <option value="22">22</option>
                                                  <option value="23">23</option>
                                                  <option value="00">00</option>
                                                </select>
                                                </div>
                                                
                                                
                                                <div class="fromrage col-md-6">
                                                <p><strong>To:</strong> Hours</p>
                                                <select placeholder="name@example.com" name="end_time">
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                                  <option value="10">10</option>
                                                  <option value="11">11</option>
                                                  <option value="12">12</option>
                                                  <option value="13">13</option>
                                                  <option value="14">14</option>
                                                  <option value="15">15</option>
                                                  <option value="16">16</option>
                                                  <option value="17">17</option>
                                                  <option value="18">18</option>
                                                  <option value="19">19</option>
                                                  <option value="20">20</option>
                                                  <option value="21">21</option>
                                                  <option value="22">22</option>
                                                  <option value="23">23</option>
                                                  <option value="00">00</option>
                                                </select>
                                                </div>
                                                
                                            </div>
                                        </div>  
                                        
                                        <div class="fullwdth">
                                            <label class="userfieldsnk" for="exampleFormControlInput1">Availability(days)</label>
                                            
                                            <span class="useridprint"><?php echo Request::session()->get('userdetails', 'default')->avilability; ?></span>
                                            <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                            
                                            <div class="profilepanel revelfieldedit formslogins">
                                                <?php
                                                    $unique_elm = explode(",",Request::session()->get('userdetails', 'default')->avilability)
                                                ?>
                                                
                                                <select name="avilability[]" placeholder="name@example.com" class="limitedNumbChosen" multiple="multiple">
                                                  <option value="M" <?php if(in_array("M", $unique_elm)) { echo "selected"; } ?>>Monday</option>
                                                  <option value="T" <?php if(in_array("T", $unique_elm)) { echo "selected"; } ?>>Tuesday</option>
                                                  <option value="W" <?php if(in_array("W", $unique_elm)) { echo "selected"; } ?>>Wednesday</option>
                                                  <option value="Th" <?php if(in_array("Th", $unique_elm)) { echo "selected"; } ?>>Thursday</option>
                                                  <option value="F" <?php if(in_array("F", $unique_elm)) { echo "selected"; } ?>>Friday</option>
                                                  <option value="Sat" <?php if(in_array("Sat", $unique_elm)) { echo "selected"; } ?>>Saturday</option>
                                                  <option value="S" <?php if(in_array("S", $unique_elm)) { echo "selected"; } ?>>Sunday</option>
                                                </select>
                                            </div>
                                            
                                            
                                            
                                        </div>
                                        
                                        
                                        
                                        <div class="fullwdth">
                                            <label class="userfieldsnk" for="exampleFormControlInput1">Location</label>
                                            <span class="useridprint"><?php echo Request::session()->get('userdetails', 'default')->city_name; ?></span>
                                            <div class="editdownf accordion"><i class="fa fa-map-marker"></i></div>
                                            
                                            <div class="profilepanel revelfieldedit formslogins">
                                                
                                                <select class="limitedNumbChosen" id="city_id" name="city_id" >
                        <option value="">select city</option>
                        <?php foreach ($city_list as $value) { ?>   
        <option value="<?php echo $value->id.','.$value->name; ?>" <?php if(Request::session()->get('userdetails', 'default')->city_id == $value->id){ echo "selected"; } ?>><?php echo ucfirst($value->name); ?></option>
       <?php } ?>
        </select>
                                            </div>
                                            
                                        </div>
                                        
                                        </div>
                                        
                                        <div class="insurspaces">
                                        <div class="col-md-12 col-sm-12 equalpart bdrsep insurancapts">
                                            <label class="userfieldsnk" for="exampleFormControlInput1" style="color:red;">Insurance Accepted:-</label>
                                            <?php 
                                            
                                            if(!empty(Request::session()->get('userdetails', 'default')->insurance_accept1)){
                                            
                                            $insarray = explode(",",Request::session()->get('userdetails', 'default')->insurance_accept);

                                            $insarray23 = explode(",",Request::session()->get('userdetails', 'default')->insurance_accept1); ?>
                                            <span class="useridprint" >
                                                <?php
                                                    $ik2255 = 0;
                                                    foreach ($insarray as  $value) {

                                                     ?>
                                                     <p style="background:#31708f;border-radius: 5px;"><a href="{{url('/')}}/rem/{{$insarray23[$ik2255]}}"><i class="fa fa-close"></i></a>{{ucfirst($value)}}</p>
                                                        <?php

                                                        $ik2255++;
                                                    }
                                                    
                                                    
                                            } else {
                                                echo "Not Any Insurance Accepted";
                                            }
                                                 ?>
                                               
                                                
                                            </span>
                                             <div class="editdownf accordion"><i class="fa fa-edit"></i></div>
                                             <div class="profilepanel revelfieldedit formslogins" style="max-height: auto;">
                                                
                                                
                                    <input type="text" class="form-control" id="insurance_accept1" placeholder="enter your insurance" name="insurance_accept1">
                                  
                                  <input type="hidden" class="form-control" id="insurance_accept" placeholder="enter your insurance" name="insurance_accept">
                                  <ol id="demo"></ol>
                  <div class="addinsuranc">
                  <input type='button' class="addinsubtn" onclick='changeText2()' value='Submit' />
                
                  </div>
                                            </div>
                                            
                                        </div>
                                        
                                  <div id="countryList">
                                    </div>      
                                        
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <div class="actionloginbs">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                            
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                            </form>
                        <div id="London" class="tabcontent">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-feedss">
                                <div class="">
                                     <div class="bookbtn">
                                        <button class="posttablinks btn btn-primary"  id="myModel236">Add Posts <i class="fa fa-plus"></i></button>
                                      </div>
                                    <div class="hading_by_title">
                                        <h4>Your Posts</h4>
                                    </div>

                                    <?php
                                    

                                    if(empty($get_news_feedlist)){ ?>
                                        <p class="nodtafdpos">No record found</p>
                            <?php       } else {

                                    foreach ($get_news_feedlist as $value) {
                                        
                                    //print_r($value);

                             ?>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <div class="your-posts-section">
                                            <div class="your-posts-pic-para">
                                                <a href="{{$value->shareurl}}" class="newsfeed_a_all">
                                                    <div class="post-profile">
                                                        <img src="{{$value->image}}" class="img-responsive">
                                                    </div>
                                                    <div class="post-profile-paraes">
                                                        <h4>{{ucfirst($value->title)}}</h4>
                                                        <span class="desg-post">Gernal Physician</span>
                                                        <p>{{ucfirst($value->description)}}</p>
                                                    </div>
                                                </a>  
                                            </div>
                                            <div class="chatcomment delepostuda" data="{{$value->post_id}}">
                                                    <i class="fa fa-trash"></i>
                                            </div>
                                            <div class="your-post-anchore">
                                                <ul class="yourpost-ul">
                                                    <li><a href="{{$value->shareurl}}"><span class="yourposticon"><i class="fa fa-comment" aria-hidden="true"></i></span></a></li>                                          
                                                    <li><a href="#"><span class="yourposticon"><i class="fa fa-share-alt" aria-hidden="true"></i></span></a>
                                                        <ul class="your-post-share-ul">
                                                            <li><a href="https://api.whatsapp.com/send?&text={{$value->shareurl}}" target="_blank">Whatsapp</a></li>
                                     <li><a href="https://www.facebook.com/sharer/sharer.php?u={{$value->shareurl}}" target="_blank">Facebook</a></li>
                                     <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{$value->shareurl}}&title={{ucfirst($value->title)}}&summary={{ucfirst($value->description)}}&source=Healthapp" target="_blank">LinkedIn</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                <?php

}

                                    }
                                    ?>
                                    
                                    
                                </div>
                        </div>
                        </div>

                        <div id="Paris" class="tabcontent">
                        
                           <!-- <div class="enablenotify">
                            <label class="switch">
                                  <input type="checkbox" checked="">
                                  <span class="slidergreen round"></span>
                            </label>
                            <p class="enabtxt">Enable/Disable Notification</p>
                           </div> -->
                           <?php
                              // Here is the data we will be sending to the service
                              $some_data = array(
                                'user_id' => Request::session()->get('userdetails', 'default')->id, 
                                'usertype' => "user"
                              );  

                              $curl = curl_init();
                              // You can also set the URL you want to communicate with by doing this:
                              // $curl = curl_init('http://localhost/echoservice');

                              // We POST the data
                              curl_setopt($curl, CURLOPT_POST, 1);
                              // Set the url path we want to call
                              curl_setopt($curl, CURLOPT_URL, url('/')."/api/notifylist");  
                              // Make it so the data coming back is put into a string
                              curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                              // Insert the data
                              curl_setopt($curl, CURLOPT_POSTFIELDS, $some_data);

                              // You can also bunch the above commands into an array if you choose using: curl_setopt_array

                              // Send the request
                              $result = curl_exec($curl);

                             
                              curl_close($curl);

                            

                              if(!empty(json_decode($result)) && json_decode($result)->status != "FAILURE"){

                             $result1 = json_decode($result)->data;

                             //print_r($result1);

                              foreach($result1 as $value) {
                                
                             // print($value);
                                
                            ?>
                          <div class="notiftabs">
                            <div class=" userpic">
                                <img src="{{$value->image}}">
                            </div>
                            
                            <div class="usernotify">
                            <p>{{ucfirst($value->msg)}}</p> 
                            <span class="notifytimes">{{ucfirst($value->time)}}</span>
                            </div>
                            
                          </div>
                          
                         <?php

                        }

                    } else {
                        echo "No Notification found!!";
                    }

                        ?>
                          
                        </div>

                        <div id="Tokyo" class="tabcontent">
                            <div class="">
                <!--    <div class="formslogins">
                       
                        
                        <div class="regisindivs">
                        <div class="main-hd">
                            <h1>Change Password</h1>
                        </div>
                        <form action="{{action('API\HomeAPIController@changePassword')}}" method="post">
                            <div class="form-group">
                                
                                <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Old Password." name="oldpassword">
                                
                                
                                <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="New Password" name="password">
                                
                                    <input type="hidden"  name="usertype" value="doctor">

                                    
                                <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Password" name="password_confirmation">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <div class="actionloginbs">
                                <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                                                            
                            </div>
                        </form>
                        </div>
                        
                    </div> -->
                </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="docconfirmbew">
    <div class="modal fade" id="myModal2" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <!-- Modal Header -->
            
            
            <!-- Modal body -->
                <div class="modal-body">
             <div class="formslogins">
                       <div class="main-hd">
                            <h1>Change Password</h1>
                        </div>
                         <button type="button" class="close" data-dismiss="modal">×</button>
                        
                        <div class="regisindivs">
                        <img src="https://mydoctoruae.com/public/assets/images/logo.png" alt="" class="img-responsive img-fluid">
                         <div id="resultpass"></div>
                            <div class="form-group">
                                
                                <input type="text" class="form-control" id="oldpassword" placeholder="Old Password." name="oldpassword">
                                
                                
                                <input type="text" class="form-control" id="password" placeholder="New Password" name="password">
                                
                                
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    
                                <input type="text" class="form-control" id="password_confirmation" placeholder="Confirm Password" name="password_confirmation">
                                
                                <div class="actionloginbs">
                                <button id="getpasschange" type="button" class="btn btn-primary">Save</button>
                                </div>
                                                            
                            </div>
                        
                        </div>
                        
                    </div>
            </div>
            
            <!-- Modal footer -->
            
            
          </div>
        </div>
    </div>
    </div>
     <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal3" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Your Post Deleted Successfully</p>
           <button class="close allokbtns" id="datadis12">Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

    <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Password Changed successfully!!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

<div class="docconfirmbew">
    <div class="modal fade" id="myModal236" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <!-- Modal Header -->
            
            
            <!-- Modal body -->
            <div class="modal-body">
             <div class="formslogins">
                       <div class="main-hd">
                            <h1>Add NewsFeeds Here</h1>
                        </div>
                         <button type="button" class="close" data-dismiss="modal">×</button>
                        
                        <div class="regisindivs">
                        <img src="https://mydoctoruae.com/public/assets/images/logo.png" alt="" class="img-responsive img-fluid">
                        <form action="{{url('/')}}/postnews" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                             
                                            <input type="text" class="form-control" id="title_postnews" name="title" placeholder="Post Title.">
                                            <span class="formerror ertitle"></span>
                                            
                                            <div class="poslable">
                                            <label class="optionalst">Post Image (optional) </label>
                                            <input type="file" class="form-control" id="exampleFormControlInput1" name="image" accept="Image/*">
                                            </div>
                                             
                                            <textarea name="description" placeholder="Description" id="description_postnews"></textarea>
                                           <span class="formerror erdesc"></span>

                                            <input type="text" class="form-control" id="exampleFormControlInput1" name="url" placeholder="News Feed url (optional)">
                                            
                                            <div class="actionloginbs">
                                            <button type="button" id="postnews1" class="btn btn-primary">Save</button>
                                            </div>
                                                                        
                                        </div>
                                           <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    </form>
                        </div>
                        
                    </div>
            </div>
            
            <!-- Modal footer -->
            
            
          </div>
        </div>
    </div>
    </div>

<script type="text/javascript">
    $(".limitedNumbChosen").select2();
</script>

<script type="text/javascript">

   $("#getpasschange").click(function(){
        var oldpassword = $("#oldpassword").val();

    var password = $("#password").val();

    var password_confirmation = $("#password_confirmation").val();

    var usertype = "doctor";

    var _token = $("#csrf-token").val();

    $.post("{{url('/')}}/changePassword",{oldpassword:oldpassword,password:password,password_confirmation:password_confirmation,usertype:usertype,_token:_token},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#myModal2').modal('hide'); 
                    $('#myModal4').modal('show'); 
            } else {
                $("#resultpass").html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    });
   });
    
    

</script>

<script type="text/javascript">
    $(".delepostuda").click(function(){
        var post_id = $(this).attr("data");
        var _token = "{{ csrf_token() }}";
        $.post("{{url('/')}}/deletepost",{post_id:post_id,_token:_token},function(result){
            $('#myModal3').modal('show'); 
        }); 
    });

    $(".datadis12").click(function(){
        location.reload();
    });
</script>

<script type="text/javascript">
                $('#postnews1').click(function(){
                        if($('#description_postnews').val() == ''){
                            $('.erdesc').text('Description field required');
                        } else if($('#description_postnews').val().length > 500){
                            $('.erdesc').text('Description field must be less than 500 words');
                        }

                        if($('#title_postnews').val() == ''){
                            $('.ertitle').text('Title field required');
                        } else if($('#title_postnews').val().length > 25){
                            $('.ertitle').text('Title field must be less than 25 words');
                        }

                        if($('#description_postnews').val() != '' && $('#description_postnews').val() != '' && $('#title_postnews').val().length <= 25 && $('#description_postnews').val().length <= 500){
                                $('#postnews1').attr('type','submit');
                                $('#postnews1').click();
                        }
                });
            </script>
            <script type="text/javascript">
                $("#myModel236").click(function (e) {
$('#myModal236').modal('toggle');
});
            </script>

            <script>


 $('#insurance_accept1').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('autocomplete.fetch') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
             $('#countryList').fadeIn();  
            $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'li', function(){  
        $('#insurance_accept1').val($(this).text());  
         $('#countryList').fadeOut();  
    });  

</script>
    @endsection