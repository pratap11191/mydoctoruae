@extends('layout.site') 
@section('title', 'Terms & Condition')
@section('content')

<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
   
</head>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;">
        <div class="container">
       <h1>Terms & Condition </h1>
        <p>Welcome to the portal <a href="/">www.Mydoctoruae.com</a>, (the "Website"). The Website is operated by Mydoctoruae LLC, UAE., having its registered office at UAE and authorized/licensed Mydoctoruae UAE Private Limited having registered office at Noida, Delhi to update the website.</p>

<p>The terms 'You' or 'Your' refer to You as the user (irrespective of whether You are a Registered User or a Non-Registered User or an Healthcare Practitioner (HCP)) and the terms 'Mydoctoruae' ,'We', 'Us', 'Company', and 'Our' refer to <strong>Mydoctoruae LLC, UAE</strong> and / or <strong>Mydoctoruae UAE Private Limited.</strong></p>

<p>PLEASE CAREFULLY READ THE FOLLOWING TERMS OF USE OF THE WEBSITE. THESE TERMS OF USE, TOGETHER WITH THE PRIVACY POLICY AVAILABLE AT <a href="/">www.Mydoctoruae.com/privacy</a>, ADDITIONAL TERMS OF USE FOR HCPS <a href="/">https://doctor.Mydoctoruae.com/terms</a> AND OTHER POLICIES WHICH MAY BE APPLICABLE TO SPECIFIC PORTIONS OF THIS WEBSITE CONSTITUTE THE TERMS OF YOUR ACCESS TO AND UAEGE OF THE WEBSITE. BY CLICKING ON THE 'I ACCEPT' BUTTON AT THE END OF THIS PAGE OR ACCESSING OR USING THIS WEBSITE, YOU AGREE TO BE BOUND BY THESE TERMS OF USE, THE PRIVACY POLICY, THE ADDITIONAL TERMS OF USE FOR HCPS AND OTHER POLICIES WHICH MAY BE APPLICABLE TO THIS WEBSITE. IF YOU DO NOT AGREE TO BE BOUND BY THESE TERMS OF USE, THE PRIVACY POLICY, THE ADDITIONAL TERMS OF USE FOR HCPS AND OTHER POLICIES WHICH MAY BE APPLICABLE TO SPECIFIC PORTIONS OF THIS WEBSITE PLEASE DO NOT ACCESS OR USE THE WEBSITE.</p>

<p>These Terms of Use, the Privacy Policy, the Additional Terms of Use for HCPs, together with any other policies which may be applicable to specific portions of Website and any disclaimers which may be present on the Website are referred to as "<strong>Agreement</strong>".</p>

<p>If You are accessing the Mydoctoruae mobile application, then this Agreement will continue to apply to such use. In addition, there may be additional terms (such as the terms imposed by mobile application stores) which will govern the use of the mobile application. In such case, the term 'Website' will be deemed to refer to the Mydoctoruae mobile application.</p>

<p>The features / services of this Website are available only in UAE. However Non-Registered Users outside UAE may browse through the information provided in the Website</p>

<p>The Agreement supersedes all previous oral and written terms and conditions (if any) communicated to You.</p>

<ul>
<h4>1. THE WEBSITE</h4>
<li>1.1. Users: The Website is accessible to visitors / non-registered users, registered users and healthcare practitioners. "<strong>Registered Users</strong>" are users who have registered themselves an account on the Website by providing Mydoctoruae with certain information and who can log on to the Website by providing their username and password. "<strong>Non-Registered Users</strong>" are users who are not registered with the Website but may access information on the Website. Certain features of the Website are available to Registered Users only. "<strong>Health Care Practitioners</strong>" or "<strong>HCPs</strong>" are duly qualified members of medical or dental profession and (a) with whom Registered Users can book appointment using the Website or (b) who can provide information to questions raised by Registered Users or (c) with whom Registered Users can consult privately with respect to their queries or d) who can generate and provide health related information and education material for general use of Registered Users."Labs" are registered laboratories that collect patient samples from Registered Users for the purpose of conducting pathological tests and varied diagnostic services referred to as "<strong>Lab Tests</strong>". Registered Users, Non-Registered Users and HCPs shall collectively be referred to as "<strong>Users</strong>".</li>
</ul>

<ul>
<li>1.2. Features of the Website: The Website offers the following features:
<ul>
<li>a) For Registered Users:
<ul>
<li>i. Platform for obtaining generic and preliminary information from HCP to a health related question: You may ask any health related question on the Website and obtain preliminary and generic information to such question from a HCP. Your identity is not disclosed to the HCPs or other Registered Users at this stage.</li>

<li>ii. Platform for viewing generic and preliminary information of HCP on questions submitted by other Registered Users: You may access the information provided by an HCP to questions submitted by other Registered Users on the Website.</li>

<li>iii. Platform to schedule in-person appointment with HCP: You may use Website to schedule an in-person appointment with HCP at the HCP's premises.</li>

<li>iv. Platform to interact with HCP privately: You may interact with an HCP privately on the chat feature available on the Website or via other communication feature which the Website may provide after paying consultation fee to the HCP and an internet handling fee to Mydoctoruae as applicable from time to time but not exceeding 50% of HCP consultation fees.</li>

<li>v. Platform to pay HCP for non-Website interaction: You may make use of the Website solely for the purpose of paying an HCP for a non-Website/offline interaction with such HCP after paying Mydoctoruae's internet handling fee.</li>

<li>vi. Platform to order Lab Tests from Labs: You may use the Website to order Lab Tests from Labs. The Website allows you to schedule a sample pick-up at Your home, which will be used by a Lab to conduct a Lab Test and share the results of such Lab Test with You through the Website. Visit <a href="/">www.Mydoctoruae.com/</a>lab-tests to learn more about the Lab Tests that are offered on the Website.</li>

</ul>
</li>

<li>b) For Non-Registered Users:
<ul>
<li>i. Access to HCP Profiles: Depending on the extent of information shared by HCPs at the time of registering with the Website, the HCPs' profile containing their name, qualification, experience, specialization, consultant fees, personal statement, education, professional memberships etc. can be accessed by You. The Website offers various search features to aid You in accessing HCP profiles.</li>

<li>i. Access to HCP Availability for in-person appointment: Based on information communicated by HCP, the Website shows indicative appointment slots available for in-person consultation at HCP's premises.</li>
</ul>
</li>

<li>c) For HCPs:
<ul>
<li>i. Platform to publish profile: You may upload Your profile containing Your name, qualification, experience, specialization, consultation fees, personal statement, education, professional memberships etc. on Website. Your profile is accessible to all Registered as well as Non-Registered Users.</li>

<li>ii. Platform for private interaction: You may use Website for private interaction with Registered Users. In course of the private interaction, You can exchange texts, images, videos over the chat feature of the platform and communicate through other means provided by the Mydoctoruae such as calls.</li>

<li>iii. Platform to receive consultation fees for non-Website interaction: You may use the Website to receive fees for Your interactions with Registered User outside the Website / offline.</li>


<li>iv. Platform to provide information to questions to health related questions submitted by Registered Users: You may provide Your preliminary and generic information to health related questions submitted by Registered Users.</li>

<li>v. Platform to publish HCP-contributed health related information and education material: You may contribute health related information and education material which Mydoctoruae may, with or without modification, in its sole discretion, publish on the Website.</li>
</ul>
</li>

</ul>
</li>


<li>1.2. Features of the Website: The Website offers the following features:

<ul>
<li>1.3. Types of content on the Website:
<ul>
<li>1.3.1. Content provided by Registered Users including questions provided by Registered Users and interactions in the course of private interactions with HCPs ("User Content")</li>
<li>1.3.2. Content provided by HCP including health related information and educational material; and information provided by way of responses to questions raised by Registered Users or in course of private interactions with Registered Users ("HCP Content");</li>
<li>1.3.3 Content provided by Labs including instructions, results, reports and any other information relating to Lab Tests ("Lab Content").</li>
<li>1.3.4. Content which Mydoctoruae (through itself or its contractors) has generated or procured for the Website ("Mydoctoruae Content");</li>
</ul>
</li>
</ul>

</li>
</ul>

<ul>
<h4>2. USE OF THE SITE</h4>
</ul>

		    
	</div>
</div>
@endsection