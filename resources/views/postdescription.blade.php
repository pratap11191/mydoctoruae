@extends('layout.site') 
@section('title', $retrun_array->title)
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <div ng-app="myApp" ng-controller="namesCtrl" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-feedss">
        <div class="container">
            
			
			
			<div class="postdecwrappers">
			
			<div class="imageuserdocs userinfo">
				<div class="profileinf">
				<img src="<?php
					if(empty($retrun_array->image)){
						echo 'https://mydoctoruae.com/public/img/user_signup.png';
					} else {
						echo url('/public/').'/'.$retrun_array->image;
					}

				?>">
				</div>
				<h1 class="userdesigname"><?php echo ucfirst($retrun_array->fullname); ?></h1>
				<p class="postypedt"><?php echo ucfirst($retrun_array->speciality_name); ?></p>
			</div>
			
			<div class="subscparades">
			<div class="posttitle">
                <h4><?php echo ucfirst($retrun_array->title); ?></h4>
            </div>

			
			<?php
			if(!empty($retrun_array->post_image)){ ?>
			    <div class="poststhumbs">
				<img src="<?php echo url('/public/').'/'.$retrun_array->post_image;
					?>">
				</div>
			<?php } ?>
				<div class="imagepostdesc">
				<h6><strong>Description:</strong></h6>
					<p><?php echo ucfirst($retrun_array->description); ?></p>
				</div>
				<?php
				
				if(!empty($retrun_array->url)){
				?>
				<div class="referenceurl">
				
				<h6>Reference Url</h6>
				<?php
				
				$parsed = parse_url($retrun_array->url);
				if (empty($parsed['scheme'])) {
					$retrun_array->url = 'http://' . ltrim($retrun_array->url, '/');
				}
				
				?>
				<ul>
					<li><a href="<?php echo $retrun_array->url; ?>" target="_blank"><i class="fa fa-hand-o-right"></i><?php echo $retrun_array->url ?></a></li>
				</ul>
				</div>
				
				<?php
				
				}
				
				?>
				
				
				<div class="sharemediapost">
				<h4>Share Post: </h4>
					<div class="your-post-anchore">
							<ul class="yourpost-ul">
								
								
								<li><a href="#"><span class="yourposticon"><i class="fa fa-share-alt" aria-hidden="true"></i></span></a>
									<ul class="your-post-share-ul">
										<li><a href="https://api.whatsapp.com/send?&text={{url('/')}}/postpage/{{Crypt::encryptString($retrun_array->post_id)}}" target="_blank">Whatsapp</a></li>
                                    	<li><a href="https://www.facebook.com/sharer/sharer.php?u={{url('/')}}/postpage/{{Crypt::encryptString($retrun_array->post_id)}}" target="_blank">Facebook</a></li>
                                    	<li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{url('/')}}/postpage/{{Crypt::encryptString($retrun_array->post_id)}}&title={{ucfirst($retrun_array->title)}}&summary={{ucfirst($retrun_array->description)}}&source=Healthapp">LinkedIn</a></li>
										<!-- <li><a href="#">Intagram</a></li> -->
									</ul>
								</li>
							</ul>
					 </div>
				</div>
				 <?php if(Request::session()->get('userdetails', 'default') != 'default'){ ?>
				<div class="commentpostdocs">
					<div class="formslogins">
						<div class="form-group">
							<h1>Please Leave Your Comment</h1>
							<div class="">
								<textarea class="form-control" id="cmt_on_single_post_msg" placeholder="Leave Your Comment"></textarea>
								<p style="color: red;">@{{ bberror }}</p>
								<div class="actionloginbs">
									<button ng-click="postRequest()" class="btn btn-primary" id="cmt_on_single_post">Post</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="commentpostdocs" id="commentpostdocs_detail">
					<label  class="cmthda" for="exampleFormControlInput1">@{{allcomm}} Comments</label>
					
					<div class="notiftabs" ng-repeat="x in names">
							<div class=" userpic">
								<img src="@{{x.image}}">
							</div>
							
							<div class="usernotify">
							<label  class="cmthda usernamedoci">(@{{x.fullname}})</label>
							<p>@{{x.message}}</p> 
							<span class="notifytimes commentdays">@{{x.created_at}}</span>
								
							</div>	
					</div>

					
				</div>
				
				
			
			</div>
			
			<div class="imageuserdocs userinfo postrecen">
			<h1>Recent Posts</h1>

			<?php

				

				foreach ($randomlist as $key => $value) {
					
					//print_r($value);
			?>
			
			<div class="post_thumbsdoc">
			
			<div class="col-md-3 col-xs-3 thumnna">
			   <?php
			   		if(!empty($value['image'])){
			   ?>
				<img src="{{url('/')}}/public/{{$value['image']}}">
				<?php
			} else { ?>
			<img src="{{url('/')}}/public/image-not-found.png">
			<?php } ?>
			</div>
			
			<div class="col-md-9 col-xs-9 thuminfo_doc">
				<h4>{{ucfirst($value['title'])}}</h4>
				<p>{{ucfirst($value['description'])}}</p>
				<a href="{{url('/')}}/postpage/{{Crypt::encryptString($value['id'])}}">Read More..</a>
			</div>
			
			</div>
			
			<?php


		}

		?>
			
			
			
			
		
			
		
			
			</div>
			
			</div>
            
        </div>
    </div>



    <script>

 // var sampleApp = angular.module('sampleApp', [], function($interpolateProvider) {
 //        $interpolateProvider.startSymbol('<%');
 //        $interpolateProvider.endSymbol('%>');
 //    });


angular.module('myApp', []).controller('namesCtrl', function($scope,$http) {
  
     $scope.bberror = "";

     $http.get("https://mydoctoruae.com/api/Commentlist/{{$retrun_array->post_id}}")
            .then(function successCallback(response){
                $scope.names = response.data.data;
                $scope.allcomm = (response.data.data).length;
                 console.log((response.data.data).length);
            }, function errorCallback(response){
                console.log("Unable to perform get request");
            });

    $scope.postRequest = function () {

        $http.post("https://mydoctoruae.com/commentOn",{post_id:"{{$retrun_array->post_id}}",message:$('#cmt_on_single_post_msg').val()})
            .then(function successCallback(response){
            	if(response.data.status == 'FAILURE'){
            		$scope.bberror = response.data.message;
            	} else {
            		$scope.names = response.data.data;
            		$scope.allcomm = (response.data.data).length;
            		$scope.bberror = "";

            	}
            	$("#cmt_on_single_post_msg").val(" ");
                console.log(response);
            }, function errorCallback(response){
                console.log("POST-ing of data failed");
            });

    };


});


</script>
    @endsection