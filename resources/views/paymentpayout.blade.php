@extends('layout.site') 
@section('title', 'Doctor Profile')
@section('content')
@if (Session::has('message_passchange'))
    <div class="alert alert-info">{{ Session::get('message_passchange') }}</div>
@endif


<form action='https://www.2checkout.com/checkout/purchase' method='post'>
<input type='hidden' name='sid' value='203924324' />
<input type='hidden' name='mode' value='2CO' />
<input type='hidden' name='li_0_type' value='product' />
<input type='hidden' name='li_0_name' value='invoice123' />
<input type='hidden' name='li_0_price' value='25.99' />
<input type='hidden' name='card_holder_name' value='Checkout Shopper' />
<input type='hidden' name='street_address' value='123 Test Address' />
<input type='hidden' name='street_address2' value='Suite 200' />
<input type='hidden' name='city' value='Columbus' />
<input type='hidden' name='state' value='OH' />
<input type='hidden' name='zip' value='43228' />
<input type='hidden' name='country' value='USA' />
<input type='hidden' name='email' value='example@2co.com' />
<input type='hidden' name='phone' value='614-921-2450' />
<input name='submit' type='submit' value='Checkout' />
</form>
<script src="https://www.2checkout.com/static/checkout/javascript/direct.min.js"></script>

<link rel="stylesheet" href="https://www.paytabs.com/theme/express_checkout/css/express.css">
<script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
<script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Button Code for PayTabs Express Checkout -->
<div class="PT_express_checkout"></div>
<script type="text/javascript">
 Paytabs("#express_checkout").expresscheckout({
 settings:{
 merchant_id: "10035457",
 secret_key: "YCuZvQ4ujqNDftMgYwLDRimsR2mHFpYD1tDwjH1C7Pgnj2DhzoUqGhTykffZEeDGHgd0XeD54UsnX6EwvzWcRQKb50aKeNrW3EW1",
 amount : "10.00",
 currency : "USD",
 title : "Mr. John Doe",
 product_names: "Product1,Product2,Product3",
 order_id: 25,
 url_redirect: "https://mydoctoruae.com/testai",
 display_customer_info:1,
 display_billing_fields:1,
 display_shipping_fields:0,
 language: "en",
 redirect_on_reject: 0,
 }
 });
</script>
 @endsection