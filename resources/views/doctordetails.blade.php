@extends('layout.site') 
@section('title', 'Doctor Details')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<div ng-app="myApp" ng-controller="namesCtrl" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 docter_details_section">
        <div class="container">
            <div class="">
                <div class="docter-details-side">
                    <div class="docter-genral-inforamation-tabs">
                        <h5>General info</h5>
                    </div>

                    <div class="side-sectiondocters">
                        <div class="box_general_3">
                            <div class="profile">
                                <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-3 rightdoc">
                                        <figure>
                                            <img src="{{$details_doctor->image}}" alt="" class="img-responsive">
                                        </figure>
                    <div class="pricedocrate">
                      <p>AED {{$details_doctor->fee}}</p>
                    </div>
                                    </div>
                                    <div class="col-lg-10 col-md-8">
                                        <div class="profile-details">
                                            <small>{{ucfirst($details_doctor->speciality_name)}}</small>
                                            <h3>DR. {{ucfirst($details_doctor->fullname)}}</h3>
                                            <span class="rating ratingdocterdetails"><i class="fa fa-star" aria-hidden="true"></i><small>(@{{docrating}}%)</small></span>
                                            <a href="javascript:void();" class="badge_list_1">
                                                <img src="assets/images/docter/badge_1.svg" width="15" height="15" alt="">
                                            </a>
                                            <ul class="contacts-docter">
                                                <li>
                                                    <h6>Address</h6>
                                                    {{ucfirst($details_doctor->city_name)}}
                                                </li>
                                                <li>
                                                    <h6>Clinic Name</h6>
                                                    {{ucfirst($details_doctor->clinic)}}
                                                </li>
                                                <li class="abailinbilty_dd">
                                                    <h6>Availability</h6>
                                                    <?php

                                                      $Availabilty_days = explode(",", $details_doctor->avilability);

                                                      if(!empty($details_doctor->avilability)){

                                                        foreach ($Availabilty_days as $key => $value) {
                                                          

                                                    ?>
                                                    <a href="javascript:void();">{{$value}}</a>
                                                  <?php } } ?>
                                                </li>
                                                <li class="abailinbilty_dd">
                                                    <h6>Time</h6>
                                                    <?php

                                                      echo $details_doctor->start_time." to ".$details_doctor->end_time;

                                                    ?>
                                                  
                                                </li>
                                            </ul>
                                        </div>
                    
                    
                                    </div>
                                </div>
                            </div>
                            <hr class="hrline-profile">
              <div class="col-md-4 col-sm-4">
                            <div class="identifiy-clinic">
                                <div class="indent_title_in">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <h3>Contact</h3>
                                   <!--  <p>Mussum ipsum cacilds, vidis litro abertis.</p> -->
                                </div>
                                <div class="wrapper_indent">
                                    <div class="chat-call">
                                        <ul>
                                            <li>
                                                <a href="tel:@{{phonedoc}}"><span class="icon-contactdetails"  >&nbsp &nbsp @{{phonedoc}}</span></a>
                                            </li>
                                          <!--   <li>
                                               <a href="docter-chat.html"> <span class="icon-contactdetails"><i class="fa fa-comments" aria-hidden="true"></i></span></a>
                                            </li> -->
                                        </ul>
                    
                    <div class="modal fade" id="myModal">
                      <div class="modal-dialog">
                        <div class="modal-content">
                        
                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Contact Details</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                          <p>Phone No:- <strong>@{{phonedoc}}</strong></p>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        
                        </div>
                      </div>
                    </div>
  
                  </div>
                                  </div>
                                </div>
              </div>
              
              <div class="col-md-4 col-sm-4">
                            <div class="identifiy-clinic">
                                <div class="indent_title_in">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                    <h3>Experties Area</h3>
                                   <!--  <p>Mussum ipsum cacilds, vidis litro abertis.</p> -->
                                </div>
                                <div class="wrapper_indent">
                                    <p>{{ucfirst($details_doctor->expertise_area)}}</p>
                                    <!-- <h6>Specializations</h6> -->
                                   <!--  <div class="row">
                                        <div class="col-lg-6">
                                            <ul class="bullets">
                                                <li>Abdominal Radiology</li>
                                                <li>Addiction Psychiatry</li>
                                                <li>Adolescent Medicine</li>
                                                <li>Cardiothoracic Radiology </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-6">
                                            <ul class="bullets">
                                                <li>Abdominal Radiology</li>
                                                <li>Addiction Psychiatry</li>
                                                <li>Adolescent Medicine</li>
                                                <li>Cardiothoracic Radiology </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
              </div>
              
                            
              
              <div class="col-md-4 col-sm-4">
                            <div class="identifiy-clinic">
                                <div class="indent_title_in">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                    <h3>Qualification</h3>
                                    <!-- <p>Mussum ipsum cacilds, vidis litro abertis.</p> -->
                                </div>
                                <div class="wrapper_indent">
                                    <p>{{ucfirst($details_doctor->qualification)}}</p>
                                </div>
                            </div>
              </div>
              
              
              <div class="insurspacesdocts">
                <div class="col-md-12 col-sm-12 equalpart bdrsep insurancapts">
                  <label class="userfieldsnk" for="exampleFormControlInput1">Insurance Accepted:-</label>
                  <span class="useridprint">
                    <?php

                                           if(!empty($details_doctor->insurance_accept)){
                                            
                                             foreach ($details_doctor->insurance_accept as  $valueing) {
                                                echo "<p>".$valueing."</p>";
                                             }
                                            ?>

                                   <?php        } else {
                                            echo "Not Any Insurance Accepted";
                                   }

                                        ?>
                                   
                  </span>
                </div>
              </div>
                    
              <p style="color: red;">@{{ bberror }}</p>
               <p style="color: green;">@{{ bberror1 }}</p>
               <?php if(Request::session()->get('userdetails', 'default') != 'default'){ ?>
              <div class="doctordetails">
                  
                  <div class="commentpostdocs col-md-4 col-xs-12">
                    <p><strong>Please give your rating</strong></p>
                    <div class="flitby rating-stars text-center">
                      <ul id="stars">
                        <li class="star" title="Poor" data-value="1">
                        <i class="fa fa-star fa-fw"></i>
                        </li>
                        <li class="star" title="Fair" data-value="2">
                        <i class="fa fa-star fa-fw"></i>
                        </li>
                        <li class="star" title="Good" data-value="3">
                        <i class="fa fa-star fa-fw"></i>
                        </li>
                        <li class="star" title="Excellent" data-value="4">
                        <i class="fa fa-star fa-fw"></i>
                        </li>
                        <li class="star" title="WOW!!!" data-value="5">
                        <i class="fa fa-star fa-fw"></i>
                        </li>
                      </ul>
                    </div>
                  </div>
                    <input type="hidden" name="rating_newway" id="rating_newway" value="0">

                  <script type="text/javascript">
                    $(".star").click(function(){
                      $("#rating_newway").val($(this).attr("data-value"));
                    })
                  </script> 

                  
                  <div class="commentpostdocs col-md-8 col-xs-12">
                    <div class="formslogins">
                      <div class="form-group">
                        <label class="cmthda" for="exampleFormControlInput1">Leave Your Comment</label>
                        <div class="">
                          <textarea class="form-control" id="cmt_on_single_post_msg" placeholder=" Your Comment"></textarea>
                          <span style="color: red;" id="cmt_on_single_post_msg_error"></span>
                          <div class="actionloginbs">
                            <button ng-click="postRequest()" class="btn btn-primary" id="cmt_on_single_post">Post</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                
                </div>
                <?php } ?>
               <hr class="hrline-profile">
                            </div>
                            
                        </div>
                    </div>
                    <div class="review-section">
                        <div class="box_general_3">
              <div class="reviews-container">
              
                
              

                
                
                <div class="review-box clearfix" ng-repeat="x in names">
                  
                  <figure class="rev-thumb"><img src="@{{x.image}}" alt="">
                  </figure>
                  <div class="rev-content">
                    <span class="rating "><i class="fa fa-star" aria-hidden="true"></i><small>(@{{x.rating}}%)</small></span>
                    <div class="rev-info">
                      @{{x.fullname}}: @{{x.created_at}}
                    </div>
                    <div class="rev-text">
                      <p>
                        @{{x.review}}
                      </p>
                    </div>
                  </div>
                </div>

                
                
                <!-- End review-box -->
              </div>
              <!-- End review-container -->
            </div>
                    </div>
                </div>
            </div>
          <!--   <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="docter-details-aside">
                    <div class="as-deesdocter-details-aside">
                        <div class="docter-booking-tabs">
                            <h5>Book a Visit</h5>
                            <p>Monday to Friday 09.00am-06.00pm</p>
                        </div>
                        <div class="row booking-feilds">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input class="form-control picker-input input-bboking" type="date" id="booking_date" >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input class="form-control td-input input-bboking" type="time">
                                </div>
                            </div>
                        </div>
                        
                         <hr class="hrline-profile">
                        <div class="price-sectionbooking">
                            <p>Consultaion Fees</p>
                            <h6>UED 99</h6>
                        </div>
                         <hr class="hrline-profile">
                        <a href="#" class="btn_1 full-width">Book Now</a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

    <script>

 // var sampleApp = angular.module('sampleApp', [], function($interpolateProvider) {
 //        $interpolateProvider.startSymbol('<%');
 //        $interpolateProvider.endSymbol('%>');
 //    });


angular.module('myApp', []).controller('namesCtrl', function($scope,$http) {
  
     $scope.bberror = "";

     $http.get("https://mydoctoruae.com/api/fulldoctordetails/{{$details_doctor->user_id}}")
            .then(function successCallback(response){
                $scope.names = response.data.data.review_list;
                 $scope.docrating = response.data.data.rating;
                 $scope.phonedoc = response.data.data.phone;
                  console.log(response.data);
            }, function errorCallback(response){
                console.log("Unable to perform get request");
            });

    $scope.postRequest = function () {

        $http.post("https://mydoctoruae.com/submitReview",{doctor_id:"{{$details_doctor->user_id}}",rating:$("#rating_newway").val(),review:$("#cmt_on_single_post_msg").val()})
            .then(function successCallback(response){
              if(response.data.status == 'FAILURE'){
                $scope.bberror = response.data.message;
              } else {
                $scope.bberror1 = "Your review is submitted and shall be displayed after approval";
                $scope.names = response.data.data.review_details;
                 $scope.docrating = response.data.data.rating;

              }
              $("#cmt_on_single_post_msg").val(" ");
                console.log(response);
            }, function errorCallback(response){
                console.log("POST-ing of data failed");
            });
    };

    
});


</script>

@endsection