@extends('layout.site1') 
@section('title', 'contact us')
@section('content')

<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  
    
    <!--main style sheet-->
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	
  
    
</head>


<div class="col-md-12 aboutus">
	<div class="container">
	    <div class="desc">
		    <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="footer-col">
                    <h5>Contact with Us</h5>
                    <ul class="contacts">
                        <li><a href="tel:61280932400"><i class="fa fa-mobile" aria-hidden="true"></i> + 971 58 834 0964</a></li>
                        <li><a href="mailto:info@findoctor.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@mydoctoruae.com</a></li>
                    </ul>
                    <div class="follow_us">
                        <h6>Follow us</h6>
                        <ul class="foolow-ull">
                            <li><a href="#0"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#0"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		
		
	</div>
</div>
 
   
@endsection
