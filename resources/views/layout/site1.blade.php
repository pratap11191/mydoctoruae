<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title') | Doctor</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
 <link rel="stylesheet" href="https://mydoctoruae.com/public/assets/css/jquery.timepicker.min.css" type="text/css">
	
    @include('include.head')
</head>
<body>


    
    
   
	

    @yield('content')


  
    

    <script src="{{asset('public/assets/js/jquery.min.js')}}"></script>
	
    <script src="{{asset('public/assets/bootstrap/bootstrap.min.js')}}"></script>
    
    <!--main custome js-->
    <script src="{{asset('public/assets/js/custome.js')}}"></script>
	
	<script src="{{asset('public/assets/js/jquery.timepicker.js')}}"></script>
    
</body>
</html>
