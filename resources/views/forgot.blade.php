@extends('layout.site') 
@section('title', 'Forgot password')
@section('content')

<div class="forgetpswda">
        <div class="container">
			<div class="loginsignbox">
				
				<div class="loginwarpper">
					<div class="formslogins">
					    <div class="logoindivs">
							<div class="main-hd">
								<h1>Forgot Password</h1>
							</div>
							@if(Session::has('message_forgot'))
							<p class="alert alert-info">{{ Session::get('message_forgot') }}</p>
							@endif
							<form action="{{action('API\HomeAPIController@forgotpassweb')}}" method="post">
								<div class="form-group">
									<label for="exampleFormControlInput1">Email ID/ Phone Number</label>
									<input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Email ID/ Phone Number">
									<span class="formerror"><?php
                                	if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                		echo $messages['email']['0'];
                                	}
                                ?></span>
									
								
									<div class="actionloginbs">
									<button type="submit" class="btn btn-primary">Submit</button>
									</div>
									<!-- <div class="center section"><small class="fs13">New to My Doctor UAE?<a  href="{{action('API\HomeAPIController@usersignup')}}"> Sign Up</a></small></div> -->						
								</div>
								<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection