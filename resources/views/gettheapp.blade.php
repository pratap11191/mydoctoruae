@extends('layout.site') 
@section('title', 'Get App')
@section('content')


<div class="bgaappl">
<div class="container">
<div class="col-md-12">
<div class="logoappd"><a href="http://mydoctoruae.com"><img src="http://mydoctoruae.com/public/assets/images/white-logo.png" class="img-responsive" alt=""></a></div>
<div class="menuappd">
<ul>
<li><a href="http://mydoctoruae.com">Home</a></li>
<li><a href="http://mydoctoruae.com/aboutus">About Us</a></li>
<li><a href="#">Terms of Use </a></li>
</ul>
</div>
</div>

<div class="col-md-6 col-xs-12">
<div class="backhda">
<h1>Mydoctor For Everyone</h1>
<p>we have created a platform which lets you connect with any medical professional instantly</p>
</div>

<div class="app_buttons">
<a href="#0" class="fadeIn"><img src="http://mydoctoruae.com/public/assets/images/apple_app.png" alt="" width="150" height="50" data-retina="complete"></a>
<a href="#0" class="fadeIn"><img src="http://mydoctoruae.com/public/assets/images/google_play_app.png" alt="" width="150" height="50" data-retina="complete"></a>
</div>
</div>
<div class="col-md-6 col-xs-12 mobcover">
<img src="/public/assets/images/myprof.png" class="img-responsive" alt="">
</div>
</div>


</div>

<div class="col-md-6 col-xs-12 text-dsc">
<img src="/public/assets/images/slider-banner/2m.png" class="img-responsive" alt="">
</div>

<div class="col-md-6 col-xs-12 text-btm">
<p>Mydoctor is the best way to seek medical help. To make it seamless and smooth, we have created a platform which lets you connect with any medical professional instantly. Find out immediately about your insurance acceptance, availability of doctor and reviews about the place you want to visit..</p>
</div>

<div class="col-md-12 col-xs-12 screensapp">
<div class="container">
<div class="insidetext">
 <h2>Mydoctor For Everyone</h2>
<p><strong>Stay healthy and fit:</strong> The Health Feed from healthcare experts aims to keep you healthy and fit. If that is not all, you can find a doctor nearby, check doctor's profile and book an appointment too.</p>
</div>
<div class="screens">
<ul>
<li><img src="/public/assets/images/sc1.png"></li>
<li><img src="/public/assets/images/sc2.png"></li>
<li><img src="/public/assets/images/sc3.png"></li>
<li><img src="/public/assets/images/sc4.png"></li>
</ul>

</div>

</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 download-app">
        <div class="container">
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="mobile-section-scree">
                    <img src="http://mydoctoruae.com/public/assets/images/app_img.svg" class="img-responsive" alt="">
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <div class="mobile-section-details">
						<small>Application</small>
						<h3>Download <strong>My Doctor App</strong> Now!</h3>
						<p class="lead">We have created a platform which lets you connect with any medical professional instantly. Find out immediately about your insurance acceptance, availability of doctor and reviews about the place you want to visit. </p>
						<div class="app_buttons">
							<a href="#0" class="fadeIn"><img src="http://mydoctoruae.com/public/assets/images/apple_app.png" alt="" width="150" height="50" data-retina="complete"></a>
							<a href="#0" class="fadeIn"><img src="http://mydoctoruae.com/public/assets/images/google_play_app.png" alt="" width="150" height="50" data-retina="complete"></a>
					   </div>
                </div>
            </div>
        </div>
    </div>

<style>


.col-xs-12.col-sm-12.col-md-12.col-lg-12.main-header.doctorsflow.statichdr {
    display: none;
}

</style>

@endsection