<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<body>

<div ng-app="myApp" ng-controller="namesCtrl">

<p>Type a letter in the input field:</p>

<p><input type="text" ng-model="test"></p>

<ul>
  <li ng-repeat="x in names | filter:test">
  @{{ x.name }}
  </li>
</ul>

</div>

<script>

 // var sampleApp = angular.module('sampleApp', [], function($interpolateProvider) {
 //        $interpolateProvider.startSymbol('<%');
 //        $interpolateProvider.endSymbol('%>');
 //    });


angular.module('myApp', []).controller('namesCtrl', function($scope,$http) {
  

     $http.get("https://mobulous.app/healthapp/api/specialty")
            .then(function successCallback(response){
                $scope.names = response.data.data.specialties;
                 
            }, function errorCallback(response){
                console.log("Unable to perform get request");
            });

    
});


</script>

<p>The list will only consists of names matching the filter.</p>


</body>
</html>
