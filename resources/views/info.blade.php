 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Healthapp | Services</title>
<link rel="shortcut icon" href="http://mobulous.in/homefuud/totrr_favicon.ico"/>
<style>


.content {
    width: 60%;
    margin: 50px auto;
    padding: 20px;
  }
  .content h1 {
    font-weight: 400;
    text-transform: uppercase;
    margin: 0;
  }
  .content h2 {
    font-weight: 400;
    text-transform: uppercase;
    color: #333;
    margin: 0 0 20px;
  }
  .content p {
    font-size: 1em;
    font-weight: 300;
    line-height: 1.5em;
    margin: 0 0 20px;
  }
  .content p:last-child {
    margin: 0;
  }
  .content a.button {
    display: inline-block;
    padding: 10px 20px;
    background: #ff0;
    color: #000;
    text-decoration: none;
  }
  .content a.button:hover {
    background: #000;
    color: #ff0;
  }
  .content.title {
    position: relative;
    background: none;
    border: 2px dashed #333;
  }
  .content.title h1 span.demo {
    display: inline-block;
    font-size: .5em;
    padding: 5px 10px;
    background: #000;
    color: #fff;
    vertical-align: top;
    margin: 7px 0 0;
  }
  .content.title .back-to-article {
    position: absolute;
    bottom: -20px;
    left: 20px;
  }
  .content.title .back-to-article a {
    padding: 10px 20px;
    background: #f60;
    color: #fff;
    text-decoration: none;
  }
  .content.title .back-to-article a:hover {
    background: #f90;
  }
  .content.title .back-to-article a i {
    margin-left: 5px;
  }
  .content.white {
    background: #fff;
    box-shadow: 0 0 10px #999;
  }
  .content.black {
    background: #000;
  }
  .content.black p {
    color: #999;
  }
  .content.black p a {
    color: #08c;
  }

  .accordion-container {
    width: 100%;
    margin: 0 0 20px;
    clear: both;
  }
  .accordion-toggle {
    margin-bottom: 15px;
    position: relative;
    display: block;
    padding: 15px;
    font-size: 1.2em;
    font-weight: 300;
    background: #BBB;
    color: #fff;
    text-decoration: none;
    font-family: Bree serif !important;
  }
  .accordion-toggle.open {
    background: #1e2e65;
    color: #fff;
  }
  .accordion-toggle:hover {
    background:rgb(237,156,40);
    color: #fff!important;
  }
  .accordion-toggle span.toggle-icon {
    position: absolute;
    top: 17px;
    left: 20px;
    font-size: 1.5em;
  }
  .accordion-content {
    display: none;
    padding: 20px;
    overflow: auto;
    font-family: Bree serif;
  }
  .accordion-content img {
    display: block;
    float: left;
    margin: 0 15px 10px 0;
    max-width: 100%;
    height: auto;
  }

  /* media query for mobile */
  @media (max-width: 767px) {
    .content {
      width: auto;
    }
    .accordion-content {
      padding: 10px 0;
      overflow: inherit;
    }
  }
</style>
<div class="container pages">
    <div class="row text-left" style="padding:10px;">
      <h2 style="text-align:center;"></h2>
        <div class="page-header"><h3>Service Listing</h3></div>
        <div class="page-header">Service Url : https://mydoctoruae.com/api/</div><br/>
     


<!-- *************************login normal*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">1) Login for user and doctor both</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
'email'       => 'required|max:255',
'password'    => 'required|max:255',
'deviceToken'   => 'required|max:255',
'deviceType' => 'required|max:255',
'usertype' => 'required|in:user,doctor'
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>:Brief Description </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>login</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "fullname": "mohit",
        "phone": "9140405191",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "is_admin": "0",
        "email": "pratap11192@gmail.com",
        "city_id": "1",
        "confirmpassword": "",
        "admin_status": "1",
        "created_at": "2018-07-09 15:46:37",
        "updated_at": "2018-07-09 15:47:54",
        "user_id": "1",
        "token": "pNa13uDOv52XwNn8gRHxtLrhg"
    },
    "message": "User login successfully.",
    "requestKey": "api/login"
}   </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************signup*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">2) SignUp</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
fullname:required
phone:required
email:required
password:required
password_confirmation:required
deviceType:required (ios or android)
deviceToken:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios </p>
          <p> <h5>requestKey: </h5>users</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "fullname": "mohit",
        "phone": "9140405191",
        "email": "pratap11192@gmail.com",
        "updated_at": "2018-07-09 15:46:37",
        "created_at": "2018-07-09 15:46:37",
        "token": "FPuu4MhjRrs84ArmbIs5gFlp2",
        "user_id": "1"
    },
    "message": "User created successfully.",
    "requestKey": "api/users"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
        <p> <h5>Notes: </h5></p>
      </div>
      

<!-- ************************* city update ****************************** -->


 <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">3) city selection by user</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
city_name:required
user_id:required
token:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>city_update</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "id": 1,
        "fullname": "mohit",
        "phone": "9140405191",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "is_admin": "0",
        "email": "pratap11192@gmail.com",
        "city_id": "1",
        "confirmpassword": "",
        "admin_status": "1",
        "created_at": "2018-07-09 15:46:37",
        "updated_at": "2018-07-09 15:47:54"
    },
    "message": "City successfully updated as user",
    "requestKey": "api/city_update"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


<!-- ****************************************** specialties list ****************** -->


<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">4)  specialties list</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>specialty</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "specialties": [
            "Internal Medicine",
            "Pediatrician",
            "Cosmetologist",
            "Medical Cosmetologist",
            "Aesthetic Surgeon",
            "Minimal Access Surgery"
        ]
    },
    "message": "Specialties retrieve successfully.",
    "requestKey": "api/specialty"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ****************************** user otp **************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">5)  User Otp </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
email => required
phone => required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>userotp</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "id": 1,
        "phone": "9140405186",
        "otp": "4957",
        "created_at": "2018-07-11 02:54:46",
        "updated_at": "2018-07-11 09:55:28"
    },
    "message": "Otp send successfully.",
    "requestKey": "api/userotp"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ******************************************* doctor sign up *************** -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">6)  Doctor signup </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
email => required
phone => required
speciality_id=> required
clinic => required
licence_number => required
expertise_area=> required
password => required
insurance_accept => 
password_confirmation =>required
deviceType => required
deviceToken => required
fullname=>required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>doctor</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "email": "abc@mail.com",
        "speciality_id": "1",
        "clinic": "gora gav",
        "licence_number": "78945624",
        "expertise_area": "skin health",
        "password": "123456789",
        "insurance_accept": "",
        "updated_at": "2018-07-11 10:32:50",
        "created_at": "2018-07-11 10:32:50",
        "token": "q1rKI0msM69aydnWEY7uEm3xc",
        "user_id": "1",
        "city_id": ""
    },
    "message": "Doctor created successfully.",
    "requestKey": "api/doctor"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ******************************* doctor city selection ************* -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">7) city selection by doctor</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
city_name:required
user_id:required (doctor id)
token:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>city_update_doctor</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "id": 1,
        "email": "abc@mail.com",
        "city_id": "6",
        "speciality_id": "1",
        "clinic": "gora gav",
        "licence_number": "78945624",
        "expertise_area": "skin health",
        "insurance_accept": null,
        "password": "123456789",
        "admin_status": "1",
        "remember_token": "",
        "created_at": "2018-07-11 10:32:50",
        "updated_at": "2018-07-11 10:44:52",
        "phone": ""
    },
    "message": "City successfully updated for doctor",
    "requestKey": "api/city_update_doctor"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>



      <!-- ******************* post news api ************************************* -->


      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">8) post news api for user and doctor</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
title:required
url:required
user_id:required
usertype:required|in:user,doctor
token:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>postnews</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "title": "abcd",
        "url": "http://www.cricbuzz.com/cricket-news/103057/tnpl-2018-supreme-court-rejects-provision-for-outstation-players-in-tnpl",
        "user_id": "1",
        "usertype": "user",
        "description": "The decision comes as a setback for TNPL, which had plans in place to allow each franchise to include up to two players outside the state",
        "updated_at": "2018-07-11 13:03:26",
        "created_at": "2018-07-11 13:03:26",
        "id": 1
    },
    "message": "Otp send successfully.",
    "requestKey": "api/postnews"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


<!-- *********************************** post list api ******************************** -->

  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">9) news feed</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
user_id:required,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>newsfeedlist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "title": "vvvv",
            "url": "",
            "description": "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu,",
            "created_at": "2018-07-17 11:02:18",
            "fullname": "dsp",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "25",
            "speciality_name": "",
            "post_image": "https://mobulous.app/healthapp/public/postimage/2ab4274577e6f2006033ac7a3b3a65759037.jpg",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "bbb",
            "url": "",
            "description": "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu,",
            "created_at": "2018-07-17 11:01:39",
            "fullname": "dsp",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "24",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Ghhh",
            "url": "",
            "description": "sgsjsjsj",
            "created_at": "2018-07-17 03:59:18",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "23",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:48:54",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "22",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:48:53",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "21",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:48:52",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "20",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:48:50",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "19",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:48:46",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "18",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Hahahaha",
            "url": "",
            "description": "gsgzgdd",
            "created_at": "2018-07-17 03:59:22",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "17",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:47:31",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "16",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Hahahaha",
            "url": "",
            "description": "gsgzgdd",
            "created_at": "2018-07-17 03:59:25",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "15",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Hahahaha",
            "url": "",
            "description": "gsgzgdd",
            "created_at": "2018-07-17 03:59:28",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "14",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:46:31",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "13",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:46:28",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "12",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Hahahaha",
            "url": "",
            "description": "gsgzgdd",
            "created_at": "2018-07-17 03:59:32",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "11",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Hahahaha",
            "url": "",
            "description": "gsgzgdd",
            "created_at": "2018-07-17 03:59:35",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "10",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Vgkcjcj",
            "url": "",
            "description": "xhxjcjcjcj",
            "created_at": "2018-07-17 10:44:45",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "9",
            "speciality_name": "",
            "post_image": "https://mobulous.app/healthapp/public/postimage/35dc5e3159ba021935b6a47cc687a32b4023.jpg",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:44:13",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "8",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Vgkcjcj",
            "url": "",
            "description": "xhxjcjcjcj",
            "created_at": "2018-07-17 03:59:04",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "7",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Vgkcjcj",
            "url": "",
            "description": "xhxjcjcjcj",
            "created_at": "2018-07-17 03:59:07",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "6",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Vgkcjcj",
            "url": "",
            "description": "xhxjcjcjcj",
            "created_at": "2018-07-17 03:59:11",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "5",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "Vgkcjcj",
            "url": "",
            "description": "xhxjcjcjcj",
            "created_at": "2018-07-17 03:59:15",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "4",
            "speciality_name": "",
            "post_image": "",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:40:22",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "3",
            "speciality_name": "",
            "post_image": "https://mobulous.app/healthapp/public/postimage/70898b6ec251ceada2ee928507ffce769520.png",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "main title",
            "url": "dsf",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 09:23:37",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "2",
            "speciality_name": "",
            "post_image": "https://mobulous.app/healthapp/public/postimage/8c2eaca012502b1da354818fdbdbb0cb4135.png",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        },
        {
            "title": "abcd",
            "url": "http://www.cricbuzz.com/cricket-news/103057/tnpl-2018-supreme-court-rejects-provision-for-outstation-players-in-tnpl",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 09:23:09",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "1",
            "speciality_name": "",
            "post_image": "https://mobulous.app/healthapp/public/postimage/3e09a070049abaa64e3b8d6c87dea2c35157.png",
            "usertype": "user",
            "likeflag": "0",
            "saveflag": "0"
        }
    ],
    "message": "Post list retrieve successfully Submited",
    "requestKey": "api/newsfeedlist"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

<!-- ************************************ change password for both user and doctor ************************* -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">10) Change password for both user and doctor</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
oldpassword:required,
password:required,
usertype:required|in:user,doctor,
token:required,
user_id:required,
password_confirmation:required,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>changepass</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Password Changed successfully",
    "requestKey": "api/changepass"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ************************************* your post of user or doctor by user_id ****************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">11) Post list of a user for both user and doctor with user_id</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
usertype:required|in:user,doctor,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>postlistbyusertypeid/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "title": "abcd",
            "url": "http://cricbuzz.com",
            "description": "Get Live Cricket Score, Scorecard, Schedules of International &amp; Domestic cricket matches along with Latest News and ICC Cricket Rankings of Players on Cricbuzz.",
            "created_at": "2018-07-16 09:29:28",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "8",
            "speciality_name": ""
        },
        {
            "title": "abcd",
            "url": "cricbuzz.com",
            "description": "Get Live Cricket Score, Scorecard, Schedules of International &amp; Domestic cricket matches along with Latest News and ICC Cricket Rankings of Players on Cricbuzz.",
            "created_at": "2018-07-16 08:06:46",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "7",
            "speciality_name": ""
        },
        {
            "title": "abcd",
            "url": "http://cricbuzz.com",
            "description": "Get Live Cricket Score, Scorecard, Schedules of International &amp; Domestic cricket matches along with Latest News and ICC Cricket Rankings of Players on Cricbuzz.",
            "created_at": "2018-07-16 08:01:21",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "6",
            "speciality_name": ""
        },
        {
            "title": "abcd",
            "url": "http://www.cricbuzz.com",
            "description": "Get Live Cricket Score, Scorecard, Schedules of International &amp; Domestic cricket matches along with Latest News and ICC Cricket Rankings of Players on Cricbuzz.",
            "created_at": "2018-07-16 07:51:22",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "5",
            "speciality_name": ""
        },
        {
            "title": "abcd",
            "url": "http://www.cricbuzz.com",
            "description": "Get Live Cricket Score, Scorecard, Schedules of International &amp; Domestic cricket matches along with Latest News and ICC Cricket Rankings of Players on Cricbuzz.",
            "created_at": "2018-07-16 07:45:54",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "4",
            "speciality_name": ""
        },
        {
            "title": "abcd",
            "url": "https://www.cricbuzz.com/cricket-news/103080/shafiul-ruled-out-of-windies-test-due-to-ankle-injury-bangladesh-cricket-team",
            "description": "The pacer is likely to be out of action for three to six weeks",
            "created_at": "2018-07-12 14:29:29",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "3",
            "speciality_name": ""
        },
        {
            "title": "abcd",
            "url": "http://www.cricbuzz.com/cricket-news/103057/tnpl-2018-supreme-court-rejects-provision-for-outstation-players-in-tnpl",
            "description": "The decision comes as a setback for TNPL, which had plans in place to allow each franchise to include up to two players outside the state",
            "created_at": "2018-07-11 13:03:26",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "1",
            "speciality_name": ""
        }
    ],
    "message": "Post list retrieve successfully",
    "requestKey": "api/postlistbyusertypeid/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

      <!-- ************************ like post by a user or doctor ********************* -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">12) like or unlike post for both user and doctor</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
token:required,
usertype:required,
user_id:required,
post_id:required,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>likepost</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Post liked successfully",
    "requestKey": "api/likepost"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

<!-- ************************** save post by user or doctor *********************** -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">13) save or unsave post for both user and doctor</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
token:required,
usertype:required,
user_id:required,
post_id:required,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>savepost</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Post save successfully",
    "requestKey": "api/savepost"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

      <!-- ************************ comment list regarding to a post *********************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">14) comment list for a post</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>Commentlist/post_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "comment_id": "2",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "fullname": "mohit",
            "email": "pratap11192@gmail.com",
            "message": "asfdafsdfsdfsd",
            "created_at": "1 minute ago"
        },
        {
            "comment_id": "1",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "fullname": "mohit",
            "email": "pratap11192@gmail.com",
            "message": "asfdafsdfsdfsd",
            "created_at": "1 minute ago"
        }
    ],
    "message": "Post Comment list retrieved successfully.",
    "requestKey": "api/Commentlist/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ************************************** comment by user or doctor both on a post ***************-->


      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">15) comment on a post by user or doctor both </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                post_id:required,
user_id:required,
token:required,
usertype:required,
message:required,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>commentOnPost</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "comment_id": "3",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "fullname": "mohit",
            "email": "pratap11192@gmail.com",
            "message": "asfdafsdfsdfsd",
            "created_at": "1 second ago"
        },
        {
            "comment_id": "2",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "fullname": "mohit",
            "email": "pratap11192@gmail.com",
            "message": "asfdafsdfsdfsd",
            "created_at": "8 minutes ago"
        },
        {
            "comment_id": "1",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "fullname": "mohit",
            "email": "pratap11192@gmail.com",
            "message": "asfdafsdfsdfsd",
            "created_at": "9 minutes ago"
        }
    ],
    "message": "Post Comment list retrieved successfully.",
    "requestKey": "api/commentOnPost"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>



<!-- ********************************** edit user profile ******************************** -->


<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">16) User profile edit </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                token:required,
                email:(optional)
                phone:optional
                image :optional
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>editUserProfile/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "fullname": "mohit",
        "phone": "9140405198",
        "country_code": "789",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "is_admin": "0",
        "email": "pratap1119@gmail.com",
        "city_id": "",
        "admin_status": "1",
        "created_at": "2018-07-17 09:15:18",
        "updated_at": "2018-07-17 13:05:30",
        "user_id": "1"
    },
    "message": "User Profile successfully updated",
    "requestKey": "api/editUserProfile/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

<!--  ************************************ view user profile  ************************************ -->


<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">17) View user profile </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>viewUserProfile/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "fullname": "mohit",
        "phone": "9140405198",
        "country_code": "789",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "is_admin": "0",
        "email": "pratap1119@gmail.com",
        "city_id": "",
        "admin_status": "1",
        "created_at": "2018-07-17 09:15:18",
        "updated_at": "2018-07-17 13:05:30",
        "user_id": "1"
    },
    "message": "User Profile successfully updated",
    "requestKey": "api/editUserProfile/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

      <!-- ********************************** view doctor profile ************************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">18) View doctor profile </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>viewdoctorprofile/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "email": "hdhd@hdyd.com",
        "city_id": "5",
        "fullname": "hdhxx",
        "speciality_id": "5",
        "clinic": "Hdhdhxhxx",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "licence_number": "Hdhxhc",
        "expertise_area": "Jdhdd",
        "insurance_accept": "",
        "password": "123465",
        "admin_status": "1",
        "remember_token": "",
        "created_at": "2018-07-17 02:52:23",
        "updated_at": "2018-07-17 09:52:23",
        "phone": "9898565656",
        "user_id": "1"
    },
    "message": "Doctor Profile details",
    "requestKey": "api/viewdoctorprofile/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


<!-- ****************************** saved post list of a user or doctor both ****************** -->


  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">19) Save post list for user or doctor both </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                usertype:required|in:user,doctor;
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>savepostlist/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "title": "main title",
            "url": "",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 10:48:53",
            "fullname": "rajeev",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "21",
            "speciality_name": "",
            "post_image": "",
            "shareurl": "http://mobulous.app/healthapp/postpage/eyJpdiI6IjgrTmllTnZMc3JCOXRnVnNUQkdzUWc9PSIsInZhbHVlIjoiZ1NIQko1aGtINGtCSWVRcGoweExTUT09IiwibWFjIjoiMGNmMTdmMWFkYzU2NzVlN2E1NTMyYjcyZDAxMzBmNGUxNTQ1MzgyZGVlODI3MTgxYTYzM2M1ZDY0YmI0ZWM0ZSJ9"
        },
        {
            "title": "abcd",
            "url": "http://www.cricbuzz.com/cricket-news/103057/tnpl-2018-supreme-court-rejects-provision-for-outstation-players-in-tnpl",
            "description": "dsahfgshdgfsd shdgfjsdgfhgsdf",
            "created_at": "2018-07-17 09:23:09",
            "fullname": "mohit",
            "image": "https://mobulous.app/fametales/public/img/user_signup.png",
            "post_id": "1",
            "speciality_name": "",
            "post_image": "http://mobulous.app/healthapp/public/postimage/3e09a070049abaa64e3b8d6c87dea2c35157.png",
            "shareurl": "http://mobulous.app/healthapp/postpage/eyJpdiI6Ik5halRIVHVjc0lwbThlclI5TWdnanc9PSIsInZhbHVlIjoia2c5NlR6MkljU3liQmdLQXFYcFFlZz09IiwibWFjIjoiYzhhNjhkNDRkYWU3ZTkzYTcwYTc3YWZjNTJlNzNjZmI1NTMxYzlhMTc1Y2ZiMDllNDc1N2IyYmVmZjUwZGQ1MSJ9"
        }
    ],
    "message": "Saved Post list retrieve successfully",
    "requestKey": "api/savepostlist/3"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- *********************************** delete post ************************** -->


      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">20) delete post for user or doctor both </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
                usertype:required|in:user,doctor;
                user_id:required,
                post_id:required,
                token:required,
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>deletepost</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "post deleted successfully",
    "requestKey": "api/deletepost"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


<!-- ************************** all specialty list *************************************************** -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">21) all specialty list </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
               
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>specialty_all</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "specialties": [
            {
                "id": "1",
                "name": "Internal Medicine"
            },
            {
                "id": "2",
                "name": "Pediatrician"
            },
            {
                "id": "3",
                "name": "Cosmetologist"
            },
            {
                "id": "4",
                "name": "Medical Cosmetologist"
            },
            {
                "id": "5",
                "name": "Aesthetic Surgeon"
            },
            {
                "id": "6",
                "name": "Minimal Access Surgery"
            },
            {
                "id": "8",
                "name": "Obstetrician"
            },
            {
                "id": "10",
                "name": "Dead Doc Spencer"
            },
            {
                "id": "11",
                "name": "Know"
            },
            {
                "id": "12",
                "name": "Pandey"
            },
            {
                "id": "13",
                "name": "dddc"
            },
            {
                "id": "14",
                "name": "Heart"
            },
            {
                "id": "15",
                "name": "Heart"
            },
            {
                "id": "16",
                "name": "hshshs"
            },
            {
                "id": "17",
                "name": "hshshs"
            },
            {
                "id": "18",
                "name": "hshshs"
            },
            {
                "id": "19",
                "name": "hshshs"
            },
            {
                "id": "20",
                "name": "Human Body"
            },
            {
                "id": "21",
                "name": "Jsjsjs"
            },
            {
                "id": "22",
                "name": "surgen"
            },
            {
                "id": "23",
                "name": "surgery"
            },
            {
                "id": "24",
                "name": "sousoys"
            },
            {
                "id": "25",
                "name": "heart"
            },
            {
                "id": "26",
                "name": "Other"
            },
            {
                "id": "27",
                "name": "hshshs"
            },
            {
                "id": "28",
                "name": "ck speciality"
            },
            {
                "id": "29",
                "name": "Other"
            },
            {
                "id": "30",
                "name": "hshshs"
            },
            {
                "id": "31",
                "name": "hshshs"
            },
            {
                "id": "32",
                "name": "hshshs"
            },
            {
                "id": "33",
                "name": "Aothodonal Full Time"
            },
            {
                "id": "34",
                "name": "gogo"
            },
            {
                "id": "35",
                "name": "sfsdfsdfs"
            },
            {
                "id": "36",
                "name": "sfsdfsdfs"
            },
            {
                "id": "37",
                "name": "sfsdfsdfs"
            },
            {
                "id": "38",
                "name": "sfsdfsdfs"
            },
            {
                "id": "39",
                "name": "sfsdfsdfs"
            },
            {
                "id": "40",
                "name": "sfsdfsdfs"
            },
            {
                "id": "41",
                "name": "sfsdfsdfs"
            },
            {
                "id": "42",
                "name": "sfsdfsdfs"
            },
            {
                "id": "43",
                "name": "asdasdas1240"
            },
            {
                "id": "44",
                "name": "Eyes Speciality"
            }
        ]
    },
    "message": "Specialties list retrieve successfully.",
    "requestKey": "api/specialty_all"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

<!-- *********************** all city list ********************************* -->


      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">22) city list </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
               
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>all_city</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "id": "1",
            "name": "Dubai"
        },
        {
            "id": "2",
            "name": "Fujairah"
        },
        {
            "id": "3",
            "name": "Ras Al Khaimah"
        },
        {
            "id": "4",
            "name": "Umm Al Quwain"
        },
        {
            "id": "5",
            "name": "Sharjah"
        },
        {
            "id": "6",
            "name": "Abu Dhabi"
        },
        {
            "id": "7",
            "name": "Ajman"
        }
    ],
    "message": "City list retrieve successfully",
    "requestKey": "api/all_city"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ******************** specialties by city ************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">23) specialties by city </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
               city_id=>(optional)

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>specialtiesbycity</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "id": "2",
            "name": "Pediatrician"
        },
        {
            "id": "22",
            "name": "surgen"
        },
        {
            "id": "25",
            "name": "heart"
        },
        {
            "id": "33",
            "name": "Aothodonal Full Time"
        },
        {
            "id": "4",
            "name": "Medical Cosmetologist"
        },
        {
            "id": "6",
            "name": "Minimal Access Surgery"
        }
    ],
    "message": "Specialties list by city retrieve successfully",
    "requestKey": "api/specialtiesbycity"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

      <!-- ************************************* doctor name or clinic listing ************************************* -->


       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">24) doctor name or clinic listing </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
               city_id=>(optional)
               speciality_id=>(optional)
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>dorcname</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "name": "dr raaz"
        },
        {
            "name": "Gora gav1"
        }
    ],
    "message": "Doctor and clinic name list retrieve successfully",
    "requestKey": "api/dorcname"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ********************* doctor list on search ******************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">25) doctor list on search </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
               city_id=>(optional)
               speciality_id=>(optional)
               search_name=>(optional)
               minrange & maxrange=>(optional)
               insurance=>a,b like that(optional)
               rating=>in(0,1,2,3,4,5)(optional)
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>doctordetaillist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "doctorlist": [
            {
                "fullname": "dr raaz",
                "user_id": "2",
                "clinic": "Gora gav1",
                "avilability": [
                    "M",
                    "T",
                    "Th"
                ],
                "image": "http://mobulous.app/healthapp/public/userimage/4d0423c809d09809d8596d9bf90d04a21990.png",
                "speciality_name": "Internal Medicine",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "msg",
                "user_id": "5",
                "clinic": "nsnsns",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "surgen",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "manik android",
                "user_id": "7",
                "clinic": "Clinic1234567890",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Minimal Access Surgery",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "Manik the tester",
                "user_id": "9",
                "clinic": "mobulous",
                "avilability": [
                    "M",
                    "T",
                    "Wed",
                    "Th",
                    "F",
                    "Sat",
                    "S"
                ],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "heart",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "mk mk2",
                "user_id": "19",
                "clinic": "Jdjdjd",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Medical Cosmetologist",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "rajevvb hauzkhas shamrma",
                "user_id": "24",
                "clinic": "Auothodonal Clinic",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Aothodonal Full Time",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "garima",
                "user_id": "41",
                "clinic": "Clinic",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Pediatrician",
                "city_name": "Dubai",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "Ajeet",
                "user_id": "17",
                "clinic": "Kusama",
                "avilability": [
                    "Sat"
                ],
                "image": "http://mobulous.app/healthapp/public/userimage/1b2a77fb8eed2b0b1c6e94d7f50841726859.jpg",
                "speciality_name": "Aesthetic Surgeon",
                "city_name": "Fujairah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "Dr. Dev",
                "user_id": "42",
                "clinic": "Dev Clinic",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Internal Medicine",
                "city_name": "Fujairah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "doc dre",
                "user_id": "18",
                "clinic": "Basement Clinic Dre",
                "avilability": [
                    "F"
                ],
                "image": "http://mobulous.app/healthapp/public/userimage/96b08b7bd58c21fe58877c3d65e7281a4251.jpg",
                "speciality_name": "Internal Medicine",
                "city_name": "Ras Al Khaimah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "hdhxx",
                "user_id": "1",
                "clinic": "Hdhdhxhxx",
                "avilability": [
                    "M"
                ],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Aesthetic Surgeon",
                "city_name": "Sharjah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "cj",
                "user_id": "8",
                "clinic": "mobulous",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "sousoys",
                "city_name": "Sharjah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "hshshse",
                "user_id": "13",
                "clinic": "Jssjjsjs",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Other",
                "city_name": "Sharjah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "shhsh",
                "user_id": "14",
                "clinic": "Mobulous",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "hshshs",
                "city_name": "Sharjah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "llllll",
                "user_id": "15",
                "clinic": "Mobulous",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "hshshs",
                "city_name": "Sharjah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "jsjsjsjhhh",
                "user_id": "16",
                "clinic": "Mobulous",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "hshshs",
                "city_name": "Sharjah",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "jsjsjsjjeej",
                "user_id": "3",
                "clinic": "Jsjjsjsjs",
                "avilability": [
                    "T"
                ],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Jsjsjs",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "jmg",
                "user_id": "4",
                "clinic": "Hcy",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Dead Doc Spencer",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "msg msg",
                "user_id": "6",
                "clinic": "jsjsjs",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "surgery",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "Prashant Chaudhary",
                "user_id": "11",
                "clinic": "Mobulous",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "hshshs",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "ck ck",
                "user_id": "12",
                "clinic": "ck clinic",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "ck speciality",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "hdjdj",
                "user_id": "39",
                "clinic": "Eyes Clinic",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Eyes Speciality",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "drdre",
                "user_id": "43",
                "clinic": "Dr Dre Clinic",
                "avilability": [
                    "S",
                    " M"
                ],
                "image": "http://mobulous.app/healthapp/public/userimage/51aec815aa1b3e4719e43d46cc2094c25172.jpg",
                "speciality_name": "Medical Cosmetologist",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "dr. ram singh",
                "user_id": "46",
                "clinic": "Ram Clinix",
                "avilability": [
                    "M"
                ],
                "image": "http://mobulous.app/healthapp/public/userimage/cb63fc3afe89a2a893491072e90c69dc3791.jpg",
                "speciality_name": "Aesthetic Surgeon",
                "city_name": "Abu Dhabi",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "ganesh",
                "user_id": "27",
                "clinic": "Nuro",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Aesthetic Surgeon",
                "city_name": "Ajman",
                "fee": "100",
                "rating": "100"
            },
            {
                "fullname": "aby",
                "user_id": "40",
                "clinic": "Jac",
                "avilability": [],
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "speciality_name": "Aesthetic Surgeon",
                "city_name": "Ajman",
                "fee": "100",
                "rating": "100"
            }
        ],
        "minrange": "10",
        "maxrange": "80",
        "insurance_accept_list": [
            "a",
            "b",
            "d",
            "Ins1",
            "Ins2",
            "Kotak Life"
        ]
    },
    "message": "Doctors list retrieve successfully",
    "requestKey": "api/doctordetaillist"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


<!-- ********************************** doctor detail api ************************* -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">26) doctor details </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
             
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>fulldoctordetails/doctor_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "fullname": "hdhxx",
        "user_id": "1",
        "clinic": "Hdhdhxhxx",
        "avilability": "",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "speciality_name": "Aesthetic Surgeon",
        "city_name": "Sharjah",
        "expertise_area": "Jdhdd",
        "insurance_accept": "",
        "qualification": "na",
        "fee": "100",
        "rating": "100",
        "num_of_review": 2,
        "review_list": [
            {
                "image": "https://mobulous.app/fametales/public/img/user_signup.png",
                "rating": "100",
                "review": "nice work",
                "fullname": "mohit"
            },
            {
                "image": "http://mobulous.app/healthapp/public/userimage/a25deaaa6f686e1505709016b35148298369.jpg",
                "rating": "100",
                "review": "nice work",
                "fullname": "dsp"
            }
        ]
    },
    "message": "Doctors details retrieve successfully",
    "requestKey": "api/fulldoctordetails/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ******************************* edit doctor profile ********************** -->


      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">27) edit doctor profile </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
              token:required
              image:
              city_id:1(format)
              qualification:
              fees:
              start_time:2(format)
              end_time:9(format)
              clinic:
              avilability:Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday(format)
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>editdoctorprofile/doctor_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "email": "manik@g.com",
        "city_id": "1",
        "fullname": "Manik the tester",
        "speciality_id": "25",
        "clinic": "mobulous",
        "image": "https://mobulous.app/fametales/public/img/user_signup.png",
        "licence_number": "heart",
        "expertise_area": "heart",
        "insurance_accept": "asdasda,asdasd",
        "qualification": "dasdsadasdad",
        "avilability": "M,T,Wed,Th,F,Sat,S",
        "start_time": "2",
        "end_time": "9",
        "password": "qwerty",
        "fee": "25",
        "admin_status": "1",
        "remember_token": "",
        "created_at": "2018-08-03 00:25:41",
        "updated_at": "2018-08-03 07:26:40",
        "phone": "8555155721",
        "user_id": "9"
    },
    "message": "Doctor Profile updated successfully",
    "requestKey": "api/editdoctorprofile/9"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ************************** submit doctor review ****************** -->


       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">28) submit doctor review </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
              token:required
              user_id:required
              doctor_id:required
              rating:required
              review:

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>submitReview</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Doctors review submited successfully",
    "requestKey": "api/submitReview"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

<!-- ************************************ reason for cancellation list ********************** -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">29) reason for cancellation list </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
           

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>reasonlist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "reason_id": "1",
            "name": "a"
        },
        {
            "reason_id": "2",
            "name": "b"
        }
    ],
    "message": "Reason list retrieve successfully",
    "requestKey": "api/reasonlist"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

      <!-- ********************* reason submit ************************** -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">30) reason cancellation submit </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
           token:required
           user_id:required
           doctor_id:required
           reason_id:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>reasonSubmit</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Cancellation submited successfully",
    "requestKey": "api/reasonSubmit"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

<!-- ************************ issue list api with image ***************** -->

<a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">31) issue list api with image </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
        

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>issuelist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "id": "1",
            "name": "Internal Medicine",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "2",
            "name": "Pediatrician",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "3",
            "name": "Cosmetologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "4",
            "name": "Medical Cosmetologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "5",
            "name": "Aesthetic Surgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "6",
            "name": "Minimal Access Surgery",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "7",
            "name": "Other",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "45",
            "name": "Cosmetic/ Plastic Surgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "46",
            "name": "Dermatologist/ Cosmetologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "47",
            "name": "Cosmetic/ Plastic Surgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "48",
            "name": "Gynaecologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "49",
            "name": "Infertility Specialist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "50",
            "name": "Gynaecological Endoscopy",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "51",
            "name": "Paediatric & Adolescent Gynaecologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "52",
            "name": "Gynaecologic Oncologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "53",
            "name": "IVF (In Vitro Fertilization) Specialist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "54",
            "name": "Infertility Specialist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "55",
            "name": "Shoulder Specialist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "56",
            "name": "Orthopedic Surgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "57",
            "name": "Arthroscopy & Sports Surgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "58",
            "name": "Joint Replacement Surgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "59",
            "name": "Trichologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "60",
            "name": "Venereologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "61",
            "name": "Cardiologist",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "62",
            "name": "Spine Surgery",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        },
        {
            "id": "63",
            "name": "Neurosurgeon",
            "img": "http://mobulous.app/healthapp/public/assets/images/categrese/categrese-8.png"
        }
    ],
    "message": "Issue list retrieve successfully.",
    "requestKey": "api/issuelist"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- ******************* request sent by user ***************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">32) request sent by user </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
           token:required
           user_id:required
           doctor_id:required
         

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>notifyfromuser</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Request sent successfully",
    "requestKey": "api/notifyfromuser"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

      <!-- ******************* booking confirmed by doctor ***************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">33) booking confirmed by doctor</span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
          doctor_id:required
          token:required
          firstname:required
          lastname:required
          email:required
          phone:required
          age:required
          gender:required
          apdate:required
          aptime:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>notifyfromdoctor</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "Booking confirmed successfully",
    "requestKey": "api/notifyfromdoctor"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>

       <!-- ******************* notification list for both user or doctor ***************************** -->


       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">34) notification list for both user or doctor </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
          
           user_id:required
           usertype:required (In case of doctor notification listing usertype is user and vice versa)
         

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>notifylist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "msg": "shady sent invitation on 10/08/2018",
            "image": "http://mobulous.app/healthapp/public/userimage/13b8bc032c38e2e185bcf24daa191bb31417.jpg",
            "user_id": "32",
            "usertype": "user",
            "time": "3 days before"
        },
        {
            "msg": "shady sent invitation on 13/08/2018",
            "image": "http://mobulous.app/healthapp/public/userimage/13b8bc032c38e2e185bcf24daa191bb31417.jpg",
            "user_id": "32",
            "usertype": "user",
            "time": "8 hours before"
        },
        {
            "msg": "shady sent invitation on 13/08/2018",
            "image": "http://mobulous.app/healthapp/public/userimage/13b8bc032c38e2e185bcf24daa191bb31417.jpg",
            "user_id": "32",
            "usertype": "user",
            "time": "8 hours before"
        }
    ],
    "message": "Notification list retrieved successfully",
    "requestKey": "api/notifylist"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes:In case of doctor notification listing usertype is user and vice versa </h5></p>
      </div>

      <!-- ***************** booking list for both user or doctor ********************* -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">35) booking list for both user or doctor </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
          
           user_id:required
           usertype:required 
         

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>bookinglist</p>
          <p> <h5>Sample Response: in case of doctor</h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "month": "Aug 2018",
        "days_list": {
            "day1": {
                "day_name": "T",
                "day": "14"
            },
            "day2": {
                "day_name": "W",
                "day": "15"
            },
            "day3": {
                "day_name": "Th",
                "day": "16"
            },
            "day4": {
                "day_name": "F",
                "day": "17"
            },
            "day5": {
                "day_name": "Sat",
                "day": "18"
            },
            "day6": {
                "day_name": "S",
                "day": "19"
            },
            "day7": {
                "day_name": "M",
                "day": "20"
            }
        },
        "booking_list": {
            "T": [
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/72d35032381b5c3a99c388e1edf2e9cf4364.jpg",
                    "fullname": "rajeev",
                    "city_name": "Sharjah",
                    "apdate": "14/8/2018",
                    "aptime": "18:30",
                    "user_id": "3",
                    "email": "rajeev@gmail.com",
                    "phone": "526666222",
                    "msg": "Appointment: 01/01/1970 at 06:30pm"
                },
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/72d35032381b5c3a99c388e1edf2e9cf4364.jpg",
                    "fullname": "rajeev",
                    "city_name": "Sharjah",
                    "apdate": "14/8/2018",
                    "aptime": "18:38",
                    "user_id": "3",
                    "email": "rajeev@gmail.com",
                    "phone": "526666222",
                    "msg": "Appointment: 01/01/1970 at 06:38pm"
                }
            ],
            "W": [
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/72d35032381b5c3a99c388e1edf2e9cf4364.jpg",
                    "fullname": "rajeev",
                    "city_name": "Sharjah",
                    "apdate": "15/8/2018",
                    "aptime": "18:33",
                    "user_id": "3",
                    "email": "rajeev@gmail.com",
                    "phone": "526666222",
                    "msg": "Appointment: 01/01/1970 at 06:33pm"
                },
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/72d35032381b5c3a99c388e1edf2e9cf4364.jpg",
                    "fullname": "rajeev",
                    "city_name": "Sharjah",
                    "apdate": "15/8/2018",
                    "aptime": "18:06",
                    "user_id": "3",
                    "email": "rajeev@gmail.com",
                    "phone": "526666222",
                    "msg": "Appointment: 01/01/1970 at 06:06pm"
                }
            ]
        }
    },
    "message": "Doctor Booking list retrieve successfully",
    "requestKey": "api/bookinglist"
}
            </pre>
            <p> <h5>Sample Response: in case of user</h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "month": "Aug 2018",
        "days_list": {
            "day1": {
                "day_name": "T",
                "day": "14"
            },
            "day2": {
                "day_name": "W",
                "day": "15"
            },
            "day3": {
                "day_name": "Th",
                "day": "16"
            },
            "day4": {
                "day_name": "F",
                "day": "17"
            },
            "day5": {
                "day_name": "Sat",
                "day": "18"
            },
            "day6": {
                "day_name": "S",
                "day": "19"
            },
            "day7": {
                "day_name": "M",
                "day": "20"
            }
        },
        "booking_list": {
            "T": [
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/4d0423c809d09809d8596d9bf90d04a21990.png",
                    "fullname": "dr raaz",
                    "speciality_name": "Internal Medicine",
                    "city_name": "Abu Dhabi",
                    "apdate": "14/8/2018",
                    "aptime": "12",
                    "doctor_id": "2",
                    "avilability": "M,T,W,Th",
                    "fee": "10",
                    "msg": "Appointment: 14/8/2018 at 12:00am"
                },
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/51aec815aa1b3e4719e43d46cc2094c25172.jpg",
                    "fullname": "drdre",
                    "speciality_name": "Medical Cosmetologist",
                    "city_name": "Abu Dhabi",
                    "apdate": "14/8/2018",
                    "aptime": "18:30",
                    "doctor_id": "43",
                    "avilability": "S, M",
                    "fee": "100",
                    "msg": "Appointment: 14/8/2018 at 06:30pm"
                },
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/51aec815aa1b3e4719e43d46cc2094c25172.jpg",
                    "fullname": "drdre",
                    "speciality_name": "Medical Cosmetologist",
                    "city_name": "Abu Dhabi",
                    "apdate": "14/8/2018",
                    "aptime": "18:38",
                    "doctor_id": "43",
                    "avilability": "S, M",
                    "fee": "100",
                    "msg": "Appointment: 14/8/2018 at 06:38pm"
                }
            ],
            "W": [
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/51aec815aa1b3e4719e43d46cc2094c25172.jpg",
                    "fullname": "drdre",
                    "speciality_name": "Medical Cosmetologist",
                    "city_name": "Abu Dhabi",
                    "apdate": "15/8/2018",
                    "aptime": "18:33",
                    "doctor_id": "43",
                    "avilability": "S, M",
                    "fee": "100",
                    "msg": "Appointment: 15/8/2018 at 06:33pm"
                },
                {
                    "image": "http://mobulous.app/healthapp/public/userimage/51aec815aa1b3e4719e43d46cc2094c25172.jpg",
                    "fullname": "drdre",
                    "speciality_name": "Medical Cosmetologist",
                    "city_name": "Abu Dhabi",
                    "apdate": "15/8/2018",
                    "aptime": "18:06",
                    "doctor_id": "43",
                    "avilability": "S, M",
                    "fee": "100",
                    "msg": "Appointment: 15/8/2018 at 06:06pm"
                }
            ]
        }
    },
    "message": "User Booking list retrieve successfully",
    "requestKey": "api/bookinglist"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> <h5>Notes: </h5></p>
      </div>


      <!-- *********************** notification on / off api ******************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">36) notification on / off for both user or doctor </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
          
           user_id:required
           usertype:required 
         

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>notificationtrigger</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "OFF"
    },
    "message": "Notification state change successfully",
    "requestKey": "api/notificationtrigger"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> </p>
      </div>

      <!-- ********************** user review list for doctor ************ -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">37) user review list for doctor </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>reviewandratting/doctor_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "avg_rate": "5",
        "review_count": "2",
        "details": [
            {
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "rating": "100",
                "review": "nice work",
                "fullname": "mohit"
            },
            {
                "image": "http://mobulous.app/healthapp/public/userimage/a25deaaa6f686e1505709016b35148298369.jpg",
                "rating": "100",
                "review": "nice work",
                "fullname": "dsp"
            }
        ]
    },
    "message": "Review list retrieved successfully",
    "requestKey": "api/reviewandratting/1"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> </p>
      </div>

      <!-- ************************************ forgot password *********************** -->

 <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">38) forgot password for both user or doctor </span>
      </a>
      <div class="accordion-content">
     <!--  <p>signup_type = normal,amazon,facebook,twitter,gmail</p>
       <p>account_type = saving,current</p>
      <p>payment_method = amazon,paypal,google,credit_debit,apple</p>
    <p>  user_type   = host,foodie  <p>
    <p>notification_status = on,off</p> -->

              <h5>Parameters: </h5>
              <pre>
              email:
              usertype:
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
         
          <p> <h5>requestKey: </h5>forgotpass</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "status": "success"
    },
    "message": "User mail sent successfully",
    "requestKey": "api/forgotpass"
}
            </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
         <p> </p>
      </div>

      <!-- **************************** send message ************************************ -->

  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">39) send message </span>
      </a>
      <div class="accordion-content">
     
          <p>
              <h5>Parameters: </h5>
              <pre>
               user_id:required
              doctor_id:required
              msg:required
              token:required
              usertype:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>sendmessage</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "unique_id": "5",
        "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
        "msg": "how are you",
        "time": "1 second ago",
        "flag": "1"
    },
    "message": "Message sent successfully",
    "requestKey": "api/sendmessage"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- ************************ get message ************************************** -->

  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">40) get message </span>
      </a>
      <div class="accordion-content">
     
          <p>
              <h5>Parameters: </h5>
              <pre>
               token:required
                user_id:required
                doctor_id:required
                usertype:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>getmessage</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": {
        "room_name": "garima",
        "msg_thread": [
            {
                "unique_id": "2",
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "msg": "hello",
                "time": "44 minutes ago",
                "flag": "1"
            },
            {
                "unique_id": "1",
                "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
                "msg": "hello",
                "time": "46 minutes ago",
                "flag": "0"
            }
        ]
    },
    "message": "Message retrieve successfully",
    "requestKey": "api/getmessage"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- ************************* last message thread ***************************** -->

  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">41)last message thread </span>
      </a>
      <div class="accordion-content">
     
          <p>
              <h5>Parameters: </h5>
              <pre>
               usertype:required
                user_id:required
                token:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>chatthread</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "data": [
        {
            "image": "http://mobulous.app/healthapp/public/img/user_signup.png",
            "user_id": "2",
            "fullname": "garima",
            "msg": "hello",
            "chat_id": "1",
            "created_at": "1 hour ago"
        }
    ],
    "message": "Message thread retrieve successfully",
    "requestKey": "api/chatthread"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>

<!-- ************************* Payment api 1 ***************************** -->

  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">42)Payment authorization api </span>
      </a>
      <div class="accordion-content">
     
          <p>
              <h5>Parameters: </h5>
              <pre>
               email:required
                value:required
                currency:required
                trackId:required
                cardToken:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>request URL: </h5>https://mydoctoruae.com/simplecurl.php</p>
          <p> <h5>Sample Response: </h5>
            <pre>
/*

    // error
    {
"errorCode": "84133",
"message": "Unable to determine channel url",
"eventId": "fd4136aa-9de5-4c72-b719-d88fa2e25a1f"
}


    // success
    * {
  "id": "charge_test_CD4929FD251X71AFA8B0",
  "liveMode": false,
  "created": "2018-10-12T11:35:02Z",
  "value": 10,
  "currency": "AED",
  "trackId": "track1234546",
  "description": null,
  "email": "ram@gmail.com",
  "chargeMode": 1,
  "transactionIndicator": 1,
  "customerIp": null,
  "responseMessage": "Approved",
  "responseAdvancedInfo": "Approved",
  "responseCode": "10000",
  "status": "Authorised",
  "authCode": "667295",
  "isCascaded": false,
  "autoCapture": "Y",
  "autoCapTime": 24,
  "card": {
    "customerId": "cust_6EFD7C8F-EDFA-4CFC-8C86-80513E4F5514",
    "expiryMonth": "06",
    "expiryYear": "2020",
    "billingDetails": {
      "addressLine1": null,
      "addressLine2": null,
      "postcode": null,
      "country": null,
      "city": null,
      "state": null,
      "phone": {

      }
    },
    "id": "card_01F92EAC-62AA-47F4-9DA4-DCC539134BFB",
    "last4": "4242",
    "bin": "424242",
    "paymentMethod": "Visa",
    "fingerprint": "4E9512BE82393FCC54A07D8E354EBF7A352149C861B6528CD4E92E040D171882",
    "name": "ram",
    "cvvCheck": "Y",
    "avsCheck": "S"
  },
  "riskCheck": true,
  "customerPaymentPlans": null,
  "metadata": {

  },
  "shippingDetails": {
    "addressLine1": null,
    "addressLine2": null,
    "postcode": null,
    "country": null,
    "city": null,
    "state": null,
    "phone": {

    }
  },
  "products": [

  ],
  "udf1": null,
  "udf2": null,
  "udf3": null,
  "udf4": null,
  "udf5": null
}
     /</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: Please ignore base url and use request url</h5></p>
      </div>

      <!-- *************************** end api ****************** -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
      $('.accordion-toggle').on('click', function(event){
        event.preventDefault();

        // create accordion variables
        var accordion = $(this);
        var accordionContent = accordion.next('.accordion-content');
        var accordionToggleIcon = $(this).children('.toggle-icon');

        // toggle accordion link open class
        accordion.toggleClass("open");

        // toggle accordion content
        accordionContent.slideToggle(250);

        // change plus/minus icon
        if (accordion.hasClass("open")) {
          accordionToggleIcon.html("<i class='fa fa-minus-circle'></i>");
        } else {
          accordionToggleIcon.html("<i class='fa fa-plus-circle'></i>");
        }
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function () {
      $('ul.nav a').each(function(){
        if(location.href === this.href){
          $(this).addClass('active');
          $('ul.nav a').not(this).removeClass('active');
          return false;
        }
      });
    });
    </script>
</body>
</html>
