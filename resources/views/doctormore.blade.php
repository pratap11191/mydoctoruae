@extends('layout.site') 
@section('title', 'doctorprofile more details')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="container">
            <div class="morepagessection">
                <div class="">
                        <div class="tabdocts">
                          <a href="{{url('/subscription')}}" target="_blank" class="tablinks">Subscription Plans</a>
                          <button class="tablinks active" onclick="openCity(event, 'Paris')">View Ratings and Reviews</button>
                          <button class="tablinks" onclick="openCity(event, 'Tokyo')">Saved</button>
                        </div>

                        

                        <div id="Paris" style="display:block;" class="tabcontent">
                           <?php

                             $curl = curl_init();
                                // Set some options - we are passing in a useragent too here
                                curl_setopt_array($curl, array(
                                    CURLOPT_RETURNTRANSFER => 1,
                                    CURLOPT_URL => url('/')."/api/reviewandratting/".Request::session()->get('userdetails', 'default')->id,
                                    CURLOPT_USERAGENT => 'Codular Sample cURL Request'
                                ));
                                // Send the request & save response to $resp
                                $resp = curl_exec($curl);
                                // Close request to clear up some resources
                                curl_close($curl);
                              
                             
                            // echo $resp;

                             $resp1 = json_decode($resp);

                             $colorrate = 5;
                                
                            ?>   

                        
                        <div class="averageratings">
                        <h6>Average Ratings</h6>
                            <ul>
                            <?php

                                for ($i=1; $i <= round($resp1->data->avg_rate); $i++) { 
                                   
                            ?>
                            <li><i class="fa fa-star" style="color: #bbae0f;"></i></li>
                           <?php

                           $colorrate--;

                       }

                       ?>
                        <?php

                                for ($i=1; $i <= round($colorrate); $i++) { 
                                   
                            ?>
                            <li><i class="fa fa-star"></i></li>
                           <?php

                          // $colorrate--;

                       }

                       ?>
                            </ul>
                        </div>
                        
                            <h4>Reviews ({{$resp1->data->review_count}})</h4>
                            <?php

                                foreach ($resp1->data->details as $key => $value) {
                                    
                            ?>
                          <div class="notiftabs">
                            <div class=" userpic">
                                <img src="{{$value->image}}">
                            </div>
                            
                            <div class="usernotify">
                            <h6>{{ucfirst($value->fullname)}}</h6>
                            <p><?php if(!empty($value->review)){ echo ucfirst($value->review); } else { echo "No Review"; } ?></p> 
                            <span class="notifytimes"><i class="fa fa-star"></i> {{$value->rating}}%</span>
                            </div>
                            
                          </div>
                          
                         
                          <?php

                      }

                      ?>
                   
                        </div>

                        <div id="Tokyo" class="tabcontent">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-feedss">
        <div class="">
            <div class="hading_by_title">
                <h4>Saved Posts</h4>
            </div>
           <?php

               if($items->isEmpty()){
                echo "<p style='color:black;'>No Record found!!</p>";

              } else {

            	foreach ($items as $value) {
            		
            		//print_r($value);
            	
            ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="your-posts-section">
                    <div class="your-posts-pic-para">
                        <a href="{{$value->shareurl}}" class="newsfeed_a_all">
                            <div class="post-profile">
                                <img src="{{$value->image}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                 <h4>{{ucfirst($value->fullname)}}</h4>
                                <span class="desg-post">{{ucfirst($value->speciality_name)}}</span>
                                <p>{{ucfirst($value->description)}}</p>
                            </div>
                        </a>  
                    </div>
                    <div class="your-post-anchore">
                        <ul class="yourpost-ul">
                            <li><a href="{{$value->shareurl}}"><span class="yourposticon"><i class="fa fa-comment" aria-hidden="true"></i></span></a></li>
                            
                            <li><a href="#"><span class="yourposticon"><i class="fa fa-share-alt" aria-hidden="true"></i></span></a>
                                <ul class="your-post-share-ul">
                                   <li><a href="https://api.whatsapp.com/send?&text={{$value->shareurl}}" target="_blank">Whatsapp</a></li>
                                     <li><a href="https://www.facebook.com/sharer/sharer.php?u={{$value->shareurl}}" target="_blank">Facebook</a></li>
                                     <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{$value->shareurl}}&title={{ucfirst($value->title)}}&summary={{ucfirst($value->description)}}&source=Healthapp" target="_blank">LinkedIn</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php

        }

      }

        ?>

        {{ $items->links() }}
            
        </div>
    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection