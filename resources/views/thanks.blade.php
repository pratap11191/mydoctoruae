@extends('layout.site') 
@section('title', 'Thank You')
@section('content')
<div style="padding:150px;">
    <h4>Thanks for signing up with Us.</h4>
    <p>You will be able to access application features once We approve your profile.</br>You will receive an E-mail shortly.</p>
</div>
@endsection