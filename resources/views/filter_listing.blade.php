@extends('layout.site') 
@section('title', 'Doctor Listing')
@section('content')
<style type="text/css">
	.srcfilts{
		padding: 10px;
	}
</style> 
<div class="top_categres doctorlistingsbg">
        <div class="container-fluid">
		
		<div class="hading_by_title">
						<h4>Doctors</h4>
					</div>
			<div class="col-md-3 col-sm-3 flitby">
			<div class="filterbyapp">
			<h3>Filter By</h3>
			</div>
			
			<div class="formslogins">
			    <div class="form-group">
				
				<div class="filterscap editdownf accordion">
				<p><strong>By City</strong></p><i class="fa fa-plus"></i>
				</div>
				
				<div class="profilepanel srcfilts">
					<form action="{{url('/')}}/listing" method="post">
						<input type="hidden" id="city_id1" name="city_id" value="">
						<input type="hidden" id="speciality_id1" name="speciality_id" value="">
						<input type="hidden" id="rat_val" name="rating" value="">
						<input type="hidden" id="insurance_val" name="insurance" value="">
						<input type="hidden" id="maxfees" name="maxfees" value="">
						<input type="hidden" id="search_name1" name="search_name" value="">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<input type="submit" id="filter_form" style="display: none;">
					</form>
					<select class="limitedNumbChosen" id="city_id" >
					<option value="100" <?php if(Request::session()->get('city_id') == 100){ echo "selected"; } ?>>Select City</option>
					<?php foreach ($city_list as $value) { ?>	
					<option value="<?php echo $value->id; ?>" <?php if(Request::session()->get('city_id') == $value->id){ echo "selected"; } ?>><?php echo ucfirst($value->name); ?></option>
					<?php } ?>
					</select>
				</div>
		
				<div class="filterscap editdownf accordion">
						<p><strong>By Speciality</strong></p><i class="fa fa-plus"></i>
				</div>
				
				<div class="profilepanel srcfilts">
					<select class="limitedNumbChosen" id="speciality_id" >
									<option value="100" <?php if(Request::session()->get('city_id') == 100){ echo "selected"; } ?>>Select Specialty</option>
					<?php foreach ($specialties_list as $value) { ?>	
					<option value="<?php echo $value->id; ?>" <?php if(Request::session()->get('speciality_id') == $value->id){ echo "selected"; } ?>><?php echo ucfirst($value->name); ?></option>
				   <?php } ?>
					</select>
				</div>
		
				<div class="filterscap editdownf accordion">
						<p><strong>By Clinic</strong></p><i class="fa fa-plus"></i>
				</div>
		
				<div class="profilepanel srcfilts">
				
		
						<select class="limitedNumbChosen" id="search_name"  >
										<option value="100" <?php if(Request::session()->get('search_name') == 100){ echo "selected"; } ?>>Search Doctor Name or Clinic</option>
							<?php foreach ($name_list1 as $value) { ?>	
						<option value="<?php echo ucfirst($value->fullname); ?>" <?php if(Request::session()->get('search_name') == $value->fullname){ echo "selected"; } ?>><?php echo ucfirst($value->fullname); ?></option>
					   <?php } ?>
						</select>
				</div>
				
				<div class="filterscap editdownf accordion">
						<p><strong>By Ratings</strong></p><i class="fa fa-plus"></i>
				</div>
		        
				<div class="profilepanel srcfilts">
						<div class='rating-stars text-center'>
							<ul id='stars'>
							  <li class='star <?php if(Request::session()->get('rating') >= 1){ echo "selected"; } ?>' title='Poor' id="star1" data-value='1'>
								<i class='fa fa-star fa-fw'></i>
							  </li>
							  <li class='star <?php if(Request::session()->get('rating') >= 2){ echo "selected"; } ?>' title='Fair' id="star2" data-value='2'>
								<i class='fa fa-star fa-fw'></i>
							  </li>
							  <li class='star <?php if(Request::session()->get('rating') >= 3){ echo "selected"; } ?>' title='Good' id="star3" data-value='3'>
								<i class='fa fa-star fa-fw'></i>
							  </li>
							  <li class='star <?php if(Request::session()->get('rating') >= 4){ echo "selected"; } ?>' title='Excellent' id="star4" data-value='4'>
								<i class='fa fa-star fa-fw'></i>
							  </li>
							  <li class='star <?php if(Request::session()->get('rating') >= 5){ echo "selected"; } ?>' title='WOW!!!' id="star5" data-value='5'>
								<i class='fa fa-star fa-fw'></i>
							  </li>
							</ul>
						</div>
				</div>

				<script type="text/javascript">
					$('.star').click(function(){

						 $('#rat_val').val($(this).attr('data-value'));	

						
					});
				</script>
				
				<div class="filterscap editdownf accordion">
						<p><strong>By Range</strong></p><i class="fa fa-plus"></i>
				</div>
			
				<div class="profilepanel srcfilts">
				     <div class="slidecontainer">
						  <input type="range" min="{{$minrange}}" max="{{$maxrange}}" value="<?php if(!empty(Request::session()->get('maxfees'))){ echo Request::session()->get('maxfees'); } else { echo $minrange; } ?>" class="slider" id="myRange">
						  <p>Price Min: <span id="demo"></span></p>
					</div>
				</div>
				
				<div class="filterscap editdownf accordion">
						<p><strong>Insurance Accepted</strong></p><i class="fa fa-plus"></i>
				</div>
				
				<div class="profilepanel srcfilts">
				<div class="col-md-6"><input type="radio" name="accepted" class="accepted opentypes" value="1" <?php if(!empty(Request::session()->get('insurance')) && Request::session()->get('insurance') != '100'){ echo "checked"; } ?>>Yes</div>
                <div class="col-md-6"><input type="radio" name="accepted" class="accepted closetypes" value="0" <?php if(empty(Request::session()->get('insurance')) || Request::session()->get('insurance') == '100'){ echo "checked"; } ?>> No</div>
										
				</div>
				
                 
				
				<div class="isurancebx">
				          <?php
				          	if(!empty($insurance_accept_name)){

				          		$checkins = explode(",", Request::session()->get('insurance'));

				          		foreach ($insurance_accept_name as  $value) {
				          			
				          ?>
						   <div class="checkbox">
						   <label>
							 <input class="chkinput" type="checkbox" data="{{$value}}" <?php if(in_array($value, $checkins)){ echo "checked"; } ?>> {{$value}}
						   </label>
						 </div>
						 <?php

						}

						}
						?>
					</div>

					<script type="text/javascript">
						$('.accepted').click(function(){
							if($(this).val() == '1'){
								$('.chkinput').click(function(){
									 if($(this).prop("checked") == true){
							               	var datalist = "<?php echo Request::session()->get('insurance'); ?>";

											var datalist1 = datalist.split(",");

											var a = datalist1.indexOf($(this).attr('data'));

											if(a > 0){

											} else {
												datalist = datalist+","+$(this).attr('data');
											}
											
											$('#insurance_val').val(datalist);
											//$('#filter_form').click();
											
							            }
							            else if($(this).prop("checked") == false){
							                var datalist = "<?php echo Request::session()->get('insurance'); ?>";

											var datalist1 = datalist.split(",");

											var a = datalist1.indexOf($(this).attr('data'));

											if(a > 0){
												datalist = datalist.splice(a, 1);
												
											} else {
												
											}
											
											$('#insurance_val').val(datalist);
											//$('#filter_form').click();
							            }
								
								});
							} else {
								$('#insurance_val').val('100');
								//$('#filter_form').click();
							}
						});
					</script>

					<button type="button" id="uniquebtn" class="btn btn-success">Apply</button>
					
				</div>
			</div>
			</div>
			
			
			<div class="col-md-9 col-sm-9 cateisses">
			
			  <div class="filterbytabs">
			  <div class="col-md-5">
			  <!-- <p class="tagsap">By Location <i class="fa fa-times"></i></p> 
			  <p class="tagsap">By Type <i class="fa fa-times"></i></p>
			  <p class="tagsap">By Type <i class="fa fa-times"></i></p> -->
			  <a href="{{url('/')}}/clearallsesion" class="tagsap">Clear All </a>
			  </div>
			  <script type="text/javascript">
			  	$("#allclearbyme").click(function(){

			  	});
			  </script>
			  <div class="col-md-7 serachbydocts">
			  <input class="searchkeyword" type="text" placeholder="Search..">
			
			  </div>
			  </div>
			      <div id="result_area"></div>
					<?php

					//dd($items['items']);

					   if($items->isEmpty()){
					   		echo "<p style='color:black;'>No Record Found!</p>";
					   } else {

						foreach ($items as  $value) {
							
							

					?>
					
					<a href="{{url('/')}}/doctordetails/{{$value->user_id}}" class="friendslist" id="docomo<?php echo ucfirst($value->fullname); ?>">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="your-posts-section bookingtabs doctlsttbsa holder" id="<?php echo ucfirst($value->fullname); ?>" >
						
                    <div class="your-posts-pic-para" >
                            <div class="post-profile">
                                <img src="{{$value->image}}" class="img-responsive">
                            </div>
                            <div class="post-profile-paraes">
                                <h4>Dr. <?php echo ucfirst($value->fullname); ?></h4>
                                <span class="bookinfo desg-post">{{ucfirst($value->speciality_name)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value->clinic)}}</span>
								<span class="bookinfo desg-post">{{ucfirst($value->city_name)}}</span>
                                <!-- <p class="appoints">Appointments:22/05/2018 at 2:00pm</p> -->
                            </div>
						<div class="chatcomment">
						   <span class="rating ratingdocterdetails"><i class="fa fa-star" aria-hidden="true"></i><small>({{round($value->rating)}}%)</small></span>
						<!-- <i class="fa fa-comment" aria-hidden="true"></i> -->
						<p>AED {{ucfirst($value->fee)}}</p>
						<?php

							if($value->paid == "1"){
						?>
						<div class="sponseredoct"><p>Sponsored</p></div>
						<?php

					}

					?>
						</div>    
                    </div>
					<div class="your-post-anchore">
								<ul>
									<li><div class="yourposticon"><span class="availbledays">Availability:</span> <ul class="yourpost-ul"><?php $newavilability = explode(",", $value->avilability);
									if(!empty($value->avilability)){
									foreach ($newavilability as  $value1) {

										echo "<li>".$value1."</li>";
									}
								}
									 ?></ul></div></li>
								</ul>
					</div> 
                </div>
					</div>
					</a>
					<?php

				} }

				?>
				<div class="col-md-12 gayb"> {{ $items->links() }} </div>
			</div>
        </div>
    </div>
  <script type="text/javascript">
  	$(".limitedNumbChosen").select2();

  	// $(".limitedNumbChosen").change(function(){
  	// 	var city_id = $('#city_id').val();
  	// 	var speciality_id = $('#speciality_id').val();
  	// 	var search_name = $('#search_name').val();
  	// 	$('#city_id1').val(city_id);
  	// 	$('#speciality_id1').val(speciality_id);
  	// 	$('#search_name1').val(search_name);
  	// 	//alert(city_id);
  	// 	$('#filter_form').click();
  	// });

  	$("#myRange").change(function(){
  		$("#maxfees").val($(this).val());
  	})

  	$("#uniquebtn").click(function(){
  		var city_id = $('#city_id').val();
  		var speciality_id = $('#speciality_id').val();
  		var search_name = $('#search_name').val();
  		$('#city_id1').val(city_id);
  		$('#speciality_id1').val(speciality_id);
  		$('#search_name1').val(search_name);
  		//$('#rat_val').val($(this).attr('data-value'));	
  		$('#filter_form').click();
  		//alert("hii");
  	});

  </script>
    
  <script type="text/javascript">
  	
  	$(document).ready(function(){
    $(".searchkeyword").keyup(function(){
        var str = $(".searchkeyword").val();
        var count = 0;
//loop through the friendslist
$(".friendslist .holder").each(function(index){
            if($(this).attr("id")){
                //case insenstive search
                if(!$(this).attr("id").match(new RegExp(str, "i"))){
                    $(this).fadeOut("fast");
                }else{
                    $(this).fadeIn("slow");
                    count++;
                }
            }
        });
        //display no of results box when str is not empty
        if(str == '') { $("#result_area").hide(); $(".gayb").show(); } else { $("#result_area").show(); $(".gayb").hide(); }
        //display no of results found

        if(count == 0){
        	$("#result_area").text("No Record Found for "+str);
        } else {
        	$("#result_area").text("Top "+count+" results for "+str);
        }
        
    });
});

  </script>  
	

@endsection