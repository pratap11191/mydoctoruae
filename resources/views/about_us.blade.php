<head>
  <title>::My Doctor::</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    
    <!--main style sheet-->
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	
  
    
</head>
<body>

<div class="col-md-12 aboutus">
	<div class="container">
	    <div class="desc">
		    

			<h4>About Us</h4>
			<p>Mydoctor is the best way to seek medical help. To make it seamless and smooth, we have created a platform which lets you connect with any medical professional instantly. Find out immediately about your insurance acceptance, availability of doctor and reviews about the place you want to visit..</p>
        </div>
		
		<div class="feature">
			

			<ul>
				<li>
				<h6>Mydoctor For Everyone</h6>
					<ul>
						<li>
						
							<p> <strong>Talk to Doctor: </strong> You can ask queries for free or get into one-on-one interaction with doctors.</p>
							<p> <strong>Stay healthy and fit:</strong> The Health Feed from healthcare experts aims to keep you healthy and fit. If that is not all,
							you can find a doctor nearby, check doctor's profile and book an appointment too.</p>
						</li>
					</ul>
				</li>
				
				<h6>Mydoctor For Doctors</h6>
				<p>Mydoctor ensures that doctors save time and enhance practice effortlessly.</p>
				
				<li><p><strong>Grow Your Practice:</strong> Through this doctors grow their outreach to patients across the country, 
				by interacting with them. They not only get thanked by those whom they help out but also get recommended by peers. 
				This increases their popularity in the online community and in the medical fraternity.</p></li>
				
				<li><p><strong>Manage Your Practice:</strong> This is highly evolved and easy-to-use practice management software. It enables doctors to manage information for one or more clinics.
				Doctors can be in touch with their patients and manage their practice on the go with mydoctor app.</li>
				
			</ul>
			

		</div>
	</div>
</div>
