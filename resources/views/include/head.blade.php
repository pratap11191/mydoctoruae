  
  <link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
    <link rel="icon" href="/public/assets/images/fav.png" type="image/png" sizes="16x16">
    <!--main style sheet-->
    <link rel="stylesheet" href="{{url('/')}}/public/assets/css/style.css" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,500,600,700,800,900" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <meta charset="UTF-8">
  <meta name="description" content="Mydoctor is the best way to seek medical help. To make it seamless and smooth, we have created a platform which lets you connect with any medical professional instantly. Find out immediately about your insurance acceptance, availability of doctor and reviews about the place you want to visit.">
  <meta name="keywords" content="Anesthetist,Ophthalmologist,Orthopedic,ENT surgeon,Hematologist,Dermatologist,Radiologist,General Physician,Physiotherapy,Osteopathic doctor,Internal medicine,Neurologist,Nephrologist,Urologist,Plastic surgeon,Endodontist,Periodontist,Implantologist,Prosthodontist,Pediatric dentist,Orthodontist,Ayurveda">
  <meta name="author" content="MyDoctor UAE">