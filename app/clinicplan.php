<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinicplan extends Model
{
    //
    
    protected $fillable = [
        'clinic_id','sub_id','exp_date'
    ];
}
