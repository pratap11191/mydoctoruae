<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancellation extends Model
{
    //

    protected $fillable = [
        'user_id','doctor_id','reason_id'
    ];

}
