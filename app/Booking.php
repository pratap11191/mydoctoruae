<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //

    protected $fillable = [
        'firstname','lastname','email','phone','age','gender','apdate','aptime','doctor_id'
    ];

}
