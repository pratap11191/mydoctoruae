<?php
class Reason extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
			$this->load->model('Common_model');
			//$this->load->library('upload');
			if(empty($this->session->userdata['logged_in']['email'])) {
			redirect('Admin');
				}

			}

		public function index(){
			 	
			 	$data['reasondata'] = $this->Common_model->getData('reasons');
		
				$this->load->view('reasonlist',$data);
			
		}

		public function doctordetails(){
		 	$data['specialitydata'] = $this->Common_model->getspeciality('specialties'); 	
	    	echo json_encode($data);
			//$this->load->view('userlist',$data);
	
		}

		public function addReason(){

		
			$this->load->view('add_reason');

		  }

	  	public function saveReason(){
	  	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','name','trim|required',array('required'=>'Please enter Name of Cancellation Reasons'));
		
               	    
	 if ($this->form_validation->run() == true){


	 	
		if($this->input->post()){

		   //$filename = $this->upload->data('file_name');
	       $data = array(
						'name'				=>$this->input->post('name'),
						'created_at'		=>date('d-m-y h:i:s')
						);

			$datainsert = $this->Common_model->insertRecord('reasons',$data);
			if($datainsert){
			      	$this->session->set_flashdata('success','Add Reason successfully');
			      	redirect('reason');
			      }else{
			      	$this->session->set_flashdata('error',' occur an error Reason not insertd');
			      	redirect('reason/addReason');
			      }
				
		           } 
	            }

	    else{
        $this->load->view('add_reason');
		 }

	}

		

	public function reasonDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
        $del = $this->db->delete('reasons'); 
		if($del){

     $this->session->set_flashdata('success','Reason deleted successfully');
     redirect('reason');

		}else{
  $this->session->set_flashdata('error','Reason not  delete an error occur');
     redirect('reason');

   }
		
	} 
	
     



}



?>