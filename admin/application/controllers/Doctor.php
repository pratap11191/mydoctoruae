<?php
class Doctor extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
			$this->load->model('Doctor_model');
			$this->load->model('Common_model');
			$this->load->library('email');
			if(empty($this->session->userdata['logged_in']['email'])) {
			redirect('Admin');
				}

			}
			
	

		public function index(){
			 	
			 /*$data['doctordata'] = $this->Doctor_model->getdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'));*/

			 $data['doctordata'] = $this->Common_model->getData('doctors');
			 /*echo"<pre>";
			 print_r($data['doctordata']);
			 exit;*/
			 
			 $this->load->view('doctorlist',$data);
			
		}


public function searchdata(){

  $value = $this->uri->segment(3);
  if($value){


 if($value=='Previousweek'){
                          $enddate = date('Y:m:d'); 
                          //$startdate=Date('Y:m:d', strtotime("-7 days"));
                          $startdate=Date('Y:m:d', strtotime("-1 week"));

/*$data['doctordata'] =  $this->Doctor_model->getsearchdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'),$startdate,$enddate);*/

 $data['doctordata'] = $this->Doctor_model->getsearchdoctornew('doctors',$startdate,$enddate);
}
if($value=='Nextweek'){

$startdate = date('Y:m:d'); 
$enddate=Date('Y:m:d', strtotime("+1 week"));

/*$data['doctordata'] =  $this->Doctor_model->getsearchdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'),$startdate,$enddate);*/
$data['doctordata'] = $this->Doctor_model->getsearchdoctornew('doctors',$startdate,$enddate);


}
}else{
$data['doctordata'] = $this->Common_model->getData('doctors');

}

 $this->load->view('searchdoctor',$data);

}

public function searchmonthdata(){

 $month = $this->uri->segment(3);
 if($month){
 $startdate = date('Y:'.$month.':01');
 $enddate = date('Y:'.$month.':31');

 /*$data['doctordata'] =  $this->Doctor_model->getsearchdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'),$startdate,$enddate);*/

 $data['doctordata'] = $this->Doctor_model->getsearchdoctornew('doctors',$startdate,$enddate);
}else{
	$data['doctordata'] = $this->Common_model->getData('doctors');
}
 $this->load->view('searchdoctor',$data);
}


public function searchyeardata(){

 $year = $this->uri->segment(3);
 if($year){
 $startdate = date($year.':01:01');
 $enddate = date($year.':12:31');
//exit;
/*$data['doctordata'] =  $this->Doctor_model->getsearchdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'),$startdate,$enddate);*/

$data['doctordata'] = $this->Doctor_model->getsearchdoctornew('doctors',$startdate,$enddate);
}else{
	$data['doctordata'] = $this->Common_model->getData('doctors');
}
 $this->load->view('searchdoctor',$data);
}




		public function doctordetails(){

		 	$data['doctordata'] = $this->Doctor_model->getdoctor('doctors',array('doctors.id','doctors.fullname','doctors.phone','doctors.image','doctors.email','doctors.clinic','doctors.expertise_area','doctors.fee','doctors.admin_status','cities.name as cityname','specialties.name as speciality'));

		 	
	    	echo json_encode($data);
			
	
		}

		public function addDoctor(){


                 
			$data['pagetitle'] =$this->uri->segment(1);
			$data['citydata'] = $this->Doctor_model->getcity();
			$data['specialitydata'] = $this->Doctor_model->getDoctorspeciality();
			$this->load->view('add_doctor',$data);

		  }

	  	public function saveDoctor(){
	  	

				$this->load->library('form_validation');
				$this->form_validation->set_rules('name','name','trim|required',array('required'=>'Please enter your Full Name'));  
				$this->form_validation->set_rules('email','email','trim|required|is_unique[doctors.email]|valid_email',array('required'=>'Please enter Email ID','valid_email'=>'Please enter valid Email ID','is_unique'=>'This Email already exists'));

				$this->form_validation->set_rules('password','password','trim|required|min_length[5]|max_length[12]',array('required'=>'Please enter Password','min_length'=>'Password must be at least 6 and at most 15 characters','max_length'=>'Password must be at least 6 and at most 15 characters'));
				$this->form_validation->set_rules('phone','phone Number','trim|is_unique[doctors.phone]|required',array('required'=>'Please enter Phone Number','is_unique'=>'Please enter valid Phone Number'));
				$this->form_validation->set_rules('city','city','trim|required',array('required'=>'Please enter City!'));
				$this->form_validation->set_rules('speciality','speciality ','trim|required',array('required'=>'Please enter Speciality'));
				$this->form_validation->set_rules('clinic','clinic','trim|required',array('required'=>'Please enter Clinic Name'));
				$this->form_validation->set_rules('licence','licence','trim|required',array('required'=>'Please enter License Number'));
				$this->form_validation->set_rules('expertise','expertise','trim|required',array('required'=>' Please enter Expertise Areas'));
				
		     /*$this->form_validation->set_rules('alphabets_text_field', 'Text Field Five', 'required|alpha',
            array('required'=>'Please enter Text Field Five!','alpha'=>'Only alphabets please!'));
         */
          
               	    
			 if ($this->form_validation->run() == true){


	 	
			if($this->input->post()){

		    	  $data = array(
							'fullname'			=>$this->input->post('name'),
							'email'	  			=>$this->input->post('email'),
							'password'			=>$this->input->post('password'),
							'phone'			    =>$this->input->post('phone'),
							'city_id'			=>$this->input->post('city'),
							'speciality_id'		=>$this->input->post('speciality'),
							'clinic'			=>$this->input->post('clinic'),
							'licence_number'	=>$this->input->post('licence'),
							'expertise_area'	=>$this->input->post('expertise')
							
							
							);


			      $datainsert = $this->Doctor_model->insert('doctors',$data);

         
                             
                                         $str          = 'abEREWDDfdfsd54654654fdcdef';
                                         $token        = str_shuffle($str);
                                         $deviceToken  = str_shuffle($str);
                                     
                          $data= array(
                                       'user_id'     =>$datainsert,
                                       'token'       =>$token,
                                       'deviceType'  =>'website',
                                       'deviceToken' =>$deviceToken
                                     );

                          $datainserted = $this->Doctor_model->insert('dtokens',$data);

			      if($datainsert){
			      	$this->session->set_flashdata('success','Add Doctor successfully');
			      	redirect('doctor');
			      }else{
			      	$this->session->set_flashdata('error',' occur an error doctor not insertd');
			      	redirect('doctor/addDoctor');
			      }
				
		           } 
	            }

	    else{

		
		$data['citydata'] = $this->Doctor_model->getcity();
		$data['specialitydata'] = $this->Doctor_model->getDoctorspeciality();

        $this->load->view('add_doctor',$data);
		 }

	 }
 
	public function changeStatus(){
		$id = $this->uri->segment(3);
		//echo"<br>";
		$status = $this->uri->segment(4);
	$getemail =$this->Doctor_model->getdataforcolumn('doctors',array('email'),$id);
		//echo"<br>".print_r($email[0]->email);
          $email = $getemail[0]->email;
		if($status==1){
			$data = array(
						'admin_status'=>0);
		}else{
			$data = array(
						'admin_status'=>1);

			    $email2="info@mydoctoruae.com";
	           	$this->email->from($email2, 'Mydoctoruae UAE');
	            $this->email->to($email);
	            $this->email->subject('Welcome to Mydoctoruae');
	            $this->email->message('Your Account has been activated now!');
	            $send = $this->email->send();
	            
		}


               $this->db->where('id', $id);
			   $update =  $this->db->update('doctors', $data);
			   if($update){

			   $this->session->set_flashdata('success','doctor status Updated successfully');
			     redirect('doctor');

			   }else{
			   	$this->session->set_flashdata('error','doctor  status not  Updated an error occur');
			     redirect('doctor');

			   }

	}


	public function doctorDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
        $del = $this->db->delete('doctors'); 
		if($del){

	     $this->session->set_flashdata('success','doctor deleted successfully');
	     redirect('doctor');

		}else{
  		$this->session->set_flashdata('error','doctor not  delete an error occur');
    	 redirect('doctor');

   }
		
	} 
	

}



?>
