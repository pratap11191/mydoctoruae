<?php
class Booking extends CI_Controller{

public function __construct(){
	
	parent::__construct();
	$this->load->model('Doctor_model');
	if(empty($this->session->userdata['logged_in']['email'])) {
	redirect('Admin');
		}

	}

	 public function index(){
	 	$data['bookingdata'] = $this->Doctor_model->getBooking('bookings',array('bookings.id','bookings.firstname','bookings.created_at','bookings.lastname','bookings.email','bookings.phone','doctors.fullname as doctorname'));

	 	
	$this->load->view('booking_list',$data);
	
		}

public function searchdata(){
  $value = $this->uri->segment(3);
 if($value=='Previousweek'){
                          $enddate = date('Y-m-d'); 
                          $startdate=Date('Y-m-d', strtotime("-7 days"));

$data['bookingdata'] = $this->Doctor_model->getsearchBooking('bookings',array('bookings.id','bookings.firstname','bookings.created_at','bookings.lastname','bookings.email','bookings.phone','bookings.apdate','doctors.fullname as doctorname'),$startdate,$enddate);
}
if($value=='Nextweek'){

$startdate = date('Y-m-d'); 
$enddate=Date('Y-m-d', strtotime("+7 days"));
$data['bookingdata'] = $this->Doctor_model->getsearchBooking('bookings',array('bookings.id','bookings.firstname','bookings.created_at','bookings.lastname','bookings.email','bookings.phone','bookings.apdate','doctors.fullname as doctorname'),$startdate,$enddate);

}
//echo $this->db->last_query();
//exit;			 	
 $this->load->view('searchbooking',$data);

}
		
 public function searchmonthdata(){

 $month = $this->uri->segment(3);
 $startdate = date('Y-0'.$month.'-01');
 $enddate = date('Y-0'.$month.'-31');

$data['bookingdata'] = $this->Doctor_model->getsearchBooking('bookings',array('bookings.id','bookings.firstname','bookings.created_at','bookings.lastname','bookings.email','bookings.phone','doctors.fullname as doctorname'),$startdate,$enddate);

//echo $this->db->last_query();
//exit;
 $this->load->view('searchbooking',$data);
}

public function searchyeardata(){

 $year = $this->uri->segment(3);
 $startdate = date($year.'-01-01');
 $enddate = date($year.'-12-31');

$data['bookingdata'] = $this->Doctor_model->getsearchBooking('bookings',array('bookings.id','bookings.firstname','bookings.created_at','bookings.lastname','bookings.email','bookings.phone','doctors.fullname as doctorname'),$startdate,$enddate);

 $this->load->view('searchbooking',$data);
}



 public function doctordetails(){

	 	$data['bookingdata'] = $this->Doctor_model->getBooking('bookings',array('bookings.id','bookings.firstname','bookings.lastname','bookings.email','bookings.phone','doctors.fullname as doctorname'));

	 	
	
	
	    echo json_encode($data);
		//$this->load->view('userlist',$data);
	
		}

	public function bookingDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
        $del = $this->db->delete('bookings'); 
		if($del){

     $this->session->set_flashdata('success','Booking deleted successfully');
     redirect('booking');

		}else{
  $this->session->set_flashdata('error','Booking not  delete an error occur');
     redirect('booking');

   }
		
	} 

	
     



}



?>
