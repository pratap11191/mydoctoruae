<?php
class Review extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
			$this->load->model('Doctor_model');
			$this->load->model('Common_model');
			$this->load->library('email');
			if(empty($this->session->userdata['logged_in']['email'])) {
			redirect('Admin');
				}

			}

		public function index(){
			 	
			/* $data['doctordata'] = $this->Doctor_model->getdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'));*/

			 $data['reviewdata'] = $this->Common_model->getData('doctorratings');
			 $this->load->view('review_list',$data);
			
		}



 
	public function changeStatus(){
		$id = $this->uri->segment(3);
		//echo"<br>";
		$status = $this->uri->segment(4);
	
		if($status==1){
			$data = array(
						'status'=>'0');
		}else{
			$data = array(
						'status'=>'1');

			    
	            
		}


               $this->db->where('id', $id);
			   $update =  $this->db->update('doctorratings', $data);
			   if($update){

			   $this->session->set_flashdata('success','Review status Updated successfully');
			     redirect('review');

			   }else{
			   	$this->session->set_flashdata('error','Review  status not  Updated an error occur');
			     redirect('review');

			   }

	}


	public function reviewDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
        $del = $this->db->delete('doctorratings'); 
		if($del){

	     $this->session->set_flashdata('success','Review deleted successfully');
	     redirect('review');

		}else{
  		$this->session->set_flashdata('error','Review not  delete an error occur');
    	 redirect('review');

   }
		
	} 
	

}



?>
