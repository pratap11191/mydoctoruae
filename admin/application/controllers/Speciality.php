<?php
class Speciality extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
			$this->load->model('Common_model');
			//$this->load->library('upload');
			if(empty($this->session->userdata['logged_in']['email'])) {
			redirect('Admin');
				}

			}

		public function index(){
			 	
			 	$data['specialitydata'] = $this->Common_model->getspeciality('specialties');
			 	
		   
			$this->load->view('specialitylist',$data);
			
		}

		public function specialitydetails(){
		 	$data['specialitydata'] = $this->Common_model->getspeciality('specialties'); 	
	    	echo json_encode($data);
			//$this->load->view('userlist',$data);
	
		}

		public function addSpeciality(){

			//$data['citydata'] = $this->Common_model->getcity();
			//$data['specialitydata'] = $this->Common_model->getDoctorspeciality();

			/*echo"<pre>";
			print_r($data['specialitydata']);
			exit;*/

			$this->load->view('add_speciality');

		  }

	  	public function saveSpeciality(){
	  	

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','name','trim|required',array('required'=>'Please enter Name of Speciality'));
	//	$this->form_validation->set_rules('image','image','trim|required',array('required'=>'Please select a file'));
		
		$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/public/assets/images/categrese/';
		$config['allowed_types'] = '*';
		//$config['max_size']     = '100';
		//$config['max_width'] = '1024';
		//$config['max_height'] = '768';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		 
          
               	    
	 if ($this->form_validation->run() == true && $this->upload->do_upload('image')){


	 	
		if($this->input->post()){

		   $filename = $this->upload->data('file_name');
	       $data = array(
						'name'				=>$this->input->post('name'),
						'img'	  			=>'/public/assets/images/categrese/'.$filename,
						'remark'			=>1,
						'created_at'		=>date('d-m-y h:i:s')
						);
						
					

			$datainsert = $this->Common_model->insertRecord('specialties',$data);
			if($datainsert){
			      	$this->session->set_flashdata('success','Add Specialties successfully');
			      	redirect('speciality');
			      }else{
			      	$this->session->set_flashdata('error',' occur an error Specialties not insertd');
			      	redirect('speciality/addSpeciality');
			      }
				
		           } 
	            }

	    else{

		$error = $this->upload->display_errors();
		$this->session->set_flashdata('error',$error);
        $this->load->view('add_speciality');
        //redirect('speciality/addSpeciality');
		 }

	}

		

	public function specialityDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
        $del = $this->db->delete('specialties'); 
		if($del){

     $this->session->set_flashdata('success','speciality deleted successfully');
     redirect('speciality');

		}else{
  $this->session->set_flashdata('error','speciality not  delete an error occur');
     redirect('speciality');

   }
		
	} 
	
     



}



?>