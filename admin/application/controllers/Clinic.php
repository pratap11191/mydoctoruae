<?php
class Clinic extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
			$this->load->model('Doctor_model');
			$this->load->model('Common_model');
			$this->load->library('email');
			if(empty($this->session->userdata['logged_in']['email'])) {
			redirect('Admin');
				}

			}

		public function index(){
			 	
			 /*$data['doctordata'] = $this->Doctor_model->getdoctor('doctors',array('doctors.id','doctors.fullname','doctors.created_at','doctors.phone','doctors.image','doctors.email','doctors.admin_status','cities.name as cityname','specialties.name as speciality'));*/
			 $data['clinicdata'] = $this->Common_model->getData('clinics');
			 $this->load->view('cliniclist',$data);
			
		}


 
		public function changeStatus(){
			$id     = $this->uri->segment(3);
			$status = $this->uri->segment(4);
		    $getemail =$this->Doctor_model->getdataforcolumn('clinics',array('email'),$id);
			//echo"<br>".print_r($email[0]->email);
	        $email = $getemail[0]->email;
	       
			if($status==1){
				$data = array(
							'admin_status'=>0);
			}else{
				$data = array(
							'admin_status'=>1);

			    $email2="info@mydoctoruae.com";
	           	$this->email->from($email2, 'Mydoctor UAE');
	            $this->email->to($email);
	            $this->email->subject('Welcome to Mydoctoruae');
	            $this->email->message('Your Account has been activated now!');
	            $send = $this->email->send();
	           /* if($send){
	            	echo"send email successfully";
	            	exit;
	            }*/
	            
		}


               $this->db->where('id', $id);
			   $update =  $this->db->update('clinics', $data);
			   if($update){

			   $this->session->set_flashdata('success','Clinics status Updated successfully');
			     redirect('clinic');

			   }else{
			   	$this->session->set_flashdata('error','Clinics status not  Updated an error occur');
			     redirect('clinic');

			   }

	}


	public function clinicDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
        $del = $this->db->delete('clinics'); 
		if($del){

	     $this->session->set_flashdata('success','Clinics deleted successfully');
	     redirect('clinic');

		}else{
  		$this->session->set_flashdata('error','Clinics not  delete an error occur');
    	 redirect('clinic');

   }
		
	} 
	

}



?>
