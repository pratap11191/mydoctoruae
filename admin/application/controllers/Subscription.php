<?php
class Subscription extends CI_Controller{

		public function __construct(){
			
			parent::__construct();
			$this->load->model('Common_model');
			//$this->load->library('upload');
			if(empty($this->session->userdata['logged_in']['email'])) {
			redirect('Admin');
				}

			}

		public function index(){

			
			 	
			 	$data['subscriptiondata'] = $this->Common_model->getData('subscriptions');
			 	
				$this->load->view('subscription',$data);
			
		}

		

		public function addSubscription(){

		
			$this->load->view('add_subscription');

		  }

	  	public function saveSubscription(){
	  	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title','title','trim|required',array('required'=>'Please enter Title'));
		$this->form_validation->set_rules('description','description','trim|required',array('required'=>'Please enter Description'));
		$this->form_validation->set_rules('price','price','trim|required',array('required'=>'Please enter Price'));
		//$this->form_validation->set_rules('image','image','trim|required',array('required'=>'Please choose an  image'));
		$this->form_validation->set_rules('distance','distance','trim|required',array('required'=>'Please enter distance'));
		$this->form_validation->set_rules('validity','validity','trim|required',array('required'=>'Please enter Validity'));
		
		
            
	      // $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/healthapp/public/subscription/';
	      $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/public/subscription/';
		$config['allowed_types'] = '*';
		$this->load->library('upload', $config);
		$this->upload->initialize($config); 
		
		 
          
               	    
	 if ($this->form_validation->run() == true && $this->upload->do_upload('image')){


	 	
		if($this->input->post()){

		   $filename = $this->upload->data('file_name');
		   

	      

	       $data = array(
						'title'		        =>$this->input->post('title'),
						'description'		=>$this->input->post('description'),
						'image'	  		    =>'public/subscription/'.$filename,
						'price'		        =>$this->input->post('price'),
						'distance'		    =>$this->input->post('distance'),
						'validity'		    =>$this->input->post('validity'),
						'created_at'		=>date('d-m-y h:i:s')
						);
						
						

			$datainsert = $this->Common_model->insertRecord('subscriptions',$data);
			if($datainsert){
			      	$this->session->set_flashdata('success','Add subscription successfully');
			      	redirect('subscription ');
			      }else{
			      	$this->session->set_flashdata('error',' occur an error subscription not insertd');
			      	redirect('subscription/addSubscription');
			      }
				
		           } 
	            }

	    else{

		$error = $this->upload->display_errors();
		$this->session->set_flashdata('error',$error);
        $this->load->view('add_subscription');
        //redirect('speciality/addSpeciality');
		 }

	}

 public function subscriptionEdit(){
 	$id = $this->uri->segment(3);
 	$data['subscriptiondata'] = $this->Common_model->getData('subscriptions',array('id'=>$id));
 	$this->load->view('edit_subscription',$data);

    

 }
	public function updateSubscription(){
      
       
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title','title','trim|required',array('required'=>'Please enter Title'));
		$this->form_validation->set_rules('description','description','trim|required',array('required'=>'Please enter Description'));
		$this->form_validation->set_rules('price','price','trim|required',array('required'=>'Please enter Price'));
		//$this->form_validation->set_rules('image','image','trim|required',array('required'=>'Please choose an  image'));
		$this->form_validation->set_rules('distance','distance','trim|required',array('required'=>'Please enter distance'));
		$this->form_validation->set_rules('validity','validity','trim|required',array('required'=>'Please enter Validity'));




	 if ($this->form_validation->run() == true){

		if($this->input->post()){

		      $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/public/subscription/';
	          $config['allowed_types']        = 'gif|jpeg|png|jpg';
	          //$config['max_size']             = 100;
	          // $config['max_width']            = 1024;
	          //$config['max_height']           = 768;

	          $this->load->library('upload', $config);
	          $this->upload->initialize($config);
	         if($this->upload->do_upload('image')){


	         	 $filename = $this->upload->data('file_name'); 


	             $data = array(
						'title'		        =>$this->input->post('title'),
						'description'		=>$this->input->post('description'),
						'image'	  		    =>'public/subscription/'.$filename,
						'price'		        =>$this->input->post('price'),
						'distance'		    =>$this->input->post('distance'),
						'validity'		    =>$this->input->post('validity'),
						'created_at'		=>date('d-m-y h:i:s')
						);
	        

	          }else{
				 $data = array(
										'title'		        =>$this->input->post('title'),
										'description'		=>$this->input->post('description'),
										'price'		        =>$this->input->post('price'),
										'distance'		    =>$this->input->post('distance'),
										'validity'		    =>$this->input->post('validity'),
										'created_at'		=>date('d-m-y h:i:s')
										);


	                  }
 /*$darta = $this->upload->data();
                       print_r($darta);
                       exit;*/

	      $idforupdate = $this->input->post('hidden');
	      $this->db->where('id', $idforupdate);
            $updated =  $this->db->update('subscriptions', $data);


			if($updated){
			      	$this->session->set_flashdata('success','Update subscription successfully');
			      	redirect('subscription');
			      }else{
			      	$this->session->set_flashdata('error',' occur an error subscription not insertd');
			      	redirect('subscription/EditSubscription');
			      }
				
		           } 
	            }

	}	

	public function subscriptionDelete(){
		$id = $this->uri->segment(3);
		
		
		$this->db->where('id', $id);
                $del = $this->db->delete('subscriptions'); 
		if($del){

     $this->session->set_flashdata('success','Subscription deleted successfully');
     redirect('subscription');

		}else{
  $this->session->set_flashdata('error','Subscription not  delete an error occur');
     redirect('subscription');

   }
		
	} 
	
     



}



?>