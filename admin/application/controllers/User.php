<?php
class User extends CI_Controller{

public function __construct(){
	
	parent::__construct();
	$this->load->model('Doctor_model');
	if(empty($this->session->userdata['logged_in']['email'])) {
	redirect('Admin');
		}
		
	}

	 public function index(){
                   

	 	$data['userdata'] = $this->Doctor_model->getdata('users',array('users.id','users.fullname','users.created_at','users.phone','users.image','users.email','users.admin_status','cities.name as cityname'),array('users.is_admin'=>'0'));

	
	 $this->load->view('userlist',$data);
	
		}

               public function searchdata(){
                     $value = $this->uri->segment(3);
                         if($value=='Previousweek'){
                          $enddate = date('Y:m:d'); 
                          //$startdate=Date('Y:m:d', strtotime("-7 days"));
                          $startdate=Date('Y:m:d', strtotime("-1 week"));
                         $data['userdata'] = $this->Doctor_model->
getdatafordate('users',array('users.id','users.fullname','users.created_at','users.phone','users.image','users.email','users.admin_status','cities.name as cityname'),array('users.is_admin'=>'0'),$startdate,$enddate);

}
if($value=='Nextweek'){

$startdate = date('Y:m:d'); 
//$enddate=Date('Y:m:d', strtotime("+7 days"));
$enddate=Date('Y:m:d', strtotime("+1 week"));


$data['userdata'] = $this->Doctor_model->
getdatafordate('users',array('users.id','users.fullname','users.created_at','users.phone','users.image','users.email','users.admin_status','cities.name as cityname'),array('users.is_admin'=>'0'),$startdate,$enddate);


}
$this->load->view('searchuser',$data);
    }

public function searchmonthdata(){

 $month = $this->uri->segment(3);
 $startdate = date('Y:'.$month.':01');
 $enddate = date('Y:'.$month.':31');

 $data['userdata'] = $this->Doctor_model->
 getdatafordate('users',array('users.id','users.fullname','users.created_at','users.phone','users.image','users.email','users. admin_status','cities.name as cityname'),array('users.is_admin'=>'0'),$startdate,$enddate);
 
 $this->load->view('searchuser',$data);
}

public function searchyeardata(){

 $year = $this->uri->segment(3);
 $startdate = date($year.':01:01');
 $enddate = date($year.':12:31');
//exit;
 $data['userdata'] = $this->Doctor_model->
 getdatafordate('users',array('users.id','users.fullname','users.created_at','users.phone','users.image','users.email','users. admin_status','cities.name as cityname'),array('users.is_admin'=>'0'),$startdate,$enddate);
 
 $this->load->view('searchuser',$data);
}


			public function data(){
				 	$data = $this->Doctor_model->getdata('users',array('id','fullname','phone','image','email','city_id','admin_status'),array('is_admin'=>'0'));

			    $temparr = array();

				 	foreach($data as $val){
				 		$temparr['id'] = $val->id;
				 		$temparr['fullname'] = $val->fullname;
				 		$temparr['image'] = $val->image;
				 		$temparr['email'] = $val->email;

				 	    $cityid = $val->city_id;
				 	    $data = $this->Doctor_model->fetchConditionaldata('cities',array('name'),array('id'=>$cityid));

				 	    echo"<pre>";
				 	    print_r($data);
				 	    exit;

				 		$temparr['admin_status'] = $val->admin_status;



				 	}
				//$this->load->view('userlist',$data);
				
					}




		 public function userdetails(){
	 	 $data['userdata'] = $this->Doctor_model->getdata('users',array('users.id','users.fullname','users.phone','users.image','users.email','users.admin_status','cities.name as cityname'),array('users.is_admin'=>'0'));

	 	/*echo $this->db->last_query();
	 	exit;*/
	 	/*echo"<pre>";
	 	print_r($data['userdata']);
	 	exit;*/

	    echo json_encode($data);
		//$this->load->view('userlist',$data);
		
			}

		public function addUser(){

			/*echo"jiiii";
			exit;*/
			$this->load->view('add_user');

		}
	
		public function userDelete(){
			$id = $this->uri->segment(3);
			
			
			$this->db->where('id', $id);
	        $del = $this->db->delete('users'); 
			if($del){

	     $this->session->set_flashdata('success','User deleted successfully');
	     redirect('user');

			}else{
	  $this->session->set_flashdata('error','User not  delete an error occur');
	     redirect('user');

	  	 }
		
		} 

			public function changeStatus(){
				$id = $this->uri->segment(3);
				//echo"<br>";
				$status = $this->uri->segment(4);
				if($status==1){
					$data = array(
								'admin_status'=>0);
				}else{
					$data = array(
								'admin_status'=>1);
				}


		              $this->db->where('id', $id);
		   $update =  $this->db->update('users', $data);
		   if($update){

		   $this->session->set_flashdata('success','User status Updated successfully');
		     redirect('user');

		   }else{
		   	$this->session->set_flashdata('error','User  status not  Updated an error occur');
		     redirect('user');

		   }

		}



  
	
   public function url(){
    
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'];
}  

public function logoutAdmin(){

  $this->session->unset_userdata('logged_in');
  redirect('Admin');

}

}



?>