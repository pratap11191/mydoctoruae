<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*== title data ==*/
define('TITLE', 'Mission Co-ordination');
/*== table name ==*/
define('USERS', 'coordination_user');
define('STATES', 'coordination_state');
define('SMARTCITY', 'coordination_smc_city');
define('YEARS', 'coordination_year');

define('SMARTCITYFORM1', 'coordination_smc_form1');
define('SMARTCITYFORM1ADDROW', 'coordination_smc_form1_add_row');
define('SMARTCITYFORM1ADDCITY', 'coordination_smc_form1_add_city');
define('SMARTCITYFORM2', 'coordination_smc_form2');
define('SMARTCITYFORM2ADDROW', 'coordination_smc_form2_add_row');
define('SMARTCITYFORM3', 'coordination_smc_form3');
define('SMARTCITYFORM3ADDROW', 'coordination_smc_form3_add_row');
define('SMARTCITYFORM3ADDCITY', 'coordination_smc_form3_add_city');
define('SMARTCITYFORM4', 'coordination_smc_form4');
define('SMARTCITYFORM4ADDROW', 'coordination_smc_form4_add_row');
define('SMARTCITYFORM1FRGOI', 'coordination_smc_form1_fund_rel_goi');
define('SMARTCITYFORM1FRGOS', 'coordination_smc_form1_fund_rel_gos');
define('SMARTCITYFORM1PP', 'coordination_smc_form1_project_progress');


define('AMRUTFORM1', 'coordination_amr_form1');
define('AMRUTFORM1ADDROW', 'coordination_amr_form1_add_row');
define('AMRUTFORM2', 'coordination_amr_form2');
define('AMRUTFORM2ADDROW', 'coordination_amr_form2_add_row');
define('AMRUTFORM3', 'coordination_amr_form3');
define('AMRUTFORM3ADDROW', 'coordination_amr_form3_add_row');
define('AMRUTFORM4', 'coordination_amr_form4');
define('AMRUTFORM4ADDROW', 'coordination_amr_form4_add_row');


define('SBMFORM1', 'coordination_sbm_form1');
define('SBMFORM1ADDROW', 'coordination_sbm_form1_add_row');
define('SBMFORM2', 'coordination_sbm_form2');
define('SBMFORM2ADDROW', 'coordination_sbm_form2_add_row');
define('SBMFORM3', 'coordination_sbm_form3');
define('SBMFORM3ADDROW', 'coordination_sbm_form3_add_row');
define('SBMFORM4', 'coordination_sbm_form4');
define('SBMFORM4ADDROW', 'coordination_sbm_form4_add_row');
define('SBMFORMSTATE', 'coordination_sbm_form_state');

define('PMAYFORM1', 'coordination_pmay_form1');
define('PMAYFORM1ADDROW', 'coordination_pmay_form1_add_row');
define('PMAYFORM2', 'coordination_pmay_form2');
define('PMAYFORM2ADDROW', 'coordination_pmay_form2_add_row');
define('PMAYFORM3', 'coordination_pmay_form3');
define('PMAYFORM3ADDROW', 'coordination_pmay_form3_add_row');
define('PMAYFORM4', 'coordination_pmay_form4');
define('PMAYFORM4ADDROW', 'coordination_pmay_form4_add_row');

define('NULMFORM1', 'coordination_nulm_form1');
define('NULMFORM1ADDROW', 'coordination_nulm_form1_add_row');

define('NULMFORM2', 'coordination_nulm_form2');
define('NULMFORM2ADDROW', 'coordination_nulm_form2_add_row');
define('NULMFORM3', 'coordination_nulm_form3');
define('NULMFORM3ADDROW', 'coordination_nulm_form3_add_row');
define('NULMFORM4', 'coordination_nulm_form4');
define('NULMFORM4ADDROW', 'coordination_nulm_form4_add_row');

define('HRIDAYCITIES', 'coordination_hriday_city');
define('HRIDAYFORM1', 'coordination_hriday_form1');
define('HRIDAYFORM1ADDROW', 'coordination_hriday_form1_add_row');
define('HRIDAYFORM4', 'coordination_hriday_form4');
define('HRIDAYFORM4ADDROW', 'coordination_hriday_form4_add_row');

define('URBANTRANSPORTCITY', 'coordination_ut_city');
define('URBANTRANSPORTFORM1', 'coordination_ut_form1');
define('URBANTRANSPORTFORM1ADDROW', 'coordination_ut_form1_add_row');



/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
