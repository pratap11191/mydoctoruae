<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			
                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>


            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Post-By</th>
                                <th>Created Date</th> 
                                <th>User-Type</th>
				                         <th>Action</th>
                            

                            </tr>
                        </thead>
                        <tbody>

                        	<?php
                             $x=1;
                              foreach($postdata as $post){ 
                            ?>
                            <tr>
                                <td><?php echo $x; ?></td>
                                <td><?php echo $post->title?></td>
                               



                                <td>

                                	<?php if($post->post_image){?>
 <img  height="50" width="50" src="<?php
   
			echo "https://".$_SERVER['SERVER_NAME'].'/public/'.$post->post_image;    ?>">
<?php }else{ ?>

<img  height="50" width="50" src="<?php 
			echo "https://".$_SERVER['SERVER_NAME'].'/public/userimage/'.'123.png';
      ?>">


<?php }?>
                                </td>

                                 <td><?php echo $post->description?></td>

                          
                                <td><?php echo $post->fullname?></td>
                                <td><?php echo date('d-m-Y',strtotime($post->created_at));?></td>
                                <td><?php echo $post->usertype;?></td>
                                
<td ><a href="<?php echo base_url();?>post/postDelete/<?php echo $post->post_id; ?>" onclick="return confirm('Are you sure  Delete this Post?')" ><p title="Delete Post"><button class="btn btn-danger">Delete</button></p></a></td>
                                
                                

                            </tr>
                        <?php  $x++; } 
                       ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
