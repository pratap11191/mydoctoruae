<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			
                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>

<div class="col-md-3" style="padding-left:0;">
   
    Select Week:
   <select name="week" onchange="weekFunction(this.value);">
   <option value="">Select Week</option>
   <option value="Previousweek">Previous Week</option>
   <option value="Nextweek">Next Week</option>
   </select>
  </div>
<div class="col-md-3">
   
    Select Month:
   <select name="month" onchange="monthFunction(this.value);">
   <option value="">Select Month</option>
   <option value="1">January </option>
   <option value="2">February</option>
   <option value="3">March</option>
   <option value="4">April</option>
   <option value="5">May</option>
   <option value="6">June</option>
   <option value="7">July</option>
   <option value="8">August</option>
   <option value="9">September</option>
   <option value="10">October</option>
   <option value="11">November</option>
   <option value="12">December</option>
   </select>
  </div>
<div class="col-md-3" style="margin-bottom: 18px;">
   
    Select Year:
   <select name="Year" onchange="yearFunction(this.value);">
   <option value="">Select Year</option>
   <option value="2018">2018</option>
   <option value="2017">2017</option>
   <option value="2016">2016</option>
   <option value="2015">2015</option>
   </select>
</div>

            
<table id="myTable" class="display" style="width:100%;">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                                <th>Email</th>
				<th>Booking Date</th>
                                <th>Phone</th>
                                <th>Booking By</th>
				<th>Action</th>
				
                            

                            </tr>
                        </thead>
                        <tbody>

                        	<?php $x=1;
                               foreach($bookingdata as $book){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $book->firstname.'-'.$book->lastname?></td>
                                <td><?php echo $book->email?></td>
				<td><?php echo date("d-m-Y h:i:sa", strtotime($book->created_at));
					
						?></td>
                                <td><?php echo $book->phone?></td>
                                <td><?php echo $book->doctorname?></td>
            <td ><a href="<?php echo base_url();?>booking/bookingDelete/<?php echo $book->id; ?>" onclick="return confirm('Are you sure  Delete this Booking?')" ><p title="Delete Booking"><button class="btn btn-danger">Delete</button></p></a></td>
                                
                   

                            </tr>
                        <?php  $x++;} ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>




<script>

function weekFunction(value)
{
   // alert(value);
var dynamic_url = "<?php echo base_url()?>booking/searchdata/"+value;
window.location.href = dynamic_url;
}
</script>

<script>
function monthFunction(value)
{
    //alert(value);
var dynamic_url = "<?php echo base_url()?>booking/searchmonthdata/"+value;
window.location.href = dynamic_url;
}


</script>



<script>
function yearFunction(value)
{
  // alert(value);
var dynamic_url = "<?php echo base_url()?>booking/searchyeardata/"+value;
window.location.href = dynamic_url;
}


</script>
