<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
  <!-- WRAPPER -->
  <div id="wrapper">
    <!-- NAVBAR -->
    <?php include('common/header.php');?>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <?php include('common/left_bar.php');?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
      <!-- MAIN CONTENT -->
      <div class="main-content">
    

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>


  


            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>

                                <th>Doctor Name</th>
                                <th>Email</th>
                               
                                <th>Review</th>
                                <th>Posted By</th>
                               <th>Posted Date</th>
                               
                               
                               <!--<th>Action </th>-->
                               <th>Action</th> 

                            </tr>
                        </thead>
                        <tbody>

                          <?php

                          if($reviewdata){
                           $x=1;
                           foreach($reviewdata as $review){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                 <?php 
                                $docid = $review->doctor_id;
                                $data  = $this->Common_model->getData('doctors',array('id'=>$docid));
                              

                                ?>
                               
                                <td><?php if($data){ echo $data[0]->fullname;} ?></td>
                               
                                <td><?php if($data){ echo $data[0]->email;}?></td>

                              
                                <td><?php echo $review->review; ?></td>
                                <?php 
                                $uid = $review->user_id;
                                $datause  = $this->Common_model->getData('users',array('id'=>$uid));
                              

                                ?>
                               
                                <td><?php if($datause){ echo $datause[0]->email;}?></td>
                                <td><?php echo date('d-m-Y',strtotime($review->created_at));?></td>
                                
                                
            <!--<td ><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>"><p title="Delete Doctor"><i class="fa fa-trash"></p></a></td>-->



                                 <?php 
                    if($review->status==1){ ?>

    <td><a href="<?php echo base_url();?>review/reviewDelete/<?php echo $review->id; ?>" onclick="return confirm('Are you sure  Delete this Review?')" ><p title="Delete Review"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>review/changeStatus/<?php echo $review->id?>/<?php echo $review->status ?>"><p title="Deactive Review"><button class="btn btn-success"> Active</button> </p></a></td>

                    <?php }else{
                     ?>

    <td><a href="<?php echo base_url();?>review/reviewDelete/<?php echo $review->id; ?>" onclick="return confirm('Are you sure  Delete this Review?')"  ><p title="Delete Review"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>review/changeStatus/<?php echo  $review->id?>/<?php echo $review->status?>"><p title="Active Review">  <button class="btn btn-success">Deactive </button> </p></a></td>

<?php } ?>


</td>


                                

                            </tr>
                        <?php $x++;
} } ?>
                        </tbody>
 
    </table>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <?php include('common/footer.php');?>
  </div>
  <!-- END WRAPPER -->
  <!-- Javascript -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>









