	<div id="sidebar-nav" class="sidebar">

			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">

 <?php $menuname =$this->uri->segment(1);
	?>
						
	
   
  <li><a href="<?php echo base_url('user');?>" class="<?php  if($menuname =='user')echo 'active'; ?>"><i class="fa fa-users"></i><span>User List</span></a></li>
						
  <li><a href="<?php echo base_url('doctor');?>"  class="<?php  if($menuname =='doctor')echo 'active'; ?>"><i class="fa fa-user-md" style="font-size:24px"></i> <span>Doctor List</span></a></li>

  <li><a href="<?php echo base_url('post');?>"  class="<?php  if($menuname =='post')echo 'active'; ?>"><i class="fa fa-envelope-o"></i> <span>Post List</span></a></li>

  <li><a href="<?php echo base_url('booking');?>"  class="<?php  if($menuname =='booking')echo 'active'; ?>"><i class="fa fa-address-book" style="font-size:24px"></i> <span>Booking List</span></a></li>

  <li><a href="<?php echo base_url('clinic');?>"  class="<?php  if($menuname =='clinic')echo 'active'; ?>"><i class="fa fa-address-book"></i> <span>Clinics</span></a></li>


  <li><a href="<?php echo base_url('speciality');?>"  class="<?php  if($menuname =='speciality')echo 'active'; ?>"><i class="lnr lnr-cog"></i> <span>Speciality List</span></a></li>

  <li><a href="<?php echo base_url('reason');?>"  class="<?php  if($menuname =='reason')echo 'active'; ?>"><i class="fa fa-exclamation"></i> <span>Cancellation Reasons Info</span></a></li>

<li><a href="<?php echo base_url('subscription');?>"  class="<?php  if($menuname =='subscription')echo 'active'; ?>"><i class="fa fa-users"></i> <span>Subscription</span></a></li>

<li><a href="<?php echo base_url('review');?>"  class="<?php  if($menuname =='review')echo 'active'; ?>"><i class="fa fa-address-book"></i> <span>Review</span></a></li>


	
</li>
						
						
					</ul>
				</nav>
			</div>
		</div>
