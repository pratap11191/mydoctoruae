<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">


	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 
	
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">

	<link rel="icon" type="image/png" sizes="96x96" href="assets/fav.png">

<?php if($this->uri->segment(1)=='user'){
		$pagetitle='User List';
    }else if($this->uri->segment(1)=='doctor'){
		$pagetitle='Doctor List';
    }else if($this->uri->segment(1)=='post'){
		$pagetitle='Post List';
    }else if($this->uri->segment(1)=='booking'){
		$pagetitle='Booking List';
    }else if($this->uri->segment(1)=='speciality'){
		$pagetitle='Specialty List';
    }else if($this->uri->segment(1)=='reason'){
		$pagetitle='Reason List';
    }else if($this->uri->segment(1)=='subscription'){
		$pagetitle='subscription List';
    }else if($this->uri->segment(1)=='review'){
		$pagetitle='Review List';
    }else if($this->uri->segment(1)=='clinic'){
		$pagetitle='Clinic List';
    }else{
$pagetitle='';
}?>
<title><?php echo $pagetitle;?> | My Doctor</title>
