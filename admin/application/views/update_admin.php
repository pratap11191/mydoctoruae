<!doctype html>
<html lang="en">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- VENDOR CSS -->
  <link rel="stylesheet" href="https://mobulous.app/healthapp_admin/assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://mobulous.app/healthapp_admin/assets/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://mobulous.app/healthapp_admin/assets/vendor/linearicons/style.css">


  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 
  
  <!-- MAIN CSS -->
  <link rel="stylesheet" href="https://mobulous.app/healthapp_admin/assets/css/main.css">
  <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  <link rel="stylesheet" href="https://mobulous.app/healthapp_admin/assets/css/demo.css
  ">
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  <!-- ICONS -->
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">

<body>
  <!-- WRAPPER -->
  <div id="wrapper">
    <!-- NAVBAR -->
    <?php include('common/header.php');?>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <?php include('common/left_bar.php');?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
      <!-- MAIN CONTENT -->
      <div class="main-content">

         <?php if($this->session->flashdata('error')){ ?>

<div class="alert alert-success">
  <strong>Error</strong> <?php echo $this->session->flashdata('error');?>
</div>
                <?php 

            }?>

<div class="container" style="width: 70%">
  
<form action="<?php echo base_url('admin/updateprofile');?>" method="post" enctype="multipart/form-data">

  <input type="hidden" name="hidden" value="<?php echo $admindata[0]->id;?>">
  <fieldset>
    <legend>Admin Details</legend>
   
 <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" class="form-control" name="name"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter name" value="<?php echo $admindata[0]->fullname;?>" required>
     
    </div>


    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email"  name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo $admindata[0]->email;?>" required>
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>

   

    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php echo $admindata[0]->confirmpassword;?>" required>
    </div>



<div>

  <img src="<?php echo base_url()?>assets/<?php echo $admindata[0]->image ?>" alt="Smiley face" height="42" width="42">
</div>

<div class="form-group">
      <label for="exampleInputPassword1">Image</label>
      <input type="file" name="image" class="form-control" id="exampleInputPassword1" placeholder="Password" value="">
    </div>

    



    <button type="submit" class="btn btn-primary">UPDATE</button>
  </fieldset>
</form>

</div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <?php include('common/footer.php');?>
  </div>
  <!-- END WRAPPER -->
  <!-- Javascript -->
  <script src="https://mobulous.app/healthapp_admin/assets/vendor/jquery/jquery.min.js"></script>
  <script src="https://mobulous.app/healthapp_admin/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="https://mobulous.app/healthapp_admin/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="https://mobulous.app/healthapp_admin/assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
