<style type="text/css">
	 .brand .left {
    float: left;
    padding: 30px 39px;
    background-color: #1c998c;
}

.navbar-nav > li > a > span {
    position: relative;
    top: 2px;
    color: white;
}


.auth-box .left {
    float: left;
    width: 42%;
    height: 100%;
    padding: 0 30px;
    text-align: center;
    background-color: #1c998c;
}

a {
    color: white;
}
</style>

<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login | My Doctor</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/fav.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">

<!-- 
								<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div> -->

<div class="brand" bg-color:red>


				<a href="#"><img src="<?php
   
			echo "https://".$_SERVER['SERVER_NAME'].'/public/assets/images/white-logo.png';
     ?>" alt="Klorofil Logo" class="img-responsive logo" style="max-width: 50%;"></a>


			</div>



								<p class="lead">Login to your account</p>
							</div>

							<?php 
							if($this->session->flashdata('error')){ ?>
								<div class="alert alert-danger">
									<?php echo $this->session->flashdata('error');?>
								</div>
							<?php }

						if($this->session->flashdata('success')){ ?> ?>

								<div class="alert alert-success">
									<?php echo $this->session->flashdata('success');?>
								</div>


							<?php } ?>

							
							<form class="form-auth-small" action="<?php echo base_url('Admin/loginAdmin');?>" method="post">
								
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input type="text" name="name" class="form-control" id="signin-email" value="" placeholder="Admin" required>
								</div>

								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" name="password" class="form-control" id="signin-password" value="" placeholder="Password" required>
								</div>

								<!-- <div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox">
										<span>Remember me</span>
									</label>
								</div> -->
								<button type="submit" class="btn btn-primary btn-lg btn-block" name="submit">LOGIN</button>
								<div class="bottom">
									<span class="helper-text" ><i class="fa fa-lock"></i> <a href="<?php echo base_url('admin/forgetpassword');?>" >Forgot password?</a></span>
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<!-- <div class="content text">
							<h1 class="heading">Free Bootstrap dashboard template</h1>
							<p>by The Develovers</p>
						</div> -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
