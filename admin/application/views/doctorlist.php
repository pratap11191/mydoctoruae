<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			<a href="<?php echo base_url('doctor/addDoctor');?>"><input type="submit"  name="submit" value="Add Doctor" class="btn btn-primary maindtg">
			</a>

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>


<div class="col-md-3" style="padding-left:0;">
   
    Select Week:
   <select name="week" onchange="weekFunction(this.value);">
   <option value="">Select Week</option>
   <option value="Previousweek">Previous Week</option>
   <option value="Nextweek">Next Week</option>
   </select>
  </div>
  
<div class="col-md-3">
   
    Select Month:
   <select name="month" onchange="monthFunction(this.value);">
   <option value="">Select Month</option>
   <option value="1">January </option>
   <option value="2">February</option>
   <option value="3">March</option>
   <option value="4">April</option>
   <option value="5">May</option>
   <option value="6">June</option>
   <option value="7">July</option>
   <option value="8">August</option>
   <option value="9">September</option>
   <option value="10">October</option>
   <option value="11">November</option>
   <option value="12">December</option>
   </select>
  </div>
  <div class="col-md-3">
   
    Select Year:
   <select name="Year" onchange="yearFunction(this.value);">
   <option value="">Select Year</option>
   <option value="2018">2018</option>
   <option value="2017">2017</option>
   <option value="2016">2016</option>
   <option value="2015">2015</option>
   </select>
  </div>

            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Image</th>
                                <th>Email</th>
                                <th>City</th>
                               <th>Created Date</th>
                               <th>Speciality</th> 
                               
                               <!--<th>Action </th>-->
                               <th>Status</th> 

                            </tr>
                        </thead>
                        <tbody>

                        	<?php

                          if($doctordata){
                           $x=1;
foreach($doctordata as $doctor){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $doctor->fullname?></td>
                                <td><?php echo $doctor->phone?></td>



                                <td>

                                	<?php if($doctor->image){?>
 <img  height="50" width="50" src="<?php 
			echo "https://".$_SERVER['SERVER_NAME'].'/public/'.$doctor->image;    ?>">
<?php }else{ ?>

<img  height="50" width="50" src="<?php 
   
			echo "https://".$_SERVER['SERVER_NAME'].'/public/img/user_signup.png';  ?>">


<?php }?>
                                </td>





                                
                                <td><?php echo $doctor->email?></td>
                                <?php 
                                $cityid = $doctor->city_id;
                                $data = $this->Common_model->getData('cities',array('id'=>$cityid));
                              

                                ?>
                                <td><?php if($data){ echo $data[0]->name;}?></td>
                                <td><?php echo date('d-m-Y',strtotime($doctor->created_at));?></td>
                                <?php 
                                $specialityid = $doctor->speciality_id;
                                $datas = $this->Common_model->getData('specialties',array('id'=>$specialityid));
                              
                                ?>
                                <td><?php if($datas){ echo $datas[0]->name;}?></td>
                                
            <!--<td ><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>"><p title="Delete Doctor"><i class="fa fa-trash"></p></a></td>-->



                                 <?php 
                    if($doctor->admin_status==1){ ?>

    <td><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>" onclick="return confirm('Are you sure  Delete this Doctor?')" ><p title="Delete Doctor"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>doctor/changeStatus/<?php echo $doctor->id?>/<?php echo $doctor->admin_status ?>"><p title="Deactive Doctor"><button class="btn btn-success"> Active</button> </p></a></td>

                    <?php }else{
                     ?>

    <td><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>" onclick="return confirm('Are you sure  Delete this Doctor?')"  ><p title="Delete Doctor"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>doctor/changeStatus/<?php echo  $doctor->id?>/<?php echo $doctor->admin_status?>"><p title="Active Doctor">  <button class="btn btn-success">Deactive </button> </p></a></td>

<?php } ?>
                                

                            </tr>
                        <?php $x++;
} } ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>



<script>

function weekFunction(value)
{
   // alert(value);
var dynamic_url = "<?php echo base_url()?>doctor/searchdata/"+value;
window.location.href = dynamic_url;
}
</script>

<script>
function monthFunction(value)
{
    //alert(value);
var dynamic_url = "<?php echo base_url()?>doctor/searchmonthdata/"+value;
window.location.href = dynamic_url;
}


</script>



<script>
function yearFunction(value)
{
  // alert(value);
var dynamic_url = "<?php echo base_url()?>doctor/searchyeardata/"+value;
window.location.href = dynamic_url;
}


</script>



