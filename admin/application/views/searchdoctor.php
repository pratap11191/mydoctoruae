<!doctype html>
<html lang="en">

<?php //include('common/head.php');?>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../assets/vendor/linearicons/style.css">


	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 
	
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="../../assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="../../assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">

	<link rel="icon" type="image/png" sizes="96x96" href="assets/fav.png">

<?php if($this->uri->segment(1)=='user'){
		$pagetitle='User List';
    }else if($this->uri->segment(1)=='doctor'){
		$pagetitle='Doctor List';
    }else if($this->uri->segment(1)=='post'){
		$pagetitle='Post List';
    }else if($this->uri->segment(1)=='booking'){
		$pagetitle='Booking List';
    }else if($this->uri->segment(1)=='speciality'){
		$pagetitle='Specialty List';
    }else if($this->uri->segment(1)=='reason'){
		$pagetitle='Reason List';
    }else{
$pagetitle='';
}?>
<title><?php echo $pagetitle;?> | My Doctor</title>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			<a href="<?php echo base_url('doctor/addDoctor');?>"><input type="submit"  name="submit" value="Add Doctor" class="btn btn-primary maindtg">
			</a>

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php }
$urlseg = $this->uri->segment(3);
?>


<div class="col-md-3" style="padding-left:0;">
   
    Select Week:
   <select name="week" onchange="weekFunction(this.value);">
   <option value="">Select Week</option>
   <option value="Previousweek" <?php if($urlseg=='Previousweek'){ echo ' selected="selected"'; }?> >Previous Week</option>
   <option value="Nextweek" <?php if($urlseg=='Nextweek'){ echo ' selected="selected"'; }?>>Next Week</option>
   </select>
  </div>

<div class="col-md-3">
   
    Select Month:
   <select name="month" onchange="monthFunction(this.value);">
   <option value="">Select Month</option>
   <option value="1" <?php if($urlseg=='1'){ echo ' selected="selected"'; }?> >January </option>
   <option value="2" <?php if($urlseg=='2'){ echo ' selected="selected"'; }?> >February</option>
   <option value="3" <?php if($urlseg=='3'){ echo ' selected="selected"'; }?> >March</option>
   <option value="4" <?php if($urlseg=='4'){ echo ' selected="selected"'; }?> >April</option>
   <option value="5" <?php if($urlseg=='5'){ echo ' selected="selected"'; }?> >May</option>
   <option value="6" <?php if($urlseg=='6'){ echo ' selected="selected"'; }?> >June</option>
   <option value="7" <?php if($urlseg=='7'){ echo ' selected="selected"'; }?> >July</option>
   <option value="8" <?php if($urlseg=='8'){ echo ' selected="selected"'; }?> >August</option>
   <option value="9" <?php if($urlseg=='9'){ echo ' selected="selected"'; }?> >September</option>
   <option value="10" <?php if($urlseg=='10'){ echo ' selected="selected"'; }?> >October</option>
   <option value="11" <?php if($urlseg=='11'){ echo ' selected="selected"'; }?> >November</option>
   <option value="12" <?php if($urlseg=='12'){ echo ' selected="selected"'; }?> >December</option>
   </select>
  </div>
  <div class="col-md-3">
    Select Year:
   <select name="Year" onchange="yearFunction(this.value);">
   <option value="">Select Year</option>
   <option value="2018" <?php if($urlseg=='2018'){ echo ' selected="selected"'; }?> >2018</option>
   <option value="2017" <?php if($urlseg=='2017'){ echo ' selected="selected"'; }?> >2017</option>
   <option value="2016" <?php if($urlseg=='2016'){ echo ' selected="selected"'; }?> >2016</option>
   <option value="2015" <?php if($urlseg=='2015'){ echo ' selected="selected"'; }?> >2015</option>
   </select>
  </div>

            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Image</th>
                                <th>Email</th>
                                <th>City</th>
                               <th>Created Date</th>
                               <th>Speciality</th> 
                               
                               <!--<th>Action </th>-->
                               <th>Status</th> 

                            </tr>
                        </thead>
                        <tbody>

                        	<?php
if($doctordata){

                           $x=1;
foreach($doctordata as $doctor){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $doctor->fullname?></td>
                                <td><?php echo $doctor->phone?></td>



                              <!--   <td>

                                	<?php if($doctor->image){?>
 <img  height="50" width="50" src="<?php if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
   
			echo $protocol."://".$_SERVER['SERVER_NAME'].'/public/'.$doctor->image;
 }      ?>">
<?php }else{ ?>

<img  height="50" width="50" src="<?php if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
   
			echo $protocol."://".$_SERVER['SERVER_NAME'].'/public/userimage/'.'123.png';
 }      ?>">


<?php }?>
                                </td> -->

                                
                                <td>

                                  <?php if($doctor->image){?>
 <img  height="50" width="50" src="<?php 
      echo "https://".$_SERVER['SERVER_NAME'].'/public/'.$doctor->image;    ?>">
<?php }else{ ?>

<img  height="50" width="50" src="<?php 
   
      echo "https://".$_SERVER['SERVER_NAME'].'/public/img/user_signup.png';  ?>">


<?php }?>
                                </td>





                                
                                <td><?php echo $doctor->email?></td>


                                <?php 
                                $cityid = $doctor->city_id;
                                $data = $this->Common_model->getData('cities',array('id'=>$cityid));
                              

                                ?>
                                <td><?php if($data){ echo $data[0]->name;}?></td>
                                <td><?php echo date('d-m-Y',strtotime($doctor->created_at));?></td>
                                 <?php 
                                $specialityid = $doctor->speciality_id;
                                $datas = $this->Common_model->getData('specialties',array('id'=>$specialityid));
                              
                                ?>
                                <td><?php if($datas){ echo $datas[0]->name;}?></td>
                                
            <!--<td ><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>"><p title="Delete Doctor"><i class="fa fa-trash"></p></a></td>-->



                                 <?php 
                    if($doctor->admin_status==1){ ?>

    <td><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>" onclick="return confirm('Are you sure  Delete this Doctor?')" ><p title="Delete Doctor"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>doctor/changeStatus/<?php echo $doctor->id?>/<?php echo $doctor->admin_status ?>"><p title="Deactive Doctor"><button class="btn btn-success"> Active</button> </p></a></td>

                    <?php }else{
                     ?>

    <td><a href="<?php echo base_url();?>doctor/doctorDelete/<?php echo $doctor->id; ?>" onclick="return confirm('Are you sure  Delete this Doctor?')"  ><p title="Delete Doctor"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>doctor/changeStatus/<?php echo  $doctor->id?>/<?php echo $doctor->admin_status?>"><p title="Active Doctor">  <button class="btn btn-success">Deactive </button> </p></a></td>

<?php } ?>
                                

                            </tr>
                        <?php $x++;
} } ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="../../assets/vendor/jquery/jquery.min.js"></script>
	<script src="../../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="../../assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../../assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>



<script>

function weekFunction(value)
{
   // alert(value);
var dynamic_url = "<?php echo base_url()?>doctor/searchdata/"+value;
window.location.href = dynamic_url;
}
</script>

<script>
function monthFunction(value)
{
    //alert(value);
var dynamic_url = "<?php echo base_url()?>doctor/searchmonthdata/"+value;
window.location.href = dynamic_url;
}


</script>



<script>
function yearFunction(value)
{
  // alert(value);
var dynamic_url = "<?php echo base_url()?>doctor/searchyeardata/"+value;
window.location.href = dynamic_url;
}


</script>



