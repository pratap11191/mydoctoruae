<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			<!-- <a href="<?php echo base_url('doctor/addDoctor');?>"><input type="submit"  name="submit" value="Add Doctor" class="btn btn-primary maindtg">
			</a> -->

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>



            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Image</th>
                                <th>Email</th>
                                <th>City</th>
                               <th>Created Date</th>
                               <th>Speciality</th> 
                               
                               <!--<th>Action </th>-->
                               <th>Status</th> 

                            </tr>
                        </thead>
                        <tbody>

                        	<?php

                          if($clinicdata){
                           $x=1;
foreach($clinicdata as $clinic){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $clinic->fullname?></td>
                                <td><?php echo $clinic->phone?></td>



                                <td>

                                	<?php if($clinic->image){?>
 <img  height="50" width="50" src="<?php 
			echo "https://".$_SERVER['SERVER_NAME'].'/clinicflow/public/assets/images/'.$clinic->image;    ?>">
<?php }else{ ?>

<img  height="50" width="50" src="<?php 
   
			echo "https://".$_SERVER['SERVER_NAME'].'/public/img/user_signup.png';  ?>">


<?php }?>
                                </td>





                                
                                <td><?php echo $clinic->email?></td>
                                <?php 
                                $cityid = $clinic->city_id;
                                $data = $this->Common_model->getData('cities',array('id'=>$cityid));
                              

                                ?>
                                <td><?php if($data){ echo $data[0]->name;}?></td>
                                <td><?php echo date('d-m-Y',strtotime($clinic->created_at));?></td>
                                <?php 
                                $specialityid = $clinic->speciality_id;
                                $datas = $this->Common_model->getData('specialties',array('id'=>$specialityid));
                              
                                ?>
                                <td><?php if($datas){ echo $datas[0]->name;}?></td>
                                



                                 <?php 
                    if($clinic->admin_status==1){ ?>

    <td><a href="<?php echo base_url();?>clinic/clinicDelete/<?php echo $clinic->id; ?>" onclick="return confirm('Are you sure  Delete this Clinic?')" ><p title="Delete Clinic"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>clinic/changeStatus/<?php echo $clinic->id?>/<?php echo $clinic->admin_status ?>"><p title="Deactive Doctor"><button class="btn btn-success"> Active</button> </p></a></td>

                    <?php }else{
                     ?>

    <td><a href="<?php echo base_url();?>clinic/clinicDelete/<?php echo $clinic->id; ?>" onclick="return confirm('Are you sure  Delete this Clinic?')"  ><p title="Delete Clinic"><button class="btn btn-danger">Delete</button></p></a><a href="<?php echo base_url();?>clinic/changeStatus/<?php echo  $clinic->id?>/<?php echo $clinic->admin_status?>"><p title="Active Clinic">  <button class="btn btn-success">Deactive </button> </p></a></td>

<?php } ?>


                                

                            </tr>
                        <?php $x++;
} } ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>





