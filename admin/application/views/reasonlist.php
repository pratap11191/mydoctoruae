<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			<a href="<?php echo base_url('reason/addReason');?>"><input type="submit"  name="submit" value="Add Reason" class="btn btn-primary maindtg">
			</a>

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>


            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                              <!--  <th>Created-At</th> -->
                               <th>Action </th> 
                               
                            </tr>
                        </thead>
                        <tbody>

                        	<?php
                            $x=1;
                            foreach($reasondata as $reason){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $reason->name?></td>
                            	<!-- <td><?php echo $reason->created_at?></td>   -->

                            <td ><a href="<?php echo base_url();?>reason/reasonDelete/<?php echo $reason->id; ?>" onclick="return confirm('Are you sure  Delete this Reason?')"><p title="Delete Reason"><button class="btn btn-danger">Delete</button></p></a></td>
                                

                            </tr>
                    <?php 
                     
                       $x++; }  ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
