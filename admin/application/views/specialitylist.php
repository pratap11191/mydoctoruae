<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			<a href="<?php echo base_url('speciality/addspeciality');?>"><input type="submit"  name="submit" value="Add Speciality" class="btn btn-primary maindtg">
			</a>

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>


            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                                <th>Image</th>
                               <!--  <th>Created-At</th> -->
                                <th>Remark</th>
                               <th>Action </th> 
                               
                            </tr>
                        </thead>
                        <tbody>

                        	<?php
                        	if($specialitydata){
                            $x=1;
                            foreach($specialitydata as $special){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $special->name?></td>
                               



                                <td>

                <?php 

                 if($special->img){


                // $name  = $special->img;
                 //$exp   = explode('/',$name);
                // array_pop($exp);
                // $imp = implode('/',$exp);
                // echo $imp;
                 //exit;

               //  if($imp=='/public/assets/images/categrese'){

                 
          


                                    ?>
 <img  height="50" width="50" src="<?php 
   
			echo "https://".$_SERVER['SERVER_NAME'].'/'.$special->img;    ?>">
<?php  } else{ ?>

<img  height="50" width="50" src="<?php
   
			echo "https://".$_SERVER['SERVER_NAME'].'/public/userimage/'.'123.png';     ?>">


<?php }?>
                                </td>
                                
                           <!--   -->
                            <?php if($special->remark==1){ 
                            $name ='Admin';
                                        }else{
                                        $name ='Doctor';
                                    }?>
                            <td><?php echo $name?></td>  

                            <td ><a href="<?php echo base_url();?>speciality/specialityDelete/<?php echo $special->id; ?>" onclick="return confirm('Are you sure  Delete this Speciality?')" ><p title="Delete Speciality"><button class="btn btn-danger">Delete</button></a></td>
                                

                            </tr>
                    <?php 
                     
                       $x++; }  } ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
