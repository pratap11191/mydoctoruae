<!doctype html>
<html lang="en">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- VENDOR CSS -->
  <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../assets/vendor/linearicons/style.css">


  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 
  
  <!-- MAIN CSS -->
  <link rel="stylesheet" href="../assets/css/main.css">
  <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  <link rel="stylesheet" href="../assets/css/demo.css">
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  <!-- ICONS -->
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
<link rel="icon" type="image/png" sizes="96x96" href="../assets/fav.png">
<?php if($this->uri->segment(2)=='addDoctor'){
		$pagetitle='Add Doctor';
    }else{
$pagetitle='';
}?>
<title><?php echo $pagetitle;?> | My Doctor </title>
<body>
  <!-- WRAPPER -->
  <div id="wrapper">
    <!-- NAVBAR -->
    <?php include('common/header.php');?>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <?php include('common/left_bar.php');?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
      <!-- MAIN CONTENT -->
      <div class="main-content">

         <?php if($this->session->flashdata('error')){ ?>

<div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error');?>
</div>
                <?php 

            }?>

<div class="container" style="width: 70%">
  
<form action="<?php echo base_url('doctor/saveDoctor');?>" method="post" enctype="multipart/form-data">
  <fieldset>
    <legend>Doctor Info</legend>
   
 <div class="form-group">
      <label for="exampleInputEmail1">Full Name</label>
      <input type="text" class="form-control" name="name"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter Full name" value="<?php echo set_value('name');?>" >
     
    </div>


    <?php if(form_error('name')){?>
   <span class="formerror" style="color: red;">
  <?php echo form_error('name');?>
</span>
    <?php } ?>

    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email"  name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo set_value('email');?>">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>

    
    <?php if(form_error('email')){?>
     <span class="formerror" style="color: red;">
  <strong></strong><?php echo form_error('email');?>
</span>
    <?php } ?>

    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php echo set_value('password');?>" >
    </div>

     <?php if(form_error('password')){?>
   <span class="formerror" style="color: red;">
  <strong></strong><?php echo form_error('password');?>
</span>
    <?php } ?>


    <div class="form-group">
      <label for="exampleInputEmail1">Phone Number</label>
      <input type="number" class="form-control" name="phone"  id="exampleInputphone" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="<?php echo set_value('phone');?>" maxlength="10" >
     
    </div>

    <?php if(form_error('phone')){?>
     <span class="formerror" style="color: red;">
  <strong></strong><?php echo form_error('phone');?>
</span>
    <?php } ?>

   

    <div class="form-group">
      <label for="exampleSelect1">Select City</label>
     
      <?php

       $cityarr = array();
       foreach($citydata as $city){
       $cityarr[$city->id] = $city->name;

       }


       $options = $cityarr;

      $shirts_on_sale = array('small', set_value('city'));
      echo form_dropdown('city', $options, set_value('city'),array('class'=>'form-control'));
                ?>
    </div>


    
  <?php if(form_error('city')){?>
   <span class="formerror" style="color: red;">
    <strong></strong><?php echo form_error('city');?>
  </span>
    <?php } ?>




    <div class="form-group">
      <label for="exampleSelect1">Select Speciality</label>
      
      <?php

 
      $specialityarr = array();
      foreach($specialitydata as $value){
          $specialityarr[$value->id] =$value->name;

       }

       $options = $specialityarr;
      

        $shirts_on_sale = array('small', set_value('speciality'));
        echo form_dropdown('speciality', $options, set_value('exarea'),array('class'=>'form-control'));
            ?>
    </div>
     


    <?php if(form_error('speciality')){?>
    <span class="formerror" style="color: red;">
    <strong></strong><?php echo form_error('speciality');?>
  </span>
    <?php } ?>


   
 <div class="form-group">
      <label for="exampleInputEmail1">Clinic</label>
      <input type="text" class="form-control" name="clinic"  id="exampleInputclinic" aria-describedby="emailHelp" placeholder="Enter clinic" value="<?php echo set_value('clinic');?>">
     
    </div>

     <?php if(form_error('clinic')){?>
    <span class="formerror" style="color: red;">
    <strong></strong><?php echo form_error('clinic');?>
  </span>
    <?php } ?>  


     <div class="form-group">
      <label for="exampleInputEmail1">License No.</label>
      <input type="text" class="form-control" name="licence"  id="exampleInputlicence" aria-describedby="emailHelp" placeholder="Enter licence No." value="<?php echo set_value('licence');?>">
     
    </div>

     <?php if(form_error('licence')){?>
     <span class="formerror" style="color: red;">
    <strong></strong><?php echo form_error('licence');?>
  </span>
    <?php } ?>  


     <div class="form-group">
      <label for="exampleInputEmail1">Expertise Area</label>
      <input type="text" class="form-control" name="expertise"  id="exampleInputlicence" aria-describedby="emailHelp" placeholder="Enter expertise Area" value="<?php echo set_value('expertise');?>">
     
    </div>

     <?php if(form_error('expertise')){?>
    <span class="formerror" style="color: red;">
    <strong></strong><?php echo form_error('expertise');?>
  </span>
    <?php } ?> 



   




  







    <button type="submit" class="btn btn-primary">Submit</button>
  </fieldset>
</form>

</div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <?php include('common/footer.php');?>
  </div>
  <!-- END WRAPPER -->
  <!-- Javascript -->
  <script src="../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="../assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
