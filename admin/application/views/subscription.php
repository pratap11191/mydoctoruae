<!doctype html>
<html lang="en">

<?php include('common/head.php');?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include('common/header.php');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include('common/left_bar.php');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
			<a href="<?php echo base_url('subscription/addSubscription');?>"><input type="submit"  name="submit" value="Add Subscription" class="btn btn-primary maindtg">
			</a>

                <?php if($this->session->flashdata('success')){ ?>

<div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
</div>
                <?php 

            }if($this->session->flashdata('error')){?>

                <div class="alert alert-danger">
  <strong>Error</strong> <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>


            
            
<table id="myTable" class="display" style="width:100%">
          <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th> 
                                <th>Price</th>
                                <th>Distance</th>
                                <th>Validity</th>
                                <th>Action </th> 
                               
                            </tr>
                        </thead>
                        <tbody>

                        	<?php
                            if(!empty($subscriptiondata)){
                            $x=1;
                            foreach($subscriptiondata as $subscription){ ?>
                            <tr>
                                <td><?php echo $x;?></td>
                                <td><?php echo $subscription->title?></td>
                               



                                <td>

                <?php 

                 if($subscription->image){ ?>
 <img  height="50" width="50" src="https://<?php echo $_SERVER['SERVER_NAME']; ?>/<?php echo $subscription->image;?>">
<?php  } else{ ?>

<img  height="50" width="50" src="https://<?php echo $_SERVER['SERVER_NAME']; ?>/public/userimage/123.png">


<?php }?>
                                </td>
                                
                           <!--   -->
                           
                            <td><?php echo $subscription->description?></td> 
                            
                            <td><?php echo $subscription->price?></td>  
                            
                             <td><?php echo $subscription->distance?></td> 
                            
                            <td><?php echo $subscription->validity?></td>  

                            <td >

                            <a href="<?php echo base_url();?>subscription/subscriptionEdit/<?php echo $subscription->id; ?>" onclick="return confirm('Are you sure  Edit this Subscription?')" ><p title="Edit Subscription"><button class="btn btn-success">Edit</button></a> 

                            <a href="<?php echo base_url();?>subscription/subscriptionDelete/<?php echo $subscription->id; ?>" onclick="return confirm('Are you sure  Delete this Subscription?')" ><p title="Delete Subscription"><button class="btn btn-danger">Delete</button></a></td>
                                

                            </tr>
                    <?php 
                     
                       $x++; }  }  ?>
                        </tbody>
 
    </table>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include('common/footer.php');?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
