<!doctype html>
<html lang="en">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- VENDOR CSS -->
  <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../assets/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../assets/vendor/linearicons/style.css">


  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 
  
  <!-- MAIN CSS -->
  <link rel="stylesheet" href="../../assets/css/main.css">
  <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  <link rel="stylesheet" href="../../assets/css/demo.css">
  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  <!-- ICONS -->
  <!--<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">-->
  <link rel="icon" type="image/png" sizes="96x96" href="../assets/fav.png">

<?php if($this->uri->segment(2)=='addspeciality'){
		$pagetitle='Add Specialty';
    }else{
$pagetitle='';
}?>
<title><?php echo $pagetitle;?> | My Doctor</title>
<body>
  <!-- WRAPPER -->
  <div id="wrapper">
    <!-- NAVBAR -->
    <?php include('common/header.php');?>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <?php include('common/left_bar.php');?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
      <!-- MAIN CONTENT -->
      <div class="main-content">

         <?php if($this->session->flashdata('error')){ ?>

<span class="formerror" style="color: red;">
  <strong>Error</strong> <?php echo $this->session->flashdata('error');?>
</span>
                <?php 

            }?>

<div class="container" style="width: 70%">
  
<form action="<?php echo base_url('subscription/updateSubscription');?>" method="post" enctype="multipart/form-data">

<input type="hidden" name="hidden" value="<?php echo $subscriptiondata[0]->id;?>">
  <fieldset>
    <legend>Subscription Info</legend>
   
 
    
     <div class="form-group">
      <label for="exampleInputEmail1">Title</label>
      <input type="text" class="form-control" name="title"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter Title" value="<?php echo $subscriptiondata[0]->title;?>" >
     
    </div>

    <?php if(form_error('title')){?>
    <span class="formerror" style="color: red;">

  <strong></strong><?php echo form_error('title');?>
</span>
    <?php } ?>
    
    
     <div class="form-group">
      <label for="exampleInputEmail1">Description</label>
      <input type="text" class="form-control" name="description"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter description" value="<?php echo $subscriptiondata[0]->description;?>" >
     
    </div>

    <?php if(form_error('description')){?>
    <span class="formerror" style="color: red;">

  <strong></strong><?php echo form_error('description');?>
</span>
    <?php } ?>
    
    
    
     <div class="form-group">
      <label for="exampleInputEmail1">Price</label>
      <input type="text" class="form-control" name="price"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter price" value="<?php echo $subscriptiondata[0]->price;?>" >
     
    </div>

    <?php if(form_error('price')){?>
    <span class="formerror" style="color: red;">

  <strong></strong><?php echo form_error('price');?>
</span>
    <?php } ?>
<img src="https://<?php echo $_SERVER['SERVER_NAME']; ?>/<?php echo $subscriptiondata[0]->image;?>" alt="Smiley face" height="42" width="42">

<div class="form-group">
      <label for="exampleInputPassword1">Image</label>
      <input type="file" name="image" class="form-control" id="exampleInputPassword1" placeholder="Password" value="" >
    </div>



    <?php if(form_error('image')){?>
   <span class="formerror" style="color: red;">

  <strong></strong><?php echo form_error('image');?>
</span>
    <?php } ?>
   


     <div class="form-group">
      <label for="exampleInputEmail1">Distance</label>
      <input type="text" class="form-control" name="distance"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter distance" value="<?php echo $subscriptiondata[0]->distance;?>" >
     
    </div>

    <?php if(form_error('distance')){?>
    <span class="formerror" style="color: red;">

  <strong></strong><?php echo form_error('distance');?>
</span>
    <?php } ?>


   <div class="form-group">
      <label for="exampleInputEmail1">Validity</label>
      <input type="text" class="form-control" name="validity"  id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter validity" value="<?php echo $subscriptiondata[0]->validity;?>" >
     
    </div>

    <?php if(form_error('validity')){?>
    <span class="formerror" style="color: red;">

  <strong></strong><?php echo form_error('validity');?>
</span>
    <?php } ?>







    <button type="submit" class="btn btn-primary">Update</button>
  </fieldset>
</form>

</div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <?php include('common/footer.php');?>
  </div>
  <!-- END WRAPPER -->
  <!-- Javascript -->
  <script src="../../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="../../assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="../../assets/scripts/klorofil-common.js"></script>
</body>

</html>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>

