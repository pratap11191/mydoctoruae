<?php
class Doctor_model extends CI_Model{
	
				public function __construct(){
					
					parent::__construct();
					
				//$this->load->model('');
				}	

			public function insert($table,$data){
				$this->db->insert($table,$data);
			 	return $last = $this->db->insert_id();
			}


			public function row_count($table) {
			        return $this->db->count_all($table);
			    }
 

			public function fetchall($limit, $start,$table) {
					$this->db->select('*');
					$this->db->from($table);
			        $this->db->limit($limit, $start);
			        
			        $query = $this->db->get();
                    return  $query->result();
                    
                    
			   }

		        public function joinings($con){
					/*
					$this->db->select('users.*','users.fullname','users.phone','users.image','email','users.city_id','users.admin_status','cities.name');*/



				$this->db->select('users.*','cities*');
				$this->db->from('users');
				$this->db->join('cities','users.city_id=cities.id');
					$query=$this->db->get();
					return $query->result();
					 }




			 /*  public function getdata($table,$column,$con){
			   		$this->db->select($column);
					$this->db->from($table);
					$this->db->where($con); 
					  $this->db->order_by("id", "DESC");
			        $query = $this->db->get();
                    return  $query->result();
                    

			   }*/

			     public function getdata($table,$column,$con){
			   		$this->db->select($column);
					$this->db->from($table);
					$this->db->join('cities','users.city_id = cities.id');
					$this->db->where($con);
                                         
					$this->db->order_by("users.id", "DESC");
			        $query = $this->db->get();
                    return  $query->result();
                    

			   }

                                public function getdatafordate($table,$column,$con,$startdate,$enddate){
			   		$this->db->select($column);
					$this->db->from($table);
					$this->db->join('cities','users.city_id = cities.id');
					$this->db->where($con);
                                        $this->db->where('users.created_at >=', $startdate);
                                        $this->db->where('users.created_at <=', $enddate);
                                         
					$this->db->order_by("users.id", "DESC");
			        $query = $this->db->get();
                    return  $query->result();
                    

			   }

			      public function getdataforcolumn($table,$column,$id){
				   		
				   		$this->db->select($column);
						$this->db->from($table);
						$this->db->where('id',$id); 
						$this->db->order_by('id','asc'); 
				        $query = $this->db->get();
	                    return  $query->result();

			   }

			     public function getdoctor($table,$column){
				   		
				   		$this->db->select($column);
						$this->db->from($table);
						$this->db->join('cities','doctors.city_id = cities.id');
						$this->db->join('specialties','doctors.speciality_id = specialties.id');
						$this->db->order_by("doctors.id", "DESC");
						//$this->db->where($con); 
				        $query = $this->db->get();
	                    return  $query->result();
 
			     }

                                  public function getsearchdoctor($table,$column,$startdate,$enddate){
				   		
				   		$this->db->select($column);
						$this->db->from($table);
						$this->db->join('cities','doctors.city_id = cities.id');
						$this->db->join('specialties','doctors.speciality_id = specialties.id');
						$this->db->order_by("doctors.id", "DESC");
						$this->db->where('doctors.created_at >=', $startdate);
                                                $this->db->where('doctors.created_at <=', $enddate); 
				        $query = $this->db->get();
	                    return  $query->result();
 
			     }

			         public function getsearchdoctornew($table,$startdate,$enddate){
				   		
				   		$this->db->select('*');
						$this->db->from($table);
						$this->db->order_by("id", "DESC");
						$this->db->where('created_at >=', $startdate);
                                                $this->db->where('created_at <=', $enddate); 
				        $query = $this->db->get();
	                    return  $query->result();
 
			     }


			    

			     public function getBooking($table,$column){
				   		
				   		$this->db->select($column);
						$this->db->from($table);
						$this->db->join('doctors','bookings.doctor_id = doctors.id');
						
						$this->db->order_by("bookings.id", "DESC");
						//$this->db->where($con); 
				        $query = $this->db->get();
	                    return  $query->result();
 
			     }



                                              public function getsearchBooking($table,$column,$startdate,$enddate){
				   		
				   		$this->db->select($column);
						$this->db->from($table);
						$this->db->join('doctors','bookings.doctor_id = doctors.id');
						
						$this->db->order_by("bookings.id", "DESC");
						$this->db->where('bookings.apdate >=', $startdate);
                                                $this->db->where('bookings.apdate <=', $enddate); 
				        $query = $this->db->get();
	                    return  $query->result();
 
			     }


			     public function getpost($table,$column){
				   		
				   		$this->db->select($column);
						$this->db->from($table);
						$this->db->order_by("id", "desc");
						//$this->db->where('usertype','user'); 
				        $query = $this->db->get();
	                    return  $query->result();
 
			     }

				public function getnamepostby($table,$column,$id){

				     			 $this->db->select($column);
	                       		 $this->db->from($table);
                                 $this->db->where('id',$id);
                                 $query = $this->db->get();
                                  return $query->result();

                                }


                      




			   public function getcity(){
				$this->db->select(array('id','name'));
				$this->db->from('cities');
				$query = $this->db->get();
				return  $query->result();
			 		
			    }

			   public function getDoctorspeciality(){
				$this->db->select(array('id','name'));
				$this->db->from('specialties');
				$query = $this->db->get();
				return  $query->result();
					
			    }

////////////////////////////////////////////////////////
			/*
			public function fetchall(){
			//$this->db->select()->from('user_rg');
			//$this->db->limit($limit, $start);
			$query = $this->db->get('user_re');
			return $query->result();
			}*/
/////////////////////////////////////////////////////////
 public function loginuser($tbl,$data){
$condition = "email =" . "'" . $data['username'] . "' AND " . "confirmpassword =" . "'" . $data['password'] . "'";
					$this->db->select('*');
					$this->db->from($tbl);
					$this->db->where($condition);
					$this->db->limit(1);
					$query = $this->db->get();
					return $query->result();
			}

			public function read_user_information($tbl,$username) {

			$condition = "email =" . "'" . $username . "'";
			$this->db->select('*');
			$this->db->from($tbl);
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
			return $query->result();
			} else {
			return false;
			}
			}

         public function fetchSingle($id,$table){
         	$this->db->where('id',$id);
         	$query = $this->db->get($table);
            return $query->result();
         }

          public function updatedeUser($id,$data,$table){
         	$this->db->where('id', $id);
            $this->db->update($table, $data);
            return true;
         	//$query = $this->db->get('user_re');
            //return $query->result();
         }


		public function deleteUser($id,$table){

			$this->db->where('id', $id);
           $this->db->delete($table);  
		}               



    public function validuser($username){

    	
    	
    	$query =$this->db->where('username',$username)
                   ->get('user_reg');
       return $query->num_rows();
    }

   public function getinformation($tbl,$id,$column){
	       $this->db->select($column);
	       $this->db->from($tbl);
	       $this->db->where($id);
	       $query =  $this->db->get();
	       return $query->row();
   }



   public function getsingleRow($tbl,$con){
   			$this->db->select('*');
   			$this->db->from($tbl);
   			$this->db->where($con);
   			$query = $this->db->get();
   			return $query->result();

   }

   public function update_data($tbl,$con,$data_array){

   	  $this->db->where($con);
   	  $this->db->update($tbl,$data_array);
   	  //return $query->get();
   }

   public function emailverification($tbl){

   	$this->db->select('*');
   	$this->db->from($tbl);
   	$this->db->where($con);
   	$query = $this->db->get();
   	return $query->num_rows();
   }

public function fetchtest($table,$con){
         		$this->db->select('*');
			   	$this->db->from($table);
			   	$this->db->where($con);
			   	$query = $this->db->get();
			   	return $query->result_array();
		 }
		 
	   //************* new query 17/02/2018 ***************//
 public function insertData($tbl,$data){

	$this->db->insert($tbl,$data);
	
 }

 function fetchConditionaldata($tbl,$fields,$con=''){

	$this->db->select($fields);
	$this->db->from($tbl);
	if(!EMPTY($con))
	$this->db->where($con);
	$query = $this->db->get();
	//return ($query->num_rows>0)?$query->result():FALSE;
	return $query->result();


 }

 		public function deleterecord($tbl,$con){

			$this->db->where($con);
			return $this->db->delete($tbl);
			

		 }

		 public function fetchSinglerow($tbl,$fields,$con){
			 $this->db->select($fields);
			 $this->db->from($tbl);
			 $this->db->where($con);
			 $query = $this->db->get();
			 return $query->row();

			


		 }

		 	public function updateentity($tbl,$con,$data){

				$this->db->where($con);
				$this->db->update($tbl,$data);
				return true;
				

			 }





			 public function sellAlldata($tbl,$con,$limit='',$offset=''){

			 		$this->db->select('*');
			 		$this->db->from($tbl);
			 		$this->db->where($tbl);
			 		if($limit)
			 		$this->db->limit($limit,$offset);
				    $query = $this->db->get();
				    return $query->result();

					

			 }
			 public function loginadmin($tbl,$username,$pass){
			 	$this->db->select('id');
				$this->db->from($tbl);
				$this->db->where($username);
				$this->db->where($pass);
				$query = $this->db->get();
				return $query->num_rows();



			 }
		 /***************************** new query 17/02/2018**************************/


}

?>