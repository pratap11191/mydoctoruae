<?php
class Common_model extends CI_Model{
	

	public function getConditionalLastRow($tbl,$con,$clm){

		if(empty($tbl) || empty($con) || empty($clm))
		return NULL;
	
			$this->db->select($clm);
			$this->db->from($tbl);
			$this->db->where($con);
			$this->db->order_by('id','desc');
			$this->db->limit(1);
	$q 	=   $this->db->get();

		return $q->num_rows()?$q->row():NULL;

	}


			public function getrows($tbl,$column,$con){

					$this->db->select($column);
					$this->db->from($tbl);
					if(!empty($con))
					$this->db->where($con);
				$q =$this->db->get();

				return $q->num_rows();
			  }


			  public function getspeciality($tbl,$con=''){

					$this->db->select('*');
					$this->db->from($tbl);
					if(!empty($con))
					$this->db->where($con);
				    $this->db->order_by("id", "desc");
				    $q =$this->db->get();

				return $q->num_rows()?$q->result():NULL;
			  }


			  
			  public function getData($tbl,$con=''){

					$this->db->select('*');
					$this->db->from($tbl);
					if(!empty($con))
					$this->db->where($con);
					$this->db->order_by("id", "desc");
				    $q =$this->db->get();

				return $q->num_rows()?$q->result():NULL;
			  }



			public function getAllCOnditionalRow($tbl,$column,$con){

					$this->db->select($column);
					$this->db->from($tbl);
					if(!empty($con))
					$this->db->where($con);
				$q =$this->db->get();

				return $q->num_rows()?$q->result():NULL;
			  }

			public function insertRecord($tbl,$data){

				$q = $this->db->insert($tbl,$data);
				return $this->db->insert_id();

			  }

			public function getLastEntry($tbl,$clm,$con,$order_by,$order){

				$this->db->select($clm);
				$this->db->from($tbl);

				if(!empty($con))
				$this->db->where($con);

				$this->db->order_by($order_by,$order);
				$this->db->limit(1);
			$q =$this->db->get();

				return $q->num_rows()?$q->row():NULL;

			  }

			public function updateRecord($tbl,$data,$con){

				 	
				   	$this->db->where($con);
			   $q =	$this->db->update($tbl,$data);
					return $q;
			  }

			public function deleteRec($tbl,$con){

				$this->db->delete($tbl,$con);
			  }

	
	

}

?>